//
//  AccountCreationProcessModel.m
//  HonkioApi
//
//  Created by Shurygin Denis on 8/3/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "AccountCreationProcessModel.h"
#import "NSError+ApiError.h"
#import "HonkioApi.h"

@implementation AccountCreationProcessModel {
    ResponseHandler responseHandler;
    UserAccount* userAccount;
    Shop* shop;
    int requestFlags;
    NSString* queryId;
}

-(id)init {
    self = [super init];
    if (self) {
        _isPaymentCheckEnabled = true;
    }
    return self;
}

-(id)initWithAccount:(UserAccount*)account shop:(Shop*)shop handler:(ResponseHandler)handler {
    self = [self init];
    if (self) {
        self->responseHandler = handler;
        self->userAccount = account;
        self->shop = shop;
    }
    return self;
}

-(void)start {
    [super start];
    [HonkioApi userAddAccount:userAccount flags:0 handler:[self buildEmptyAccountCreationHandler]];
}

-(void)abort {
    [super abort];
    [self removePendingCard];
}

-(void)onComplete:(Response *)response {
    [HonkioApi userGet:0 handler: ^(Response* response) {
        [super onComplete:response];
        self->responseHandler(response);
    }];
}

-(void)onPending:(Response*)response {
    [super onPending:response];
    queryId = [response transactoinId];
    responseHandler(response);
}

-(void)checkStatus {
    if (self.status == PENDING) {
        if (queryId) {
            [HonkioApi queryPaymentRequest:queryId shop:self->shop flags:0 handler:[self buildAcceptCheckHandler]];
        }
        else
            [self removePendingCard];
    }
}

-(NSString*)pendingQueryId {
    return queryId;
}

-(ResponseHandler)buildEmptyAccountCreationHandler {
    return ^(Response* response) {
        if ([response isStatusError] || [response isStatusReject]) {
            [self abort];
            self->responseHandler(response);
        }
        else if (self.status != ABORTED) {
            if ([response isStatusAccept]) {
                if (self.isPaymentCheckEnabled) {
                    Shop* paymentShop = self->shop != nil ? self->shop : [[HonkioApi mainShopInfo] shop];
                    NSString* shopType = [paymentShop type];
                    if ([SHOP_TYPE_PAYMENT isEqualToString: shopType]) {
                        [HonkioApi paymentRequest:0 currency:nil account:self->userAccount shop:paymentShop flags:0 handler:[self buildPendingHandler] ];
                    }
                    else
                        [HonkioApi paymentRequest:[NSArray new] account:self->userAccount shop:paymentShop flags:0 handler:[self buildPendingHandler]];
                }
                else {
                    [self onComplete:response];
                }
            }
            else
                self->responseHandler(response);
        }
        else
            [self removePendingCard];
        
    };
}

-(ResponseHandler)buildPendingHandler {
    return ^(Response *response) {
        if ([response isStatusError] || [response isStatusReject]) {
            [self removePendingCard];
            self->responseHandler(response);
        }
        else if(self.status != ABORTED) {
            if([response isStatusAccept]){
                [self onComplete:response];
            }
            else if([response isStatusPending]) {
                [self onPending:response];
            }
            else {
                [self removePendingCard];
                self->responseHandler(response);
            }
        }
        else {
            [self removePendingCard];
        }
    };
}

-(ResponseHandler)buildAcceptCheckHandler {
    return ^(Response *response) {
        if ([response isStatusError] || [response isStatusReject]) {
            [self removePendingCard];
            self->responseHandler(response);
        }
        else if([response isStatusAccept]){
            [self onComplete:response];
        }
        else if(self.status == ABORTED)
            [self removePendingCard];
        else if ([response isStatusPending])
            [super onPending:response];
    };
}

-(void)removePendingCard {
    if(self.status != FINISHED) {
        User* user = [HonkioApi activeUser];
        for (long i = [user.accounts count] - 1; i >= 0; i--) {
            UserAccount* account = user.accounts[i];
            if ([[account type] isEqualToString: [userAccount type]] && [[account number] isEqualToString: [userAccount number]]) {
                [HonkioApi userDeleteAccount:account flags: MSG_FLAG_LOW_PRIORITY handler:nil];
            }
        }
    }
}

@end
