//
//  MerchantShopsList.h
//  HonkioSDK
//
//  Created by Mikhail Li on 24/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MerchantShop.h"

@interface MerchantShopsList : NSObject

/** List of MerchantShop items */
@property (nonatomic, copy) NSArray<MerchantShop*> *list;

@end
