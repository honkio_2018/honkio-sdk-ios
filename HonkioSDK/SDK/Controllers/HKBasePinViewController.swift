//
//  HKBasePinViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 4/26/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

// TODO doc describe 'delegate', 'reason', 'messageString' and 'pinLength'

/**
HKBasePinViewController provides base functonlality for working with PIN based views. Monitors PIN digits count and notifies the listener (see delegate) that PIN code has been entered or canceled.
* Should not be instantiated due to doesn't have any view to display the state.
*/

open class HKBasePinViewController: HKBaseViewController {
    
    open var messageString : String? {
        didSet {
            if self.messageLabel != nil {
                self.messageLabel.text = self.messageString
            }
        }
    }
    open var pinLength = 4
    
    fileprivate var tapCounter: Int = 0
    fileprivate var pinCode: String = ""
    
    open weak var delegate: HKPinProtocol?
    open var reason : Int!
    
    @IBOutlet open weak var messageLabel: UILabel!

    /// Sets the message text to provided message string value on loading view.
    override open func viewDidLoad() {
        
        super.viewDidLoad()
        
        if self.messageString != nil && self.messageLabel != nil {
            self.messageLabel.text = self.messageString
        }
    }

    /**
    Appends pin character to a PIN code string when a keyboard key has been pressed.
    - parameter sign: The character (String type value) to be appended.
    */
    open func addPinSign(_ sign: String) {
        self.tapCounter += 1
        if self.tapCounter > self.pinLength {
            
            self.tapCounter -= 1
            return
        }
        
        self.pinCode += sign
        self.setDotActive(self.tapCounter, isActive: true)
        
        if self.tapCounter == self.pinLength {
            self.pinEntered(self.pinCode)
        }
    }

    /**
    Removes last pin character from a PIN code string when a back (backspace) key has been pressed.
    */
    open func deleteLastPinSign() {
        if self.tapCounter > 0 {
            self.setDotActive(self.tapCounter, isActive: false)
            self.tapCounter -= 1
            self.pinCode = String(self.pinCode.dropLast())
        }
    }
    
    //TODO write docs
    open func clearPin() {
        if self.tapCounter > 0 {
            for i in 1...self.tapCounter {
                self.setDotActive(i, isActive: false)
            }
            self.tapCounter = 0
            self.pinCode = ""
        }
    }

    /**
    Sets the pin char symbol active/inactive
    * Must be overriden.
    - parameter dotNumber: The number of PIN symbol to be activated/deactivated.
    - parameter isActive: The value describes whether the symbol is to be activated or deactivated.
    */
    open func setDotActive(_ dotNumber: Int, isActive:Bool) {
        preconditionFailure("Method setDotActive must be overridden")
    }

    /**
    Notifies the listener provided by delegate that the PIN code has been entered.
    - parameter pin: PIN code value having been entered.
    */
    open func pinEntered(_ pin: String) {
        self.delegate?.pinEntered(self.pinCode, reason: self.reason)
    }

    /**
    Notifies the listener provided by delegate that the PIN code entering action has been
    cancelled.
    */
    open func cancelAction() {
        self.delegate?.pinCanceled(self.reason)
    }
}
