//
//  PushStorage.h
//  HonkioApi
//
//  Created by Shurygin Denis on 5/15/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>

/// DEPRECATED!!!
@interface PushStorage : NSObject

+(NSArray*)allPushes;

@end
