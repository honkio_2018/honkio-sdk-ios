//
//  HKUserRoleStructureList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKUserRoleStructure.h"

@interface HKUserRoleStructureList : NSObject

/** Array of HkStructure items */
@property (nonatomic, copy) NSArray<HKUserRoleStructure*> *list;

@end
