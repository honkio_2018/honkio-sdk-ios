//
//  RateListJsonParser.m
//  HonkioSDK
//
//  Created by Mikhail Li on 08/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "RateListJsonParser.h"
#import "RateList.h"
#import "Rate.h"

@implementation RateListJsonParser

-(id)parseDictionary:(Message *)message dict:(NSDictionary *)dict {
    RateList *assetList = [RateList new];
    NSMutableArray<Rate *> *list = [NSMutableArray new];
    
    NSArray *assetListJson = dict[@"rates"];
    if (assetListJson) {
        for (NSDictionary *rateJson in assetListJson) {
            Rate *rate = [[Rate alloc] initWithValue:[rateJson[@"mark"] intValue] andCount:[rateJson[@"count"] intValue]];
            [list addObject:rate];
        }
    }
    
    assetList.list = list;
    return assetList;
}

@end
