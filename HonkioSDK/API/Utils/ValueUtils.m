//
//  ValueUtils.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/17/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "ValueUtils.h"

@implementation ValueUtils

static NSString* const IbanLettersAndDecimals = @"ABCDEFGHIJKLKMNOPQRSTUVWXYZ0123456789";

static NSString* const SsnCheckChars_fi = @"0123456789ABCDEFHJKLMNPRSTUVWXY";

+ (BOOL)checkSsn:(NSString *)ssnString {
    return [self checkFinlandSsn:ssnString];
}

+ (BOOL)checkSsn:(NSString *)ssnString withCountryCode:(NSString *)countryCode {
    if (!ssnString || !countryCode) {
        return false;
    }
    
    if ([countryCode isEqualToString: @"ITA"] || [countryCode isEqualToString: @"IT"]) {
        return [self checkItalianSsn:ssnString];
    }
    else if ([countryCode isEqualToString: @"FIN"] || [countryCode isEqualToString: @"FI"]) {
        return [self checkFinlandSsn:ssnString];
    }
    else if ([countryCode isEqualToString: @"GBR"] || [countryCode isEqualToString: @"GB"]) {
        return [self checkUKSsn:ssnString];
    }
    else if ([countryCode isEqualToString: @"SWE"] || [countryCode isEqualToString: @"SE"]) {
        return [self checkSwedenSsn:ssnString];
    }
    
    return true;
}


+ (NSDate*)getBirthdayFromSsn:(NSString *)ssnString withCountryCode:(NSString *)countryCode {
    if(!ssnString || !countryCode)
        return nil;
    
    if ([countryCode isEqualToString: @"FIN"] || [countryCode isEqualToString: @"FI"]) {
        return [ValueUtils getBirthdayFromFinlandSsn:ssnString];
    }
    
    return nil;
}

+(BOOL)checkFinlandSsn:(NSString *)ssnString {
    if (!ssnString)
        return false;
    
    ssnString = [[ssnString uppercaseString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (ssnString.length == 11) {
        
        
        NSMutableString *string = [[NSMutableString alloc] initWithString:ssnString];
        char control_char = [string characterAtIndex: 10];
        [string deleteCharactersInRange: NSMakeRange(10, 1)];
        [string deleteCharactersInRange: NSMakeRange(6, 1)];
        int index = string.intValue;
        index = index % SsnCheckChars_fi.length;
        return [SsnCheckChars_fi characterAtIndex: index] == control_char;
    }
    return false;
}

+ (NSDate*)getBirthdayFromFinlandSsn:(NSString *)ssnString {
    if (!ssnString)
        return false;
    
    ssnString = [[ssnString uppercaseString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (ssnString && ssnString.length == 11) {
        NSString* ssn = [ssnString uppercaseString];
        
        int day = [[ssn substringWithRange:NSMakeRange(0, 2)] intValue];
        int month = [[ssn substringWithRange:NSMakeRange(2, 2)] intValue];
        int year = [[ssn substringWithRange:NSMakeRange(4, 2)] intValue];
        
        char charCentury = [ssn characterAtIndex: 6];
        
        switch (charCentury) {
            case '+':
                year += 1800;
                break;
            case '-':
                year += 1900;
                break;
            case 'A':
                year += 2000;
                break;
            case 'B':
                year += 2100;
                break;
            default:
                return nil;
        }
        
        NSCalendar* calendar = [NSCalendar currentCalendar];
        [calendar setTimeZone: [NSTimeZone timeZoneWithName:@"UTC"]];
        
        return [calendar dateWithEra:1 year:year month:month day:day hour:0 minute:0 second:0 nanosecond:0];
        
    }
    return nil;
}

+ (BOOL)checkItalianSsn:(NSString *)ssnString {
    if(!ssnString)
        return NO;
    
    NSString *code = [[ssnString stringByReplacingOccurrencesOfString:@"\\s+" withString:@""].uppercaseString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (code.length != 16) {
        return NO;
    }
    
    NSString *pattern = @"[A-Z]{6}[0-9]{2}[A-E,H,L,M,P,R-T][0-9]{2}[A-Z0-9]{5}";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    if (regex != nil) {
        NSArray *result = [regex matchesInString:code options:0 range: NSMakeRange(0, code.length)];
        return result.count > 0;
    }
    
    return NO;
}

+ (BOOL)checkUKSsn:(NSString *)ssnString {
    if(!ssnString)
        return NO;
    
    NSString *code = [[ssnString stringByReplacingOccurrencesOfString:@"\\s+" withString:@""].uppercaseString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (code.length != 9) {
        return NO;
    }
    
    NSString *pattern1 = @"^[A-CEGHJ-NOPR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-D\\s]{1}";
    NSRegularExpression *regex1 = [NSRegularExpression
                                   regularExpressionWithPattern:pattern1
                                                        options:NSRegularExpressionCaseInsensitive
                                                          error:nil];
    
    NSString *pattern2 = @"(^GB)|(^BG)|(^NK)|(^KN)|(^TN)|(^NT)|(^ZZ)";
    NSRegularExpression *regex2 = [NSRegularExpression
                                   regularExpressionWithPattern:pattern2
                                   options:NSRegularExpressionCaseInsensitive
                                   error:nil];
    
    if (regex1 != nil && regex2 != nil) {
        NSArray *result1 = [regex1 matchesInString:code options:0 range: NSMakeRange(0, code.length)];
        NSArray *result2 = [regex2 matchesInString:code options:0 range: NSMakeRange(0, code.length)];
        return result1.count > 0 && result2.count == 0;
    }
    
    return NO;
}

+ (BOOL)checkSwedenSsn:(NSString *)ssnString {
    if(!ssnString)
        return NO;
    
    NSString *code = [[[ssnString stringByReplacingOccurrencesOfString:@"-" withString:@""] stringByReplacingOccurrencesOfString:@"+" withString:@""].uppercaseString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // Remove century and check number
    if (code.length == 12)
        code = [code substringFromIndex:2];
    else if(code.length == 10) {
        // Do nothing
    }
    else
        return NO;
    
    // Remove check number
    int check = [[code substringFromIndex:9] intValue];
    NSString* sValue = [code substringToIndex:9];
    int result = 0;
    for (int i = 0; i < sValue.length; i++) {
        NSString* sub = [sValue substringWithRange:NSMakeRange(i, 1)];
        int tmp = [sub intValue];
        if (i % 2 == 0)
            tmp = tmp * 2;
        
        if (tmp > 9)
            result += 1 + tmp % 10;
        else
            result += tmp;
    }
    
    BOOL isValid = (check + result) % 10 == 0;
    
    int month = [[code substringWithRange:NSMakeRange(2, 2)] intValue];
    int day = [[code substringWithRange:NSMakeRange(4, 2)] intValue];
    BOOL isSSN = month < 13 && month > 0 && day < 32 && day > 0;
    
    return isValid && isSSN;
}

+ (BOOL)checkIban:(NSString*) ibanString {
    ibanString = [[ibanString stringByReplacingOccurrencesOfString:@" " withString:@""] uppercaseString];
    
    if ([ibanString length] <= 5) {
        return NO;
    }
    
    NSCharacterSet *invalidChars = [[NSCharacterSet characterSetWithCharactersInString:IbanLettersAndDecimals] invertedSet];
    
    if ([ibanString rangeOfCharacterFromSet:invalidChars].location != NSNotFound)
    {
        return NO;
    }
    
    int checkDigit = [ibanString substringWithRange:NSMakeRange(2, 2)].intValue;
    ibanString = [NSString stringWithFormat:@"%@%@",[ibanString substringWithRange:NSMakeRange(4, ibanString.length - 4)], [ibanString substringWithRange:NSMakeRange(0, 4)]] ;
    
    for (int i = 0; i < ibanString.length; i++) {
        unichar c = [ibanString characterAtIndex:i];
        if (c >= 'A' && c <= 'Z') {
            ibanString = [NSString stringWithFormat:@"%@%d%@", [ibanString substringWithRange:NSMakeRange(0, i)], (c - 'A' + 10),[ibanString substringWithRange:NSMakeRange(i+1, ibanString.length - i - 1)]];
        }
    }
    ibanString = [[ibanString substringWithRange:NSMakeRange(0, ibanString.length - 2)] stringByAppendingString:@"00"];
    
    while(true)
    {
        int iMin = (int)MIN(ibanString.length, 9);
        NSString* strPart = [ibanString substringWithRange:NSMakeRange(0, iMin)];
        int decnumber = strPart.intValue;
        if(decnumber < 97 || ibanString.length < 3)
            break;
        int del = decnumber % 97;
        ibanString =  [NSString stringWithFormat:@"%d%@", del, [ibanString substringFromIndex:iMin]];
    }
    int check = 98 - ibanString.intValue;
    
    return checkDigit == check;
}

@end
