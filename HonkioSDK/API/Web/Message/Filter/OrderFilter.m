//
//  OrderFilter.m
//  HonkioApi
//
//  Created by Shurygin Denis on 12/2/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "OrderFilter.h"

@implementation OrderFilter

@synthesize queryOnlyCount;

-(void)fillParams:(NSMutableDictionary *)params {
    [AbsFilter applyIfNotNil:@"query_status" value: self.status forParams:params];
    [AbsFilter applyIfNotNil:@"query_id" value:self.queryId forParams:params];
    [AbsFilter applyIfNotNil:@"query_owner" value:self.owner forParams:params];
    [AbsFilter applyIfNotNil:@"query_third_person" value:self.thirdPerson forParams:params];
    [AbsFilter applyIfNotNil:@"query_distance" value:self.distance forParams:params];
    [AbsFilter applyIfNotNil:@"query_search_latitude" value:self.latitude forParams:params];
    [AbsFilter applyIfNotNil:@"query_search_longitude" value:self.longitude forParams:params];
    
    [AbsFilter applyIfNotNil:@"query_model" value:self.model forParams:params];
    
    if (self.excludeExpired)
        params[@"query_exclude_expired"] = @"1";
    
    if (self.queryProductDetails)
        params[@"query_product_details"] = [NSNumber numberWithBool:YES];
    
    if (self.queryUnreadMessages)
        params[@"query_unread_messages"] = [NSNumber numberWithBool:YES];
    
    if (self.childMerchants)
        params[@"query_child_merchants"] = [NSNumber numberWithBool:YES];
    
    if ([self isQueryOnlyCount])
        params[@"query_only_count"] = @(YES); //@YES
    
    if (self.custom) {
        for(NSString* key in self.custom) {
            [AbsFilter applyIfNotNil:key value:self.custom[key] forParams:params];
        }
    }
    
    [super fillParams:params];
}

-(void) addCustom:(NSString*)key value:(NSObject*) value {
    NSMutableDictionary* customDict;
    if (self.custom) {
        customDict = self.custom.mutableCopy;
        customDict[key] = value;
        self.custom = [NSDictionary dictionaryWithDictionary:customDict];
    }
    else {
        self.custom = @{key : value};
    }
    
}

-(void)setStatuses:(NSArray<NSString *> *)statuses {
    self.status = [statuses componentsJoinedByString:@"|"];
}

-(NSArray<NSString*>*)statuses {
    return [self.status componentsSeparatedByString:@"|"];
}

-(void) removeCustom:(NSString*)key {
    if (self.custom) {
        NSMutableDictionary* customDict = self.custom.mutableCopy;
        [customDict removeObjectForKey:key];
        self.custom = [NSDictionary dictionaryWithDictionary:customDict];
    }
}

@end
