//
//  PushParser.h
//  HonkioApi
//
//  Created by Shurygin Denis on 2/12/15.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Push.h"

@interface PushParser : NSObject

+(Push*)parse:(NSDictionary*)dict;

@end
