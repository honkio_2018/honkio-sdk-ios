//
//  HKUserAccountsTableViewController.swift
//  WorkPilots
//
//  Created by developer on 2/25/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
The table view controller for managing user payment accounts table view.
*/
open class HKUserAccountsTableViewController: HKBaseUserAccountsTableViewController, HKViewControllerCompletionDelegate {
    
    public static let ITEM_CELL_IDENTIFIER = "HKUserAccountsTableViewController.ITEM_CELL_IDENTIFIER"
    public static let HEADER_CELL_IDENTIFIER = "HKUserAccountsTableViewController.HEADER_CELL_IDENTIFIER"
    public static let ADD_BUTTON_CELL_IDENTIFIER = "HKUserAccountsTableViewController.ADD_BUTTON_CELL_IDENTIFIER"
    
    static let TAG_ADD_ACCOUNT = "HKUserAccountsTableViewController.TAG_ADD_ACCOUNT"
    
    open var supportedMerchants: [String] = [(HonkioApi.mainShopInfo()?.merchant.merchantId)!]

    /**
    On viewDidLoad builds the view.
    */
    override open func viewDidLoad() {
        self.buildViewProgrammaticaly()
        super.viewDidLoad()
    }
    
    /**
     Build items source.
     - parameter accountsSection: The accounts section that should be used in data source building.
     */
    override open func buildSource(_ accountsSection : HKTableViewSection) {
        self.itemsSource.removeAll()
        
        let headerSection = HKTableViewSection()
        
        headerSection.items.append((HKUserAccountsTableViewController.HEADER_CELL_IDENTIFIER, nil, nil))
        itemsSource.append(headerSection)
        
        itemsSource.append(accountsSection)
        
        let addButtonSection = HKTableViewSection()
        
        addButtonSection.items.append((HKUserAccountsTableViewController.ADD_BUTTON_CELL_IDENTIFIER, nil, { (viewController, cell, item) in
            let addButtonCell = cell as! HKUserAccountsAddButtonTableViewCell
            if !addButtonCell.isSetuped {
                addButtonCell.button.addTarget(viewController, action: #selector(HKUserAccountsTableViewController.addAction), for: UIControlEvents.touchUpInside)
                addButtonCell.isSetuped = true
            }
        }))
        itemsSource.append(addButtonSection)
        
        var otherAccountsList : [UserAccount] = []

        // ===================================
        // Operaror payment account
        var operatorAccount = HonkioApi.activeUser()?.findAccount(byType: ACCOUNT_TYPE_OPERATOR)
        
        if operatorAccount == nil {
            operatorAccount = UserAccount(type: ACCOUNT_TYPE_OPERATOR, andNumber: ACCOUNT_NUMBER_ZERO)
        }
        operatorAccount!.isEnabled = HonkioApi.mainShopInfo()?.merchant.isAccountSupported(ACCOUNT_TYPE_OPERATOR) ?? false
        otherAccountsList.append(operatorAccount!)
        
        // ===================================
        // Bonus invoice account
        var isHasBonuses = false;
        for account in HonkioApi.activeUser()!.accounts {
            if ACCOUNT_TYPE_BONUS == account.type && supportedMerchants.contains(account.number) {
                account.isEnabled = HonkioApi.mainShopInfo()?.merchant.isAccountSupported(ACCOUNT_TYPE_BONUS) ?? false
                otherAccountsList.append(account)
                isHasBonuses = true
            }
        }
        
        if !isHasBonuses {
            let bonusAccount = UserAccount(type: ACCOUNT_TYPE_BONUS, andNumber: HonkioApi.mainShopInfo()?.merchant.merchantId)
            bonusAccount!.accountDescription = HonkioApi.mainShopInfo()?.merchant.name
            bonusAccount!.isHasLeft = true
            bonusAccount!.isEnabled = false
            otherAccountsList.append(bonusAccount!)
        }

        // ===================================
        // Merchant invoice account
        var isHasInvoices = false;
        for account in HonkioApi.activeUser()!.accounts {
            if ACCOUNT_TYPE_INVOICE == account.type && supportedMerchants.contains(account.number) {
                account.isEnabled = HonkioApi.mainShopInfo()?.merchant.isAccountSupported(ACCOUNT_TYPE_INVOICE) ?? false
                otherAccountsList.append(account)
                isHasInvoices = true
            }
        }
        
        if !isHasInvoices {
            let invoiceAccount = UserAccount(type: ACCOUNT_TYPE_INVOICE, andNumber: HonkioApi.mainShopInfo()?.merchant.merchantId)
            invoiceAccount!.accountDescription = HonkioApi.mainShopInfo()?.merchant.name
            invoiceAccount!.isEnabled = false
            otherAccountsList.append(invoiceAccount!)
        }
        
        // ===================================
        // Add accounts
        let otherAccountsSection = HKTableViewSection()
        
        let cellBinder = self.buildCellBunder()
        
        for account in otherAccountsList {
            if account.isEnabled {
                otherAccountsSection.items.append((HKUserAccountsTableViewController.ITEM_CELL_IDENTIFIER, account, cellBinder))
            }
        }
        
        for account in otherAccountsList {
            if !account.isEnabled {
                otherAccountsSection.items.append((HKUserAccountsTableViewController.ITEM_CELL_IDENTIFIER, account, cellBinder))
            }
        }
        
        itemsSource.append(otherAccountsSection)
    }

    /**
    - Returns: Item cell identificator for reuser cells.
     */
    open override func cellIdentifier(_ item: UserAccount) -> String {
        return HKUserAccountsTableViewController.ITEM_CELL_IDENTIFIER
    }

    /**
    - Returns: The cellBuilder instance for a payment account.
    */
    open override func buildCellBunder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let accountCell = cell as! HKUserAccountsTableViewCell
            let account = item as! UserAccount
            
            accountCell.account = account
        }
    }

    /**
    Shows progress indicator.
    - parameter view: The view the progress indicator is attached to.
    */
    open func showProgress(_ view: UIView) {
        BusyIndicator.shared.showProgressView(self.view)
    }

    /// Hides progress indicator
    open func hideProgress() {
        BusyIndicator.shared.hideProgressView()
    }

    /**
    - Returns: True if the row in *tableView* at *indexPath* is editable, otherwise false.
    - parameter tableView: The UITableView for the row.
    - parameter indexPath: The IndexPath to a row.
    */
    open func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    /**
    Commits the data edited in row. Shows the progress and alert if the commiting type is UITableViewCellEditingStyleDelete.
    If ok allert button clicked then notifies the server about commiting the data.
    - FIXME: The function actually only deletes the payment account.
    - FIXME: Add functionality for insert.
    - parameter tableView: The UITableView the function is called by.
    - parameter editingStyle: The enum value describes the type of action is performing.
    - parameter indexPath: The IndexPath value for an item is commiting.
    */
    
    open func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {       
        if editingStyle == .delete {
            
            // Delete the row from the data source
            CustomAlertView(title: "pay_accounts_list_dlg_remowe_title".localized,
                          message: "pay_accounts_list_dlg_remowe_message".localized,
                cancelButtonTitle: "dlg_common_button_cancel".localized,
               cancelButtonAction: nil)
                .addButtonWithTitle("dlg_common_button_ok".localized, action: {
                    
                    let section = self.itemsSource[(indexPath as IndexPath).section]
                    
                    let account = section.items[(indexPath as IndexPath).row].item as! UserAccount
                    
                    self.showProgress(self.view)
                    
                    HonkioApi.userDelete(account, flags: 0) { (response) in
                        
                        self.hideProgress()
                        
                        if !AppErrors.isError(response) {

                            if response.isStatusAccept() {
                                
                                section.items.remove(at: (indexPath as IndexPath).row)
                                tableView.deleteRows(at: [indexPath], with: .fade)
                                return
                            }
                        }
                        
                        let _ = self.handleDeleteAccountError(response.anyError())
                    }
                })
                .show()
            
        } else if editingStyle == .insert {
            
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }

    /**
    Tries handle the delete account error. If error was not handled shows the custom alert within the description of error.
    - parameter response: The response object caused the error.
    - parameter error: The NSError object could contain the error.
    - Returns: True;
    */
    open func handleDeleteAccountError(_ error: ApiError?) -> Bool {
        
        if !AppErrors.handleError(self, error: error) {
            CustomAlertView(apiError: error, okAction: nil).show()
        }
        return true
    }
    
    open func onComplete(_ viewController: UIViewController, tag: String?) {
        if tag == HKUserAccountsTableViewController.TAG_ADD_ACCOUNT {
            _ = navigationController?.popViewController(animated: true)
            reloadData()
        }
    }
    
    @objc fileprivate func addAction(_ sender: UIBarButtonItem) {
        if let controller = AppController.getViewControllerById(AppController.SettingsUserAddCreditCard, viewController: self) {
            if let addCardViewController = controller as? HKBaseViewController {
                addCardViewController.setCompletionDelegate(self, tag: HKUserAccountsTableViewController.TAG_ADD_ACCOUNT)
            }
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @objc fileprivate func closeAction(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    fileprivate func buildViewProgrammaticaly() {
        
        self.title = "screen_title_pay_accounts_list".localized
        
        tableView  = self.makeTableView()
        
        view.backgroundColor = UIColor(netHex: 0xffffff)
        
        view.addSubview(tableView)
        
        let views : [String : AnyObject] = [
            "tableView" : tableView
        ]
        
        let formatArray : [String] = [
            "V:|-0-[tableView]-0-|",
            "H:|-0-[tableView]-0-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    fileprivate func makeTableView() -> UITableView {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44.0
        tableView.backgroundColor = UIColor.clear
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorStyle = .singleLine
        tableView.allowsSelection = true
        tableView.allowsSelectionDuringEditing = false
        tableView.allowsMultipleSelection = false
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.keyboardDismissMode = .onDrag
        tableView.delegate = self
        tableView.dataSource = self
        
        //For iOS 9 and Above
        if #available(iOS 9, *) {
            tableView.cellLayoutMarginsFollowReadableWidth = false
        }
        
        tableView.register(HKUserAccountsTableCheckableViewCell.self,
                           forCellReuseIdentifier: HKUserAccountsTableViewController.ITEM_CELL_IDENTIFIER)
        tableView.register(HKUserAccountsHeaderTableViewCell.self,
                           forCellReuseIdentifier: HKUserAccountsTableViewController.HEADER_CELL_IDENTIFIER)
        tableView.register(HKUserAccountsAddButtonTableViewCell.self,
                           forCellReuseIdentifier: HKUserAccountsTableViewController.ADD_BUTTON_CELL_IDENTIFIER)
        
        return tableView
    }
    
    fileprivate func initializeDataSource() {
        self.buildSource(self.buildAccountsSection())
    }
}
