//
//  ChatMessageFilter.m
//  HonkioSDK
//
//  Created by Dev on 01.09.16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "ChatMessageFilter.h"

@implementation ChatMessageFilter

-(void)fillParams:(NSMutableDictionary*)params {
    [AbsFilter applyIfNotNil:@"order_id" value:self.orderId forParams:params];
    [super fillParams:params];
}

@end
