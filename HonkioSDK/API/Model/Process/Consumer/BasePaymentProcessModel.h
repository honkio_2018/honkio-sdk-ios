//
//  BasePaymentProcessModel.h
//  HonkioApi
//
//  Created by Shurygin Denis on 11/27/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "BaseProcessModel.h"
#import "QueryPaymentProcessModel.h"
#import "ApiResponseHandler.h"
#import "Shop.h"
#import "UserAccount.h"

// TODO write docs
@class BasePaymentProcessModel;

@interface PayParams : NSObject

@property (nonatomic) Shop* shop;
@property (nonatomic) NSString* orderId;
@property (nonatomic) UserAccount* account;

@property (nonatomic) double amount;
@property (nonatomic) NSString* currency;

@property (nonatomic) int flags;
@property (nonatomic) NSSet* disallowedAccounts;

-(id) initWithShop:(Shop*)shop oirderId:(NSString*)orderId account:(UserAccount*)account flags:(int)flags;

-(BasePaymentProcessModel*) instantiateProcessModel;

-(BOOL) isAccountValid;

@end

@protocol RequestPinProtocol <NSObject>

-(void)requestPin:(BasePaymentProcessModel*) model reason:(int)reason;

@end

@interface BasePaymentProcessModel : BaseProcessModel

@property (weak, nonatomic) id<RequestPinProtocol> pinDelegate;
@property (readonly, nonatomic) QueryPaymentProcessModel* queryProcessModel;

-(void)doPayment:(PayParams*)payParams handler:(ResponseHandler)handler;

-(void)query;
-(void)queryLoop:(long)loopDelay;
-(void)stopQuery;

-(void)pay:(PayParams*)payParams;


/**
 * Should be called when PIN entered.
 * @param pin Entered PIN code.
 * @param reason Reason for the PIN request.
 */
-(void)pinEntered:(NSString*)pin reason:(int)reason;

/**
 * Should be called when PIN pad canceled without entering a PIN.
 * @param reason Reason for the PIN request.
 */
-(void)pinCanceled:(int)reason;


@end
