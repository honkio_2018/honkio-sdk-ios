//
//  HKSettingsViewController.swift
//  Honkio SDK
//
//  Created by developer on 1/13/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

/**
The SettingsViewController is the view controller for settings page. Also it implements functions for
wizard completition and view controller completition.
*/

open class HKSettingsViewController: HKBaseTableViewController, HKWizardViewControllerCompletionDelegate {
    
    static let TAG_CHANGE_PIN = "HKSettingsViewController.CHANGE_PIN"
    
    public static let CELL_SETTINGS_ITEM = "CELL_SETTINGS_ITEM"
    public static let CELL_SETTINGS_INVOICE_ITEM = "CELL_SETTINGS_INVOICE_ITEM"
    public static let CELL_SETTINGS_LOGO_ITEM = "CELL_SETTINGS_LOGO_ITEM"
    
    @IBOutlet weak var tableView: UITableView!
    
    open var isAllowInvoice = false;
    
    open fileprivate(set) lazy var settingsSection : HKTableViewSection = self.makeSettingsSection()

    /**
    Builds the view, sets background color to White, sets row height and estimated row height and
    calls the building of items source on viewDidLoad event.
    */
    override open func viewDidLoad() {
        
        self.title = "settings_title".localized
        self.buildViewProgrammaticaly()
        
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 54
        
        buildSource(settingsSection)
    }
    
    /**
     Removes observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages on moved to parent controlled view.
     */
    open override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self, name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_SET_PHOTO)), object: nil)
        }
        else {
            
            NotificationCenter.default.addObserver(self, selector: #selector(HKBaseOrdersListViewController.reloadTableData(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_SET_PHOTO)), object: nil)
        }
    }

    /**
    Build the items source.
    - parameter lasySection: The section to be added into items source.
    */
    open func buildSource(_ lasySection : HKTableViewSection) {
        itemsSource.append(lasySection)
    }

    /**
    Presents the corresponding view controller based on the section was selected.
    - parameter tableView: The UITableView the selection has been made.
    - parameter indexPath: The IndexPath to the item was selected.
    */
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let section = self.itemsSource[(indexPath as IndexPath).section]
        let item = section.items[(indexPath as IndexPath).row]
        
        if let settingsItem = item.item as? HKSettingsItem {
            
            switch(settingsItem.type) {
                
            case HKSettingsItem.ITEM_TYPE_USER_ACCOUNT:
                if let controller = AppController.getViewControllerById(AppController.SettingsUserAccount, viewController: self) {
                    self.showViewController(controller)
                }
                break
                
            case HKSettingsItem.ITEM_TYPE_PAYMENT_ACCOUNTS:
                if let controller = AppController.getViewControllerById(AppController.ManageCards, viewController: self) {
                    self.showViewController(controller)
                }
                break
                
            case HKSettingsItem.ITEM_TYPE_CHANGE_PIN:
                
                if let controller = AppController.getViewControllerById(AppController.ChangePin, viewController: self) as? HKBaseViewController {
                    
                    let _ = HKCancelActionVCStrategy(parent: controller)
                    
                    let navi = HKWizardNavigationController(rootViewController: controller)
                    navi.wizardViewControllers.append(navi.buildViewControllerConstructor(AppController.RegistrationPin))
                    navi.setCompletionDelegate(self, tag: HKSettingsViewController.TAG_CHANGE_PIN)
                    
                    self.present(navi, animated: true, completion: nil)
                }
                
                break
                
            case HKSettingsItem.ITEM_TYPE_CHANGE_PASSWORD:
                if let controller = AppController.getViewControllerById(AppController.ChangePassword, viewController: self) {
                    self.showViewController(controller)
                }
                break
                
            case HKSettingsItem.ITEM_TYPE_LOST_PASSWORD:
                if let controller = AppController.getViewControllerById(AppController.ForgotPassword, viewController: self) {
                    self.showViewController(controller)
                }
                break
                
            case HKSettingsItem.ITEM_TYPE_TOU:
                if let controller = AppController.getViewControllerById(AppController.Tou, viewController: self) {
                    self.showViewController(controller)
                }
                break
                
            case HKSettingsItem.ITEM_TYPE_GDPR:
                if let controller = AppController.getViewControllerById(AppController.Gdpr, viewController: self) {
                    self.showViewController(controller)
                }
                break
                
            case HKSettingsItem.ITEM_TYPE_ABOUT:
                if let controller = AppController.getViewControllerById(AppController.SettingsAbout, viewController: self) {
                    self.showViewController(controller)
                }
                break
                
            case HKSettingsItem.ITEM_TYPE_LOGOUT:
                
                CustomAlertView(title: "settings_btn_logout".localized, message: "settings_dlg_logout_message".localizedWithComment("Do you really want to logout?"), cancelButtonTitle: "dlg_common_button_cancel".localizedWithComment("Cancel"), cancelButtonAction: nil)
                    .addButtonWithTitle("dlg_common_button_ok".localizedWithComment("OK"), action: { self.logout() })
                    .show()
                
                break
            default:
                break
            }
        }
    }

    /**
    Logs out the current user from server.
    */
    open func logout () {
        BusyIndicator.shared.showProgressView(self.view)
        HonkioApi.logout(0) { (error) in
            BusyIndicator.shared.hideProgressView()
            if (error != nil) {
                if (!AppErrors.handleError(self, error: error)) {
                    CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
            else {
                var controller: UIViewController?
                
                if AppController.instance.isGuestModeAvailable() {
                    controller = AppController.getViewControllerById(AppController.Main, viewController: self)
                }
                else if let loginController = AppController.getViewControllerById(AppController.Login, viewController: self) as? HKOAuthLoginViewController {
                    loginController.doLogin = false
                    controller = loginController
                }
                if controller != nil {
                    var viewController: UIViewController = controller!
                    
                    if !controller!.isKind(of: UINavigationController.self) {
                        
                        let navi = UINavigationController(rootViewController: controller!)
                        viewController = navi
                    }
                    
                    let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
                    appDelegate.changeRootViewController(viewController)
                }
            }
        }
    }

    /**
    - Returns: The value represents the height of the section row.
    - parameter tableView: The UITableView the height of row is returned.
    - parameter section: The section index the height of row is returned.
    */
    open func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 20
    }

    /**
    Shows change PIN custom alert if the current view controller is tagged as "SettingsViewController.CHANGE_PIN".
    - parameter viewController: Not used.
    - parameter tag: The string tag representing the current view controller.
    - parameter wizardObject: No used.
    */
    open func onWizardComplete(_ viewController: UIViewController, tag: String?, wizardObject: AnyObject?) {
        switch tag {
            
        case HKSettingsViewController.TAG_CHANGE_PIN?:
            
            CustomAlertView(okTitle: "change_pin_dlg_accept_title".localized, okMessage: "change_pin_dlg_accept_message".localized, okAction: {
                
                self.presentedViewController?.dismiss(animated: true, completion: nil)
                
            }).show()
            
            break
            
        default:
            
            break
        }
    }

    /**
    Checks if the provided section is Settings section.
    - parameter someSection: The section to be tested.
    - Returns: True if the provided section is Settings section.
    */
    open func isSettingsSection(_ someSection : HKTableViewSection) -> Bool {
        return settingsSection == someSection
    }
    
    
    // TODO: docs
    open func showViewController(_ viewController: UIViewController) {
        if self.navigationController != nil {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else {
            
            if viewController.isKind(of: HKBaseViewController.self) {
                let _ = HKCancelActionVCStrategy(parent: viewController as! HKBaseViewController)
            }
            
            let navi = UINavigationController(rootViewController: viewController)
            self.present(navi, animated: true, completion: nil)
        }
    }
        
    open func dismissViewController(_ viewController: UIViewController) {
        if self.navigationController != nil {
            if self.navigationController!.viewControllers.last == viewController {
                _ = self.navigationController!.popViewController(animated: true)
            }
            else {
                for i in 0..<self.navigationController!.viewControllers.count {
                    if self.navigationController?.viewControllers[i] == viewController {
                        self.navigationController?.viewControllers.remove(at: i)
                        return
                    }
                }
            }
        }
        else {
            viewController.presentedViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func reloadTableData(_ notification : Notification) {
        if self.tableView != nil {
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Private methods
    fileprivate func makeSettingsSection() -> HKTableViewSection {
        
        let section = HKTableViewSection()
        
        let settingsCellBinder = { (_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void in
            
            let settingsCell = cell as! HKSettingsTableViewCell
            settingsCell.fillFromItem(item as? HKSettingsItem)
        }
        
        section.items.append((
            HKSettingsViewController.CELL_SETTINGS_ITEM,
            HKSettingsItem() {
                $0.iconName = "hk_ic_profile";
                $0.title = "settings_addres_title".localized;
                $0.type = HKSettingsItem.ITEM_TYPE_USER_ACCOUNT },
            settingsCellBinder))
        
        section.items.append((
            HKSettingsViewController.CELL_SETTINGS_ITEM,
            HKSettingsItem() {
                $0.iconName = "hk_ic_account_card";
                $0.title = "settings_btn_set_pay_account".localized;
                $0.type = HKSettingsItem.ITEM_TYPE_PAYMENT_ACCOUNTS },
            settingsCellBinder))
        
        if self.isAllowInvoice {
            section.items.append((
                HKSettingsViewController.CELL_SETTINGS_INVOICE_ITEM,
                HKSettingsItem() {
                    $0.iconName = "hk_ic_pin";
                    $0.title = "settings_btn_change_pin".localized;
                    $0.type = HKSettingsItem.ITEM_TYPE_CHANGE_PIN },
                { (viewController, cell, item) -> Void in
                    let settingsCell = cell as! HKSettingsTableInvoiceCell
                    settingsCell.fillFromItem(item as? HKSettingsItem)
                }))
        }
        
        section.items.append((
            HKSettingsViewController.CELL_SETTINGS_ITEM,
            HKSettingsItem() {
                $0.iconName = "hk_ic_pin";
                $0.title = "settings_btn_change_pin".localized;
                $0.type = HKSettingsItem.ITEM_TYPE_CHANGE_PIN },
            settingsCellBinder))
        
        section.items.append((
            HKSettingsViewController.CELL_SETTINGS_ITEM,
            HKSettingsItem() {
                $0.iconName = "hk_ic_lock";
                $0.title = "settings_btn_change_password".localized;
                $0.type = HKSettingsItem.ITEM_TYPE_CHANGE_PASSWORD },
            settingsCellBinder))
        
        section.items.append((
            HKSettingsViewController.CELL_SETTINGS_ITEM,
            HKSettingsItem() {
                $0.iconName = "hk_ic_question";
                $0.title = "registration_dlg_consumer_exist_btn_forgot_password".localized;
                $0.type = HKSettingsItem.ITEM_TYPE_LOST_PASSWORD },
            settingsCellBinder))
        
        section.items.append((
            HKSettingsViewController.CELL_SETTINGS_ITEM,
            HKSettingsItem() {
                $0.iconName = "hk_ic_tou";
                $0.title = "settings_btn_terms_of_use".localized;
                $0.type = HKSettingsItem.ITEM_TYPE_TOU },
            settingsCellBinder))

//        Hide until functionality will be created on servers
//        section.items.append((
//            HKSettingsViewController.CELL_SETTINGS_ITEM,
//            HKSettingsItem() {
//                $0.iconName = "hk_ic_gdpr";
//                $0.title = "settings_btn_gdpr".localized;
//                $0.type = HKSettingsItem.ITEM_TYPE_GDPR },
//            settingsCellBinder))
        
        section.items.append((
            HKSettingsViewController.CELL_SETTINGS_ITEM,
            HKSettingsItem() {
                $0.iconName = "hk_ic_info";
                $0.title = "settings_btn_about".localized;
                $0.type = HKSettingsItem.ITEM_TYPE_ABOUT },
            settingsCellBinder))
        
        section.items.append((
            HKSettingsViewController.CELL_SETTINGS_ITEM,
            HKSettingsItem() {
                $0.iconName = "hk_ic_logoff";
                $0.title = "settings_btn_logout".localized;
                $0.type = HKSettingsItem.ITEM_TYPE_LOGOUT },
            settingsCellBinder))
        
        section.items.append((HKSettingsViewController.CELL_SETTINGS_LOGO_ITEM, nil, { (viewController, cell, item) in
            
        }))
        
        return section
    }
    
    fileprivate func buildViewProgrammaticaly() {
        // setup Subviews
        let newtable = self.makeTableView()
        self.tableView = newtable
        self.registerCells(self.tableView!)
        view.addSubview(self.tableView)
        
        // setup Constraints
        let views : [String : AnyObject] = [
            "tableView" : self.tableView,
            ]
        
        let formatArray : [String] = [
            "V:|-0-[tableView]-0-|",
            "H:|-0-[tableView]-0-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        view.backgroundColor = UIColor.white
    }
    
    fileprivate func makeTableView() -> UITableView {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 54.0
        tableView.backgroundColor = UIColor.clear
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorStyle = .none
        tableView.allowsSelection = true
        tableView.allowsSelectionDuringEditing = false
        tableView.allowsMultipleSelection = false
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.keyboardDismissMode = .onDrag
        tableView.delegate = self
        tableView.dataSource = self
        
        //For iOS 9 and Above
        if #available(iOS 9, *) {
            tableView.cellLayoutMarginsFollowReadableWidth = false
        }
        
        return tableView
    }
    
    fileprivate func registerCells(_ tableView: UITableView) {
        tableView.register(HKSettingsTableLogoCell.self,
                           forCellReuseIdentifier: HKSettingsViewController.CELL_SETTINGS_LOGO_ITEM)
        tableView.register(HKSettingsTableViewCell.self,
                           forCellReuseIdentifier: HKSettingsViewController.CELL_SETTINGS_ITEM)
        tableView.register(HKSettingsTableInvoiceCell.self,
                           forCellReuseIdentifier: HKSettingsViewController.CELL_SETTINGS_INVOICE_ITEM)
        
    }
}
