//
//  AppErrors.swift
//  WorkPilots
//
//  Created by developer on 2/10/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation
/**
Contains the implementations of methods for managing the error handling in the application
*/
open class BaseAppErrors : AppErrors {
    
    override public init() {
        
    }

    /**
    Handles the error.
    - parameter controller: The UIViewController that needs to handle the error.
    - parameter error: The NSError object describing the error.
    - parameter fallAction: The action to be performed after error has been handled.
    - Returns: True if error has been successfully handled.
    */
    override open func handleErrorImpl(_ controller: UIViewController, error: ApiError?, fallAction:(() -> Void)?) -> Bool {
        if error != nil {
            
            switch(error!.code) {
                
                // ======= ErrApi =======
                
            case ErrApi.noConnection.rawValue:
                CustomAlertView(okTitle: "dlg_bad_connection_title".localized, okMessage: "dlg_bad_connection_message".localized, okAction: fallAction).show()
                return true
                
            case ErrApi.badResponse.rawValue:
                CustomAlertView(okTitle: "dlg_bad_status_code_title".localized, okMessage: "dlg_bad_status_code_message".localized, okAction: fallAction).show()
                return true
                
            case ErrApi.wrongHash.rawValue:
                CustomAlertView(okTitle: "Wrong hash", okMessage: "Wrong hash!", okAction: fallAction).show()
                return true
                
            case ErrApi.accountNotSupported.rawValue:
                CustomAlertView(okTitle: "dlg_unsuported_account_title".localized, okMessage: "dlg_unsuported_account_message".localized, okAction: fallAction).show()
                return true
                
            case ErrApi.canceledByUser.rawValue:
                // DO nothing
                if fallAction != nil {
                    fallAction!()
                }
                return true
                
                // ====== ErrSystem ======
                
            case ErrSystem.serviceWorks.rawValue:
                let message = (error?.desc?.count ?? 0 > 0) ? error!.desc! : "dlg_service_work_message".localized
                CustomAlertView(okTitle: "dlg_service_work_title".localized, okMessage: message, okAction: fallAction).show()
                
                return true
                
                // ====== ErrCommon ======
                
            case ErrCommon.invalidValueForParameter.rawValue:
                
                if let array:NSArray = error!.fields as NSArray? {
                    
                    if array.contains("identity_client") {
                        
                        CustomAlertView(okTitle: "dlg_unsupported_app_title".localized, okMessage: "dlg_unsupported_app_message".localized, okAction: nil).show()
                        return true
                    }
                }
                break
                
            case ErrCommon.invalidUserLogin.rawValue:
                
                if let connection = HonkioApi.activeConnection() {
                    
                    connection.setToInvalid()
                    connection.increasePinFails()
                }
                
                if Connection.pinFailsCount() >= MAX_PIN_FAIL_COUNT {
                    
                    CustomAlertView(okTitle: nil, okMessage: "dlg_pin_logout_message".localized, okAction: nil).show()
                }
                else {
                    
                    CustomAlertView(okTitle: nil, okMessage: "dlg_otp_lost_message".localized, okAction: buildLogoutAction(controller)).show()
                }
                
                return true
                
            case ErrApi.invalidPin.rawValue:
                if let connection = HonkioApi.activeConnection() {
                    if connection.isValid() {
                        CustomAlertView(okTitle: nil, okMessage: "dlg_pin_invalid_message".localized, okAction: nil).show()
                        return true
                    }
                }
                
                CustomAlertView(okTitle: nil, okMessage: "dlg_pin_logout_message".localized, okAction: buildLogoutAction(controller)).show()
                return true
                
            default:
                
                return false
            }
        }
        
        return false
    }
    /**
    Creates the action to be performed for logout the user.
    - parameter controller: The UIViewController requesting the logout action.
    - Returns: The action containing server notification about user is to be logger in and changing root ViewController
    to LoginViewControler.
    */
    open func buildLogoutAction(_ controller: UIViewController) -> (()->Void) {
        weak var weakController = controller
        return  {() in
            
            HonkioApi.logout(MSG_FLAG_HIGH_PRIORITY) { (error) in
                if error != nil {
                    HonkioApi.logout(MSG_FLAG_OFFLINE_MODE)
                }
            }
            
            if let strongController = weakController {
                
                if AppController.instance.isGuestModeAvailable() {
                    if let viewController = AppController.getViewControllerById(AppController.Main, viewController: strongController) {
                        
                        let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
                        appDelegate.changeRootViewController(viewController)
                    }
                }
                else {
                    if let controller = AppController.getViewControllerById(AppController.Login, viewController: strongController) as? HKOAuthLoginViewController {
                        controller.doLogin = false
                        
                        var viewController: UIViewController = controller
                        
                        if controller.navigationController == nil {
                            
                            let navi = UINavigationController(rootViewController: controller)
                            viewController = navi
                        }
                        
                        let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
                        appDelegate.changeRootViewController(viewController)
                    }
                }
            }
        }
    }
}

