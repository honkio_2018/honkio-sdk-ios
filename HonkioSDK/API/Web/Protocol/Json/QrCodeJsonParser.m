//
//  QrCodeJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "QrCodeJsonParser.h"
#import "AssetJsonParser.h"
#import "HKQrCode.h"
#import "HKAsset.h"

@implementation QrCodeJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    HKQrCode* qrCode = [HKQrCode new];
    
    if (dict != nil) {
        qrCode.qrId = dict[@"id"];
        qrCode.merchantId = dict[@"merchant"];
        qrCode.urlBase = dict[@"url_base"];
        qrCode.urlParams = dict[@"url_params"];
        qrCode.applications = [self parseAplications: dict[@"applications"]];
        qrCode.objectType = dict[@"object_type"];
        
        if ([@"null" isEqualToString:qrCode.objectType])
            qrCode.objectType = nil;
        
        if (qrCode.objectType) {
            if ([HK_QR_TYPE_ASSET isEqualToString:qrCode.objectType]) {
                HKAsset* asset = [AssetJsonParser parseAsset: dict[@"object"]];
                qrCode.object = asset;
            }
        }
    }
    return qrCode;
}

-(NSArray*) parseAplications:(NSArray*)array {
    return nil;
}

@end
