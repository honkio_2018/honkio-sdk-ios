//
//  HKUserAccountsHeaderTableViewCell.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/28/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

// TODO docs
class HKUserAccountsHeaderTableViewCell: HKBaseTableViewCell {

    fileprivate(set) open lazy var paymentLabel : UILabel = self.makePaymentLabel()

    override open func setupStyle() {
        super.setupStyle()
        
        selectionStyle = .none
        
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
        
        selectedBackgroundView = UIView()
        backgroundColor = UIColor.clear
    }

    override open func setupSubviews() {
        super.setupSubviews()
        contentView.addSubview(paymentLabel)
    }

    override open func setupConstraints() {
        super.setupConstraints()
        
        let views : [String : AnyObject] = [
            "paymentLabel" : paymentLabel
        ]
        
        let formatArray : [String] = [
            "V:|-25-[paymentLabel]-8-|",
            "H:|-16-[paymentLabel]-16-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    fileprivate func makePaymentLabel() -> UILabel {
        let view : UILabel = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.text = "pay_accounts_list_title".localized
        
        return view;
    }
}
