//
//  AppInfo.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/28/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "Identity.h"
#import "HKTouInfo.h"

@interface AppInfo : Identity

/** Identity of the main app shop */
@property (nonatomic) Identity* shopIdentity;

/** OAuth credentials */
@property (nonatomic) Identity* oauthIdentity;

-(id)init;

-(id)initWithId:(NSString*)identityId andPassword:(NSString*)password;

-(id)initFrom:(Identity*)identity;

-(HKTouInfo*) getTouInfo;
-(HKTouInfo*) getTouInfo:(NSString*) key;
-(void) setTouInfo:(HKTouInfo*) touInfo;

@end
