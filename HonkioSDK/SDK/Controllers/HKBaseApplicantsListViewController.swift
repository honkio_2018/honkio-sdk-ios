//
//  HKBaseApplicantsListViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/9/17.
//  Copyright © 2017 developer. All rights reserved.
//

import Foundation

//TODO write dics
open class HKBaseApplicantsListViewController: HKBaseLazyTableViewController {
    public var shopIdentity : Identity?
    public fileprivate(set) var filter : ApplicantFilter = ApplicantFilter()
    
    @IBInspectable public var filterOrder : String?
    @IBInspectable public var filterQueryRole: String?
    
    /**
     Loads the list of applicants by specified filter.
     - parameter responseHandler: The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
     - parameter count: The number of applicants to be loaded from server.
     - parameter skip: The number of applicants to be skipped on loading.
     */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(filter, count: count, skip: skip)
        
        HonkioApi.orderGetApplicants(filter, shop:shopIdentity, flags: 0, handler: responseHandler)
    }
    
    /**
     Creates the list from the result of the Response
     - parameter responce: The Response instance within the result of processing.
     - Returns: A list of applicants.
     */
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! ApplicantsList).list
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! Applicant), listItem, itemBinder))
        }
        return items
    }
    
    /**
     Gets the cell indetifier for provided Applicant.
     * Must be overriden.
     - parameter item: Applicant.
     - Returns: The identifier of cell.
     */
    open func cellIdentifier(_ item: Applicant) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }
    
    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }
    
    /**
     Sets the filter params for the current filter.
     - parameter filter: The filter to be updated.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
     */
    open func setupFilter(_ filter: ApplicantFilter, count: Int, skip: Int) {
        self.filter.orderId = filterOrder
        self.filter.queryRole = filterQueryRole
        
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
}
