//
//  HKBaseUserRoleDescriptionsListViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/5/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

// TODO write docs
open class HKBaseUserRoleDescriptionsListViewController: HKBaseLazyTableViewController {
    
    public var shopIdentity : Identity?
    public fileprivate(set) var filter : UserRoleDescriptionsFilter = UserRoleDescriptionsFilter()
    
    @IBInspectable public var filterType : String?
    
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(self.filter, count: count, skip: skip)
        HonkioApi.userRoleDescriptionsList(self.filter, shop:self.shopIdentity, flags: 0, handler: responseHandler)
    }
    
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! UserRoleDescriptionsList).list
    }
    
    open override func mapItem(_ item: Any?) -> ((identifierOrNibName: String, item: Any?, initializer: ((UIViewController, UITableViewCell, Any?) -> Void)?)) {
        return (cellIdentifier(item as! UserRoleDescription), item, self.buildCellBunder())
    }
    
    open func cellIdentifier(_ item: UserRoleDescription) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }
    
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }
    
    open func setupFilter(_ filter: UserRoleDescriptionsFilter, count: Int, skip: Int) {
        filter.type = filterType
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
}
