//
//  HKBaseLazyContentViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/11/19.
//  Copyright © 2019 developer. All rights reserved.
//

import UIKit

open class HKBaseLazyContentViewController<ContentType>: HKBaseViewController {
    
    open var contentItem: ContentType? {
        didSet {
            if let contentItem = self.contentItem {
                self.contentItemDidSet(contentItem)
            }
        }
    }
    
    private var progressView: UIView!
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.showProgressView()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.contentItem == nil {
            showProgressView()
        }
    }
    
    open func contentItemDidSet(_ contentItem: ContentType) {
        let viewController = buildViewController(contentItem)
        self.addChildViewController(viewController)
        
        self.view.addSubview(viewController.view)
        
        let views : [String : AnyObject] = [
            "contentView" : viewController.view
        ]
        
        let formatArray : [String] = [
            "H:|-0-[contentView]-0-|",
            "V:|-0-[contentView]-0-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
        
        viewController.didMove(toParentViewController: self)
        
        // Fade animation
        viewController.view.alpha = 0.0
        UIView.animate(withDuration: 0.5, animations: {
            viewController.view.alpha = 1.0
            self.progressView.alpha = 0.0
        }) { (bool) in
            self.progressView.removeFromSuperview()
        }
    }
    
    open func buildViewController(_ contentItem: ContentType) -> UIViewController {
        preconditionFailure("Method buildViewController must be overridden")
    }
    
    open func showProgressView() {
        if self.progressView == nil {
            self.progressView = UIProgressView()
            self.progressView.translatesAutoresizingMaskIntoConstraints = false
            self.progressView.backgroundColor = UIColor.clear
            
            self.view.addSubview(self.progressView)
            
            NSLayoutConstraint.activate([
                NSLayoutConstraint(item: self.progressView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1, constant: 1),
                NSLayoutConstraint(item: self.progressView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 1)
                ])
        }
    }
    
}
