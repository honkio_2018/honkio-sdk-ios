//
//  AssetFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/1/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "AssetFilter.h"

@implementation AssetFilter

@synthesize querySchedule;

-(void)fillParams:(NSMutableDictionary *)params {
    [AbsFilter applyIfNotNil:@"query_search_latitude" value:self.latitude forParams:params];
    [AbsFilter applyIfNotNil:@"query_search_longitude" value:self.longitude forParams:params];
    [AbsFilter applyIfNotNil:@"query_distance" value:self.radius forParams:params];
    [AbsFilter applyIfNotNil:@"query_structure" value:self.structureIds forParams:params];
    [AbsFilter applyIfNotNil:@"query_merchant" value:self.merchantId forParams:params];
    if (self.isQuerySchedule)
        [AbsFilter applyIfNotNil:@"query_schedule" value:@(YES) forParams:params];
    if (self.propertiesFilter)
        [AbsFilter applyIfNotNil:@"query_properties" value:[self.propertiesFilter toParams] forParams:params];
    
    [AbsFilter applyIfNotNil:@"query_sort" value:[self.sortFilter toParams] forParams:params];
    
    [super fillParams:params];
}

@end
