//
//  HKBaseViewController.swift
//  WorkPilots
//
//  Created by developer on 12/11/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

/**
BaseViewController monitors life cycle and user interface state, notifyes HKViewControllerLifeCycleObserver about life cycle and state of view.
- SeeAlso: HKViewControllerLifeCycleObserver.
*/

open class HKBaseViewController: UIViewController, UIAlertViewDelegate, UITextFieldDelegate, UITextViewDelegate, HKLifeCycleObserverParentViewController {
    
    let KEYBOARD_ANIMATION_DURATION = CGFloat(0.3)
    let MINIMUM_SCROLL_FRACTION = CGFloat(0.2)
    let MAXIMUM_SCROLL_FRACTION = CGFloat(0.8)
    let PORTRAIT_KEYBOARD_HEIGHT = CGFloat(216)
    let LANDSCAPE_KEYBOARD_HEIGHT = CGFloat(162)
    
    open var isAllowKeyboardOffset = true
    
    fileprivate var animatedDistance : CGFloat?

    fileprivate weak var completionDelegate : HKViewControllerCompletionDelegate?
    fileprivate var tag: String?
    fileprivate var lifeCycleObservers: [HKViewControllerLifeCycleObserver] = []
    
    public init() {
        super.init(nibName: nil, bundle:  nil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    /**
    Sets completion delegate with the provided listener.
    - parameter completionDelegate: The provided listener class.
    - parameter tag: The string value the completion call will be tagged with.
     - SeeAlso: notifyCompletionDelegate
    */
    open func setCompletionDelegate(_ completionDelegate: HKViewControllerCompletionDelegate?, tag: String?) {
        self.completionDelegate = completionDelegate
        self.tag = tag
    }

    /**
     Notifies completion delegate about completion.
     - SeeAlso: setCompletionDelegate
    */
    open func notifyCompletionDelegate() {
        if let delegate = self.completionDelegate {
            delegate.onComplete(self, tag: self.tag)
        }
    }

    /**
    Connects to the provided life cycle observer.
    - parameter observer: The life cycle observer we're going to connect to.
    */
    open func addLifeCycleObserver(_ observer: HKViewControllerLifeCycleObserver) {
        self.lifeCycleObservers.append(observer)
        observer.didAddToViewController(self)
    }
    
    open func getLifeCycleObserver(_ tag: String) -> HKViewControllerLifeCycleObserver? {
        for i in 0..<self.lifeCycleObservers.count {
            if self.lifeCycleObservers[i].tag == tag {
                return self.lifeCycleObservers[i]
            }
        }
        return nil
    }
    
    // TODO write docs
    open func removeLifeCycleObserver(_ observer: HKViewControllerLifeCycleObserver) {
        for i in 0..<self.lifeCycleObservers.count {
            if self.lifeCycleObservers[i] === observer {
                self.lifeCycleObservers.remove(at: i)
                observer.didAddToViewController(nil)
                return
            }
        }
    }

    /// Life cycle observer should be notified the ViewController state has changed.
    override open func viewDidLoad() {
        super.viewDidLoad()
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewDidLoad()
        }
    }

    /**
    Life cycle observer should be notified the ViewController state has changed.
    - parameter animated: Whether the state has changed with animation.
    */
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewWillAppear(animated)
        }
    }

    /**
    Life cycle observer should be notified the ViewController state has changed.
    - parameter animated: Whether the state has changed with animation.
    */
    override open func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewDidAppear(animated)
        }
    }

    /**
    Life cycle observer should be notified the ViewController state has changed.
    - parameter animated: Whether the state has changed with animation.
    */
    override open func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewWillDisappear(animated)
        }
    }

    /**
    Life cycle observer should be notified the ViewController state has changed.
    - parameter animated: Whether the state has changed with animation.
    */
    override open func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewDidDisappear(animated)
        }
    }

    /**
    Calls presentViewController.
    The completion handler, if provided, will be invoked after the presented
    controllers viewDidAppear: callback is invoked.
    - parameter viewController: A ViewController to be provided to a viewController call.
    - parameter completionAction: The handler to be called after viewDidAppear.
    */
    open func presentViewControllerWithNavigation(_ viewController: UIViewController, completionAction : (() -> Void )?) {
        
        let naviController = UINavigationController(rootViewController: viewController)

        // FIXME:
        // naviController.navigationBar.setColors() // TODO FIX IT!!!!!!
        self.present(naviController, animated: true, completion: completionAction)
    }

    /**
    Overrides the basic touchesBeganWithEvent for resigning the first view responder.
    - parameter touches: Set of UITouches
    - parameter event: The corresponding UIEvent.
    */
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dismissKeyboard()
    }

    /**
    Sets the text view look in editing state.
    - parameter textView: The UITextView which is in editing state.
    */
    open func textViewDidBeginEditing(_ textView: UITextView) {
        self.textDidBeginEditing(textView)
    }

    /**
    Sets the text view look in normal state.
    - parameter textView: The UITextView which is in editing state.
    */
    open func textViewDidEndEditing(_ textView: UITextView) {
        self.textDidEndEditing(textView)
    }

    /**
    Sets the text field look in editing state.
    - parameter textField: The UITextField which is in editing state.
    */
    open func textFieldDidBeginEditing(_ textField: UITextField) {
        self.textDidBeginEditing(textField);
    }

    /**
    Sets the text field look in normal state.
    - parameter textField: The UITextField which is in editing state.
    */
    open func textFieldDidEndEditing(_ textField: UITextField) {
        self.textDidEndEditing(textField)
    }
    
    public func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(HKBaseViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc public func dismissKeyboard() {
        self.view.endEditing(true)
    }

    func textDidBeginEditing(_ textView: UIView) {
        if isAllowKeyboardOffset == false {
            return
        }
        
        let textFieldRect = self.view.window?.convert(textView.bounds, from:textView);
        let viewRect = self.view.window?.convert(self.view.bounds, from:self.view);
        let midline = textFieldRect!.origin.y + 0.5 * textFieldRect!.size.height;
        let numerator = midline - viewRect!.origin.y - MINIMUM_SCROLL_FRACTION * viewRect!.size.height;
        let denominator = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * viewRect!.size.height;
        var heightFraction = numerator / denominator;
        
        if heightFraction < 0.0 {
            heightFraction = 0.0;
        }
        else if heightFraction > 1.0 {
            heightFraction = 1.0;
        }
        
        let orientation = UIApplication.shared.statusBarOrientation;
        if orientation == .portrait || orientation == .portraitUpsideDown {
            animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
        }
        else {
            animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
        }
        
        var viewFrame = self.view.frame;
        viewFrame.origin.y -= animatedDistance!;
        
        UIView.beginAnimations(nil, context:nil);
        UIView.setAnimationBeginsFromCurrentState(true);
        UIView.setAnimationDuration(TimeInterval(KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
    }
    
    func textDidEndEditing(_ textView: UIView) {
        if isAllowKeyboardOffset == false {
            return
        }
        
        var viewFrame = self.view.frame;
        viewFrame.origin.y += animatedDistance!;
        
        UIView.beginAnimations(nil, context:nil);
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(TimeInterval(KEYBOARD_ANIMATION_DURATION))
        
        self.view.frame = viewFrame
        
        UIView.commitAnimations()
    }
}
