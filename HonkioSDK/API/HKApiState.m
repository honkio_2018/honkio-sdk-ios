//
//  HKApiState.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/12/19.
//  Copyright © 2019 developer. All rights reserved.
//

#import "HKApiState.h"
#import "NotificationHelper.h"
#import "HonkioApi.h"
#import "StoreData.h"
#import "HKDeviceJsonParser.h"

#define SAVED_DEVICE @"HKApiState.SAVED_DEVICE"

@implementation HKApiState
    
    -(id)initWithUrl:(NSURL*)apiUrl appIdentity:(Identity*)appIdentity {
        self = [super init];
        if (self) {
            _apiUrl = apiUrl;
            _appIdentity = appIdentity;
            
            [self restoreProperties];
            [self initOservers];
        }
        return self;
    }

    -(void)restoreProperties {
        NSDictionary* savedDevice = [StoreData loadFromStore:SAVED_DEVICE];
        if(savedDevice) {
            HKDevice* device = [[HKDeviceJsonParser new] parseDictionary:nil dict:savedDevice];
            if ([device device_id]) {
                _device = device;
            }
        }
        
    }
    
    -(void) initOservers {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceNotification:) name: [NotificationHelper notifyNameOnComplete:MSG_COMMAND_SET_DEVICE] object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(serverInfoNotification:) name: [NotificationHelper notifyNameOnComplete:MSG_COMMAND_SERVER_INFO] object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appInfoNotification:) name: [NotificationHelper notifyNameOnComplete:MSG_COMMAND_APP_INFO] object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shopInfoNotification:) name: [NotificationHelper notifyNameOnComplete:MSG_COMMAND_SHOP_INFO] object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userGetNotification:) name: [NotificationHelper notifyNameOnComplete:MSG_COMMAND_USER_GET] object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userGetNotification:) name: [NotificationHelper notifyNameOnComplete:MSG_COMMAND_USER_SET] object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutNotification:) name: [NotificationHelper notifyNameOnComplete:MSG_COMMAND_LOGOUT] object:nil];
    }
    
    -(void)deviceInfoDidGet:(HKDevice*)device {
        if(device) {
            _device = device;
            [StoreData saveToStore:[device toParams] forKey:SAVED_DEVICE];
        }
    }
    
    -(void)serverInfoDidGet:(ServerInfo*)serverInfo {
        if (serverInfo) {
            _serverInfo = serverInfo;
        }
    }
    
    -(void)appInfoDidGet:(AppInfo*)appInfo {
        if (appInfo) {
            _appInfo = appInfo;
        }
    }
    
    -(void)shopInfoDidGet:(ShopInfo*)shopInfo {
        if (shopInfo) {
            
            BOOL isMainShop = [[[self.appInfo shopIdentity] identityId] isEqualToString:[[shopInfo shop] identityId]];
            
            if (isMainShop) {
                [[shopInfo shop] setPassword: [[self.appInfo shopIdentity] password]];
                _shopInfo = shopInfo;
            }
        }
    }
    
    -(void)userDidGet:(User*)user {
        if (user) {
            _user = user;
        }
    }
    
    - (void) deviceNotification:(NSNotification *) notification {
        HKDevice* result = [[[notification userInfo] valueForKey: NOTIFY_EXTRA_RESULT] result];
        [self deviceInfoDidGet:result];
    }
    
    - (void) serverInfoNotification:(NSNotification *) notification {
        ServerInfo* result = [[[notification userInfo] valueForKey: NOTIFY_EXTRA_RESULT] result];
        [self serverInfoDidGet:result];
    }
    
    - (void) appInfoNotification:(NSNotification *) notification {
        AppInfo* result = [[[notification userInfo] valueForKey: NOTIFY_EXTRA_RESULT] result];
        [self appInfoDidGet:result];
    }
    
    - (void) shopInfoNotification:(NSNotification *) notification {
        ShopInfo* result = [[[notification userInfo] valueForKey: NOTIFY_EXTRA_RESULT] result];
        [self shopInfoDidGet:result];
    }
    
    - (void) userGetNotification:(NSNotification *) notification {
        User* result = [[[notification userInfo] valueForKey: NOTIFY_EXTRA_RESULT] result];
        [self userDidGet:result];
    }
    
    - (void) logoutNotification:(NSNotification *) notification {
        _user = nil;
    }
    
@end
