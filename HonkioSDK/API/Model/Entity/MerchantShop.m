//
//  MerchantShop.m
//  HonkioSDK
//
//  Created by Mikhail Li on 21/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "MerchantShop.h"
#import "AbsFilter.h"

@implementation MerchantShop {
    NSArray<NSString*> *generalFields;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self resetGeneralFields];
    }
    return self;
}

-(void)setGeneralFields:(NSArray<NSString*>*)fields {
    generalFields = fields;
}

-(void)resetGeneralFields {
    self.generalFields = [NSArray arrayWithObjects:
                          MERCHANT_SHOP_FIELD_NAME, MERCHANT_SHOP_FIELD_ADDRESS,
                          MERCHANT_SHOP_FIELD_LOGO_LARGE, MERCHANT_SHOP_FIELD_LOGO_SMALL,
                          MERCHANT_SHOP_FIELD_MAX_DISTANCE, MERCHANT_SHOP_FIELD_SHOP_TYPE,
                          MERCHANT_SHOP_FIELD_SERVICE_TYPE, MERCHANT_SHOP_FIELD_SHOP_MODE,
                          MERCHANT_SHOP_FIELD_ACTIVE, MERCHANT_SHOP_FIELD_DAY_BREAK,
                          MERCHANT_SHOP_FIELD_EMAIL, MERCHANT_SHOP_FIELD_LATITUDE,
                          MERCHANT_SHOP_FIELD_LONGITUDE, MERCHANT_SHOP_FIELD_TAG_LIST,
                          MERCHANT_SHOP_FIELD_DESCRIPTION, nil];
}

-(NSDictionary<NSString*, id>*)toParams {
    NSMutableDictionary<NSString*, id>* params = [NSMutableDictionary new];
    for(NSString* field in generalFields) {
        [AbsFilter applyIfNotNil:field value:self.settings[field] forParams:params];
    }
    [AbsFilter applyIfNotNil:MERCHANT_SHOP_FIELD_CREATE value:self.settings[MERCHANT_SHOP_FIELD_CREATE] forParams:params];
    return params;
}

@end
