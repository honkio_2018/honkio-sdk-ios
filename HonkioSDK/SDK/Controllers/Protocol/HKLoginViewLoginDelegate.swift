//
//  HKLoginViewLoginDelegate.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 6/4/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

public protocol HKLoginViewLoginDelegate : NSObjectProtocol {
    
    /**
     Notifies the listener that the login routine has been completed.
     - parameter isLogedin: Provides the value describint whether the login procedure was completed successfully.
     */
    func loginFinished(_ view: HKLoginViewProtocol, _ isLogedin: Bool)
}
