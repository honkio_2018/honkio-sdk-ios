//
//  LoadProperties.m
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/17/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import "LoadingProperties.h"

@implementation NSMutableDictionary(PropertyParser)

-(void)saveStringProperties:(NSString*)key value:(NSString*)value onlyNotEmpty:(BOOL)notEmpty
{
    if(notEmpty && !value)
        return;
    self[key] = value;
}

-(void)saveDictionaryProperties:(NSString*)key value:(NSDictionary*)value onlyNotEmpty:(BOOL)notEmpty
{
    if(notEmpty && !value)
        return;
    self[key] = value;
}

-(void)saveBoolProperties:(NSString*)key value:(NSNumber*)value onlyNotEmpty:(BOOL)notEmpty
{
    if(notEmpty && !value)
        return;
    self[key] = value;
}


@end
