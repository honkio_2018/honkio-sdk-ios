//
//  CalendarEventJsonParser.m
//  HonkioSDK
//
//  Created by Mikhail Li on 27/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "CalendarEventJsonParser.h"
#import "ApiDateFormatter.h"
#import "Product.h"

@implementation CalendarEventJsonParser

-(id)parseDictionary:(Message *)message dict:(NSDictionary *)dict {
    return [CalendarEventJsonParser parseCalendarEvent:dict];
}

+(CalendarEvent *)parseCalendarEvent:(NSDictionary *)dict {
    CalendarEvent *event = [CalendarEvent new];
    
    event.eventId = dict[@"id"];
    event.type = [dict objectForKey:@"type"] ? dict[@"type"] : EVENT_TYPE_TENTATIVE;
    event.start = [ApiDateFormatter dateFromServerString:dict[@"start"]];
    event.end = [ApiDateFormatter dateFromServerString:dict[@"end"]];
    event.initialStart = [ApiDateFormatter dateFromServerString:dict[@"initial_start"]];
    event.initialEnd = [ApiDateFormatter dateFromServerString:dict[@"initial_end"]];
    event.repeating = dict[@"repeat"];
    
    event.metaData = [NSMutableArray new];
    
    NSArray *productsJsonArray = dict[@"products"];
    if (productsJsonArray) {
        NSMutableArray<Product *> *products = [NSMutableArray arrayWithCapacity:productsJsonArray.count];
        for (NSDictionary *productJson in productsJsonArray) {
            if (productJson && [productJson isKindOfClass:[NSDictionary class]]) {
                Product *product = [[Product alloc] init:[productJson mutableCopy]];
                [BaseJsonResponseParser parseProduct:product dict:productJson];
                [products addObject:product];
            }
        }
        event.products = products;
    }
    
    return event;
}

@end
