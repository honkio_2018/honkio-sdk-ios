//
//  Extensions.swift
//  WorkPilots
//
//  Created by developer on 12/11/15.
//  Copyright © 2015 developer. All rights reserved.
//

/**
The extensions set within helpers and validators attached to classes or UI controls.
*/

import UIKit

/**
Extends the String class with the function for searaching match patterns in it by the provided pattern.
*/
extension String {
    /**
    - parameter pattern: The pattern to be used for search.
    - Returns: The list of matched strings.
    */
    public func matchesForRegexInText(_ pattern: String!) -> [String]? {
        
        do {
            
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            let result = regex.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            return result.map({ (self as NSString).substring(with: $0.range)})
        }
        catch let error as NSError {
            
            print(error.localizedDescription)
            return nil
        }
    }
}
/**
Extends the UIView control with the function for rounding view's corners.
*/
extension UIView {
    /**
    Rounds the view's corners by the processing current frame width and heught.
    */
    public func circle() {
        
        self.layer.cornerRadius = min(self.frame.width, self.frame.height) / 2
        self.clipsToBounds = true
    }
}

/**
Extends the UIButton control with the function for rounding button's corners.
*/
extension UIButton {
    /**
    WHen called rounds the button's corners by the corner raduis "5".
    */
    public func rounded() {
        
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
}
/**
Extends the UIImage control with the function for creating and image of the required size and color by provided color
and GCRect.
*/
extension UIImage {
    /**
    - parameter color: The color value the image will be creaeted in.
    - parameter rect: The rectangle describes the size of the required image.
    - Returns: The created image.
    */
    public static func imageFromColor(_ color: UIColor, rect: CGRect) -> UIImage {
        
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        context!.setFillColor(color.cgColor)
        context!.fill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
    
    public func fixRotation() -> UIImage {
        
        if self.imageOrientation == UIImageOrientation.up {
            return self;
        }
        var transform = CGAffineTransform.identity;
        
        switch (self.imageOrientation) {
        case UIImageOrientation.down, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: self.size.height);
            transform = transform.rotated(by: CGFloat.pi);
            break
            
        case UIImageOrientation.left, UIImageOrientation.leftMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0);
            transform = transform.rotated(by: CGFloat.pi / 2.0);
            break
            
        case UIImageOrientation.right, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: -CGFloat.pi / 2.0);
            break
            
        case UIImageOrientation.up, UIImageOrientation.upMirrored:
            break
        }
        
        switch (self.imageOrientation) {
        case UIImageOrientation.upMirrored, UIImageOrientation.downMirrored:
            transform = transform.translatedBy(x: self.size.width, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
            break
            
        case UIImageOrientation.leftMirrored, UIImageOrientation.rightMirrored:
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
            break
        case UIImageOrientation.up, UIImageOrientation.down, UIImageOrientation.left, UIImageOrientation.right:
            break
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height),
                            bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0,
                            space: self.cgImage!.colorSpace!,
                            bitmapInfo: self.cgImage!.bitmapInfo.rawValue);
        ctx!.concatenate(transform);
        
        switch (self.imageOrientation) {
        case UIImageOrientation.left, UIImageOrientation.leftMirrored, UIImageOrientation.right, UIImageOrientation.rightMirrored:
            // Grr...
            ctx!.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.height,height: self.size.width));
            break
            
        default:
            ctx!.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.width,height: self.size.height));
            break
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg = ctx!.makeImage();
        let img = UIImage(cgImage: cgimg!)
        //        CGContextRelease(ctx);
        //        CGImageRelease(cgimg);
        return img;
    }
}
/**
Extends the UIImageView with the function for downloading the image content from provided URL.
*/

let imageCache = NSCache<NSString, UIImage>()

extension UIImageView {
    /**
    Runs the task for downloading the image by URL and after the task is successfully completed sets the image content.
    * Thread safe.
    - parameter link: The URL the image will be downloaded from.
    - parameter content: The UIViewContentMode to be set to the UIImageView if provided.
    */
    public func imageFromURL(link:String!, errorImage: UIImage?, contentMode mode: UIViewContentMode?, isCache cache: Bool = true) {
        self.image = errorImage
        self.imageFromURL(link: link, contentMode: contentMode, isCache: cache) {
            self.image = errorImage
        }
    }
    
    public func imageFromURL(link:String!, contentMode mode: UIViewContentMode?, isCache cache: Bool = true, fallAction:@escaping (() -> Void)) {
        guard let url = URL(string: link) else {
            DispatchQueue.main.async(execute: fallAction)
            return
        }
        
        if cache {
            if let cachedImage = imageCache.object(forKey: link as NSString) {
                self.image = cachedImage
                return
            }
        }
        
        if mode != nil {
            contentMode = mode!
        }
        
        self.image = nil
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            
            guard
                
                let httpURLResponse = response as? HTTPURLResponse , httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType , mimeType.hasPrefix("image"),
                let data = data , error == nil,
                let image = UIImage(data: data)
                
                else {
                    
                    DispatchQueue.main.async(execute: fallAction)
                    return
            }
            
            DispatchQueue.main.async {
                imageCache.setObject(image, forKey: link as NSString)
                self.image = image
            }
            
        }).resume()
    }
}
/**
Extends the NSTimer with the functons for creating specially configured instances.
*/
extension Timer {
    /**
     Creates and schedules a one-time `NSTimer` instance.
     
     - Parameters:
     - delay: The delay before execution.
     - handler: A closure to execute after `delay`.
     
     - Returns: The newly-created `NSTimer` instance.
     */
    
    public class func schedule(delay: TimeInterval, handler: @escaping (CFRunLoopTimer?) -> Void) -> CFRunLoopTimer? {
        
        let fireDate = delay + CFAbsoluteTimeGetCurrent()
        let timer = CFRunLoopTimerCreateWithHandler(kCFAllocatorDefault, fireDate, 0, 0, 0, handler)
        CFRunLoopAddTimer(CFRunLoopGetCurrent(), timer, CFRunLoopMode.commonModes)
        
        return timer
    }
    
    /**
     Creates and schedules a repeating `NSTimer` instance.
     
     - Parameters:
     - repeatInterval: The interval (in seconds) between each execution of
     `handler`. Note that individual calls may be delayed; subsequent calls
     to `handler` will be based on the time the timer was created.
     - handler: A closure to execute at each `repeatInterval`.
     
     - Returns: The newly-created `NSTimer` instance.
     */
    
    public class func schedule(repeatInterval interval: TimeInterval, handler: @escaping (CFRunLoopTimer?) -> Void) -> CFRunLoopTimer? {
        
        let fireDate = interval + CFAbsoluteTimeGetCurrent()
        let timer = CFRunLoopTimerCreateWithHandler(kCFAllocatorDefault, fireDate, interval, 0, 0, handler)
        CFRunLoopAddTimer(CFRunLoopGetCurrent(), timer, CFRunLoopMode.commonModes)
        
        return timer
    }
}
/**
The ENUM within the available iOS device models.
*/
public enum Model : String {
    
    case simulator = "simulator/sandbox",
    iPod1          = "iPod 1",
    iPod2          = "iPod 2",
    iPod3          = "iPod 3",
    iPod4          = "iPod 4",
    iPod5          = "iPod 5",
    iPod6          = "iPod 6",
    iPad2          = "iPad 2",
    iPad3          = "iPad 3",
    iPad4          = "iPad 4",
    iPadPro        = "iPad Pro",
    iPhone4        = "iPhone 4",
    iPhone4S       = "iPhone 4S",
    iPhone5        = "iPhone 5",
    iPhone5S       = "iPhone 5S",
    iPhone5C       = "iPhone 5C",
    iPadMini1      = "iPad Mini 1",
    iPadMini2      = "iPad Mini 2",
    iPadMini3      = "iPad Mini 3",
    iPadMini4      = "iPad Mini 4",
    iPadAir1       = "iPad Air 1",
    iPadAir2       = "iPad Air 2",
    iPhone6        = "iPhone 6",
    iPhone6plus    = "iPhone 6 Plus",
    iPhone6S       = "iPhone 6S",
    iPhone6Splus   = "iPhone 6S Plus",
    iPhone7        = "iPhone 7",
    iPhone7plus    = "iPhone 7 Plus",
    unrecognized   = "?unrecognized?"
}

/**
The ENUM within the available iOS simulator models.
*/
public enum SimulatorModel: String {
    
    case iPad2     = "iPad 2",
    iPad3          = "iPad 3",
    iPad4          = "iPad 4",
    iPadPro        = "iPad Pro",
    iPhone4S       = "iPhone 4S",
    iPhone5        = "iPhone 5",
    iPhone5S       = "iPhone 5S",
    iPadAir1       = "iPad Air 1",
    iPadAir2       = "iPad Air 2",
    iPhone6        = "iPhone 6",
    iPhone6plus    = "iPhone 6 Plus",
    iPhone6S       = "iPhone 6S",
    iPhone6Splus   = "iPhone 6S Plus",
    iPhone7        = "iPhone 7",
    iPhone7plus    = "iPhone 7 Plus",
    unrecognized   = "?unrecognized?"

}
/**
Extends the UIDevice class with the properties for evaluating the device type and model.
*/
extension UIDevice {
    /**
    Contains the value representing whether the UIDevice is a simulator.
    */
    public var isSimulator: Bool {
        var systemInfo = utsname()
        uname(&systemInfo)
        
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        
        let identifier = machineMirror.children.reduce("") { identifier, element in
            
            guard let value = element.value as? Int8 , value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        return identifier == "i386" || identifier == "x86_64"
    }
    /**
    Contains the value representing the model type of UIDevice.
    */
    public var type: Model {
        
        var systemInfo = utsname()
        uname(&systemInfo)
        
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        
        var identifier : NSString!
        if isSimulator {
            identifier = ProcessInfo.processInfo.environment["SIMULATOR_MODEL_IDENTIFIER"] as NSString?
        }
        else {
            identifier = machineMirror.children.reduce("") { identifier, element in
                
                guard let value = element.value as? Int8 , value != 0 else { return identifier }
                let result = (identifier! as String) + String(UnicodeScalar(UInt8(value)))
                
                return result as NSString
            }
        }
        
        switch identifier {
            
        case "iPod5,1":
            return .iPod5
            
        case "iPod7,1":
            return .iPod6
            
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":
            return .iPhone4
            
        case "iPhone4,1":
            return .iPhone4S
            
        case "iPhone5,1", "iPhone5,2":
            return .iPhone5
            
        case "iPhone5,3", "iPhone5,4":
            return .iPhone5C
            
        case "iPhone6,1", "iPhone6,2":
            return .iPhone5S
            
        case "iPhone7,2":
            return .iPhone6
            
        case "iPhone7,1":
            return .iPhone6plus
            
        case "iPhone8,1":
            return .iPhone6S
            
        case "iPhone8,2":
            return .iPhone6Splus
            
        case "iPhone9,1":
            return .iPhone7
            
        case "iPhone9,2":
            return .iPhone7plus
            
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":
            return .iPad2
            
        case "iPad3,1", "iPad3,2", "iPad3,3":
            return .iPad3
            
        case "iPad3,4", "iPad3,5", "iPad3,6":
            return .iPad4
            
        case "iPad4,1", "iPad4,2", "iPad4,3":
            return .iPadAir1
            
        case "iPad5,3", "iPad5,4":
            return .iPadAir2
            
        case "iPad2,5", "iPad2,6", "iPad2,7":
            return .iPadMini1
            
        case "iPad4,4", "iPad4,5", "iPad4,6":
            return .iPadMini2
            
        case "iPad4,7", "iPad4,8", "iPad4,9":
            return .iPadMini3
            
        case "iPad5,1", "iPad5,2":
            return .iPadMini4
            
        case "iPad6,7", "iPad6,8":
            return .iPadPro
            
        default:
            return .unrecognized
        }
    }
}


extension NSObject {
    
    public func synchronized(_ closure: () -> Void) {
        
        objc_sync_enter(self)
        
        defer {
            
            objc_sync_exit(self)
        }
        
        closure()
    }
}

/**
Extenda UserAccount class with the property returning the credit card number.
*/
extension UserAccount {
    /// Contains the credit card number for an account.
    public var creditCardNumber: String? {
        if let number = self.accountDescription?.matchesForRegexInText("[\\d*]+") {
            if number.count > 0 {
                return number[0]
            }
        }
        return nil
    }
}
