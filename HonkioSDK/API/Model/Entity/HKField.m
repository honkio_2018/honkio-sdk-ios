//
//  HKField.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/13/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "HKField.h"

@implementation HKField

- (instancetype)init
{
    self = [super init];
    if (self) {
        _isVisible = YES;
    }
    return self;
}

@end
