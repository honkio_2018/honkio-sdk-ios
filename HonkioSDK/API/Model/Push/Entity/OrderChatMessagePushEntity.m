//
//  OrderChatMessagePushEntity.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/19/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "OrderChatMessagePushEntity.h"
#import "Push.h"

@implementation OrderChatMessagePushEntity

-(id)initWithContent:(NSDictionary*)content {
    return [super initWithType:PUSH_TYPE_ORDER_CHAT_MESSAGE andContent:content];
}

-(NSString*) value1 {
    return self.orderId;
}

-(NSString*) value2 {
    return nil;
}

@end
