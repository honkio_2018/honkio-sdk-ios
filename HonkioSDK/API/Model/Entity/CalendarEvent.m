//
//  CalendarEvent.m
//  HonkioSDK
//
//  Created by Mikhail Li on 26/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "CalendarEvent.h"
#import "AbsFilter.h"
#import "ApiDateFormatter.h"

@implementation CalendarEvent

@synthesize eventId, initialStart, initialEnd, repeating;

- (instancetype)initWithEvent:(CalendarEvent *)event
{
    self = [super init];
    if (self) {
        self.eventId = event.eventId;
        self.initialStart = event.initialStart;
        self.initialEnd = event.initialEnd;
        self.repeating = event.repeating;
        if (event.products)
            self.products = [[NSArray alloc] initWithArray:event.products copyItems:YES];
    }
    return self;
}

-(NSDictionary *)toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [AbsFilter applyIfNotNil:@"id" value:self.eventId forParams:params];
    [AbsFilter applyIfNotNil:@"type" value:self.type forParams:params];
    [AbsFilter applyIfNotNil:@"repeat" value:self.repeating forParams:params];
    
    if (self.initialStart) {
        NSString *startDate = [ApiDateFormatter stringFromDate:self.initialStart];
        [AbsFilter applyIfNotNil:@"start" value:startDate forParams:params];
    }
    
    if (self.initialEnd) {
        NSString *endDate = [ApiDateFormatter stringFromDate:self.initialEnd];
        [AbsFilter applyIfNotNil:@"end" value:endDate forParams:params];
    }
    
    if (self.products) {
        NSMutableArray *productIds = [NSMutableArray arrayWithCapacity:self.products.count];
        for (Product *product in self.products) {
            [productIds addObject:product.productId];
        }
        [AbsFilter applyIfNotNil:@"products" value:productIds forParams:params];
    }

    return params;
}

-(NSString *)description {
    NSString *startDate = [ApiDateFormatter stringFromDate:self.start
                                                 dateStyle:NSDateFormatterShortStyle
                                                 timeStyle:NSDateFormatterShortStyle];
    NSString *endDate = [ApiDateFormatter stringFromDate:self.end
                                               dateStyle:NSDateFormatterShortStyle
                                               timeStyle:NSDateFormatterShortStyle];
    NSString *initialStartDate = [ApiDateFormatter stringFromDate:self.start
                                                 dateStyle:NSDateFormatterShortStyle
                                                 timeStyle:NSDateFormatterShortStyle];
    NSString *initialEndDate = [ApiDateFormatter stringFromDate:self.end
                                               dateStyle:NSDateFormatterShortStyle
                                               timeStyle:NSDateFormatterShortStyle];
    return [NSString stringWithFormat:@"Event: start=%@, end=%@, initial start=%@, initial end=%@, repeat %@, type=%@",
        startDate, endDate, initialStartDate, initialEndDate, self.repeating, self.type];
}
@end
