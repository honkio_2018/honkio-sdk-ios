//
//  NSLayoutConstraint.swift
//  Doctor-ios
//
//  Created by Alexander Borovikov on 20.06.16.
//  Copyright © 2016 Alexander Borovikov. All rights reserved.
//

import Foundation
import UIKit
/**
The extensions set for NSLayoutConstraint within helpers for constraint creating.
*/
extension NSLayoutConstraint {
    /**
    - Returns: The new array of constraints (has been built based on ASCII art-like visual format string).
    - parameter formatArray: The array of strings containing the set of visual formats.
    - parameter options: The array of NSLayoutFormatOptions.
    - parameter metrics: The array of metrics to be applied to the newly created constraints.
    - parameter views: The array of objects the constraints will be applied on.
    */
    public class func constraintsWithFormatArray(_ formatArray: [String], options: NSLayoutFormatOptions,
                                                 metrics: [String : AnyObject]?,
                                                 views: [String : AnyObject]) -> [NSLayoutConstraint] {
        
        var constraints: [NSLayoutConstraint] = []
        
        for format : String in formatArray {
            constraints += NSLayoutConstraint.constraints(withVisualFormat: format, options: options,
                                                                          metrics: metrics,
                                                                          views: views)
        }
        
        return constraints;
    }

    /**
    Create constraints explicitly. Constraints are of the form "view1.attr1 = view2.attr2 * multiplier + constant"
    If your equation does not have a second view and attribute, use nil and NSLayoutAttributeNotAnAttribute.
    * attr1 and attr2 will be defined as .CenterX
    - Returns: The new constraint.
    - parameter view1: The objects view1.
    - parameter view2: The objects view2.
    - parameter multiplier: The multiplier.
    - parameter constant: The constant.
    */
    public class func constraintsCenterX(_ view1: AnyObject, toItem view2: AnyObject?,
                                         multiplier: CGFloat, constant: CGFloat) -> NSLayoutConstraint {
        
        let constraint = NSLayoutConstraint(
            item: view1,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: view2,
            attribute: .centerX,
            multiplier: multiplier,
            constant: constant)
        
        return constraint
    }
    /**
    Create constraints explicitly. Constraints are of the form "view1.attr1 = view2.attr2 * multiplier + constant"
    If your equation does not have a second view and attribute, use nil and NSLayoutAttributeNotAnAttribute.
    * attr1 and attr2 will be defined as .CenterY
    - Returns: The new constraint.
    - parameter view1: The objects view1.
    - parameter view2: The objects view2.
    - parameter multiplier: The multiplier.
    - parameter constant: The constant.
    */
    public class func constraintsCenterY(_ view1: AnyObject, toItem view2: AnyObject?,
                                         multiplier: CGFloat, constant: CGFloat) -> NSLayoutConstraint {
        
        let constraint = NSLayoutConstraint(
            item: view1,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: view2,
            attribute: .centerY,
            multiplier: multiplier,
            constant: constant)
        
        return constraint
    }
}
