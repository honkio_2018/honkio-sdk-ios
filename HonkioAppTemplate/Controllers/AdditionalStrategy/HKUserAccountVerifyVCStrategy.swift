//
//  HKUserAccountVerifyVCStrategy.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 7/11/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

open class HKUserAccountVerifyVCStrategy: HKViewControllerLifeCycleObserver, UIWebViewDelegate {
    
    open var delegate: HKUserAccountVerifyVCStrategyDelegate?
    open var removeOnCompletion : Bool = false
    
    private var accountCreator: AccountCreationProcessModel?

    public func handle(_ response: Response) -> Bool {
        if self.isHasCreditCardVerifyPendings(response) {
            
            if response.url == nil {
                return false
            }
            
            if let webView = AppController.getViewControllerById(AppController.WebView, viewController: self.viewController) as? HKWebPageViewDelegate {
                
                // Account creation will not startted by calling start method so no need to provide correct account and shop
                accountCreator = AccountCreationProcessModel(account: nil, shop: Shop(from: response.message.shopIdentity), handler: {[weak self] (response) in
                    if self?.isHasCreditCardVerifyPendings(response) ?? false {
                        // Do nothing
                    }
                    else if response.isStatusAccept() || response.isStatusPending() {
                        // show a good message
                        self?.accountCreator = nil
                        self?.notifyCompletion(response)
                    }
                    else {
                        self?.notifyError(response.anyError())
                    }
                    
                })
                // Manualy move model to status pending because current transaction is already on this status
                accountCreator?.onPending(response)
                
                webView.webViewDelegate = self
                webView.showUrl(string: response.url)
                
                if let lcoParent = webView as? HKLifeCycleObserverParentViewController {
                    let cancelAction = HKCancelActionVCStrategy(parent: lcoParent)
                    cancelAction.afterCancelAction = {[unowned self] (strategy) in
                        self.accountCreator?.abort()
                        self.notifyError(ApiError(code: ErrApi.canceledByUser.rawValue))
                    }
                }
                
                let navi = UINavigationController(rootViewController: webView as! UIViewController)
                self.viewController.present(navi, animated: true, completion: nil)
                
                return true
            }
            
        }
        return false
    }

    open func webViewDidFinishLoad(_ webView: UIWebView) {
        self.accountCreator?.checkStatus()
    }
    
    private func notifyCompletion(_ response: Response) {
        self.getDelegate()?.userAccountVerifyComplete(response)
        self.viewController.dismiss(animated: true, completion: nil)
        self.removeFromParentIfNeeded()
    }
    
    private func notifyError(_ error: ApiError) {
        self.getDelegate()?.userAccountVerifyError(error)
        self.viewController.dismiss(animated: true, completion: nil)
        self.removeFromParentIfNeeded()
    }
    
    private func getDelegate() -> HKUserAccountVerifyVCStrategyDelegate? {
        var strategyDelegate = delegate
        
        if strategyDelegate == nil {
            strategyDelegate = self.parent as? HKUserAccountVerifyVCStrategyDelegate
        }
        
        return strategyDelegate
    }
    
    private func isHasCreditCardVerifyPendings(_ response: Response) -> Bool {
        return response.isStatusPending() && (response.hasPending("creditcardverify") || response.hasPending("seitatecverify"))
    }
    
    private func removeFromParentIfNeeded() {
        if self.removeOnCompletion {
            self.parent?.removeLifeCycleObserver(self)
            self.delegate = nil
        }
    }
}
