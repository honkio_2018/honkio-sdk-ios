//
//  HKTouInfo.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/31/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TOU_KEY_DEFAULT @"default"

@interface HKTouInfo : NSObject

@property (nonatomic, readonly, nonnull) NSString* key;
@property (nonatomic, readonly, nonnull) NSString* url;
@property (nonatomic, readonly) int version;

-(id)initWithKey:(nonnull NSString*) key url:(nonnull NSString*) url version:(int)version;
-(id)initWithUrl:(nonnull NSString*) url version:(int)version;

@end
