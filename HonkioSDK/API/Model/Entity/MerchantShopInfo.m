//
//  MerchantShopInfo.m
//  HonkioSDK
//
//  Created by Mikhail Li on 21/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "MerchantShopInfo.h"
#import "MerchantShop.h"

static const long long serialVersionUID = -1332023438543275349L;

@interface MerchantShopInfo ()

@end

@implementation MerchantShopInfo

- (BOOL)isEqual:(id)object {
    if (self == object)
        return YES;
    
    if (!object || ![object isKindOfClass:[self class]])
        return NO;
    
    MerchantShopInfo *info = object;
    return info.timeStamp == _timeStamp && ((_shop != nil && [_shop isEqual:info.shop]) ||
        (_shop == nil && info.shop == nil));
}

- (NSUInteger)hash
{
    NSUInteger result = 1;
    NSUInteger prime = 31;
    
    result = prime * result + [[NSNumber numberWithDouble:_timeStamp] hash];
    result = prime * result + [_shop hash];
    
    return result;
}

@end
