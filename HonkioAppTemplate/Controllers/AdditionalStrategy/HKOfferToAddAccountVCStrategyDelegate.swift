//
//  HKOfferToAddAccountVCStrategyDelegate.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/12/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

// TODO docs
public protocol HKOfferToAddAccountVCStrategyDelegate {
    
    func paymentAccountDidAdded(_ controller: HKOfferToAddAccountVCStrategy)
    func paymentAccountNotAdded(_ controller: HKOfferToAddAccountVCStrategy)
    
}
