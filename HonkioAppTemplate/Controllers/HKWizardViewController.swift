//
//  HKWizardViewController.swift
//  WorkPilots
//
//  Created by developer on 12/14/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

//TODO: docs
open class HKWizardViewController: HKBaseWizardViewController {
    
    /**
     Sets the text field look in editing state. Any error notofications for invalid values
     are ignored in editing state.
     - parameter textField: The UITextField which is in editing state.
     */
    override open func textFieldDidBeginEditing(_ textField: UITextField) {
        super.textFieldDidBeginEditing(textField)
        textField.disableError()
    }
}
