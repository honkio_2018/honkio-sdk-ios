//
//  HKOrderViewProtocol.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/8/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

public protocol HKOrderViewProtocol: class {
    
    var order: Order! { get set }
    
}
