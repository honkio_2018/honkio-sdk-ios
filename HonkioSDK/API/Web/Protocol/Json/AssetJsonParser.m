//
//  AssetJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "AssetJsonParser.h"
#import "StructureJsonParser.h"

@implementation AssetJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [AssetJsonParser parseAsset:dict];
}

+(HKAsset*)parseAsset:(NSDictionary*)assetDict {
    if (assetDict) {
        HKAsset* asset = [HKAsset new];
        asset.assetId = assetDict[@"id"];
        asset.name = assetDict[@"name"];
        asset.visible = [assetDict[@"visible"] boolValue];
        asset.merchantId = assetDict[@"merchant"];
        asset.group = assetDict[@"group"];
        asset.latitude = [assetDict[@"latitude"] doubleValue];
        asset.longitude = [assetDict[@"longitude"] doubleValue];
        asset.properties = assetDict[@"properties"];
        asset.stricture = [StructureJsonParser toStructure:assetDict[@"structure"]];
        asset.hasValidSchedule = [assetDict[@"has_valid_schedule"] boolValue];
        return asset;
    }
    return nil;
}

@end
