//
//  CustomInputAlertView.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/19/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

/// The class represents alert manager to simplify usage of custom alerts.

open class CustomInputAlertView: NSObject, UIAlertViewDelegate  {
    
    var alerts = [CustomInputAlertView]()
    
    var title : String?
    var message: String
    var input: String?
    var hint: String?
    
    var okAction : ((_ input: String) -> Void)?
    var cancelAction : (() -> Void)?
    
    /**
     Constructs the instance within the provided title, message and action.
     The text "dlg_common_button_ok".localized will be used for button.
     - parameter title: The title of the alert view.
     - parameter okMessage: The the text of the alert message.
     - parameter okAction: The action to be run when button pressed.
     */
    public init(title: String?, message: String, action: ((_ input: String) -> Void)?) {
        
        self.title = title
        self.message = message
        self.okAction = action
        
        super.init()
    }
    
    open func input(_ input: String?) -> CustomInputAlertView {
        self.input = input
        return self
    }
    
    open func hint(_ hint: String?) -> CustomInputAlertView {
        self.hint = hint
        return self
    }
    
    open func cancelAction(_ action: (() -> Void)?) -> CustomInputAlertView {
        self.cancelAction = action
        return self
    }
    
    open func show() {
        
        let alertView = UIAlertView(title: self.title, message: self.message, delegate: self, cancelButtonTitle: "dlg_common_button_ok".localized)

        alertView.alertViewStyle = .plainTextInput
        alertView.addButton(withTitle: "dlg_common_button_cancel".localized)
        alertView.textField(at: 0)?.text = self.input
        alertView.textField(at: 0)?.placeholder = self.hint

        alerts.append(self)
        alertView.show()
    }
    
    /**
     UIAlertViewDelegate
     Will be called when user clicks the button on alert view.
     Action will be performed if it was attached to a clicked button.
     - parameter alertView: The UIAlertView object the button has been pressed on.
     - parameter buttonIndex: The index of the pressed button, which is used for finding and running corresponding action.
     */
    open func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        
        if buttonIndex == 0 {
            
            okAction?((alertView.textField(at: 0)?.text)!)
            return
        }
        else {
            cancelAction?()
        }
        
        // remove from holding array
        if let index = alerts.index(of: self) {
            
            alerts.remove(at: index)
        }
    }
}
