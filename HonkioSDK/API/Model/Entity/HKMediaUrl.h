//
//  HKMediaUrl.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HKMediaUrl : NSObject

@property (nonatomic) NSString *objectId;
@property (nonatomic) NSURL *uploadUrl;
@property (nonatomic) NSString *uploadToken;

@end
