//
//  OrderController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/28/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
* OrderLoaderVCStrategy the class which processes the loading of order, provided by the it's identificator.
*/

open class HKOrderLoaderVCStrategy: HKViewControllerLifeCycleObserver {

/**
* The instance of loaded Order.
*/
    open var order : Order! {
        didSet {
            if order != nil && order.orderId == self.orderIdToLoad {
                self.orderIdToLoad = nil
            }
        }
    }

    /**
     Represents the instance to be called when status of loading process has changed.<br>
     If delegate is nil the parrent view controller will be used as delegate if it response for delegate protocol.
    */
    open var delegate : HKOrderLoaderVCStrategyDelegate? {
        set {
            _delegate = newValue
        }
        
        get {
            if _delegate == nil {
                return self.parent as? HKOrderLoaderVCStrategyDelegate
            }
            return _delegate
        }
    }
    fileprivate var _delegate : HKOrderLoaderVCStrategyDelegate?
    
    fileprivate var orderIdToLoad : String?
    
    /**
    * Load order by it's ID. If parent ViewController is not visible the loading will start when view will appear.
    */
    open func loadOrderWhenViewDidApear(_ orderId: String) {
        self.orderIdToLoad = orderId;
        self.loadOrderIfNeeded()
    }
    
    /**
     * Load order by it's ID. Loading start immidiatly even loader is not attached to ViewController.
     * There can be a reason why orderWillLoad wasn't delivered to delegate.
     */
    open func loadOrder(_ orderId: String) {
        self.orderIdToLoad = nil
        self.orderWillLoad()
        
        let filter = OrderFilter()
        filter.queryId = orderId;
        filter.queryProductDetails = true
        
        weak var weakSelf = self
        HonkioApi.userGetOrders(filter, flags: 0, handler: { (response) in
            if let strongSelf = weakSelf {
                if response.isStatusAccept() {
                    let ordersList = response.result as! OrdersList
                    if ordersList.list.count > 0 {
                        strongSelf.orderDidLoad(ordersList.list[0])
                    }
                    else {
                        strongSelf.onOrderLoadingError(ApiError.unknown())
                    }
                }
                else {
                    strongSelf.onOrderLoadingError(response.anyError())
                }
            }
        })
    }

    /**
     * Runs when containing view got viewDidAppear.
     */
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadOrderIfNeeded()
    }
    
    func orderWillLoad() {
        if self.delegate != nil {
            self.delegate?.orderWillLoad(self)
        }
    }

    func orderDidLoad(_ order : Order) {
        self.order = order
        if self.delegate != nil {
            self.delegate?.orderDidLoad(self, order: order)
        }
    }
    
    func onOrderLoadingError(_ error: ApiError) {
        if self.delegate != nil {
            self.delegate?.onOrderLoadingError(self, error: error)
        }
    }
    
    fileprivate func loadOrderIfNeeded() {
        if self.orderIdToLoad != nil {
            if self.order != nil && self.order!.orderId == self.orderIdToLoad {
                self.orderIdToLoad = nil
            }
            else if self.isViewControllerVisible() {
                loadOrder(self.orderIdToLoad!)
            }
        }
    }
    
}
