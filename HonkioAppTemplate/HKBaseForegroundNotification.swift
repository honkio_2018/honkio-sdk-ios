//
//  BusyIndicator.swift
//  WorkPilots
//
//  Created by developer on 12/15/15.
//  Copyright © 2015 developer. All rights reserved.
//
/**
Describes the set of functions to be implemented in a class that implements the HKBaseForegroundNotificationDelegate.
- SeeAlso: HKBaseAppDelegate.
*/
@objc public protocol HKBaseForegroundNotificationDelegate: class, UIApplicationDelegate {
    
    @objc optional func foregroundRemoteNotificationWasTouched(_ userInfo: [AnyHashable: Any])
    @objc optional func foregroundLocalNotificationWasTouched(_ localNotifcation: UILocalNotification)
}
/**
Describes the set of available pan statuses.
*/
enum BSPanStatus {
    
    case up
    case down
    case pull
    case none
}

import UIKit
import AVFoundation
/**
The base class for displaying notifications to a user.
*/
open class HKBaseForegroundNotification: UIView, UITextViewDelegate {
    
    public static var systemSoundID: SystemSoundID = 1004
    open weak var delegate: HKBaseForegroundNotificationDelegate?
    open var timeToDismissNotification = 4
    
    static var pendingForegroundNotifications = [HKBaseForegroundNotification]()
    
    fileprivate let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    fileprivate let vibrancyEffectView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: UIBlurEffect(style: .dark)))
    
    fileprivate let appIconImageView = UIImageView(image: UIImage(named: "AppIcon40x40"))
    
    fileprivate let titleLabel = UILabel()
    fileprivate let subtitleLabel = UILabel()
    fileprivate let leftActionButton = UIButton()
    fileprivate let rightActionButton = UIButton()
    fileprivate let sendButton = UIButton()
    fileprivate let textView = UITextView()
    fileprivate let separatorView = UIView()
    
    fileprivate var userInfo: [AnyHashable: Any]?
    fileprivate var localNotification: UILocalNotification?
    
    fileprivate var categoryIdentifier: String?
    fileprivate var soundName: String?
    
    fileprivate var tapGestureRecognizer: UITapGestureRecognizer!
    fileprivate var panGestureRecognizer: UIPanGestureRecognizer!
    
    fileprivate var leftUserNotificationAction: UIUserNotificationAction?
    fileprivate var rightUserNotificationAction: UIUserNotificationAction?
    fileprivate var currentUserNotificationTextFieldAction: UIUserNotificationAction?
    
    fileprivate var initialPanLocation = CGPoint.zero
    fileprivate var previousPanStatus = BSPanStatus.none
    fileprivate var extendingIsFinished = false
    
    fileprivate var topConstraintNotification: NSLayoutConstraint!
    fileprivate var heightConstraintNotification: NSLayoutConstraint!
    fileprivate var heightConstraintTextView: NSLayoutConstraint?
    
    fileprivate var timerToDismissNotification: Timer?
    
    fileprivate var maxHeightOfNotification: CGFloat {
        
        get {
            return heightForText(subtitleLabel.text ?? "", width: subtitleLabel.frame.size.width) + 65 + (heightConstraintTextView?.constant ?? 0)
        }
    }
    
    fileprivate var shouldShowTextView: Bool {
        
        get {
            if #available(iOS 9.0, *) {
                return leftUserNotificationAction == nil && rightUserNotificationAction?.behavior == .textInput
            } else {
                return false
            }
        }
    }
    
    //MARK: - Class Methods
    
    //MARK: - Initialization
    /**
    Construct the instance within the provided user information items.
    - parameter userInfo: The list of information items to be displayed.
    */
    public convenience init(userInfo: [AnyHashable: Any]) {
        
        self.init(frame: CGRect.zero)
        
        self.userInfo = userInfo
        
        if let payload = userInfo["aps"] as? NSDictionary {
            
            if let alertTitle = payload["alert"] as? String {
                
                titleLabel.text = Bundle.main.infoDictionary?["CFBundleName"] as? String
                subtitleLabel.text = alertTitle
                
            } else {
                
                titleLabel.text = (payload["alert"] as? Dictionary)?["title"] ?? ""
                subtitleLabel.text = (payload["alert"] as? Dictionary)?["body"] ?? ""
            }
            
            categoryIdentifier = payload["category"] as? String
            soundName = payload["sound"] as? String
        }
    }
    /**
    Initialize the instance within the the provided UILocalNotification object.
    - parameter localNotification: The UILocalNotification object to be used for alerting user.
    */
    public convenience init(localNotification: UILocalNotification) {
        
        self.init(frame: CGRect.zero)
        
        self.localNotification = localNotification
        
        if #available(iOS 8.2, *) {
            
            titleLabel.text = localNotification.alertTitle ?? ""
            
        } else {
            
            titleLabel.text = ""
        }
        
        subtitleLabel.text = localNotification.alertBody ?? ""
        categoryIdentifier = localNotification.category
        soundName = localNotification.soundName
    }
    /**
    Instantiate the class within the list of provided data.
    - parameter titleLable: The string to be used as a title of the notification view.
    - parameter subtitleLable: The string to be used as a subtitle.
    - parameter categoryIedntifier: The string defines the category of the notification.
    - parameter soundName: The string name of sound to be played on notification.
    - parameter userInfo: The list of user information items to be used for notification.
    - parameter localNotification: The UILocalNotification to be displayed.
    */
    public convenience init(titleLabel: String?, subtitleLabel: String?, categoryIdentifier: String?, soundName: String?, userInfo: [AnyHashable: Any]?, localNotification: UILocalNotification?) {
        
        self.init(frame: CGRect.zero)
        
        self.titleLabel.text = titleLabel
        self.subtitleLabel.text = subtitleLabel
        self.categoryIdentifier = categoryIdentifier
        self.soundName = soundName
        
        self.userInfo = userInfo
        self.localNotification = localNotification
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        clipsToBounds = true
        translatesAutoresizingMaskIntoConstraints = false
        
        tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTapped))
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(viewPanned))
        
        addGestureRecognizer(tapGestureRecognizer)
        addGestureRecognizer(panGestureRecognizer)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - Deinitialization
    
    //MARK: - Actions
    
    //MARK: - Public
    /**
    Presents the current notification to a user.
    */
    open func presentNotification() {
        
        if let window = UIApplication.shared.keyWindow , !titleLabel.text!.isEmpty && !subtitleLabel.text!.isEmpty {
            
            window.windowLevel = UIWindowLevelStatusBar
            window.addSubview(self)
            window.bringSubview(toFront: self)
            
            topConstraintNotification = NSLayoutConstraint(item: self, attribute: .top, relatedBy: .equal, toItem: window, attribute: .top, multiplier: 1, constant: -80)
            heightConstraintNotification = NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 80)
            
            let leadingConstraint = NSLayoutConstraint(item: self, attribute: .leading, relatedBy: .equal, toItem: window, attribute: .leading, multiplier: 1, constant: 0)
            let trailingConstraint = NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: window, attribute: .trailing, multiplier: 1, constant: 0)
            
            window.addConstraints([topConstraintNotification, leadingConstraint, trailingConstraint])
            addConstraint(heightConstraintNotification)
            
            setupBlurEffectView()
            setupIconImageView()
            setupLabels()
            setupVibrancyEffectView()
            setupButtonsAndTextView()
            setupActions()
            
            HKBaseForegroundNotification.pendingForegroundNotifications.append(self)
            
            if HKBaseForegroundNotification.pendingForegroundNotifications.count == 1 {
                HKBaseForegroundNotification.pendingForegroundNotifications.first!.fire()
            }
        }
    }
    /**
    Hides the displaying notification.
    */
    @objc open func dismissView() {
        
        UIApplication.shared.delegate?.window??.windowLevel = UIWindowLevelNormal
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .beginFromCurrentState, animations: {
            
            self.topConstraintNotification.constant = -self.heightConstraintNotification.constant
            self.layoutIfNeeded()
            
            }, completion: { finished in
                
                self.removeFromSuperview()
                
                if !HKBaseForegroundNotification.pendingForegroundNotifications.isEmpty {
                    HKBaseForegroundNotification.pendingForegroundNotifications.removeFirst()
                }
        })
    }

    //MARK: - UITextViewDelegate
    /**
    On textViewDidChange changes the height of notification based on the text value of UITextView.
    - parameter textView: The UITextView the text value has been changed of.
    */
    open func textViewDidChange(_ textView: UITextView) {

        if textView.text.isEmpty {
            updateNotificationHeightWithNewTextViewHeight(30)
        } else {
            updateNotificationHeightWithNewTextViewHeight(max(heightForText(textView.text, width: textView.frame.size.width - 10), 30) + 10)
        }
    }

    //MARK: - Internal
    
    func fire() {
        
        timerToDismissNotification = Timer.scheduledTimer(timeInterval: TimeInterval(timeToDismissNotification), target: self, selector: #selector(dismissView), userInfo: nil, repeats: false)
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.topConstraintNotification.constant = 0
            self.layoutIfNeeded()
        }) 
        
        //FIXME need fix!!!
//        if let soundName = soundName {
//            
//            let componentsFromSoundName = soundName.componentsSeparatedByString(".")
//            
//            if let soundTitle = componentsFromSoundName.first, let soundExtension = componentsFromSoundName.last, let soundPath = NSBundle.mainBundle().pathForResource(soundTitle, ofType: soundExtension) {
//                
//                var soundID: SystemSoundID = 0
//                
//                AudioServicesCreateSystemSoundID(NSURL(fileURLWithPath: soundPath) , &soundID)
//                AudioServicesPlaySystemSound(soundID)
//                
//            } else {
//                AudioServicesPlaySystemSound(HKBaseForegroundNotification.systemSoundID)
//            }
//        }
        
    }
    
    @objc func viewTapped() {
        
        if let userInfo = userInfo {
            delegate?.foregroundRemoteNotificationWasTouched?(userInfo)
        } else if let localNotification = localNotification {
            delegate?.foregroundLocalNotificationWasTouched?(localNotification)
        }
        
        dismissView()
    }
    
    @objc func viewPanned() {
        
        timerToDismissNotification?.invalidate()
        timerToDismissNotification = nil
        
        let panLocation = panGestureRecognizer.location(in: superview)
        let velocity = panGestureRecognizer.velocity(in: self)
        
        switch panGestureRecognizer.state {
            
        case .began:
            
            initialPanLocation = panLocation
            
        case .changed:
            
            topConstraintNotification.constant =  min(-(initialPanLocation.y - panLocation.y), 0)
            
            previousPanStatus = velocity.y >= 0 ? .down : .up
            
            if panLocation.y >= frame.size.height - 20 && !extendingIsFinished {
                
                previousPanStatus = .pull
                heightConstraintNotification.constant = max(min(panLocation.y + 17, maxHeightOfNotification), 70)
                
                if maxHeightOfNotification - frame.size.height <= 20 {
                    
                    let alpha = (20 - (maxHeightOfNotification - frame.size.height))/20
                    
                    leftActionButton.alpha = alpha
                    rightActionButton.alpha = alpha
                    
                    if shouldShowTextView {
                        
                        textView.alpha = alpha
                        sendButton.alpha = alpha
                    }
                }
            }
            
            if maxHeightOfNotification == frame.size.height && !extendingIsFinished {
                
                extendingIsFinished = true
                
                leftActionButton.isEnabled = true
                rightActionButton.isEnabled = true
                sendButton.isEnabled = true
                textView.isEditable = true
                
                cancelPanGesture()
            }
            
        case .ended, .cancelled, .failed:
            
            if previousPanStatus == .up {
                
                dismissView()
                
            } else if previousPanStatus == .pull {
                
                presentView()
                
            } else {
                
                moveViewToTop()
            }
            
            previousPanStatus = .none
            initialPanLocation = CGPoint.zero
            
        default:
            ()
        }
    }
    
    @objc func leftActionButtonTapped(_ sender: UIButton) {
        
        var readyToDismiss = false
        
        tapGestureRecognizer.isEnabled = true
        panGestureRecognizer.isEnabled = true
        
        if #available(iOS 9.0, *) {
            
            if let behavior = leftUserNotificationAction?.behavior , behavior == .textInput {
                
                currentUserNotificationTextFieldAction = leftUserNotificationAction
                presentTextField()
                
            } else {
                
                readyToDismiss = true
            }
            
        } else {
            readyToDismiss = true
        }
        
        if readyToDismiss {
            
            if let userInfo = userInfo {
                
                delegate?.application?(UIApplication.shared, handleActionWithIdentifier: leftUserNotificationAction?.identifier ?? "", forRemoteNotification: userInfo, completionHandler: {})
                
            } else if let localNotification = localNotification {
                
                delegate?.application?(UIApplication.shared, handleActionWithIdentifier: leftUserNotificationAction?.identifier ?? "", for: localNotification, completionHandler: {})
            }
            
            dismissView()
        }
    }
    
    @objc func rightActionButtonTapped(_ sender: UIButton) {
        
        var readyToDismiss = false
        
        tapGestureRecognizer.isEnabled = true
        panGestureRecognizer.isEnabled = true
        
        if #available(iOS 9.0, *) {
            
            if let behavior = rightUserNotificationAction?.behavior , behavior == .textInput {
                
                currentUserNotificationTextFieldAction = rightUserNotificationAction
                presentTextField()
                
            } else {
                readyToDismiss = true
            }
            
        } else {
            readyToDismiss = true
        }
        
        if readyToDismiss {
            
            if let userInfo = userInfo {
                
                delegate?.application?(UIApplication.shared, handleActionWithIdentifier: rightUserNotificationAction?.identifier ?? "", forRemoteNotification: userInfo, completionHandler: {})
                
            } else if let localNotification = localNotification {
                
                delegate?.application?(UIApplication.shared, handleActionWithIdentifier: rightUserNotificationAction?.identifier ?? "", for: localNotification, completionHandler: {})
            }
            
            dismissView()
        }
    }
    
    @objc func actionButtonHighlighted(_ sender: UIButton) {
        
        tapGestureRecognizer.isEnabled = false
        panGestureRecognizer.isEnabled = false
        sender.backgroundColor = UIColor.white.withAlphaComponent(0.05)
    }
    
    @objc func actionButtonLeft(_ sender: UIButton) {
        
        tapGestureRecognizer.isEnabled = true
        panGestureRecognizer.isEnabled = true
        sender.backgroundColor = UIColor.white.withAlphaComponent(0.2)
    }
    
    @objc func sendButtonTapped(_ sender: UIButton) {
        
        if let userInfo = userInfo {
            
            if #available(iOS 9.0, *) {
                delegate?.application?(UIApplication.shared, handleActionWithIdentifier: currentUserNotificationTextFieldAction?.identifier ?? "", forRemoteNotification: userInfo, withResponseInfo: [UIUserNotificationActionResponseTypedTextKey: textView.text], completionHandler: {})
            }
            
        } else if let localNotification = localNotification {
            
            if #available(iOS 9.0, *) {
                delegate?.application?(UIApplication.shared, handleActionWithIdentifier: currentUserNotificationTextFieldAction?.identifier ?? "", for: localNotification, withResponseInfo: [UIUserNotificationActionResponseTypedTextKey: textView.text], completionHandler: {})
            }
        }
        
        dismissView()
    }
    
    //MARK: - Private
    
    fileprivate func setupBlurEffectView() {
        
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint = NSLayoutConstraint(item: blurEffectView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0)
        let bottomConstraint = NSLayoutConstraint(item: blurEffectView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraint = NSLayoutConstraint(item: blurEffectView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraint = NSLayoutConstraint(item: blurEffectView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        
        addSubview(blurEffectView)
        addConstraints([topConstraint, bottomConstraint, leadingConstraint, trailingConstraint])
        layoutIfNeeded()
    }
    
    fileprivate func setupIconImageView() {
        
        appIconImageView.translatesAutoresizingMaskIntoConstraints = false
        appIconImageView.contentMode = UIViewContentMode.scaleAspectFill
        appIconImageView.layer.cornerRadius = 5
        appIconImageView.clipsToBounds = true
        
        let constraintTop = NSLayoutConstraint(item: appIconImageView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 10)
        let constraintLeading = NSLayoutConstraint(item: appIconImageView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 10)
        let constraintWidth = NSLayoutConstraint(item: appIconImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 20)
        let constraintHeight = NSLayoutConstraint(item: appIconImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 20)
        
        appIconImageView.addConstraints([constraintWidth, constraintHeight])
        addSubview(appIconImageView)
        addConstraints([constraintTop, constraintLeading])
        layoutIfNeeded()
    }
    
    fileprivate func setupLabels() {
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont.boldSystemFont(ofSize: 14)
        titleLabel.textColor = UIColor.white
        titleLabel.sizeToFit()
        titleLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 756), for: .vertical)
        titleLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 256), for: .vertical)
        
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.font = UIFont.systemFont(ofSize: 14)
        subtitleLabel.textColor = UIColor.white
        subtitleLabel.numberOfLines = 0
        subtitleLabel.sizeToFit()
        subtitleLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 755), for: .vertical)
        subtitleLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 255), for: .vertical)
        
        let topConstraintTitleLabel = NSLayoutConstraint(item: titleLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 10)
        let leadingConstraintTitleLabel = NSLayoutConstraint(item: titleLabel, attribute: .leading, relatedBy: .equal, toItem: appIconImageView, attribute: .trailing, multiplier: 1, constant: 10)
        let trailingConstraintTitleLabel = NSLayoutConstraint(item: titleLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -10)
        
        let topConstraintSubtitleLabel = NSLayoutConstraint(item: subtitleLabel, attribute: .top, relatedBy: .equal, toItem: titleLabel, attribute: .bottom, multiplier: 1, constant: 2)
        let trailingConstraintSubtitleLabel = NSLayoutConstraint(item: subtitleLabel, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: -10)
        let leadingEdgesTitleAndSubtitleLabel = NSLayoutConstraint(item: subtitleLabel, attribute: .leading, relatedBy: .equal, toItem: titleLabel, attribute: .leading, multiplier: 1, constant: 0)
        
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addConstraints([topConstraintTitleLabel, leadingConstraintTitleLabel, trailingConstraintTitleLabel, topConstraintSubtitleLabel, trailingConstraintSubtitleLabel, leadingEdgesTitleAndSubtitleLabel])
        layoutIfNeeded()
    }
    
    fileprivate func setupVibrancyEffectView() {
        
        vibrancyEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraintVibrancyEffectView = NSLayoutConstraint(item: vibrancyEffectView, attribute: .top, relatedBy: .equal, toItem: subtitleLabel, attribute: .bottom, multiplier: 1, constant: 10)
        let bottomConstraintVibrancyEffectView = NSLayoutConstraint(item: vibrancyEffectView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0)
        let leadingConstraintVibrancyEffectView = NSLayoutConstraint(item: vibrancyEffectView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraintVibrancyEffectView = NSLayoutConstraint(item: vibrancyEffectView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0)
        let minHeightVibrancyEffectView = NSLayoutConstraint(item: vibrancyEffectView, attribute: .height, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .height, multiplier: 1, constant: 20)
        
        addSubview(vibrancyEffectView)
        vibrancyEffectView.addConstraint(minHeightVibrancyEffectView)
        addConstraints([topConstraintVibrancyEffectView, bottomConstraintVibrancyEffectView, leadingConstraintVibrancyEffectView, trailingConstraintVibrancyEffectView])
        
        separatorView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraintSeparatorView = NSLayoutConstraint(item: separatorView, attribute: .top, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .top, multiplier: 1, constant: 0)
        let heightConstraintSeparatorView = NSLayoutConstraint(item: separatorView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 1)
        let leadingConstraintSeparatorView = NSLayoutConstraint(item: separatorView, attribute: .leading, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .leading, multiplier: 1, constant: 0)
        let trailingConstraintSeparatorView = NSLayoutConstraint(item: separatorView, attribute: .trailing, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .trailing, multiplier: 1, constant: 0)
        
        vibrancyEffectView.contentView.addSubview(separatorView)
        separatorView.addConstraint(heightConstraintSeparatorView)
        vibrancyEffectView.addConstraints([topConstraintSeparatorView, leadingConstraintSeparatorView, trailingConstraintSeparatorView])
        
        let bottomHandle = UIView()
        bottomHandle.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        bottomHandle.translatesAutoresizingMaskIntoConstraints = false
        bottomHandle.clipsToBounds = true
        bottomHandle.layer.cornerRadius = 2
        
        let widthConstraintBottomHandle = NSLayoutConstraint(item: bottomHandle, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 50)
        let heightConstraintBottomHandle = NSLayoutConstraint(item: bottomHandle, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 4)
        let bottomConstraintBottomHandle = NSLayoutConstraint(item: bottomHandle, attribute: .bottom, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .bottom, multiplier: 1, constant: -2)
        let centerHorizontalConstraintBottomHandle = NSLayoutConstraint(item: bottomHandle, attribute: .centerX, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .centerX, multiplier: 1, constant: 0)
        
        vibrancyEffectView.contentView.addSubview(bottomHandle)
        bottomHandle.addConstraints([widthConstraintBottomHandle, heightConstraintBottomHandle])
        vibrancyEffectView.addConstraints([bottomConstraintBottomHandle, centerHorizontalConstraintBottomHandle])
        layoutIfNeeded()
    }
    
    fileprivate func setupButtonsAndTextView() {
        
        leftActionButton.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        rightActionButton.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        textView.backgroundColor = UIColor.white.withAlphaComponent(0.2)
        
        leftActionButton.layer.cornerRadius = 3
        leftActionButton.clipsToBounds = true
        leftActionButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        leftActionButton.addTarget(self, action: #selector(leftActionButtonTapped(_:)), for: .touchUpInside)
        leftActionButton.addTarget(self, action: #selector(actionButtonHighlighted(_:)), for: .touchDown)
        leftActionButton.addTarget(self, action: #selector(actionButtonLeft(_:)), for: .touchUpOutside)
        leftActionButton.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 757), for: .vertical)
        leftActionButton.alpha = 0
        leftActionButton.isEnabled = false
        
        rightActionButton.layer.cornerRadius = 3
        rightActionButton.clipsToBounds = true
        rightActionButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        rightActionButton.addTarget(self, action: #selector(rightActionButtonTapped(_:)), for: .touchUpInside)
        rightActionButton.addTarget(self, action: #selector(actionButtonHighlighted(_:)), for: .touchDown)
        rightActionButton.addTarget(self, action: #selector(actionButtonLeft(_:)), for: .touchUpOutside)
        rightActionButton.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 757), for: .vertical)
        rightActionButton.alpha = 0
        rightActionButton.isEnabled = false
        
        textView.layer.cornerRadius = 3
        textView.clipsToBounds = true
        textView.textColor = UIColor.white
        textView.alpha = 0
        textView.isEditable = false
        textView.delegate = self
        textView.font = UIFont.systemFont(ofSize: 14)
        
        sendButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        sendButton.alpha = 0
        sendButton.isEnabled = false
        sendButton.addTarget(self, action: #selector(sendButtonTapped(_:)), for: .touchUpInside)
        
        leftActionButton.translatesAutoresizingMaskIntoConstraints = false
        rightActionButton.translatesAutoresizingMaskIntoConstraints = false
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        textView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    fileprivate func setupActions() {
        
        if let actions = UIApplication.shared.currentUserNotificationSettings?.categories?.filter({ return $0.identifier == categoryIdentifier }).first?.actions(for: .default) , actions.count > 0 {
            
            rightUserNotificationAction = actions[0]
            leftUserNotificationAction = actions.count >= 2 ? actions[1] : nil
            
            leftActionButton.setTitle(leftUserNotificationAction?.title, for: UIControlState())
            rightActionButton.setTitle(rightUserNotificationAction?.title, for: UIControlState())
            sendButton.setTitle("Send", for: UIControlState())
            
            heightConstraintTextView = NSLayoutConstraint(item: textView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 30)
            let leadingConstraintTextView = NSLayoutConstraint(item: textView, attribute: .leading, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .leading, multiplier: 1, constant: 20)
            let topConstraintTextView = NSLayoutConstraint(item: textView, attribute: .top, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .top, multiplier: 1, constant: 10)
            
            let topConstraintSendButton = NSLayoutConstraint(item: sendButton, attribute: .top, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .top, multiplier: 1, constant: 0)
            let bottomConstraintSendButton = NSLayoutConstraint(item: sendButton, attribute: .bottom, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .bottom, multiplier: 1, constant: 0)
            let widthConstraintSendButton = NSLayoutConstraint(item: sendButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 80)
            let trailingConstraintSendButton = NSLayoutConstraint(item: sendButton, attribute: .trailing, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .trailing, multiplier: 1, constant: 0)
            let leadingConstraintSendButton = NSLayoutConstraint(item: sendButton, attribute: .leading, relatedBy: .equal, toItem: textView, attribute: .trailing, multiplier: 1, constant: 0)
            
            vibrancyEffectView.contentView.addSubview(textView)
            vibrancyEffectView.contentView.addSubview(sendButton)
            
            textView.addConstraint(heightConstraintTextView!)
            sendButton.addConstraints([widthConstraintSendButton])
            vibrancyEffectView.addConstraints([leadingConstraintTextView, topConstraintTextView, trailingConstraintSendButton, leadingConstraintSendButton, bottomConstraintSendButton, topConstraintSendButton])
            
            let topConstraintRightButton = NSLayoutConstraint(item: rightActionButton, attribute: .top, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .top, multiplier: 1, constant: 10)
            let trailingConstraintRightButton = NSLayoutConstraint(item: rightActionButton, attribute: .trailing, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .trailing, multiplier: 1, constant: -20)
            let heightConstraintRightButton = NSLayoutConstraint(item: rightActionButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 30)
            let leadingConstraintRightButton = NSLayoutConstraint(item: rightActionButton, attribute: .leading, relatedBy: .equal, toItem: leftUserNotificationAction != nil ? leftActionButton : vibrancyEffectView, attribute: leftUserNotificationAction != nil ? .trailing : .leading, multiplier: 1, constant: 20)
            
            if !shouldShowTextView {
                
                vibrancyEffectView.contentView.addSubview(rightActionButton)
                rightActionButton.addConstraint(heightConstraintRightButton)
            }
            
            if leftUserNotificationAction != nil {
                
                let equalWidthConstraintButtons = NSLayoutConstraint(item: rightActionButton, attribute: .width, relatedBy: .equal, toItem: leftActionButton, attribute: .width, multiplier: 1, constant: 0)
                let equalHeightsConstraintButton = NSLayoutConstraint(item: rightActionButton, attribute: .height, relatedBy: .equal, toItem: leftActionButton, attribute: .height, multiplier: 1, constant: 0)
                let topEdgesConstraintButtons = NSLayoutConstraint(item: rightActionButton, attribute: .top, relatedBy: .equal, toItem: leftActionButton, attribute: .top, multiplier: 1, constant: 0)
                let leadingConstraintLeftButton = NSLayoutConstraint(item: leftActionButton, attribute: .leading, relatedBy: .equal, toItem: vibrancyEffectView, attribute: .leading, multiplier: 1, constant: 20)
                
                vibrancyEffectView.contentView.addSubview(leftActionButton)
                vibrancyEffectView.addConstraints([equalWidthConstraintButtons, equalHeightsConstraintButton, topEdgesConstraintButtons, leadingConstraintLeftButton])
            }
            
            if !shouldShowTextView {
                
                vibrancyEffectView.addConstraints([topConstraintRightButton, trailingConstraintRightButton, leadingConstraintRightButton])
                layoutIfNeeded()
            }
            
            if shouldShowTextView {
                currentUserNotificationTextFieldAction = rightUserNotificationAction
            }
            
        } else {
            separatorView.alpha = 0
        }
    }
    
    fileprivate func cancelPanGesture() {
        
        panGestureRecognizer.isEnabled = false
        panGestureRecognizer.isEnabled = true
    }
    
    fileprivate func presentView() {
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .beginFromCurrentState, animations: {
            
            self.heightConstraintNotification.constant = self.maxHeightOfNotification
            self.rightActionButton.alpha = 1
            self.leftActionButton.alpha = 1
            self.layoutIfNeeded()
            
            if self.shouldShowTextView {
                self.presentTextField()
            }
            
            }, completion: nil)
    }
    
    fileprivate func moveViewToTop() {
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 1, options: .beginFromCurrentState, animations: {
            
            self.topConstraintNotification.constant = 0
            self.layoutIfNeeded()
            
            }, completion: nil)
    }
    
    fileprivate func heightForText(_ text: String, width: CGFloat) -> CGFloat {
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = UIFont.systemFont(ofSize: 14)
        label.text = text
        
        label.sizeToFit()
        
        return label.frame.height
    }
    
    fileprivate func presentTextField() {
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.textView.alpha = 1
            self.textView.isEditable = true
            self.sendButton.alpha = 1
            self.sendButton.isEnabled = true
            
            self.leftActionButton.alpha = 0
            self.rightActionButton.alpha = 0
            
        }, completion: { finished in
            
            self.textView.becomeFirstResponder()
            self.leftActionButton.removeFromSuperview()
            self.rightActionButton.removeFromSuperview()
        }) 
    }
    
    fileprivate func updateNotificationHeightWithNewTextViewHeight(_ height: CGFloat) {
        
        UIView.animate(withDuration: 0.4, animations: {
            
            self.heightConstraintTextView?.constant = height
            self.heightConstraintNotification.constant = self.maxHeightOfNotification
            self.layoutIfNeeded()
        }) 
    }
}
