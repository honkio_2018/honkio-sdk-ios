//
//  FeedBack.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 4/6/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsFilter.h"
#import "UserRole.h"

@interface UserComment : AbsFilter

@property (nonatomic) NSString* commentId;

@property (nonatomic) NSString* userId;
@property (nonatomic) NSString* userRoleId;

@property (nonatomic) UserRole* voter;

@property (nonatomic) NSString* orderId;
@property (nonatomic) NSString* tagId;

@property (nonatomic, nullable) NSString* text;
@property (nonatomic) NSInteger mark;

@property (nonatomic) NSDate* time;

@property (nonatomic, copy) NSString* object;
@property (nonatomic, copy) NSString* objectType;
@property (nonatomic, copy) NSString* shopId;

@end
