//
//  MediaUploadProcessModel.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "MediaUploadProcessModel.h"
#import "HonkioApi.h"
#import "HKMediaUrl.h"

@implementation MediaUploadProcessModel {
    NSString* objectId;
    NSString* objectType;
    
    MessageBinary* messageBinary;
}

-(id)initWithObject:(NSString*)object type:(NSString*)type filePath:(NSString*)path {
    return [self initWithObject:object type:type binary:[[MessageBinary alloc] initWithFile:path]];
}


-(id)initWithObject:(NSString*)object type:(NSString*)type data:(NSData*)data mimeType:(NSString*)mimeType {
    return [self initWithObject:object type:type binary:[[MessageBinary alloc] initWithData:data andMimeType:mimeType]];
}

-(id)initWithObject:(NSString*)object type:(NSString*)type binary:(MessageBinary*)binary {
    self = [super init];
    if (self) {
        objectId = object;
        objectType = type;
        messageBinary = binary;
    }
    return self;
}

- (void)start {
    [super start];
    
    [HonkioApi userGetMediaUrl:objectId type:objectType flags:0 handler:^(Response * _Nonnull res) {
        if ([res isStatusAccept]) {
            [self mediaDidCreated:(HKMediaUrl *)[res result]];
        }
        else {
            [self onError:res];
        }
    }];
}

-(void)mediaDidCreated:(HKMediaUrl*)url {
    if (![self isAborted]) {
        [HonkioApi userUpdateMedia:url data:self->messageBinary flags:0 handler:^(Response * _Nonnull res) {
            if ([res isStatusAccept]) {
                [self onComplete:res];
            }
            else {
                [self onError:res];
                [self clearMedia:url];
            }
        }];
    }
    else {
        [self clearMedia:url];
    }
}

-(void)clearMedia:(HKMediaUrl*)url {
    [HonkioApi userClearMediaUrl:url.objectId flags:MSG_FLAG_NO_WARNING handler:nil];
}

@end
