//
//  UserRolesListJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/26/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "UserRolesList.h"

@interface UserRolesListJsonParser : BaseJsonResponseParser

@end
