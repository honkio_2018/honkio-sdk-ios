//
//  WebServiceExecutorInstanceProtocol.h
//  HonkioSDK
//
//  Created by Dash on 30.04.2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "Message.h"
#import "Task.h"
#import "ApiResponseHandler.h"

@protocol WebServiceExecutorInstanceProtocol

-(id<Task>)build:(Message*)message handler:(ResponseHandler)responseHandler;

@end
