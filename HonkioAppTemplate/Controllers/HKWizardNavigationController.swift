//
//  HKWizardNavigationController.swift
//  WorkPilots
//
//  Created by developer on 1/6/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
The navigation controller manages the wizard navigation functionality.
*/
open class HKWizardNavigationController: HKBaseWizardNavigationController {
    
    /**
    On viewWillAppear attaches cancel button to the left bar button item.
    */
    override open func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.topViewController?.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(HKWizardNavigationController.cancel))
        
        if self.viewControllers.count == 0 {
            nextViewController()
        }
    }

    /**
    Builds View controller constructor function by it's id.
    - parameter id: Id if the view controller in AppController.
    */
    open func buildViewControllerConstructor(_ id: Int) -> ((_ parent:UINavigationController) -> UIViewController?) {
        return {(parent) -> UIViewController? in
            if let controller = AppController.getViewControllerById(id, viewController: parent) {
                if parent.viewControllers.count == 0 {
                    if let baseVC = controller as? HKBaseViewController {
                        _ = HKCancelActionVCStrategy(parent: baseVC)
                    }
                }
                return controller
            }
            return nil
        }
    }
    
    /**
     Builds View controller constructor function by it's string identifier.
     - parameter id: String identifier if the view controller in AppController.
     */
    open func buildViewControllerConstructor(identifier: String) -> ((_ parent:UINavigationController) -> UIViewController?) {
        return {(parent) -> UIViewController? in
            if let controller = AppController.instance.getViewControllerByIdentifier(identifier, viewController: parent) {
                if parent.viewControllers.count == 0 {
                    if let baseVC = controller as? HKBaseViewController {
                        _ = HKCancelActionVCStrategy(parent: baseVC)
                    }
                }
                return controller
            }
            return nil
        }
    }

    /**
    Dismisses the view controller on cancel pressed.
    */
    @objc open func cancel() {
        
        self.topViewController?.dismiss(animated: true, completion: nil)
    }
}
