//
//  HKBaseOrderCommentsTableViewController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 4/4/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
The base ViewController that provides functionality for loading and filtering user comments.
* Must not be instantiated.
*/

open class HKBaseOrderCommentsTableViewController: HKBaseLazyTableViewController {
    /// Represents the identity to be used for requests to the server
    open var shopIdentity : Identity?
    /// Represents the filter to be applied when order list is to be loaded.
    open var filter : FeedbackFilter?

    /**
    Loads list of comments from server.
     - parameter responseHandler: The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
    */
    override open func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        if self.filter == nil {
            self.filter = FeedbackFilter()
        }
        self.setupFilter(filter!, count: count, skip: skip)
        
        HonkioApi.userGetComments(filter, flags: 0, handler: responseHandler)
    }

    /**
    Creates the list from the result of the Response
    - parameter responce: The Response instance within the result of processing.
    - Returns: A list of user comments.
    */
    override open func toList(_ response : Response) -> [Any?] {
        return response.result as! [UserComment]
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! UserComment), listItem, itemBinder))
        }
        return items
    }

    /**
     Gets the cell indetifier for provided user comment.
    * Must be overriden.
    - parameter item: User comment.
    - Returns: The identifier of cell.
    */
    open func cellIdentifier(_ item: UserComment) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }

    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }

    /**
    Sets the filter params for the current filter.
     - parameter filter: The filter to be updated.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
    */
    open func setupFilter(_ filter: FeedbackFilter, count: Int, skip: Int) {
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
}
