//
//  HKSettingsTableLogoCell.swift
//  HonkioSDK
//
//  Created by Dev on 09.08.16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
Represents the table view cell for logo image on settings page.
*/
open class HKSettingsTableLogoCell: HKBaseTableViewCell {
    /// The outlet for logo image.
    @IBOutlet open var icon: UIImageView!

    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    /**
    Sets the section style .None on initialization.
    */
    override open func setupStyle() {
        super.setupStyle()
        
        selectionStyle = .none
    }
    
    override open func setupSubviews() {
        super.setupSubviews()
        
        self.icon = makeIconView()
        
        addSubview(icon)
    }
    /**
    Construct and apply the set on constraints on initialization.
    * View: "iconView".
    * Constraints: "V:|-5-[iconView(40)]-5-|" and "H:|-0-[iconView(200)]".
    */
    override open func setupConstraints() {
        super.setupConstraints()
        
        let views : [String : AnyObject] = [
            "iconView" : icon,
        ]
        
        let formatArray : [String] = [
            "V:|-5-[iconView(40)]-5-|",
            "H:|-0-[iconView(200)]"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0),
            metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    /**
    Consructs the logo image.
    - Returns: The UIImageView constructed within the no auto resize and ScaleAspectFit from the bundle resource "hk_logo".
    */
    fileprivate func makeIconView() -> UIImageView {
        let v : UIImageView = UIImageView()
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.contentMode = .scaleAspectFit
        
        v.image = HKBundleUtils.image("hk_logo_secured")
        
        return v;
    }
}
