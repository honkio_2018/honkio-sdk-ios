//
//  HKSettingsTableViewCell.swift
//  WorkPilots
//
//  Created by developer on 1/14/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
The table view cell representing the setting page cell item.
*/
open class HKSettingsTableViewCell: HKBaseTableViewCell {
    /// Outlet for UIImageView in the cell.
    @IBOutlet open var icon: UIImageView!
    /// Outlet for the text lable in the cell.
    @IBOutlet open var titleLabel: UILabel!
    
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override open func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    /**
    Fills the cell with the Settings item provided. Sets the cell UILable with the text of item title. Sets cell image
    by loading the corresponding image from the framewirk bundle.
    - parameter item: Settings item to be used for filling the cell content.
    */
    open func fillFromItem(_ item: HKSettingsItem?) {
        if let settingsItem = item {
            
            var image : UIImage? = UIImage(named: settingsItem.iconName)
            
            if image == nil {
                image = UIImage(named: settingsItem.iconName, in: HKBundleUtils.framewokBundle,
                                compatibleWith: nil)
            }
            
            self.icon.image = image
            self.titleLabel.text = settingsItem.title
            
            if settingsItem.type == HKSettingsItem.ITEM_TYPE_USER_ACCOUNT {
                if let user = HonkioApi.activeUser() {
                    self.icon.imageFromURL(link: HonkioApi.getUserPhotoURL(user.userId), errorImage: nil, contentMode: nil)
                    self.icon.clipsToBounds = true
                    self.icon.layer.cornerRadius = 15
                    self.icon.contentMode = .scaleToFill

                }
            }
            else {
                self.icon.layer.cornerRadius = 0
            }
        }
    }
    /**
    Sets the cell style to .Default value.
    */
    override open func setupStyle() {
        super.setupStyle()
        
        selectionStyle = .default
    }
    /**
    Sets the cell icon and cell text.
    */
    override open func setupSubviews() {
        super.setupSubviews()
        
        self.icon = makeIconView()
        self.titleLabel = makeTitleLabel()
        
        addSubview(icon)
        addSubview(titleLabel)
    }
    /**
    Sets the cell constraints for icon and title.
    * "iconView" constraint will be "V:|-5-[iconView(30)]-5-|".
    * "titleLabel" constraint will be "V:|-5-[titleLabel]-5-|".
    * Relations will be "H:|-16-[iconView(30)]-10-[titleLabel]-16-|".
    */
    override open func setupConstraints() {
        super.setupConstraints()
        
        let views : [String : AnyObject] = [
            "iconView" : icon,
            "titleLabel" : titleLabel
        ]
        
        let formatArray : [String] = [
            "V:|-5-[iconView(30)]-5-|",
            "V:|-5-[titleLabel]-5-|",
            "H:|-16-[iconView(30)]-10-[titleLabel]-16-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0),
            metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    fileprivate func makeIconView() -> UIImageView {
        let v : UIImageView = UIImageView()
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.contentMode = .scaleAspectFit
        let view = UIView.appearance(whenContainedInInstancesOf: [HKSettingsTableViewCell.self])
        v.tintColor = view.tintColor
        
        return v;
    }
    
    fileprivate func makeTitleLabel() -> UILabel {
        let v : UILabel = UILabel()
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.textAlignment = .left
        //        v.font = AppFonts.fontOfSize(12.0)
        //        v.textColor = AppColors.primaryTextColor()
        v.lineBreakMode = .byWordWrapping
        v.numberOfLines = 0
        
        return v;
    }
    
}
