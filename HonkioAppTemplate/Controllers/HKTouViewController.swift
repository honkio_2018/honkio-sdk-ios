//
//  HKTouViewController.swift
//  WorkPilots
//
//  Created by developer on 12/15/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit
/**
The view controller for Terms of Use view.
*/
open class HKTouViewController: HKWizardViewController, UIWebViewDelegate {

    /**
    The action to be performed on comletion.
    */
    open var completeAction: (() -> Void)?
    
    @IBOutlet open weak var webView: UIWebView!
    
    @IBOutlet open weak var toolbar: UIToolbar!
    @IBOutlet open weak var acceptBarItem: UIBarButtonItem!
    
    open var touInfo: HKTouInfo?
    private var isUrlShown = false

    /**
    The action to be run when user accepts Term of Use. Shows progress indicator, notifies the server that the user
    has accepted ToU and if user is not pending runs the complitionAction.
    - parameter sender: The sender of the acceptAction. Not used.
    */
    @IBAction open func acceptAction(_ sender: AnyObject) {
    
        BusyIndicator.shared.showProgressView(self.view)
        
        HonkioApi.userTouAgree(self.touInfo?.key, flags: 0) { (response) -> Void in
            
            BusyIndicator.shared.hideProgressView()
                
            if response.isStatusAccept() {
                HonkioApi.activeUser()?.setTouVersion(HonkioApi.appIdentity()?.identityId,
                                                      touUid: self.touInfo?.key,
                                                      version: HKTouVersion(
                                                        version: self.touInfo?.version ?? HonkioApi.appInfo()?.getTouInfo(self.touInfo?.key)?.version ?? 0,
                                                        timestamp: Date()))
                self.completeAction?()
            }
            else {
                let error = response.anyError()
                if !AppErrors.handleError(self, error: error) {
                    CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
        }
    }

    /**
    On viewDidLoad is received shows progress indicator and requests the ToU HTML file from server. After file is received hides progress indicator and
    loads the received HTML file into Web View.
    */
    override open func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.toolbar.isHidden = self.completeAction == nil
        
        if !isUrlShown {
            let touInfo = self.touInfo ?? HonkioApi.appInfo()?.getTouInfo()
            
            if let touUrl = touInfo?.url, let url = URL(string: touUrl) {
                self.webView.loadRequest(URLRequest(url: url))
            }
            
            isUrlShown = true
        }
    }

    /**
    On viewDidAppear is received sets the web view scroll inset to navigation frame Y value,
    shows navigation bar and ToU new version alert.
    - parameter animated: Describes whether the viewDidAppear is animated.
    */
    override open func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        if let navigationFrame = self.navigationController?.navigationBar.frame {
            
            let y = navigationFrame.maxY
            self.webView.scrollView.contentInset = UIEdgeInsetsMake(y, 0, 0, 0)
        }
        
        self.navigationController?.isNavigationBarHidden = false
    }

    /**
    Is sent after a web view starts loading a frame.
    Hides progress indicator.
    - parameter webView: The web view that has begun loading a new frame.
    */
    open func webViewDidFinishLoad(_ webView: UIWebView) {
        
        BusyIndicator.shared.hideProgressView()
    }

    /**
    Is sent before a web view starts loading a frame.
    Shows progress indicator.
    - parameter webView: The web view that will begin loading a new frame.
    */
    open func webViewDidStartLoad(_ webView: UIWebView) {
        
        BusyIndicator.shared.showProgressView(self.view)
    }
}
