//
//  StructureListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 5/28/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "StructureListJsonParser.h"
#import "StructureJsonParser.h"
#import "HKStructureList.h"

@implementation StructureListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    NSMutableArray<HKStructure*>* list = [NSMutableArray new];
    
    NSArray* structuresArray = dict[@"assets"];
    if (structuresArray) {
        for (NSDictionary* structureDict in structuresArray) {
            HKStructure* structure = [StructureJsonParser toStructure:structureDict];
            if (structure)
                [list addObject:structure];
        }
    }
    
    HKStructureList* result = [HKStructureList new];
    result.list = list;
    
    return result;
}

@end
