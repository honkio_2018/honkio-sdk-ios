//
//  Applicant.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/9/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "UserRole.h"

@interface Applicant : NSObject

@property (nonatomic, strong) NSString* applicationId;
@property (nonatomic, strong) NSString* userId;
@property (nonatomic, strong) NSString* orderId;
@property (nonatomic, strong) UserRole* role;

@property (nonatomic, strong) NSString* timeProposal;

@end
