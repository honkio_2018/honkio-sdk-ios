//
//  WebServiceCache.m
//  HonkioApi
//
//  Created by Shurygin Denis on 7/30/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "WebServiceCache.h"

#define CACHE_FILE_EXPIRE_TIME 691200 * 2
#define CACHE_FILE_EXTENSION @".ch"
#define CACHE_FOLDER @"WebServiceCache"

@implementation WebServiceCache

+(void) put:(NSData*)data name:(NSString*)name connection:(Connection*)connection {
    [WebServiceCache put:data name:name user:[connection login]];
}

+(void) put:(NSData*)data name:(NSString*)name user:(NSString*)userName {
    [data writeToFile:[WebServiceCache getCacheFilePatch:userName fileName:name] atomically:YES];
}

+(NSData*) get:(NSString*)name connection:(Connection*)connection {
    return [WebServiceCache get:name user:[connection login]];
}

+(NSData*) get:(NSString*)name user:(NSString*)userName {
    return [[NSData alloc] initWithContentsOfFile:[WebServiceCache getCacheFilePatch:userName fileName:name]];
}

+(BOOL) isHasCache:(NSString*)name connection:(Connection*)connection {
    return [WebServiceCache isHasCache:name user:[connection login]];
}

+(BOOL) isHasCache:(NSString*)name user:(NSString*)userName {
    return [[NSFileManager defaultManager] fileExistsAtPath:[WebServiceCache getCacheFilePatch:userName fileName:name]];
}

+(void) clearForConnection:(Connection*)connection {
    [WebServiceCache clearForUser:[connection login]];
}
+(void) clearForUser:(NSString*)userName {
    [[NSFileManager defaultManager] removeItemAtPath:[WebServiceCache getCacheFolder: userName] error:NULL];
}

+(void) trim {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDateComponents* components =[[NSDateComponents alloc] init];
        components.second = CACHE_FILE_EXPIRE_TIME;
        [WebServiceCache trim:[WebServiceCache getCacheFolder:nil] toDate:[[NSCalendar currentCalendar] dateByAddingComponents:components toDate:[NSDate new] options:0]];
    });
}

+(void) trim:(NSString*)folder toDate:(NSDate*)date {
    NSFileManager* fileManager = [NSFileManager defaultManager];
    BOOL isDirectory;
    NSArray * directoryContents = [fileManager contentsOfDirectoryAtPath:[WebServiceCache getCacheFolder:nil] error: NULL];
    for (NSString* file in directoryContents) {
        if ([fileManager fileExistsAtPath:file isDirectory:&isDirectory])
            [WebServiceCache trim:file toDate:date];
        else {
            NSDictionary *attributes = [fileManager attributesOfItemAtPath:file error:nil];
            NSDate* modificationDate = [attributes fileModificationDate];
            if (modificationDate < date)
                [fileManager removeItemAtPath:file error:NULL];
        }
    }
}

+(NSString*)getUserNameFromMessage:(Message*)msg {
    if (![msg connection] && [msg parameter:MSG_PARAM_LOGIN_IDENTITY])
        return (NSString*) [msg parameter:MSG_PARAM_LOGIN_IDENTITY];
    else if (![msg connection] && [msg parameter:MSG_PARAM_USER_EMAIL])
        return (NSString*) [msg parameter:MSG_PARAM_USER_EMAIL];
    else
        return [[msg connection] login];
}

+(NSString*) getCacheFilePatch:(NSString*)userName fileName:(NSString*)fileName {
    return [[WebServiceCache getCacheFolder:userName] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", fileName, CACHE_FILE_EXTENSION]];
}

+(NSString*) getCacheFolder:(NSString*)userName {
    NSString* result = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject] stringByAppendingPathComponent:CACHE_FOLDER];
    if (userName)
        return [result stringByAppendingPathComponent:userName];
    return result;
}

@end
