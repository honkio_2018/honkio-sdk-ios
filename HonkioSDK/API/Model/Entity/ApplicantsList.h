//
//  ApplicantsList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/9/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "Applicant.h"

@interface ApplicantsList : NSObject

@property (nonatomic) NSArray<Applicant *>* list;

@end
