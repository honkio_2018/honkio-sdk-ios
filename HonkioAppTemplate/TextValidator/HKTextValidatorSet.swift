//
//  HKTextValidatorSet.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 2/20/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation
/**
The container class stands for the list of HKTextValidator. Provides funtionality for validating the
by several rules.
- SeeAlso HKTextValidator.
*/
open class HKTextValidatorSet {

    fileprivate var validators : [HKTextValidator]!
    /**
    Initialize the instance with the set of HKTextValidators.
    - parameter validators: The list of HKTextValidators to be stored in.
    */
    public init(validators: [HKTextValidator]) {
        self.validators = validators
    }
    /**
    Validates the UITextField by the set of HKTextValidators.
    - parameter textField: The UITextField the value of to be tested.
    - Returns: False if even one of the rules was not satisfied.
    */
    open func validate(_ textField: UITextField?) -> Bool {
        for i in 0..<validators.count {
            if !validators[i].validate(textField) {
                return false
            }
        }
        return true
    }
}
