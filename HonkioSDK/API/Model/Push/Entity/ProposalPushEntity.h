//
//  ProposalPushEntity.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/3/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasePushEntity.h"

@interface ProposalPushEntity : BasePushEntity

@property (nonatomic) NSString* orderId;
@property (nonatomic) NSString* orderTitle;

@property (nonatomic) NSString* userId;
@property (nonatomic) NSString* userName;
@property (nonatomic) NSString* proposalDesc;

-(id)initWithContent:(NSDictionary*)content;

@end
