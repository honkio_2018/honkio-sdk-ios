//
//  User.h
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/3/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserAccount.h"
#import "AbsFilter.h"
#import "HKTouVersion.h"
#import "AppInfo.h"

#define USER_FIRSTNAME_KEY      @"consumer_str_firstname"
#define USER_LASTNAME_KEY       @"consumer_str_lastname"
#define USER_TELEPHONE_KEY      @"consumer_str_telephone"
#define USER_COUNTRY_KEY        @"consumer_str_country"
#define USER_CITY_KEY           @"consumer_str_city"
#define USER_ADDRESS1_KEY       @"consumer_str_address1"
#define USER_ADDRESS2_KEY       @"consumer_str_address2"
#define USER_SSN_KEY            @"consumer_str_ssn"
#define USER_ZIP_KEY            @"consumer_str_zip"
#define USER_TIMEZONE_KEY       @"consumer_str_timezone"
#define USER_LANGUAGE_KEY       @"consumer_str_language"
#define USER_ACCOUNTS_KEY       @"consumer_account"
#define USER_ACTIVE_KEY         @"consumer_active"
#define USER_LOSTPASSWORD_KEY   @"lostpassword_used"
#define USER_KEYSTORE_KEY       @"consumer_dict_keystore"
#define USER_PASSWORD_KEY       @"consumer_str_password"
#define USER_EMAIL_KEY          @"consumer_str_email"
#define USER_RECEIPT_FLAG_KEY   @"consumer_bool_receipt"
#define USER_TOU_VERSION_KEY    @"consumer_dict_accepted_tou_version"
#define USER_REGISTRATION       @"consumer_registration"

#define USER_DEVICE_TOKEN_KEY   @"token"
#define USER_DEVICE_TYPE_KEY    @"type"
#define USER_DEVICE_NAME_KEY    @"name"
#define USER_DEVICE_NOTIFY_KEY  @"notify"

#define OPERATOR_BILLING            @"operator_billing"
#define KEYSTORE_ACTIVATION_KEY     @"app_reg_transaction_id"

// ------------- User ---------------------------

/**
 * An object that contains information of the user.
 * <br>Also this class extent AbsFilter and can be used like a Filter.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface User : AbsFilter

@property (nonatomic) NSString* userId;

/** User login (email). */
@property (nonatomic) NSString* login;

/** User password. */
@property (nonatomic) NSString* password;

/** User photo URL or file name in server assets. To build URL for server assets use ServerInfo.assetsUrl. */
@property (nonatomic) NSString* photoUrl;

/** User language code. All responses will translated if server support this language. */
@property (nonatomic) NSString* language;

@property (nonatomic, readonly) NSString* name;
@property (nonatomic) NSString* firstname;
@property (nonatomic) NSString* lastname;
@property (nonatomic) NSString* timezone;
@property (nonatomic) NSString* phone;
@property (nonatomic) NSString* ssn;
@property (nonatomic) NSNumber* isReceipt;

// address
@property (nonatomic) NSString* address1;
@property (nonatomic) NSString* address2;
@property (nonatomic) NSString* zip;
@property (nonatomic) NSString* city;
@property (nonatomic) NSString* state;
@property (nonatomic) NSString* country;

@property (nonatomic) UserAccount* defaultAccount;

/** User Payment Accounts array. */
@property (nonatomic) NSArray<UserAccount*>* accounts;
@property (nonatomic) NSDictionary* userKeystore;

/** YES if User is active, NO otherwise. Inactive User can't do purchases. */
@property (nonatomic,assign) BOOL isActive;

/** YES if temporary password was used for getting User object, NO otherwise. */
@property (nonatomic,assign) BOOL isTempPasswordUsed;

/** YES if Operator billing is enabled for current user, NO otherwise. */
@property (nonatomic,assign) BOOL isOperatorBillingEnabled;

/** Version of "Terms of Use" which user agree. */
@property (nonatomic) NSMutableDictionary<NSString*, NSMutableDictionary<NSString*, HKTouVersion*>*>* touVersions;

/** Registration transaction ID. Status of this transaction should help to know reasons whu user is inactive. */
@property (nonatomic) NSString* registrationId;

-(void)setDictValue:(NSString*)value forKey:(NSString*)key;
-(NSString*)getDictValue:(NSString*)key;

/**
 * Check if user has set an address.
 * @return YES if user has set an address, NO otherwise.
 */
-(BOOL)isHasAddress;

/**
 * Check if user has set any account.
 * @return YES if user has set any account, NO otherwise.
 */
-(BOOL)isHasAnyAccount;

/**
 * Check if last accepted version of "Terms of Use" for specified key is low than version from tou info.
 * @return YES if ToU for specified key is not accepted or it's version low that minimal version from parameter, NO otherwise.
 */
-(BOOL)isTouVersionObsolete:(AppInfo*)appInfo;

/**
* Check if last accepted version of "Terms of Use" for specified key is low than version from tou info.
* @return YES if ToU for specified key is not accepted or it's version low that minimal version from parameter, NO otherwise.
*/
-(BOOL)isTouVersionObsolete:(AppInfo*)appInfo touUid:(NSString*) touUid;

/**
* Check if last accepted version of "Terms of Use" for specified key is low than version from tou info.
* @return YES if ToU for specified key is not accepted or it's version low that minimal version from parameter, NO otherwise.
*/
-(BOOL)isTouVersionObsolete:(NSString*)appId touUid:(NSString*) touUid touInfo:(HKTouInfo*)touInfo;

-(UserAccount*)emptyAccountNumber:(NSString*)type;
-(UserAccount*)findAccountByType:(NSString*)type;
-(UserAccount*)findAccountByType:(NSString*)type number:(NSString*)number;

-(void)setTouVersion:(NSString*)appId touUid:(NSString*)touUid version:(HKTouVersion*) version;
-(HKTouVersion*)getTouVersion:(NSString*)appId touUid:(NSString*)touUid;
-(HKTouVersion*)getTouVersion:(NSString*)appId;

@end


