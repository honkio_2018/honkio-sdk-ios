//
//  HKModelEntity.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 5/17/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKModelEntity.h"

@implementation HKModelEntity {
    NSMutableDictionary* props;
}

- (instancetype)init {
    return [self init: nil];
}

-(instancetype)initFromEntity:(HKModelEntity*) entity {
    return [self init: [entity getProps]];
}

- (instancetype)init:(NSDictionary*) propertiesDict {
    self = [super init];
    if (self) {
        if (propertiesDict) {
            props = [propertiesDict mutableCopy];
        }
        else {
            props = [NSMutableDictionary new];
        }
    }
    return self;
}

-(id) getProperty:(NSString*) key {
    return props[key];
}

-(void) setProperty:(NSString*) key value:(id) value {
    props[key] = value;
}

-(NSMutableDictionary*) getProps {
    return props;
}

@end
