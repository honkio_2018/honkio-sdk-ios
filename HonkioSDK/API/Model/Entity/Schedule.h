//
//  Schedule.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ScheduleFilter.h"
#import "Event.h"

@interface Schedule : NSObject


@property (nonatomic) NSArray<Event*>* list;
@property (nonatomic) ScheduleFilter* filter;

@end
