//
//  StringResponseJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/30/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "StringResponseJsonParser.h"
#import "JsonRequestBuilder.h"

@implementation StringResponseJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    NSData* data = [JsonRequestBuilder toJson:dict];
    if (data) {
        return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    return nil;
}

@end
