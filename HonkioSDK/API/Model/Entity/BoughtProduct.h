//
//  ProductTransaction.h
//  HonkioApi
//
//  Created by Alexandr Kolganov on 4/22/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

@class Transaction;

/**
 * Data object that hold information about one bought product position.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface BoughtProduct : Product

// TODO write docs
-(id)initFrom:(BoughtProduct*) product;

/** Count of bought products. */
@property (nonatomic) int count;

/** Validity value of this Product. Validity period ignored. */
@property (nonatomic) BOOL isValid;

/**
 *  Gets or sets product signature
 */
@property (nonatomic, strong) NSString* signature;

/** Start of validity period. */
@property (nonatomic, strong) NSDate* validFrom;
/** End of validity period. */
@property (nonatomic, strong) NSDate* validTo;

/** Transaction of this Bought Product. */
@property (nonatomic, strong) Transaction* transaction;

/**
 *  Gets or sets time that left until the validity period finished
 *
 *  @param time Comparable date.
 *
 *  @return Valid interval.
 */
-(NSTimeInterval)validLeft:(NSDate*)time;

/**
 * Check validity value and validity period.
 * @param time Time for which there is a check.
 * @return YES if validity value is true and time is in validity period, NO otherwise.
 */
-(BOOL)isValid:(NSDate*)time;

@end
