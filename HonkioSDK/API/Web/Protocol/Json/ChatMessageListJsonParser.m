//
//  ChatMessageListJsonParser.m
//  HonkioSDK
//
//  Created by Dev on 01.09.16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "ChatMessageListJsonParser.h"
#import "ChatMessageJsonParser.h"
#import "ChatMessages.h"

@implementation ChatMessageListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    ChatMessages *chatMessages = [ChatMessages new];
    
    NSMutableArray *list = [NSMutableArray new];
    
    NSArray *messages = dict[@"messages"];
    if (messages) {
        for (NSDictionary *message in messages) {
            if([message isKindOfClass:[NSDictionary class]]) {
                [list addObject:[ChatMessageJsonParser parseChatMessage:message]];
            }
        }
    }
    chatMessages.list = list;
    
    return chatMessages;
}

@end
