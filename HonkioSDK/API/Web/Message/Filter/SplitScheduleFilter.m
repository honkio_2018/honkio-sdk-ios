//
//  SplitScheduleFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/20/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "SplitScheduleFilter.h"

@implementation SplitScheduleFilter

-(NSDictionary*) toParams {
    
    NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithDictionary:[super toParams]];
    
    [AbsFilter applyIfNotNil:@"query_childs" value: [NSNumber numberWithBool:self.isQueryChilds] forParams:params];
    [AbsFilter applyIfNotNil:@"query_step" value: [NSNumber numberWithLong:self.step] forParams:params];
    [AbsFilter applyIfNotNil:@"query_round" value: [NSNumber numberWithLong:self.round] forParams:params];
    
    if (self.duration > 0)
        [AbsFilter applyIfNotNil:@"query_duration" value: [NSNumber numberWithLong:self.duration] forParams:params];
    
    if ([self.products count] > 0) {
        NSMutableArray* productsArray = [NSMutableArray new];
        for (BookedProduct* product in self.products) {
            [productsArray addObject:@{ @"id" : [[product product] productId],
                                     @"count" : [NSNumber numberWithInt:[product count]] }];
        }
        params[@"query_product"] = productsArray;
    }
    
    return params;
}

@end
