//
//  HKMediaFileList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/13/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKMediaFile.h"

@interface HKMediaFileList : NSObject

@property (nonatomic) NSArray<HKMediaFile*> *list;

@end
