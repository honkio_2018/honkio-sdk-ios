//
//  HKCancelActionVCStrategy.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 4/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
Overrides the viewWillAppear behavior for HKViewControllerLifeCycleObserver by adding cancel action strategy.
*/

open class HKCancelActionVCStrategy : HKViewControllerLifeCycleObserver {
    
    open var afterCancelAction: ((_ strategy: HKCancelActionVCStrategy)->Void)?

    /**
    On viewWillAppear... Add left side bar button within the Cancel style and dismission  action for view controller.
    - parameter animated: Describes whether view will appear animated.
    */
    override open func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(HKCancelActionVCStrategy.cancelAction))
    }

    /**
    Describes the action to be performed on button pressed.
    * Dismissed the view controllef.
    */
    @objc open func cancelAction() {
        self.viewController.dismiss(animated: true, completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [unowned self] in
            if self.viewController.navigationController != nil {
                _ = self.viewController.navigationController?.viewControllers = []
            }
        }
        self.afterCancelAction?(self)
    }
}
