//
//  BusyIndicator.swift
//  WorkPilots
//
//  Created by developer on 12/15/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit
/**
Manages the busy indicator behavior.
*/
open class BusyIndicator: NSObject {

    var containerView = UIView()
    
    var progressView = UIView()
    
    var activityIndicator = UIActivityIndicatorView()
    /**
    Contains the reference to instance of BusyIndicator
    */
    open class var shared: BusyIndicator {
        
        struct Static {
            
            static let instance: BusyIndicator = BusyIndicator()
        }
        
        return Static.instance
    }

    /**
    Creates and shows progress indicator on attached view.
    - parameter view: The view the progress indicator is attached to.
    */
    open func showProgressView(_ view: UIView) {
        
        containerView.frame = view.frame
        containerView.center = view.center
        
        progressView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        progressView.center = view.center
        progressView.backgroundColor = UIColor(hex: 0x005F96, alpha: 0.9)
        // TODO use appearance
//        progressView.backgroundColor = UIColor(hex: AppColors.MainColor, alpha: 0.9)
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.center = CGPoint(x: progressView.bounds.width / 2, y: progressView.bounds.height / 2)
        
        progressView.addSubview(activityIndicator)
        containerView.addSubview(progressView)
        view.addSubview(containerView)
        
        activityIndicator.startAnimating()
    }

    /// Hides progress indicator and removes it from the view it was attached to.
    open func hideProgressView() {
        
        activityIndicator.stopAnimating()
        containerView.removeFromSuperview()
    }
}
