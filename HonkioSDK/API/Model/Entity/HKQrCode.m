//
//  HKQrCode.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKQrCode.h"

@implementation HKQrCode


-(id)init {
    self = [super init];
    return self;
}

-(id)initFrom:(HKQrCode*) qrCode {
    self = [super init];

    self.qrId = qrCode.qrId;
    self.merchantId = qrCode.merchantId;
    
    self.objectType = qrCode.objectType;
    self.object = qrCode.object;
    
    self.urlBase = qrCode.urlBase;
    self.urlParams = qrCode.urlParams;
    
    return self;
}

@end
