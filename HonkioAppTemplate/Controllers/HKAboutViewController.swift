//
//  HKAboutViewController.swift
//  HonkioAppTemplate
//
//  Created by Mikhail Li on 14/09/2018.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

open class HKAboutViewController: HKBaseViewController {
    
    @IBOutlet open weak var logoImage: UIImageView!
    @IBOutlet open weak var versionLabel: UILabel!
    
    override open func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
