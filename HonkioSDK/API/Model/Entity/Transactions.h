//
//  Transactions.h
//  HonkioApi
//
//  Created by Shurygin Denis on 5/12/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Transaction.h"
#import "Order.h"

/**
 * Data object that holds list of the Transactions.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface Transactions : NSObject

/** Server time in millis when Transactions list was sent. */
@property (nonatomic, strong) NSDate*  timeStamp;
@property (nonatomic, strong) NSArray<Transaction*>*  list;

@property (nonatomic, strong, nullable) NSDictionary<NSString*, Order*>*  orders;

@end
