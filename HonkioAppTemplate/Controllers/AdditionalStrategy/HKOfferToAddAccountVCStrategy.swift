//
//  HKOfferToAddAccountVCStrategy.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 4/1/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
import Foundation

/**
Based on HKViewControllerLifeCycleObserver.
Provides functionality for managing of necessity of creating a new payment account.
*/

open class HKOfferToAddAccountVCStrategy : HKViewControllerLifeCycleObserver, HKViewControllerCompletionDelegate {
    
    open var message : String = "dlg_buy_no_accounts_message".localized
    open var delegate : HKOfferToAddAccountVCStrategyDelegate?
    
    // TODO write docs
    open var removeOnCompletion : Bool = false
    
    fileprivate static let TAG = "HKOfferToAddAccountVCStrategy.TAG"
    
    /**
    Shows the CustomAlert for a user within the offer to add new payment account. If user agreeds the alert the
    registration of new payment method will be started.
    */
    open func offerToAddAccount() {
        let alert = CustomAlertView(title: nil, message: message, cancelButtonTitle: "dlg_common_button_cancel".localized, cancelButtonAction: { [unowned self] in
            
            if let strategyDelegate = self.getDelegate() {
                strategyDelegate.paymentAccountNotAdded(self)
            }
        })
        alert.addButtonWithTitle("dlg_common_button_ok".localized, action: { [unowned self] in

            if let controller = AppController.getViewControllerById(AppController.SettingsUserAddCreditCard, viewController: self.viewController) as? HKBaseViewController {
                
                _ = HKCancelActionVCStrategy(parent: controller)
                
                controller.setCompletionDelegate(self, tag: HKOfferToAddAccountVCStrategy.TAG)
                
                let navi = UINavigationController(rootViewController: controller)
                
                self.viewController.present(navi, animated: true, completion: nil)
            }
        }).show()
    }
    
    /**
    Notifies parrent view controller that the operation is completed.
    - parameter viewController: The view controller to be sent to parent view controller.
    - parameter tag: The string value the routine was tagged with.
    */
    open func onComplete(_ viewController: UIViewController, tag: String?) {
        if HKOfferToAddAccountVCStrategy.TAG == tag {
            self.viewController.dismiss(animated: true, completion: nil)

            if let strategyDelegate = self.getDelegate() {
                if HonkioApi.activeUser()!.defaultAccount != nil {
                    strategyDelegate.paymentAccountDidAdded(self)
                }
                else {
                    strategyDelegate.paymentAccountNotAdded(self)
                }
            }
            
            if let completionDelegate = self.parent as? HKViewControllerCompletionDelegate {
                completionDelegate.onComplete(viewController, tag: tag)
            }
            
            if self.removeOnCompletion {
                self.parent?.removeLifeCycleObserver(self)
            }
        }
    }
    
    private func getDelegate() -> HKOfferToAddAccountVCStrategyDelegate? {
        var strategyDelegate = delegate
        
        if strategyDelegate == nil {
            strategyDelegate = self.parent as? HKOfferToAddAccountVCStrategyDelegate
        }
        
        return strategyDelegate
    }
}
