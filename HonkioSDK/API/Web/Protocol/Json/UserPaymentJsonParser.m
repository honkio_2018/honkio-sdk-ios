//
//  UserPaymentJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "UserPaymentJsonParser.h"
#import "UserPayment.h"
#import "ApiDateFormatter.h"

@implementation UserPaymentJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    UserPayment* userPayment = [UserPayment new];
    
    NSString * timeStamp = dict[@"timestamp"];
    if(timeStamp)
        userPayment.timeStamp = [ApiDateFormatter dateFromServerString:timeStamp];
    userPayment.amount = dict[@"account_amount"];
    userPayment.currency = dict[@"account_currency" ];
    
    return userPayment;
}

@end
