//
//  AbsListFilter.h
//  HonkioApi
//
//  Created by Shurygin Denis on 12/3/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "AbsFilter.h"

/* Objective-C version ListFilter for Android */

@interface AbsListFilter : AbsFilter

/** Id of transaction that should be returned. */
@property (nonatomic, copy) NSString *queryId;

/** Items count limit. Set 0 to set default limit. */
@property (nonatomic, assign) int queryCount;

/** Items count to skip. Default value = 0. */
@property (nonatomic, assign) int querySkip;

-(void)fillParams:(NSMutableDictionary *)params;

@end
