//
//  ScheduleJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "ScheduleJsonParser.h"
#import "Event.h"
#import "Schedule.h"
#import "ApiDateFormatter.h"

@implementation ScheduleJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    NSMutableArray<Event*>* list = [NSMutableArray new];
    NSArray* eventsJsonArray = dict[@"schedule"];
    for (NSDictionary* eventJson in eventsJsonArray) {
        if (eventJson) {
            Event* event = [Event new];
            
            event.type = eventJson[@"type"];
            if (!event.type) {
                event.type = EVENT_TYPE_TENTATIVE;
            }
            
            event.start = [ApiDateFormatter dateFromServerString:eventJson[@"start"]];
            event.end = [ApiDateFormatter dateFromServerString:eventJson[@"end"]];
            
            if (event.start && event.end) {
                NSMutableArray* metaDataArray = [NSMutableArray new];
                
                NSArray* shopsArray = eventJson[@"shops"];
                if (shopsArray) {
                    for(int i = 0; i < shopsArray.count; i++) {
                        NSDictionary* shopDict = shopsArray[i];
                        if (shopDict) {
                            NSMutableDictionary<NSString*, NSString*>* metaDict = [[NSMutableDictionary alloc] initWithDictionary: shopDict[@"metadata"]];
                            [metaDict setValue:shopDict[@"shop"] forKey:EVENT_META_SHOP];
                            [metaDataArray addObject:[metaDict copy]];
                        }
                    }
                }
                
                event.metaData = [metaDataArray copy];
                
                [list addObject:event];
            }
        }
    }
    
    Schedule* schedule = [Schedule new];
    schedule.list = list;
    
    return schedule;
}

@end
