//
//  Tag.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 1/30/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tag : NSObject

@property (nonatomic, strong) NSString*  tagId;
@property (nonatomic, strong) NSString*  name;
@property (nonatomic, strong) NSString*  desc;

@property (nonatomic, strong) NSString*  currency;
@property (nonatomic) float price;
@property (nonatomic, strong) NSString*  productId;

@end
