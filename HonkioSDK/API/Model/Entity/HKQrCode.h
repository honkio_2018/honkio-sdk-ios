//
//  HKQrCode.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

// TODO write docs

#define HK_QR_TYPE_ASSET @"asset"
#define HK_QR_TYPE_SHOP @"shop"

@protocol HKQrObject

-(NSString*) getId;

@end

@interface HKQrApplication : NSObject

@end

@interface HKQrCode : NSObject


// TODO write docs
-(id)init;

// TODO write docs
-(id)initFrom:(HKQrCode*) qrCode;

@property (nonatomic) NSString* qrId;
@property (nonatomic) NSString* merchantId;

@property (nonatomic) NSString* objectType;
@property (nonatomic) id<HKQrObject> object;

@property (nonatomic) NSString* urlBase;
@property (nonatomic) NSString* urlParams;

@property (nonatomic) NSArray* applications;

@end
