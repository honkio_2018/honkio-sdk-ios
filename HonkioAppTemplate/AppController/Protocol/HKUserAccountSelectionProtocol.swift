//
//  HKUserAccountChooseProtocol.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/29/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

public protocol HKUserAccountSelectionProtocol: class {

    var merchant: MerchantSimple? { get set }
    var disallowedAccounts : Set<String>? { get set }
    var minAmount : Double? { get set }
    var currency : String? { get set }
    var selectionDelegate: HKUserAccountSelectionProtocolDelegate! { get set }
    
}
