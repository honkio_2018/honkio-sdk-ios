//
//  AppErrors.swift
//
//  Created by developer on 2/10/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
The class is to be used for subclassing the error processing.
Subclass is supposed to implement user notifications, loggin and any other application reactions.
* Must be subclassed.
*/

open class AppErrors {
    
    fileprivate static var sInstance : AppErrors!

    /**
    Sets the instance of AppErrors.
    - parameter instance: The instance of AppErrors to be set.
    */
    public static func initInstance(_ instance:AppErrors) {
        AppErrors.sInstance = instance
    }

    /**
    - parameter response: The Responce object to be tested for errors.
    - parameter error: NSError object. If provided then error is in place.
    - Returns: True if the data is erroneous otherwise false.
    */
    public static func isError(_ response: Response?) -> Bool {
        return response == nil || response!.isStatusReject() || response!.isStatusError()
    }

    /**
    Handles the error. Static call to instance of AppErrors.
    - parameter controller: ViewController is to be used for access to user interface routine.
    - parameter response: The Response object containint erroneous data.
    - parameter error: NSError object describing the error.
    - Returns: True if error has been processed otherwise false.
    */
    public static func handleError(_ controller: UIViewController, response: Response?) -> Bool {
        return AppErrors.sInstance.handleErrorImpl(controller, response: response, fallAction: nil)
    }
    
    public static func handleError(_ controller: UIViewController, error: ApiError?) -> Bool {
        return AppErrors.sInstance.handleErrorImpl(controller, error: error, fallAction: nil)
    }

    /**
    Handles the error. Static call to instance of AppErrors.
    - parameter controller: ViewController is to be used for access to user interface routine.
    - parameter response: The Response object containint erroneous data.
    - parameter error: NSError object describing the error.
    - parameter failAction: Action could be calles when the error is processing.
    - Returns: True if error has been processed otherwise false.
    */
    public static func handleError(_ controller: UIViewController, response: Response?, fallAction:(() -> Void)?) -> Bool {
        return AppErrors.sInstance.handleErrorImpl(controller, response: response, fallAction: fallAction)
    }
    
    public static func handleError(_ controller: UIViewController, error: ApiError?, fallAction:(() -> Void)?) -> Bool {
        return AppErrors.sInstance.handleErrorImpl(controller, error: error, fallAction: fallAction)
    }
    
    public init() {
        
    }

    /**
    Handles the error.
    * Could be overriden.
    - parameter controller: ViewController is to be used for access to user interface routine.
    - parameter response: The Response object containint erroneous data.
    - parameter failAction: Action could be calles when the error is processing.
    - Returns: True if error has been processed otherwise false.
    */
    open func handleErrorImpl(_ controller: UIViewController, response: Response?, fallAction:(() -> Void)?) -> Bool {
        return self.handleErrorImpl(controller, error: response != nil ? response!.anyError() : ApiError.unknown(), fallAction: fallAction)
    }
    
    open func handleErrorImpl(_ controller: UIViewController, error: ApiError?, fallAction:(() -> Void)?) -> Bool {
        preconditionFailure("WARNING: You must override this method in your custom class")
    }
}

