//
//  UserAccessList.h
//  HonkioSDK
//
//  Created by Mikhail Li on 15/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserAccessItem.h"

@interface UserAccessList : NSObject

@property (nonatomic, strong) NSArray<UserAccessItem*> *list;

-(nullable UserAccessItem *)findByMerchant:(NSString *)merchantId;
-(BOOL)isHasAccess:(NSString *)merchantId access:(NSArray *)access;

@end
