//
//  UserCommentJsonParser.m
//  HonkioSDK
//
//  Created by Dash on 28.09.2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "UserCommentJsonParser.h"
#import "ApiDateFormatter.h"

@implementation UserCommentJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [UserCommentJsonParser parseComment: dict];
}

+(UserComment*) parseComment:(NSDictionary*) commentJson {
    if (commentJson) {
        UserComment* comment = [UserComment new];
        
        comment.commentId = commentJson[@"id"];
        
        comment.userId = commentJson[@"user_id"];
        comment.userRoleId = commentJson[@"user_role_id"];
        
        comment.time = [ApiDateFormatter dateFromServerString:commentJson[@"timestamp"]];
        
        comment.text = commentJson[@"comment"];
        NSNumber* markNumber = commentJson[@"mark"];
        if (markNumber) {
            comment.mark = markNumber.intValue;
        }
        
        comment.orderId = commentJson[@"order_id"];
        comment.tagId = commentJson[@"tag_id"];
        
        comment.object = commentJson[@"object"];
        comment.objectType = commentJson[@"object_type"];
        comment.shopId = commentJson[@"shop_id"];
        
        return comment;
    }
    return nil;
}

@end
