//
//  UserRole.m
//  HonkioApi
//
//  Created by Shurygin Denis on 12/8/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "UserRole.h"

@implementation UserRole

-(id)init {
    self = [super init];
    if (self) {
        _isActive = true;
    }
    return self;
}

-(id)initFrom:(UserRole*) role {
    self = [super init];
    if (self) {
        self.roleId = role.roleId;
        self.roleName = role.roleName;
        self.roleDisplayName = role.roleDisplayName;
        self.roleDesc = role.roleDesc;
        self.userId = role.userId;
        self.userFirstName = role.userFirstName;
        self.userLastName = role.userLastName;
        self.userPhotoURL = role.userPhotoURL;
        
        self.userJobsTotal = role.userJobsTotal;
        self.userRating = role.userRating;
        
        if (role.privateProperties) {
            self.privateProperties = [NSMutableDictionary dictionaryWithDictionary:role.privateProperties];
        }
        if (role.publicProperties) {
            self.publicProperties = [NSMutableDictionary dictionaryWithDictionary:role.publicProperties];
        }
        if (role.userContactData) {
            self.userContactData = [NSMutableDictionary dictionaryWithDictionary:role.userContactData];
        }
    }
    return self;
}

-(NSDictionary*) toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    [AbsFilter applyIfNotNil:@"role_id" value:self.roleId forParams:params];
    [AbsFilter applyIfNotNil:@"user_contact_data" value:self.userContactData forParams:params];
    [AbsFilter applyIfNotNil:@"private_properties" value:self.privateProperties forParams:params];
    [AbsFilter applyIfNotNil:@"public_properties" value:self.publicProperties forParams:params];
    
    return [NSDictionary dictionaryWithDictionary:params];
}

-(NSString*) userName {
    BOOL isHasFirstname = [self.userFirstName length] > 0;
    BOOL isHasLastName = [self.userLastName length] > 0;
    
    if (isHasFirstname && isHasLastName)
        return [NSString stringWithFormat:@"%@ %@", self.userFirstName, self.userLastName];
    else if (isHasFirstname)
        return self.userFirstName;
    else if (isHasLastName)
        return self.userLastName;
    return @"";
}

-(nullable id)getPrivateProperty:(nonnull NSString *)key {
    return [self.privateProperties objectForKey:key];
}

-(nullable id)getPublicProperty:(nonnull NSString *)key {
    return [self.publicProperties objectForKey:key];
}

@end
