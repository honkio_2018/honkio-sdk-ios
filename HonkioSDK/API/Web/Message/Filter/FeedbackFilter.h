//
//  FeedbackFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 4/6/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsListFilter.h"

@interface FeedbackFilter : AbsListFilter

    @property (nonatomic, nullable) NSString* userId;
    @property (nonatomic, nullable) NSString* orderId;
    @property (nonatomic, nullable) NSString* roleId;
    @property (nonatomic, nullable) NSString* objectId;

    /** Use "HK_OBJECT_TYPE_..." define property */
    @property (nonatomic, nullable) NSString* objectType;

@end
