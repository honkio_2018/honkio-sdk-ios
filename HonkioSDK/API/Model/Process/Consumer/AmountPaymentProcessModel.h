//
//  AmountPaymentProcessModel.h
//  HonkioApi
//
//  Created by Shurygin Denis on 11/27/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "BasePaymentProcessModel.h"

@interface AmountPayParams : PayParams

@end

@interface AmountPaymentProcessModel : BasePaymentProcessModel

-(void)pay:(Shop*)shop orderId:(NSString*)orderId account:(UserAccount*)account amount:(double)amount currency:(NSString*)currency flags:(int)flags;
-(void)pay:(UserAccount*)account amount:(double)amount currency:(NSString*)currency;
-(void)pay:(UserAccount*)account amount:(double)amount currency:(NSString*)currency flags:(int)flags;

@end
