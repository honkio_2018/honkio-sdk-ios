//
//  AbsProcessModel.m
//  HonkioApi
//
//  Created by Shurygin Denis on 8/3/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "BaseProcessModel.h"

@implementation BaseProcessModel

-(id)init {
    self = [super init];
    if (self) {
        _status = CREATED;
    }
    return self;
}

-(void)abort {
    if(_status != FINISHED) {
        _status = ABORTED;
    }
}

-(void)onComplete:(Response*)response {
    _status = FINISHED;
    if (self.processDelegate) {
        [self.processDelegate onComplete:self response:response];
    }
}

-(void)onPending:(Response*)response {
    _status = PENDING;
    if (self.processDelegate && [self.processDelegate respondsToSelector:@selector(onPending:response:)]) {
        [self.processDelegate onPending:self response:response];
    }
}

-(void)onError:(Response*)response {
    if (self.processDelegate && [self.processDelegate respondsToSelector:@selector(onError:response:)]) {
        [self.processDelegate onError:self response:response];
    }
}

-(BOOL)isAborted {
    return self.status == ABORTED;
}

-(BOOL)isCompleted {
    return self.status == FINISHED;
}

-(void)reset {
    _status = CREATED;
}

-(void)start {
    if (![self isAborted]) {
        if (self.status == CREATED) {
            _status = STARTED;
            return;
        }
    }
    [NSException raise:@"Invalid State" format:@"Process is started. Use a new object."];
}

@end
