//
//  SplitScheduleFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/20/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "ScheduleFilter.h"
#import "BookedProduct.h"

@interface SplitScheduleFilter : ScheduleFilter


@property (nonatomic) BOOL isQueryChilds;
@property (nonatomic) long step;
@property (nonatomic) long duration;
@property (nonatomic) long round;
@property (nonatomic) NSArray<BookedProduct*>* products;

@end
