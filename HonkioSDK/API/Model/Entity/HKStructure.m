//
//  HKStructure.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKStructure.h"

@implementation HKStructureProperty

-(NSDictionary*) toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    params[@"type"] = self.type;
    params[@"name"] = self.name;
    params[@"is_required"] = [NSNumber numberWithBool: self.isRequired];
    params[@"title"] = self.title;

    return params;
}

@end

@implementation HKStructureNumericProperty

-(NSDictionary*) toParams {
    NSMutableDictionary* params = (NSMutableDictionary*) [super toParams];
    
    params[@"min"] = [NSNumber numberWithDouble: self.min];
    params[@"max"] = [NSNumber numberWithDouble: self.max];
    
    return params;
}

@end

@implementation HKStructureArrayProperty

-(NSDictionary*) toParams {
    NSMutableDictionary* params = (NSMutableDictionary*) [super toParams];
    
    if (self.subtype != nil) {
        params[@"subtype"] = [self.subtype toParams];
    }
    
    return params;
}

@end

@implementation HKStructureEnumPropertyValue

-(id) initWithProps:(NSDictionary*)props {
    self = [super init];
    if (self) {
        if ([props isKindOfClass:NSMutableDictionary.class]) {
            _props = (NSMutableDictionary*) props;
        }
        else {
            _props = [NSMutableDictionary dictionaryWithDictionary:props];
        }
    }
    return self;
}

-(NSString*) name {
    return _props[@"name"];
}

-(void) setName:(NSString *)name {
    _props[@"name"] = name;
}

-(NSString*) value {
    return _props[@"value"];
}

-(void) setValue:(NSString *)value {
    _props[@"value"] = value;
}

-(NSDictionary*) toParams {
    return _props;
}

@end

@implementation HKStructureEnumProperty

-(HKStructureEnumPropertyValue*)find:(NSString*)value {
    if (!self.values || !value)
        return nil;
    
    for (HKStructureEnumPropertyValue* item in self.values) {
        if([value isEqualToString: [item value]])
            return item;
    }
    
    return nil;
}

-(NSDictionary*) toParams {
    NSMutableDictionary* params = (NSMutableDictionary*) [super toParams];
    
    if (self.values != nil) {
        NSMutableArray* array = [NSMutableArray new];
        for (HKStructureEnumPropertyValue* value in self.values) {
            [array addObject:[value toParams]];
        }
        
        params[@"values"] = array;
    }
    
    return params;
}

@end

@implementation HKStructure

-(id)init {
    self = [super init];
    return self;
}
-(id)initWithId:(NSString*) objectId {
    self = [super init];
    if (self) {
        _objectId = objectId;
    }
    return self;
}

-(nullable HKStructureEnumPropertyValue *)findEnumPropertyValue:(nonnull NSString *)propertyName value:(nonnull NSString *)value {
    HKStructureProperty *property = self.properties[propertyName];
    if ([property isKindOfClass:[HKStructureEnumProperty class]]) {
        return [((HKStructureEnumProperty *)property) find:value];
    } else if ([property isKindOfClass:[HKStructureArrayProperty class]]) {
        HKStructureProperty *supProperty = ((HKStructureArrayProperty *)property).subtype;
        if ([supProperty isKindOfClass:[HKStructureEnumProperty class]]) {
            return [((HKStructureEnumProperty *)supProperty) find:value];
        }
    }
    return nil;
}

-(NSDictionary*) toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    params[@"id"] = self.objectId;
    params[@"name"] = self.name;
    
    NSMutableDictionary* propertiesMap = [NSMutableDictionary new];
    for (HKStructureProperty* property in self.properties.allValues) {
        propertiesMap[property.name] = [property toParams];
    }
    
    params[@"properties"] = propertiesMap;
    
    return params;
}

@end
