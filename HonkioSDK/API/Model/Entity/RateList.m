//
//  RateList.m
//  HonkioSDK
//
//  Created by Mikhail Li on 07/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "RateList.h"
#import "Rate.h"
#import <Math.h>

@implementation RateList

@synthesize list;

-(int) getAverageRate {
    if (self.list.count > 0) {
        int sumCount = 0;
        int sumValue = 0;
        for (Rate *rate in self.list) {
            sumValue = sumValue + rate.value * rate.count;
            sumCount = sumCount + rate.count;
        }
        return (int)round(sumValue / sumCount);
    } else {
        return 0;
    }
}

@end
