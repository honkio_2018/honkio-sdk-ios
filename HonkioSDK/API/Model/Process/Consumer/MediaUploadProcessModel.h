//
//  MediaUploadProcessModel.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseProcessModel.h"

@interface MediaUploadProcessModel : BaseProcessModel

-(id)initWithObject:(NSString*)object type:(NSString*)type filePath:(NSString*)path;
-(id)initWithObject:(NSString*)object type:(NSString*)type data:(NSData*)data mimeType:(NSString*)mimeType;

@end
