//
//  PushesListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/18/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "PushesListJsonParser.h"
#import "PushParser.h"
#import "ApiDateFormatter.h"
#import "PushesList.h"

@implementation PushesListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    PushesList* pushesList = [PushesList new];
    
    NSMutableArray* list = [NSMutableArray new];
    
    NSArray* pushesArray = dict[@"notifications"];
    if (pushesArray) {
        for (NSDictionary* pushDict in pushesArray) {
            if([pushDict isKindOfClass:[NSDictionary class]]) {
                Push* push = [PushParser parse:pushDict[@"content"]];
                if (push) {
                    push.pushId = pushDict[@"id"];
                    push.message = pushDict[@"subject"];
                    push.isReaded = [pushDict[@"read"] boolValue];
                    push.date = [ApiDateFormatter dateFromServerString:pushDict[@"timestamp"]];
                    [list addObject:push];
                }
            }
        }
    }
    
    pushesList.totalCount = [dict[@"notifications_total"] longValue];
    pushesList.unreadCount = [dict[@"notifications_unread"] longValue];
    pushesList.list = list;
    
    return pushesList;
}

@end
