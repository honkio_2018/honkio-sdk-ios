//
//  Rate.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/3/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "Rate.h"

@implementation Rate

-(id)initWithValue:(int)value andCount:(int)count {
    self = [super init];
    if (self) {
        self.value = value;
        self.count = count;
    }
    return self;
}

+(int)positiveRatesCount:(NSArray*) ratings {
    int total = 0;
    if (ratings) {
        for (Rate* rate in ratings){
            if (rate.value > 0) {
                total += rate.count;
            }
        }
    }
    return total;
}

+(int)negativeRatesCount:(NSArray*) ratings {
    int total = 0;
    if (ratings) {
        for (Rate* rate in ratings){
            if (rate.value < 0) {
                total += rate.count;
            }
        }
    }
    return total;
}

@end
