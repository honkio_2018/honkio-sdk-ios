//
//  HKUserAccountSelectionVCStrategyDelegate.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/29/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

public protocol HKUserAccountSelectionVCStrategyDelegate {

    func acountDidSelect(_ controller: HKUserAccountSelectionVCStrategy, account: UserAccount?)
    
}
