//
//  StructureJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "HKStructure.h"

@interface StructureJsonParser : BaseJsonResponseParser

+(HKStructure*)toStructure:(NSDictionary*)structureDict;
+(NSDictionary<NSString*, HKStructureProperty*>*)toPropertyiesDict:(NSDictionary*)dict;
+(HKStructureProperty*)toProperty:(NSDictionary*)propertyDict;

@end
