//
//  AccountCreationProcessModel.h
//  HonkioApi
//
//  Created by Shurygin Denis on 8/3/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "BaseProcessModel.h"
#import "ApiResponseHandler.h"
#import "UserAccount.h"
#import "Shop.h"

/**
 * Process model that implement logic of User payment account creation. Not tested!!!!
 */
@interface AccountCreationProcessModel : BaseProcessModel

/** True if account should be checked via payment request, false otherwise. */
@property (nonatomic) BOOL isPaymentCheckEnabled;

/**
 * Init Model with specified handler and account.
 * @param account new UserAccount instance.
 * @param shop Shop that should be used in account creation.
 * @param handler ResponseHandler implementation.
 */
-(id)initWithAccount:(UserAccount*)account shop:(Shop*)shop handler:(ResponseHandler)handler;

/**
 * Async check if transaction status changed from PENDING to another.<br>
 * Result will returned thry onPending.
 */
-(void)checkStatus;

/**
 * Gets pending transaction Id. If transaction status isn't Pending the nil will be returned.
 * @return Pending transaction Id or nil if transaction status isn't Pending.
 */
-(NSString*)pendingQueryId;

@end
