//
//  HKBaseMediaPagesViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/18/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

open class HKBaseMediaPagesViewController: UIPageViewController, UIPageViewControllerDataSource, HKMediaPagesViewDelegate {
    
    open var mediaList: [HKMediaFile]? {
        didSet {
            if let list = self.mediaList {
                self.orderedViewControllers = list.map {[unowned self] (mediaFile) -> UIViewController in
                    return self.viewControllerFor(mediaFile)
                }
            }
            else {
                self.orderedViewControllers = nil
            }
        }
    }
    
    open var initialPage: Int = 0 {
        didSet {
            showPage(self.initialPage)
        }
    }
    
    @IBOutlet open var pageViewController: UIPageViewController!
    
    private var orderedViewControllers: [UIViewController]?
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
    }

    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = self.orderedViewControllers?.index(of: viewController) else {
            return nil
        }

        let previousIndex = viewControllerIndex - 1

        guard previousIndex >= 0 else {
            return nil
        }

        guard self.orderedViewControllers!.count > previousIndex else {
            return nil
        }

        return self.orderedViewControllers![previousIndex]
    }

    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = self.orderedViewControllers?.index(of: viewController) else {
            return nil
        }

        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = self.orderedViewControllers!.count

        guard orderedViewControllersCount != nextIndex else {
            return nil
        }

        guard orderedViewControllersCount > nextIndex else {
            return nil
        }

        return self.orderedViewControllers![nextIndex]
    }
    
    open func viewControllerFor(_ mediaFile: HKMediaFile) -> UIViewController {
        preconditionFailure("Method viewControllerFor must be overridden")
    }
    
    private func showPage(_ index: Int) {
        if self.orderedViewControllers == nil {
            return
        }
        
        var pageIndex = index
        if pageIndex >= self.orderedViewControllers?.count ?? 0 {
            pageIndex = 0
        }
        
        if let selectedViewController = self.orderedViewControllers?[pageIndex] {
            setViewControllers([selectedViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
}
