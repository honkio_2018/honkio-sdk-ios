//
//  ShopProducts.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "ShopProducts.h"

@implementation ShopProducts


-(Product*)findProductById:(NSString*)productId {
    if (!productId)
        return nil;

    for (Product* product in self.list) {
        if ([productId isEqualToString:[product productId]]) {
            return product;
        }
    }
    
    return nil;
}

@end
