//
//  BasePushEntity.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/19/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BasePushEntity : NSObject

@property (nonatomic, readonly) NSString* type;
@property (nonatomic, readonly) NSString* uid;

-(id)initWithType:(NSString*)type;
-(id)initWithType:(NSString*)type andContent:(NSDictionary*)content;

-(void)setContent:(NSString*)key value:(NSObject*)value;
-(NSObject*)getContent:(NSString*)key;

-(NSString*) value1;
-(NSString*) value2;

@end
