//
//  CalendarScheduleJsonParser.h
//  HonkioSDK
//
//  Created by Mikhail Li on 27/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"

@class CalendarSchedule;

@interface CalendarScheduleJsonParser : BaseJsonResponseParser

-(CalendarSchedule *)parseDictionary:(Message*)message dict:(NSDictionary*)dict;

@end
