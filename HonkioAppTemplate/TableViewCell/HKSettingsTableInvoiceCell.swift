//
//  HKSettingsTableInvoiceCell.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/1/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

// TODO docs
open class HKSettingsTableInvoiceCell: HKSettingsTableViewCell {
    
    @IBOutlet open var switchMerchantInvoice: UISwitch!
    
    override open func setupSubviews() {
        super.setupSubviews()
        
        self.switchMerchantInvoice = makeSwitch()
        addSubview(self.switchMerchantInvoice)
    }
    
    override open func setupConstraints() {
        super.setupConstraints()
        
        let views : [String : AnyObject] = [
            "iconView" : icon,
            "titleLabel" : titleLabel,
            "switchMerchantInvoice" : switchMerchantInvoice
        ]
        
        let formatArray : [String] = [
            "V:|-5-[iconView(30)]-5-|",
            "V:|-5-[titleLabel]-5-|",
            "V:|-5-[switchMerchantInvoice]-5-|",
            "H:|-16-[iconView(30)]-10-[titleLabel]-8-[switchMerchantInvoice]-16-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0),
            metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    
    fileprivate func makeSwitch() -> UISwitch {
        let view : UISwitch = UISwitch()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view;
    }
}
