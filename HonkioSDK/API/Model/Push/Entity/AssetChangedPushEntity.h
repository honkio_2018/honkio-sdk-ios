//
//  AssetChangedPushEntity.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/18/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasePushEntity.h"

#define PUSH_ENTITY_ASSET_CAHNGED_ACTION_CREATED @"created"
#define PUSH_ENTITY_ASSET_CAHNGED_ACTION_UPDATED @"updated"
#define PUSH_ENTITY_ASSET_CAHNGED_ACTION_REMOVED @"removed"

@interface AssetChangedPushEntity : BasePushEntity

@property (nonatomic, nullable) NSString *assetId;
@property (nonatomic, nullable) NSString *assetName;
@property (nonatomic, nullable) NSString *structureId;
@property (nonatomic, nullable) NSString *merchantId;
@property (nonatomic, nullable) NSString *action;

-(id)initWithContent:(NSDictionary*)content;

@end
