//
//  MediaFileFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/13/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "MediaFileFilter.h"

@implementation MediaFileFilter

- (void)fillParams:(NSMutableDictionary *)params {
    [super fillParams:params];
    
    [AbsFilter applyIfNotNil:@"query_type" value:self.mediaType forParams:params];
    [AbsFilter applyIfNotNil:@"object" value:self.objectId forParams:params];
    [AbsFilter applyIfNotNil:@"access" value:self.access forParams:params];
}

@end
