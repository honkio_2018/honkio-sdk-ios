//
//  ShopFind.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShopFilter.h"
#import "Shop.h"

/**
 * Data object that hold list of shops that was get by {@link Message.Command#SHOP_FIND} command.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface ShopFind : NSObject

/** Filter that was used to get shops list. */
@property (nonatomic) ShopFilter* filter;

/** List of the found Shops. */
@property (nonatomic) NSArray<Shop*>* list;

@end
