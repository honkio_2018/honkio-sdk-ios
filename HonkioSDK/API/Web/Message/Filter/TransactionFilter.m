//
//  TransactionsFilter.m
//  HonkioApi
//
//  Created by Shurygin Denis on 8/11/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import "TransactionFilter.h"
#import "ApiDateFormatTer.h"

@implementation TransactionFilter

- (id)init
{
    self = [super init];
    if (self) {
        self.shopFilter = [[ShopFilter alloc ] init];
    }
    return self;
}

- (id)initWithShopFilter:(ShopFilter*) shopFilter
{
    self = [super init];
    if (self) {
        if (shopFilter)
            self.shopFilter = shopFilter;
        else
            self.shopFilter = [[ShopFilter alloc ] init];
    }
    return self;
}

-(void)fillParams:(NSMutableDictionary*)params {
    if (self.shopFilter)
        [self.shopFilter fillParams:params];
    
    if(self.onlyValid)
        [AbsFilter applyIfNotNil:@"query_valid" value:@"true" forParams:params];
    
    if ([self isQueryOrderDetails])
        [AbsFilter applyIfNotNil:@"query_unwind_order" value:[NSNumber numberWithBool:YES] forParams:params];
    
    if (self.startDate)
        [AbsFilter applyIfNotNil:MSG_PARAM_QUERY_START_DATE value:[ApiDateFormatter serverStringFromDate:self.startDate] forParams:params];
    
    if (self.endDate)
        [AbsFilter applyIfNotNil:MSG_PARAM_QUERY_END_DATE value:[ApiDateFormatter serverStringFromDate:self.endDate] forParams:params];
    
    [AbsFilter applyIfNotNil:@"query_timestamp" value:self.timeStamp forParams:params];
    [AbsFilter applyIfNotNil:@"query_order" value:self.orderId forParams:params];
    [AbsFilter applyIfNotNil:@"query_id" value:self.queryId forParams:params];
    [AbsFilter applyIfNotNil:@"query_type" value:self.type forParams:params];
    [AbsFilter applyIfNotNil:@"query_account_type" value:self.accountType forParams:params];
    [AbsFilter applyIfNotNil:@"query_status" value:self.status forParams:params];
    
    [super fillParams:params];
}

@end
