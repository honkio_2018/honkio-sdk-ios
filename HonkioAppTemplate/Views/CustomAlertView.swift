//
//  CustomAlertView.swift
//  HonkioSDK
//
//  Created by developer on 1/26/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

/// The struct to be used for collecting alert button text and action.

public struct AlertViewButton {
    /// Alert button text.
    var title : String
    /// Action to be run when the button has been pressed.
    var action: (() -> Void)?
}

/// The class represents alert manager to simplify usage of custom alerts.

open class CustomAlertView: NSObject, UIAlertViewDelegate {

    var alerts = [CustomAlertView]()
    
    var title : String?
    
    var message: String
    
    var cancelButton : AlertViewButton!
    
    var otherButtons = [AlertViewButton]()

    /**
    Constructs the instance within the provided NSError and Action objects.
    Will have predefined button text "dlg_unknown_error_title".localized.
    If no NSError object provided or localized error description was no found in localized resources,
    the alert message will have predefined text "dlg_unknown_error_message".localized.
    - parameter error: NSError object to be used for description of the error.
    - parameter okAction: The action object to be run on alert button pressed.
    */
    public convenience init(error: NSError?, okAction: (() -> Void)?) {
        let title = "dlg_unknown_error_title".localized
        var message = "dlg_unknown_error_message".localized
        
        if error != nil && error?.localizedDescription != nil && error!.localizedDescription.count > 0 {
            message = error!.localizedDescription
        }
        
        self.init(okTitle: title, okMessage: message, okAction: okAction)
    }
    
    public convenience init(apiError: ApiError?, okAction: (() -> Void)?) {
        let title = "dlg_unknown_error_title".localized
        var message = "dlg_unknown_error_message".localized
        
        if apiError != nil && apiError!.desc != nil && apiError!.desc.count > 0 {
            message = apiError!.desc
        }
        
        self.init(okTitle: title, okMessage: message, okAction: okAction)
    }

    /**
    Constructs the instance within the provided title, message, button text and action.
    - parameter title: The title of the alert view.
    - parameter message: The the text of the alert message.
    - parameter cancelButtonTitle: The text to be used for button.
    - parameter cancelAction: The action to be run when button pressed.
    */
    public init(title: String?, message: String, cancelButtonTitle: String, cancelButtonAction: (() -> Void)?) {
        
        self.title = title
        self.message = message
        self.cancelButton = AlertViewButton(title: cancelButtonTitle, action: cancelButtonAction)

        super.init()
    }

    /**
    Constructs the instance within the provided title, message and action.
    The text "dlg_common_button_ok".localized will be used for button.
    - parameter title: The title of the alert view.
    - parameter okMessage: The the text of the alert message.
    - parameter okAction: The action to be run when button pressed.
    */
    public convenience init(okTitle: String?, okMessage: String, okAction: (() -> Void)?) {

        self.init(title: okTitle, message: okMessage, cancelButtonTitle: "dlg_common_button_ok".localized, cancelButtonAction: okAction)
    }

    /**
    Creates and adds the new AlertViewButton struct into otherButtons list of the alert view.
    - parameter title: The title of the alert view.
    - parameter okAction: The action to be run when button pressed.
    */
    open func addButtonWithTitle(_ title: String, action: @escaping () -> Void) -> CustomAlertView {
        
        let button = AlertViewButton(title: title, action: action)
        otherButtons.append(button)
        
        return self
    }

    /**
    Creates the new UIAlertView object within the title, message and action provided
    on construction of CustomAlertView. Appends additional alert buttons into alert view if such buttons were defined.
    Shows the alert view.
    */
    open func show() {
        
        let alertView = UIAlertView(title: self.title, message: self.message, delegate: self, cancelButtonTitle: self.cancelButton.title)
        
        for button in otherButtons {
            
            alertView.addButton(withTitle: button.title)
        }
        
        alerts.append(self)
        alertView.show()
    }

    /**
    UIAlertViewDelegate
    Will be called when user clicks the button on alert view.
    Action will be performed if it was attached to a clicked button.
    - parameter alertView: The UIAlertView object the button has been pressed on.
    - parameter buttonIndex: The index of the pressed button, which is used for finding and running corresponding action.
    */
    open func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        
        if buttonIndex == 0 {
            
            cancelButton.action?()
            return
        }
        
        if otherButtons.count <= buttonIndex {
            
            let button = otherButtons[buttonIndex - 1]
            button.action?()
        }
        
        // remove from holding array
        if let index = alerts.index(of: self) {
            
            alerts.remove(at: index)
        }
    }
}
