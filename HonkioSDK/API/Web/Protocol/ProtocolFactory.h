//
//  ProtocolFactory.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "RequestBuilder.h"
#import "ResponseParser.h"

/**
 * A factory that creates a builders for requests and responses.
 */
@protocol ProtocolFactory <NSObject>

/**
 * Gets new RequestBuilder.
 * @return New RequestBuilder.
 */
-(id<RequestBuilder>)newRequestBuilder;

/**
 * Gets new ResponseParser that parse responses into provided response data class.
 * @return New ResponseParser that parse responses into provided response data class.
 */
-(id<ResponseParser>)newParser:(Class)clazz;

@end
