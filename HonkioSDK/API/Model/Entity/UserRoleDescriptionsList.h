//
//  UserRoleDescriptionsList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/5/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserRoleDescription.h"

// TODO write docs
@interface UserRoleDescriptionsList : NSObject

@property (nonatomic, strong) NSArray<UserRoleDescription*>*  list;

@end
