//
//  InventoryProduct.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BoughtProduct.h"

//TODO write docs
@interface InventoryProduct : BoughtProduct

// TODO write docs
-(id)initFrom:(InventoryProduct*) product;

@property (nonatomic, strong) NSString* inventoryId;

/** Start of validity period. */
@property (nonatomic, strong) NSDate* stamp;

@end
