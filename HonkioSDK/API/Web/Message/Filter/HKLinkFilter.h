//
//  HKLinkFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsListFilter.h"
#import "HKSortFilter.h"

@interface HKLinkFilter : AbsListFilter

@property (nonatomic, nullable) NSString* type;

@property (nonatomic, nullable) NSString* fromObject;
@property (nonatomic, nullable) NSString* fromType;

@property (nonatomic, nullable) NSString* toObject;
@property (nonatomic, nullable) NSString* toType;

@property (nonatomic, nullable) HKSortFilter* sortFilter;

@end
