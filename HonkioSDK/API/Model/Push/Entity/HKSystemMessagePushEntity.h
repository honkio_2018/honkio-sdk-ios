//
//  HKSystemMessagePushEntity.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/12/19.
//  Copyright © 2019 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasePushEntity.h"

@interface HKSystemMessagePushEntity : BasePushEntity

@property (nonatomic, nullable) NSString *subject;
@property (nonatomic, nullable) NSString *message;

-(id)initWithContent:(NSDictionary*)content;

@end
