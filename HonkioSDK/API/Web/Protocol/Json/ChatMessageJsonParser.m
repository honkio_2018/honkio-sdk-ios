//
//  ChatMessageJsonParser.m
//  HonkioSDK
//
//  Created by Dev on 01.09.16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "ChatMessageJsonParser.h"
#import "ApiDateFormatter.h"

@implementation ChatMessageJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [ChatMessageJsonParser parseChatMessage:dict];
}

+(ChatMessage*)parseChatMessage:(NSDictionary*)dict {
    
    ChatMessage *chatMessage = [ChatMessage new];
    chatMessage.senderId = dict[@"sender_id"];
    chatMessage.senderFirstName = dict[@"sender_first_name"];
    chatMessage.senderLastName = dict[@"sender_last_name"];
    chatMessage.senderRole = dict[@"sender_role_id"];
    chatMessage.receiverId = dict[@"receiver_id"];
    chatMessage.receiverName = dict[@"receiver_name"];
    chatMessage.receiverRole = dict[@"receiver_role_id"];
    chatMessage.orderId = dict[@"order_id"];
    chatMessage.orderOwner = dict[@"order_owner"];
    chatMessage.orderThirdPerson = dict[@"order_third_person"];
    chatMessage.text = dict[@"text"];
    chatMessage.time = [ApiDateFormatter dateFromServerString:dict[@"timestamp"]];
    chatMessage.shopId = dict[@"shop_id"];
    chatMessage.shopName = dict[@"shop_name"];

    return chatMessage;
}

@end
