//
//  TransactionsJsonParser.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/26/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "BaseJsonResponseParser.h"

@interface TransactionsJsonParser : BaseJsonResponseParser

@end
