//
//  ChatMessage.m
//  HonkioSDK
//
//  Created by Dev on 01.09.16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "ChatMessage.h"

@implementation ChatMessage

// TODO write docs
- (instancetype)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

// TODO write docs
-(instancetype)initFrom:(ChatMessage*)message {
    self = [super init];
    
    if (self) {
        self.senderId = message.senderId;
        self.senderFirstName = message.senderFirstName;
        self.senderLastName = message.senderLastName;
        self.senderRole = message.senderRole;
        self.receiverId = message.receiverId;
        self.receiverName = message.receiverName;
        self.receiverRole = message.receiverRole;
        self.orderId = message.orderId;
        self.text = message.text;
        self.time = message.time;
    }
    
    return self;
}

-(NSDictionary*)toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [AbsFilter applyIfNotNil:@"sender_role_id" value:self.senderRole forParams:params];
    [AbsFilter applyIfNotNil:@"receiver_id" value:self.receiverId forParams:params];
    [AbsFilter applyIfNotNil:@"receiver_role_id" value:self.receiverRole forParams:params];
    [AbsFilter applyIfNotNil:@"order_id" value:self.orderId forParams:params];
    [AbsFilter applyIfNotNil:@"text" value:self.text forParams:params];

    return params;
}

-(NSString*) senderName {
    if (self.senderFirstName == nil && self.senderLastName == nil)
        return self.shopName;
    
    return [[NSString alloc] initWithFormat:@"%@ %@", self.senderFirstName, self.senderLastName];
}

@end
