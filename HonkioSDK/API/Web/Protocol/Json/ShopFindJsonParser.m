//
//  ShopFindJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "ShopFindJsonParser.h"
#import "ShopFind.h"
#import "Shop.h"

@implementation ShopFindJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    ShopFind* shopFind = [ShopFind new];
    
    NSMutableArray<Shop*>* list = [NSMutableArray new];
    [ShopFindJsonParser parseItems:list fromArray:dict[@"product"] type:SHOP_TYPE_PRODUCT];
    [ShopFindJsonParser parseItems:list fromArray:dict[@"payment"] type:SHOP_TYPE_PAYMENT];
    
    shopFind.list = list;
    
    return shopFind;
}

+(void)parseItems:(NSMutableArray<Shop*>*)list fromArray:(NSArray*)array type:(NSString*)type {
    if (array && [array count] > 0) {
        for (NSDictionary* itemDict in array) {
            Shop* shop = [Shop new];
            [BaseJsonResponseParser parseShop:shop dict:itemDict];
            if (shop) {
                shop.password = itemDict[@"password"];
                shop.type = type;
                
                [list addObject:shop];
            }
        }
    }
}

@end
