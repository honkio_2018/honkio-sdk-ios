//
//  HKPaymentVCStrategyCallback.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 11/21/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

public protocol HKPaymentVCStrategyCallback {

    func paymentStateDidChange(_ strategy: HKPaymentVCStrategy, state: HKPaymentVCStrategy.State)
}
