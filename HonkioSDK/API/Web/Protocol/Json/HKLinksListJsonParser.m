//
//  HKLinksListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKLinksListJsonParser.h"
#import "HKLinkJsonParser.h"
#import "AssetJsonParser.h"
#import "HKLinksList.h"
#import "HKLink.h"

@implementation HKLinksListJsonParser {
    HKLinkJsonParser* linkParser;
}

-(id) init {
    self = [super init];
    
    linkParser = [HKLinkJsonParser new];
    
    return self;
}

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    NSMutableArray* array = [NSMutableArray new];
    NSMutableDictionary* assetsMap = [NSMutableDictionary new];
    
    NSArray* linksJson = dict[@"links"];
    if (linksJson) {
        for (NSDictionary* linkDict in linksJson) {
            [array addObject:[linkParser parseDictionary:message dict:linkDict]];
        }
    }
    
    NSDictionary* assetsDict = dict[@"asset"];
    if (assetsDict) {
        for (NSString* assetId in assetsDict.allKeys) {
            NSDictionary* assetDict = assetsDict[assetId];
            
            if (assetDict) {
                HKAsset* asset = [AssetJsonParser parseAsset:assetDict];
                asset.assetId = assetId;
                assetsMap[assetId] = asset;
            }
        }
    }
    
    HKLinksList* linksList = [HKLinksList new];
    linksList.list = array;
    linksList.assets = assetsMap;

    return linksList;
}


@end
