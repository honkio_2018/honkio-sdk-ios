//
//  AbsListFilter.m
//  HonkioApi
//
//  Created by Shurygin Denis on 12/3/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "AbsListFilter.h"

@implementation AbsListFilter

-(NSDictionary*) toParams {
    NSMutableDictionary* result = [[NSMutableDictionary alloc] initWithCapacity:3];
    
    [self fillParams:result];
    
    return result;
}

-(void)fillParams:(NSMutableDictionary*)params {
    if(self.queryCount > 0)
        [AbsFilter applyIfNotNil:@"query_count" value:[NSNumber numberWithInt:self.queryCount] forParams:params];
    if(self.querySkip > 0)
        [AbsFilter applyIfNotNil:@"query_skip" value:[NSNumber numberWithInt:self.querySkip] forParams:params];
    if (self.queryId)
        [AbsFilter applyIfNotNil:@"query_id" value:self.queryId forParams:params];
}

@end
