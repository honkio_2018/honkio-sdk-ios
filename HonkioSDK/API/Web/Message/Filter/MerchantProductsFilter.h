//
//  MerchantProductsFilter.h
//  HonkioSDK
//
//  Created by Mikhail Li on 20/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "AbsListFilter.h"

@interface MerchantProductsFilter : AbsListFilter

@property (nonatomic) NSString *merchantId;

@end
