//
//  Task.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

/**
 * Protocol that represent a some task that can be aborted.
 * Method abort doesn't guarantees immediately abortion of the task, some tasks doesn't allow
 * abortion after switching to special stage. For correct behaviour see concrete realisation.
 */
@protocol Task <NSObject>

/** Abortion of the task. Method doesn't guarantees abortion of the task. */
-(void)abort;

/**
 * Check if task is aborted.
 * @return True if task is aborted, false otherwise.
 */
-(BOOL)isAborted;

/**
 * Check if task is completed.
 * @return True if task is completed, false otherwise.
 */
-(BOOL)isCompleted;

@end
