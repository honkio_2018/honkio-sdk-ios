//
//  Invitation.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 11/29/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Invitation : NSObject

@property (nonatomic) double bonus;
@property (nonatomic, strong) NSString* message;

@end
