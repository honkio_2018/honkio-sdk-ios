//
//  HKBaseUserAccountsTableViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/10/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/// The base ViewController for showing list of payment accounts.

open class HKBaseUserAccountsTableViewController: HKBaseTableViewController {
    
    @IBOutlet open var tableView: UITableView!

    /// Runs on viewDidLoad is received. Notifies life cycle observer about viewDidLoad by calling super class
    /// viewDidLoad.
    /// Sets the height of table row and estimated height of row.
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    /**
    Runs on viewWillAppear is received. Notifies life cycle observer about viewDidLoad by calling super class
    viewWillAppear.
    Reload payment account data for a user from the server.
    - parameter animated: Describes whether the view will appear animated.
    */
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadData()
    }

    /// Reloads payment account data for a user from the server.
    /// Clears data source, reinitializes it and reload data into the table.
    open func reloadData() {
        
        self.itemsSource.removeAll()
        self.initializeDataSource()
        self.tableView.reloadData()
    }

    /**
     Gets the cell indetifier for provided UserAccount.
     * Must be overriden.
     - parameter item: UserAccount.
     - Returns: The identifier of cell.
     */
    open func cellIdentifier(_ item: UserAccount) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }

    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }

    /**
     Build items source.
     - parameter accountsSection: The accounts section that should be used in data source building.
     */
    open func buildSource(_ accountsSection : HKTableViewSection) {
        self.itemsSource.removeAll()
        itemsSource.append(accountsSection)
    }

    /**
    Creates payment accounts collection section and adds payment accounts into it.
    - Returns: Created and filled section.
    */
    open func buildAccountsSection() -> HKTableViewSection{
        
        let section = HKTableViewSection()
        
        for account in self.filter(HonkioApi.activeUser()!.accounts) {
            section.items.append((cellIdentifier(account), account, buildCellBunder()))
        }
        
        return section
    }
    
    open func filter(_ list: [UserAccount]) -> [UserAccount]{
        return list.filter( { $0.accountDescription != nil && $0.typeIs(ACCOUNT_TYPE_CREDITCARD) && !$0.isHiden() })
    }
    
    fileprivate func initializeDataSource() {
        self.buildSource(self.buildAccountsSection())
    }
}
