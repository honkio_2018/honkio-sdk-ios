//
//  TaskWrapper.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"

/**
 * Wrapper for Task.
 */
@interface TaskWrapper : NSObject<Task>

/**
 * Init new wrapper.
 * @param task Task that will be wrapped.
 */
-(id)initWithTask:(id<Task>)task;

/**
 * Sets new wrapped task. If wrapper already aborted new task will aborted to.
 * @param task New task to set.
 */
-(void)set:(id<Task>)task;

/**
 * Gets wrapped task.
 * @return Wrapped task.
 */
-(id<Task>)get;

@end
