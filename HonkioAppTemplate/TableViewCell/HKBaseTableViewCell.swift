//
//  HKBaseTableViewCell.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
The subclass for UITableCell extending it by customization for UI representation.
*/
open class HKBaseTableViewCell: UITableViewCell {
    /**
    Customized initializer for creatin an instance with setting styles.
    - parameter style: The style to be used on creating an instance.
    - parameter reuseIdentifier: The string indentifier for reuse.
    * Runs setupStyle, setupSubviews and setupConstraints.
    */
    // MARK: - Lifecycle
    override public init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupStyle()
        setupSubviews()
        setupConstraints()
    }
    /**
    Customized initializer for creatin an instance with setting styles.
    - parameter coder: The NSCoder to be used on creating an instance.
    * Runs setupStyle, setupSubviews and setupConstraints.
    */
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupStyle()
        setupSubviews()
        setupConstraints()
    }
    
    // MARK: - Init methods
    /**
    The nethod for settin up styles on initializer call.
    */
    open func setupStyle() {
        
    }
    /**
    The nethod for settin up subviews on initializer call.
    */
    open func setupSubviews() {
        
    }
    /**
    The nethod for settin up constraints on initializer call.
    */
    open func setupConstraints() {
        
    }
}

