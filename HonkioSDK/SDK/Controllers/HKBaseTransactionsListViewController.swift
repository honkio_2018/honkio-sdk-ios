//
//  HKBaseTransactionsListViewController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 2/26/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
 The base ViewController that provides base functionality for processing of TransactionList. ViewController adds observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages into NSNotificationCenter to automaticaly refresh list.
 */

open class HKBaseTransactionsListViewController: HKBaseLazyTableViewController {
    /// Represents the Shop identity to be used for requests to the server
    open var shopIdentity : Identity?
    /// Represents the filter to be applied when transaction list is to be loaded.
    open var filter : TransactionFilter?

    /// Adds observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages on loading
    /// controlled view.
    override open func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseTransactionsListViewController.refresh(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_PAYMENT)), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseTransactionsListViewController.refresh(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_SET_ORDER)), object: nil)
    }
    
    override open func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self, name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_PAYMENT)), object: nil)
            NotificationCenter.default.removeObserver(self, name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_SET_ORDER)), object: nil)
        }
    }
    
    @objc func refresh(_ notification : Notification) {
        refresh()
    }

    /**
    Initiates loading of transaction list.
    - parameter responseHandler: The handler to loading transactions list.
    - parameter count: The number of transactions to loaded from server.
    - parameter skip: The number of transactions to be skipped on loading.
    */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        if self.filter == nil {
            self.filter = TransactionFilter()
        }
        self.setupFilter(filter!, count: count, skip: skip)
        
        HonkioApi.userGetTransactions(filter, shop: shopIdentity, flags: 0, handler: responseHandler)
    }

    /**
    - Returns: Loaded list of transactions in response converted to List.
    - parameter response: The Response object to be converted to List.
    */
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! Transactions).list
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! Transaction), listItem, itemBinder))
        }
        return items
    }

    /**
     Gets the cell indetifier for provided Transaction.
     * Must be overriden.
     - parameter item: Transaction.
     - Returns: The identifier of cell.
     */
    open func cellIdentifier(_ item: Transaction) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }

    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }

    /**
    Sets the filter for the transaction list.
    - parameter filter: The filter is to be updated.
    - parameter count: The number of transaction to loaded from server.
    - parameter skip: The number of transaction to be skipped on loading.
    */
    open func setupFilter(_ filter: TransactionFilter, count: Int, skip: Int) {
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
}
