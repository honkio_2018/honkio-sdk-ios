//
//  StructureJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "StructureJsonParser.h"

@implementation StructureJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [StructureJsonParser toStructure:dict];
}

+(HKStructure*)toStructure:(NSDictionary*)structureDict {
    if (structureDict && [structureDict isKindOfClass: [NSDictionary class] ]) {
        HKStructure* structure = [HKStructure new];
        
        structure.objectId = structureDict[@"_id"];
        if (structure.objectId == nil)
            structure.objectId = structureDict[@"id"];
        structure.name = structureDict[@"name"];
        structure.merchantId = structureDict[@"merchant"];
        
        NSDictionary* propertiesDict = structureDict[@"properties"];
        if (propertiesDict && [propertiesDict isKindOfClass:[NSDictionary class]]) {
            structure.properties = [StructureJsonParser toPropertyiesDict:propertiesDict];
        }
        
        return structure;
    }
    else if (structureDict && [structureDict isKindOfClass: [NSString class] ]) {
        return [[HKStructure alloc] initWithId:structureDict];
    }
    return nil;
}

+(NSDictionary<NSString*, HKStructureProperty*>*)toPropertyiesDict:(NSDictionary*) dict {
    NSMutableDictionary<NSString*, HKStructureProperty*>* propertiesDict = [NSMutableDictionary new];
    
    for (NSString* key in [dict allKeys]) {
        NSDictionary* propertyDict = dict[key];
        if (propertyDict)
            propertiesDict[key] = [StructureJsonParser toProperty:propertyDict];
    }
    
    return propertiesDict;
}

+(HKStructureProperty*)toProperty:(NSDictionary*)propertyDict {
    HKStructureProperty* property = nil;
    NSString* type = propertyDict[@"type"] ? propertyDict[@"type"] : HK_STRUCTURE_PROPERTY_TYPE_STRING;
    if ([type isEqualToString: HK_STRUCTURE_PROPERTY_TYPE_INT]) {
        HKStructureNumericProperty* numericProperty = [HKStructureNumericProperty new];
        numericProperty.min = [propertyDict[@"min"] doubleValue];
        numericProperty.max = [propertyDict[@"max"] doubleValue];
        property = numericProperty;
    }
    else if ([type isEqualToString: HK_STRUCTURE_PROPERTY_TYPE_ENUM]) {
        HKStructureEnumProperty* enumProperty = [HKStructureEnumProperty new];
        NSArray* valuesArray = propertyDict[@"values"];
        if (valuesArray) {
            NSMutableArray* values = [NSMutableArray new];
            for (NSDictionary* valueDict in valuesArray) {
                if (valueDict) {
                    [values addObject: [[HKStructureEnumPropertyValue alloc] initWithProps:valueDict]];
                }
            }
            enumProperty.values = [[NSArray alloc] initWithArray: values];
        }
        property = enumProperty;
    }
    else if ([type isEqualToString: HK_STRUCTURE_PROPERTY_TYPE_ARRAY]) {
        HKStructureArrayProperty* arrayProperty = [HKStructureArrayProperty new];
        NSDictionary* subtypeDict = propertyDict[@"subtype"];
        if (subtypeDict) {
            arrayProperty.subtype = [StructureJsonParser toProperty: subtypeDict];
        }
        property = arrayProperty;
    }
    else {
        property = [HKStructureProperty new];
    }
    
    property.type = type;
    property.name = propertyDict[@"name"];
    property.title = propertyDict[@"title"];
    property.isRequired = [propertyDict[@"is_required"] boolValue];
    
    if (!property.title)
        property.title = property.name;
    
    return property;
}

@end
