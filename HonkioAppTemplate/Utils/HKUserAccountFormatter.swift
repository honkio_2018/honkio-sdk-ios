//
//  HKUserAccountFormatter.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 11/30/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit


public protocol HKUserAccountFormatterInstance {
    
    func getAccountName(_ account: UserAccount) -> String
    func getAccountDesc(_ account: UserAccount) -> String
    func getAccountImage(_ account: UserAccount) -> UIImage?
    
    func formatAmount(_ account: UserAccount) -> String
}

open class HKUserAccountFormatterInstanceImpl: HKUserAccountFormatterInstance {
    
    public init() {  }
    
    open func getAccountName(_ account: UserAccount) -> String {
        
        if account.type == nil {
            return "unknown"
        }
        
        switch account.type {
        case ACCOUNT_TYPE_CREDITCARD:
            return "account_name_creditcard".localized
        case ACCOUNT_TYPE_OPERATOR:
            return "account_name_operator".localized
        case ACCOUNT_TYPE_INVOICE:
            return "account_name_merchant_invoice".localized
        case ACCOUNT_TYPE_BONUS:
            return "account_name_bonus".localized
        default:
            return account.type
        }
    }
    
    open func getAccountDesc(_ account: UserAccount) -> String {
        if account.typeIs(ACCOUNT_TYPE_OPERATOR) {
            return "account_desc_operator".localized
        }
        else if account.isHasLeft {
            return String(format: "account_left".localized, self.formatAmount(account))
        }
        
        return account.accountDescription ?? ""
    }
    
    
    open func getAccountImage(_ account: UserAccount) -> UIImage? {
        
        var imageName: String?
        
        if account.type == ACCOUNT_TYPE_CREDITCARD {
            
            let card = account.description
            
            if card.hasPrefix("3") {
                
                imageName = "ic_card_american"
            }
            else if card.hasPrefix("4") {
                
                imageName = "ic_card_visa"
            }
            else if card.hasPrefix("5") {
                
                imageName = "ic_card_master"
            }
            else if card.hasPrefix("6") {
                
                imageName = "ic_card_discover"
            }
            else {
                imageName = "hk_ic_account_card"
            }
        }
        else if account.typeIs(ACCOUNT_TYPE_INVOICE){
            imageName = "hk_ic_account_invoice"
        }
        else if account.typeIs(ACCOUNT_TYPE_OPERATOR){
            imageName = "hk_ic_account_operator"
        }
        else if account.typeIs(ACCOUNT_TYPE_BONUS){
            imageName = "hk_ic_account_bonus"
        }
        
        return imageName != nil ? HKBundleUtils.image(imageName!) : nil
    }
    
    open func formatAmount(_ account: UserAccount) -> String {
        return String(format:"%.2f", account.left)
    }
}

public class HKUserAccountFormatter: NSObject {
    
    private static var _instance: HKUserAccountFormatterInstance!
    private static var instance: HKUserAccountFormatterInstance {
        get {
            if HKUserAccountFormatter._instance == nil {
                HKUserAccountFormatter._instance = HKUserAccountFormatterInstanceImpl()
            }
            return HKUserAccountFormatter._instance
        }
    }
    
    public static func initInstance(_ instance: HKUserAccountFormatterInstance) {
        if HKUserAccountFormatter._instance != nil {
            preconditionFailure("HKUserAccountFormatter instance already initialized")
        }
        
        HKUserAccountFormatter._instance = instance
    }

    public static func getAccountName(_ account: UserAccount) -> String {
        return HKUserAccountFormatter.instance.getAccountName(account)
    }
    
    public static func getAccountDesc(_ account: UserAccount) -> String {
        return HKUserAccountFormatter.instance.getAccountDesc(account)
    }
    
    public static func getAccountImage(_ account: UserAccount) -> UIImage? {
        return HKUserAccountFormatter.instance.getAccountImage(account)
    }
    
    public static func formatAmount(_ account: UserAccount) -> String {
        return HKUserAccountFormatter.instance.formatAmount(account)
    }
}
