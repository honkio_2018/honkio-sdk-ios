//
//  PushFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/18/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "AbsFilter.h"

#define PUSH_FILTER_QUERY_CONTENT_TYPE  @"type"
#define PUSH_FILTER_QUERY_CONTENT_ORDER @"order_id"
#define PUSH_FILTER_QUERY_CONTENT_TRANSACTION @"id"

@interface PushFilter : AbsFilter

-(id) initWithId:(NSString*)pushId;

@property (nonatomic) NSString* pushId;
@property (nonatomic) NSMutableDictionary* conten;

-(PushFilter*)addContent:(NSString*)key value:(NSString*)value;

@end
