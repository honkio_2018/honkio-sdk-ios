//
//  ApiMerchantSimple.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "MerchantSimple.h"
#import "AbsFilter.h"

@implementation MerchantSimple

-(Identity*)registrationShopIdentity {
    if (_registrationShopIdentity) {
        return _registrationShopIdentity;
    }
    return self.loginShopIdentity;
}

-(Identity*)lostPasswordShopIdentity {
    if (_lostPasswordShopIdentity) {
        return _lostPasswordShopIdentity;
    }
    return self.loginShopIdentity;
}

-(BOOL)isAccountSupported:(NSString*)accountType {
    return self.supportedAccounts && [self.supportedAccounts indexOfObject:accountType] != NSNotFound;
}

-(NSDictionary*)toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [AbsFilter applyIfNotNil:@"str_businessid" value:self.businessId forParams:params];
    [AbsFilter applyIfNotNil:@"str_name" value:self.name forParams:params];
    [AbsFilter applyIfNotNil:@"str_support_email" value:self.supportEmail forParams:params];
    [AbsFilter applyIfNotNil:@"str_support_telephone" value:self.supportPhone forParams:params];
    
    return params;
}

- (BOOL)isEqual: (id)other {
    if (self == other) {
        return YES;
    }
    
    if (!other || ![other isKindOfClass: [MerchantSimple class]]) {
        return NO;
    }
            
    MerchantSimple *merchant = other;
    return ([merchant.merchantId isEqualToString:_merchantId] &&
            [merchant.name isEqualToString:_name] &&
            [merchant.businessId isEqualToString:_businessId] &&
            [merchant.supportedAccounts isEqualToArray:_supportedAccounts] &&
            ( (_loginShopIdentity != nil && [_loginShopIdentity isEqual:merchant.loginShopIdentity]) ||
              (_loginShopIdentity == nil && merchant.loginShopIdentity == nil) ) &&
            ( (_registrationShopIdentity != nil && [_registrationShopIdentity isEqual:merchant.registrationShopIdentity]) ||
              (_registrationShopIdentity == nil && merchant.registrationShopIdentity == nil) ) &&
            ( (_lostPasswordShopIdentity != nil && [_lostPasswordShopIdentity isEqual:merchant.lostPasswordShopIdentity]) ||
              (_lostPasswordShopIdentity == nil && merchant.lostPasswordShopIdentity == nil) )
            );
}

@end
