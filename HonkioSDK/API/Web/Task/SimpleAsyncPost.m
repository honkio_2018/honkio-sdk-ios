//
//  SimpleAsyncPost.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/25/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "SimpleAsyncPost.h"

@implementation SimpleAsyncPost {
    NSData* body;
}

-(id)initWithUrl:(NSURL*)requestUrl heders:(NSDictionary*)headers body:(NSData*)bodyData responseHandler:(UrlResponseHandler)responseHandler {
    self = [super initWithUrl:requestUrl heders:headers responseHandler:responseHandler];
    if (self) {
        body = bodyData;
    }
    return self;
}

-(NSMutableURLRequest*)buildRequest {
    NSMutableURLRequest* urlRequest = [self buildRequestForMethod:@"POST"];
    //set body
    if(body)
        [urlRequest setHTTPBody:body];
    return urlRequest;
}

@end
