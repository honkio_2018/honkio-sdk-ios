//
//  PreferredUserAccounts.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/29/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

open class HKPreferredUserAccountsManager: NSObject {
    
    
    private static var _instance: HKPreferredUserAccountsManager!
    
    public static func instance() -> HKPreferredUserAccountsManager {
        if HKPreferredUserAccountsManager._instance == nil {
            HKPreferredUserAccountsManager._instance = HKPreferredUserAccountsManager()
        }
        
        return HKPreferredUserAccountsManager._instance
    }
    
    public private(set) var list: [UserAccount] = []
    
    private var userDefaults: UserDefaults!
    private let ListKey = "list"

    override public init() {
        super.init()
        userDefaults = UserDefaults(suiteName: "PreferredUserAccounts")
        
        if let restoredArray = userDefaults.array(forKey: ListKey) as? [String] {
            if restoredArray.count > 1 {
                for i in 0...(restoredArray.count / 2 - 1) {
                    let type = restoredArray[i * 2]
                    let number = restoredArray[i * 2 + 1]
                    
                    if let account = HonkioApi.activeUser()?.findAccount(byType: type, number: number) {
                        self.list.append(account)
                    }
                }
            }
        }
    }
    
    public func isPreferred(_ account: UserAccount!) -> Bool {
        guard account != nil else {
            return false
        }
        return self.isPreferred(type: account.type, number: account.number)
    }
    
    public func isPreferred(type: String!, number: String!) -> Bool {
        guard type != nil && number != nil else {
            return false
        }
        
        for account in self.list {
            if type == account.type && number == account.number {
                return true
            }
        }
        
        return false
    }
    
    public func add(_ account: UserAccount!) {
        guard account != nil else {
            return
        }
        self.add(type: account.type, number: account.number)
    }
    
    public func add(type: String!, number: String!) {
        guard type != nil && number != nil else {
            return
        }
        
        if self.isPreferred(type: type, number: number) {
            return
        }
        
        var account = HonkioApi.activeUser()?.findAccount(byType: type, number: number)
        if account == nil {
            account = UserAccount(type: type, andNumber: number)
        }
        
        self.list.append(account!)
        self.save()
    }
    
    public func remove(_ account: UserAccount!) {
        guard account != nil else {
            return
        }
        remove(type: account.type, number: account.number)
    }
    
    public func remove(type: String!, number: String!) {
        guard type != nil && number != nil else {
            return
        }
        
        var isRemoved = false
        for i in stride(from: self.list.count - 1, through: 0, by: -1) {
            let account = self.list[i]
            if type == account.type && number == account.number {
                self.list.remove(at: i)
                isRemoved = true
            }
        }
        if isRemoved {
            self.save()
        }
    }
    
    private func save() {
        var arrayToSave: [String] = []
        for account in self.list {
            arrayToSave.append(account.type)
            arrayToSave.append(account.number)
        }
        userDefaults.set(arrayToSave, forKey: ListKey)
    }
}
