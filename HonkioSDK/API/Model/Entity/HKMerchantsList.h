//
//  HKMerchantsList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/20/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SimpleListItem.h"

@interface HKMerchantsList : NSObject

@property (nonatomic) NSArray<SimpleListItem*>* list;

@end
