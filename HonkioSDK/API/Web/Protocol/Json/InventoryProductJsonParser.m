//
//  InventoryProductJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "InventoryProductJsonParser.h"

@implementation InventoryProductJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    InventoryProduct* product = [InventoryProduct new];
    [BaseJsonResponseParser parseInventoryProduct:product dict:dict];
    
    return product;
}


@end
