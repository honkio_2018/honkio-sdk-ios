//
//  TransactionsFilter.h
//  HonkioApi
//
//  Created by Shurygin Denis on 8/11/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ShopFilter.h"
#import "AbsListFilter.h"

#define TRANSACTION_TYPE_BONUS      @"bonus"
#define TRANSACTION_TYPE_PAYMENT    @"payment"
#define TRANSACTION_TYPE_ORDER      @"order"

/**
 * Filter for transactions list.
 */
@interface TransactionFilter : AbsListFilter

/** YES if only valid products should be returned, NO otherwise. */
@property (nonatomic, assign) BOOL onlyValid;
@property (nonatomic, assign) BOOL isQueryOrderDetails;

@property (nonatomic, copy) NSString* type;

//TODO write docs
@property (nonatomic, copy) NSString* orderId;

//TODO write docs
@property (nonatomic, copy) NSString* status;

/** Time stamp filter. */
@property (nonatomic, copy) NSString* timeStamp;

//TODO write docs
@property (nonatomic, copy) NSDate* startDate;
//TODO write docs
@property (nonatomic, copy) NSDate* endDate;

//TODO write docs
@property (nonatomic, copy) NSString* accountType;

/** Shop filter set for this filter. */
@property (nonatomic) ShopFilter* shopFilter;

/**
 * Init TransactionFilter with specified ShopFilter.
 */
- (id)initWithShopFilter:(ShopFilter*) shopFilter;

@end
