//
//  JsonRequestBuilder.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "JsonRequestBuilder.h"
#import "ApiSecurity.h"
#import "HKLog.h"

@implementation JsonRequestBuilder {
    NSMutableDictionary* dict;
}

-(id)init {
    self = [super init];
    if (self) {
        dict = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(void)reset {
    [dict removeAllObjects];
}

-(void)putKey:(NSString*)key value:(NSObject*) value {
    [dict setObject:value forKey:key];
}

-(void)calculateHash:(NSString*)key {
    NSData* jsonDict = [JsonRequestBuilder toJson:dict];
    
    NSError* error = nil;
    NSDictionary* dictForHash = [NSJSONSerialization
                                  JSONObjectWithData:jsonDict
                                  options:NSJSONReadingAllowFragments
                                  error:&error];
    if(error) {
        dictForHash = dict;
    }
    
    dict[@"hash"] = [ApiSecurity calculateHashForKey:key andDictionary:dictForHash];
}

-(NSData*)newEntitiesArray:(NSArray*)values {
    if (values) {
        return [JsonRequestBuilder toJson:values];
    }
    return [JsonRequestBuilder toJson:[[NSArray alloc] init]];
}

-(NSData*)entity {
    return [JsonRequestBuilder toJson:dict];
}

+(NSData*)toJson:(id)object {
    static NSJSONWritingOptions jsonOption =
#ifdef DEBUG
    NSJSONWritingPrettyPrinted;
#else
    0;
#endif
    
    NSError* err = nil;
    NSData* body = [NSJSONSerialization dataWithJSONObject:object options:jsonOption error:&err];
    if(err)
    {
        [HKLog d: [NSString stringWithFormat:@"error parsing request data: %@", (err).description]];
        return nil;
    }
    
    return body;
}

@end
