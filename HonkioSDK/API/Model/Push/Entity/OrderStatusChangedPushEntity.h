//
//  OrderStatusChanged.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/1/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasePushEntity.h"

@interface OrderStatusChangedPushEntity : BasePushEntity

@property (nonatomic) NSString* orderId;
@property (nonatomic) NSString* orderTitle;
@property (nonatomic) NSString* orderStatus;
@property (nonatomic) NSString* orderOwnerId;

@property (nonatomic) NSString* userId;
@property (nonatomic) NSString* userName;

-(id)initWithContent:(NSDictionary*)content;

@end
