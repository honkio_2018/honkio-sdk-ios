//
//  Transaction.h
//  HonkioApi
//
//  Created by Shurygin Denis on 5/12/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BoughtProduct.h"
#import "UserAccount.h"

@class MessageTransaction;

/**
 * Data object that holds information about transaction on the server.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface Transaction : NSObject

@property (nonatomic, strong) NSString* type;

@property (nonatomic, strong) NSString* transaction_id;

@property (nonatomic, strong, nullable) NSString* orderId;

/** Status of the transaction. */
@property (nonatomic, strong) NSString* status;

/** Shop ID that is contain this transaction. */
@property (nonatomic, strong) NSString* shopId;

/** Shop Name that is contain this transaction. */
@property (nonatomic, strong) NSString* shopName;

/** Reference string that was set in the payment request. */
@property (nonatomic, strong) NSString* shopReference;

/** Shop receipt for the Transaction. */
@property (nonatomic, strong) NSString* shopReceipt;

/** Merchant name that is owner of the Shop which contain this Transaction. */
@property (nonatomic, strong) NSString* merchantName;

/** Payment Account that was used for the payment. */
@property (nonatomic, strong) UserAccount* account;

@property (nonatomic) double  amount;
@property (nonatomic, strong) NSString*  currency;

/** Time whet transaction is executed. */
@property (nonatomic, strong) NSDate*    time;

/**  List of the BoughtProduct's. */
@property (nonatomic, strong) NSArray<BoughtProduct*>*   products;

/** MessageTransaction's list attached to the Transaction. */
@property (nonatomic, strong) NSArray<MessageTransaction*>*   messages;

//TODO docs
@property (nonatomic, strong) NSDictionary* accountExtra;

-(NSString*) toString;

@end

/**
 * Data object that holds information about message attached to transaction.
 */
@interface MessageTransaction : NSObject

/** Sender of the message. */
@property (nonatomic, strong) NSString* from;

/** Text of the message. */
@property (nonatomic, strong) NSString* text;

/** Time in millis when message was sent. */
@property (nonatomic, strong) NSDate* timeStamp;

@end
