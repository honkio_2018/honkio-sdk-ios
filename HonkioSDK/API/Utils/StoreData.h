//
//  StoreData.h
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/9/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import <Foundation/Foundation.h>

#define STORE_KEY_LOGIN       @"login"
#define STORE_KEY_PIN         @"saved_pin"

@interface StoreData : NSObject

+(void)saveToStore:(id)data forKey:(NSString*)key;
+(id)loadFromStore:(NSString*)key;
+(void)deleteFromStore:(NSString*)key;

@end