//
//  HKUrlHandler.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 12/5/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

public protocol HKUrlHandler {
    
    func handle(_ url: URL) -> Bool

}
