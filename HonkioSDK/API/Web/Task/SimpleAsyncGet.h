//
//  SimpleAsyncGet.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/25/15.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"
#import "ApiResponseHandler.h"

typedef void (^UrlResponseHandler)(NSURLResponse* __nullable response, NSData* __nullable data, NSError* __nullable connectionError);

/**
 * Task that sends GET requests on the given URL.<br>
 * Returns nil if request successful, error otherwise.
 *  <br><br>
 *  SimpleAsyncGet has Task protocol, so you have opportunity to abort task.
 */
@interface SimpleAsyncGet : NSObject<Task>

/**
 * Init new SimpleAsyncGet.
 * @param requestUrl URL to send GET request.
 * @param headers Headers for the request.
 * @param responseHandler Handler that will receive response.
 */
-(id __nullable)initWithUrl:(NSURL* __nonnull)requestUrl heders:(NSDictionary* __nullable)headers responseHandler:(UrlResponseHandler __nullable)responseHandler;

/**
 * Execute task. Result will returned into SimpleResponseHandler providet in the initialisation method.
 */
-(void)execute;

-(NSMutableURLRequest*  __nonnull)buildRequest;
-(NSMutableURLRequest*  __nonnull)buildRequestForMethod:(NSString*  __nonnull)method;

@end
