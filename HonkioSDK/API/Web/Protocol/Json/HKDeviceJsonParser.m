//
//  HKDeviceJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/12/19.
//  Copyright © 2019 developer. All rights reserved.
//

#import "HKDeviceJsonParser.h"
#import "HKDevice.h"

@implementation HKDeviceJsonParser
    
-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    HKDevice* device = [HKDevice new];
    
    device.device_id  = dict[DEVICE_ID];
    device.app        = dict[DEVICE_APP_KEY];
    device.token      = dict[DEVICE_TOKEN_KEY];
    device.platform   = dict[DEVICE_TYPE_KEY];
    device.name       = dict[DEVICE_NAME_KEY];
    device.notifyType = dict[DEVICE_NOTIFY_KEY];
    
    NSDictionary* properties = dict[@"properties"];
    if (properties) {
        device.manufacturer = properties[DEVICE_MANUFACTURER_KEY];
        device.model        = properties[DEVICE_MODEL_KEY];
        device.osVersion    = properties[DEVICE_OS_VERSION_KEY];
    }
    
    return device;
}

@end
