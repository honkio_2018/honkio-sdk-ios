//
//  MerchantJsonParser.m
//  HonkioSDK
//
//  Created by Mikhail Li on 03/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "MerchantJsonParser.h"
#import "Merchant.h"

@implementation MerchantJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [MerchantJsonParser parseMerchant:dict];
}

+(nullable Merchant *)parseMerchant:(NSDictionary *)dict {
    if (dict) {
        Merchant *merchant = [Merchant new];
        merchant.merchantId = dict[@"id"];
        NSDictionary* merchantJson = dict[@"merchant"];
        if (merchantJson) {
            merchant.name = merchantJson[@"str_name"];
            merchant.businessId = merchantJson[@"str_businessid"];
            merchant.bankIban = merchantJson[@"str_sepa"];
            merchant.bankName = merchantJson[@"str_bankname"];
            merchant.bankSwift = merchantJson[@"str_swift"];
            merchant.vatNumber = merchantJson[@"str_vatnumber"];
            merchant.vat = [merchantJson[@"int_vat"] doubleValue];
            
            merchant.supportEmail = merchantJson[@"str_support_email"];
            merchant.supportPhone = merchantJson[@"str_support_telephone"];
            
            merchant.email = merchantJson[@"str_email"];
            merchant.phone = merchantJson[@"str_telephone"];
            merchant.parentMerchant = merchantJson[@"parent"];
            merchant.isActive = [merchantJson[@"active"] boolValue];
        }
        return merchant;
    }
    return nil;
}


@end
