//
//  ServerInfo.h
//  HonkioApi
//
//  Created by Alexandr Kolganov on 4/21/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Data object that hold information about server.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface ServerInfo : NSObject

/** Version of the server. */
@property (nonatomic) int version;

/**
 *  Supported time zones.
 */
@property (nonatomic) NSArray*  timeZones;

// TODO write docs
@property (nonatomic) NSArray* countries;

/** Available server languages. */
@property (nonatomic) NSArray* languages;
@property (nonatomic) NSDate*  serverDate;

/** URL for SMS verification. */
@property (nonatomic) NSString* verifyURL;

/** URL for Assets. This URl should be used to build url for concrete Asset file. */
@property (nonatomic) NSString* assetsURL;

@property (nonatomic) NSString * userURL;

@property (nonatomic) NSString * oauthAuthorizeURL;
@property (nonatomic) NSString * oauthTokenURL;

@property (nonatomic, assign) double serviceFee;

/** Version of "Terms of Use" file. */
@property (nonatomic, assign) int touVersion;

/**
 * @return Index or not found(-1)
 */
-(int)findCountryByISO:(NSString*)iso;

/**
 * @return Index or not found(-1)
 */
-(int)findCountryByLocale:(NSString*)locale;

/** Suitable language that server support. */
-(NSString*)suitableLanguage;

@end

/**
 * Data object that hold information about Country.
 */
@interface CountryInfo : NSObject

/** Name of the Country on English. */
@property (nonatomic) NSString* name;

/** Name of the Country on Phone Language. */
@property (nonatomic) NSString* nameTranslated;
@property (nonatomic) NSString* locale;
@property (nonatomic) NSString* phonePrefix;
@property (nonatomic) NSString* iso;
@property (nonatomic) NSString* timezone;

@end
