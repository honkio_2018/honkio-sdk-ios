//
//  OrderFilter.h
//  HonkioApi
//
//  Created by Shurygin Denis on 12/2/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "AbsListFilter.h"

@interface OrderFilter : AbsListFilter

@property (nonatomic, copy) NSString *status;
@property (nonatomic) NSArray<NSString*> *statuses;
@property (nonatomic, copy) NSString *owner;
@property (nonatomic, copy) NSString *thirdPerson;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *distance;
@property (nonatomic, copy) NSString *model;
@property (nonatomic, assign) BOOL excludeExpired;
@property (nonatomic, assign) BOOL queryProductDetails;
@property (nonatomic, assign) BOOL queryUnreadMessages;
@property (nonatomic, assign) BOOL childMerchants;
@property (nonatomic, assign, getter=isQueryOnlyCount) BOOL queryOnlyCount;

@property (nonatomic) NSDictionary* custom;

-(void) addCustom:(NSString*)key value:(NSObject*) value;
-(void) removeCustom:(NSString*)key;

@end
