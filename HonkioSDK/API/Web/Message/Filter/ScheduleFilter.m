//
//  ScheduleFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "ScheduleFilter.h"
#import "ApiDateFormatter.h"

@implementation ScheduleFilter

-(NSDictionary*) toParams {
    
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [AbsFilter applyIfNotNil:MSG_PARAM_QUERY_START_DATE value: [ApiDateFormatter serverStringFromDate: self.startDate] forParams:params];
    [AbsFilter applyIfNotNil:MSG_PARAM_QUERY_END_DATE value: [ApiDateFormatter serverStringFromDate: self.endDate] forParams:params];
    [AbsFilter applyIfNotNil:@"query_object" value: self.objectId forParams:params];
    [AbsFilter applyIfNotNil:@"query_object_type" value: self.objectType forParams:params];
    [AbsFilter applyIfNotNil:@"query_type" value: self.queryType forParams:params];
    
    return params;
}

@end
