//
//  JsonProtocolFactory.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProtocolFactory.h"

/**
 * Is a factory for requests and responses in JSON format.
 */
@interface JsonProtocolFactory : NSObject<ProtocolFactory>

@end
