//
//  SimpleListJsonParser.m
//  HonkioSDK
//
//  Created by Mikhail Li on 03/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "SimpleListJsonParser.h"
#import "SimpleListItem.h"

@implementation SimpleListJsonParser

+(NSArray*)parseList:(Message*)message list:(NSArray *)itemList {
    NSMutableArray *list = [NSMutableArray new];
    if (itemList) {
        for (NSDictionary* itemJson in itemList) {
            if (itemJson && [itemJson isKindOfClass:[NSDictionary class]]) {
                NSString *string = [NSString stringWithFormat:@"%@", itemJson];
                SimpleListItem * item = [[SimpleListItem alloc] initWithId:itemJson[@"id"]
                                                                    string:string
                                                                      name:itemJson[@"str_name"]];
                [list addObject:item];
            }
        }
    }
    return list;
}

@end

