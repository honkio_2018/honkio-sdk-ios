//
//  MerchantShopInfo.h
//  HonkioSDK
//
//  Created by Mikhail Li on 21/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MerchantShop;

@interface MerchantShopInfo : NSObject

@property (nonatomic) MerchantShop *shop;
@property (nonatomic, assign) long long timeStamp;

- (BOOL)isEqual:(id)object;

@end
