//
//  Identity.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKModelEntity.h"

/**
 * Data object that hold id/password pair.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface Identity : HKModelEntity

@property NSString* identityId;
@property NSString* password;

/**
 * Identity with specified id and password.
 *
 * @param identityId Identity Id.
 * @param password Identity Password.
 */
-(id)initWithId:(NSString*)identityId andPassword:(NSString*)password;

-(id)initWithFrom:(Identity*)identity;

/**
 * Check if ID or Password is Empty.
 * @return True if ID or Password is Empty, false otherwise.
 */
-(BOOL)isCorupt;

- (BOOL)isEqual: (id)other;

@end
