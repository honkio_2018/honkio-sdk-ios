//
//  HKLoginViewProtocol.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 2/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

public protocol HKLoginViewProtocol: class {
    
    var doLogin : Bool { get set }
    var loginDelegate : HKLoginViewLoginDelegate? { get set }
    
}
