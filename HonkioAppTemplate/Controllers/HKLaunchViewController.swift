//
//  HKLaunchViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 4/25/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation
import UIKit
/**
Manages the UI look on application start.
*/
open class HKLaunchViewController : UINavigationController {

    /**
    Hides navigation bar on start the application. Gets and pushes Login view controller.
    */
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.showLoginScreen()
    }
    
    open func setupView() {
        self.setNavigationBarHidden(true, animated: false)
    }
    
    open func showLoginScreen() {
        if let viewController = AppController.getViewControllerById(AppController.Login, viewController: self) {
            self.pushViewController(viewController, animated: false)
        }
    }
}
