//
//  SimpleListJsonParser.h
//  HonkioSDK
//
//  Created by Mikhail Li on 03/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"

/**
 * Parse List of SimpleListItem's from JSON.
 */
@interface SimpleListJsonParser : BaseJsonResponseParser

+(NSArray*)parseList:(Message*)message list:(NSArray *)itemList;

@end
