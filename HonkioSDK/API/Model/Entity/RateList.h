//
//  RateList.h
//  HonkioSDK
//
//  Created by Mikhail Li on 07/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Rate;

@interface RateList : NSObject

@property (nonatomic) NSArray<Rate *> *list;

-(int) getAverageRate;

@end
