//
//  HKAppleMapsView.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit
import MapKit

open class HKAppleMapsView: NSObject, HKMapViewProtocol, MKMapViewDelegate {

    private weak var mapView: MKMapView!
    private static let MapPinIdentifier = "HKAppleMapsView.MapPin"
    
    public init(_ appleMapsView: MKMapView) {
        super.init()
        self.mapView = appleMapsView
        self.mapView.delegate = self
        
        // Setup taprecognisers
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(HKAppleMapsView.mapDidTap(_:)))
        let tap2Recognizer = UITapGestureRecognizer(target: self, action: #selector(HKAppleMapsView.mapDidDoubleTap(_:)))
        tap2Recognizer.numberOfTapsRequired = 2
        
        tapRecognizer.require(toFail: tap2Recognizer)
        
        self.mapView.addGestureRecognizer(tapRecognizer)
        self.mapView.addGestureRecognizer(tap2Recognizer)
    }

    open var viewController: HKBaseMapViewController? {
        didSet {
            self.mapView(self.mapView, regionDidChangeAnimated: false)
        }
    }
    
    public var showsUserLocation: Bool {
        get {
            return self.mapView.showsUserLocation
        }
        set {
            self.mapView.showsUserLocation = newValue
        }
    }
    
    public var center: CLLocationCoordinate2D {
        get {
            return self.mapView.region.center
        }
    }
    
    public var radius: CLLocationDistance {
        get {
            let span = mapView.region.span
            let center = mapView.region.center
            
            let loc1 = CLLocation(latitude: center.latitude - span.latitudeDelta * 0.5, longitude: center.longitude)
            let loc2 = CLLocation(latitude: center.latitude + span.latitudeDelta * 0.5, longitude: center.longitude)
            
            return loc1.distance(from: loc2)
        }
    }
    
    @IBAction func mapDidTap(_ recognizer: UIGestureRecognizer) {
        if recognizer.state == .recognized {
            let point = recognizer.location(in: self.mapView)
            let coordinate = self.mapView.convert(point, toCoordinateFrom: self.mapView)
            
            self.viewController?.mapDidTap(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
    }
    
    @IBAction func mapDidDoubleTap(_ recognizer: UIGestureRecognizer) {
        if recognizer.state == .recognized {
            let point = recognizer.location(in: self.mapView)
            let coordinate = self.mapView.convert(point, toCoordinateFrom: self.mapView)
            
            self.viewController?.mapDidDoubleTap(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
    }
    
    public func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = self.center
        self.viewController?.cameraDidMove(latitude: center.latitude, longitude: center.longitude, radius: self.radius)
    }
    
    public func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let annotation = (view.annotation as? HKMapAnnotation)?.item {
            self.viewController?.annotationDidSelect(annotation)
        }
    }
    
    public func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        self.viewController?.annotationDidSelect(nil)
    }
    
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let hkAnnotation = annotation as? HKMapAnnotation {
            
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: HKAppleMapsView.MapPinIdentifier)
            
            if annotationView == nil {
                if #available(iOS 11.0, *) {
                    annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: HKAppleMapsView.MapPinIdentifier)
                }
                else {
                    annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: HKAppleMapsView.MapPinIdentifier)
                }
            }
            
            annotationView?.annotation = annotation
            annotationView?.canShowCallout = true
            
            if #available(iOS 11.0, *) {
                annotationView?.displayPriority = .required
            }
        
            if let icon = hkAnnotation.item?.icon {
                annotationView?.image = icon
                annotationView?.centerOffset = CGPoint(x: 0.0, y: -icon.size.height / 2.0)
            }
            
            return annotationView
        }
        
        return nil
    }
    
    public func addAnnotation(_ annotation: HKMapViewAnnotation) {
        self.mapView.addAnnotation(self.buildAnnotation(annotation))
    }
    
    open func removeAllAnnotations() {
        let annotations = self.mapView.annotations
        self.mapView.removeAnnotations(annotations)
    }
    
    open func moveToLocation(_ coordinate: CLLocationCoordinate2D, animated: Bool) {
        var region = self.mapView.region
        region.center = coordinate
        
        if region.span.latitudeDelta > 0.04 || region.span.longitudeDelta > 0.04 {
            region.span = MKCoordinateSpan(latitudeDelta: 0.04, longitudeDelta: 0.04)
        }
        
        self.mapView.setRegion(region, animated: animated)
    }
    
    private func buildAnnotation(_ annotation: HKMapViewAnnotation) -> MKPointAnnotation {
        let pointAnnotation = HKMapAnnotation()
        pointAnnotation.coordinate = annotation.coordinate
        pointAnnotation.title = annotation.title
        pointAnnotation.subtitle = annotation.subtitle
        pointAnnotation.item = annotation
        return pointAnnotation
    }
    
    private class HKMapAnnotation: MKPointAnnotation {
        
        var item: HKMapViewAnnotation?
        
    }
}
