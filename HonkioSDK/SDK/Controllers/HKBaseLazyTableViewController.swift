//
//  HKBaseLazyTableViewController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 1/18/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation
import UIKit

let QUERY_COUNT = 20

/**
HKBaseLazyTableViewController extends the functionality of HKBaseTableViewController by adding processing of sections, loading, finding and manipulating with the data.
- SeeAlso: HKBaseTableViewController
*/

open class HKBaseLazyTableViewController : HKBaseTableViewController {
    /// Describes whether the view to be reloaded on start.
    open var refreshWhenAppear = true
    open var refreshEnabled = true
    open var refreshTitle: String = "lazy_list_last_update_message".localized
    
    var queryCount : Int = QUERY_COUNT
    var querySkip : Int = 0
    
    fileprivate(set) open var isUpdating = false
    fileprivate(set) open var isCanBeUpdated = false
    
    fileprivate(set) open var lazySection : HKTableViewSection!
    fileprivate(set) open var lazyList : [Any?]!
    
    @IBOutlet open var tableView: UITableView!
    
    var refreshControl: UIRefreshControl?

    /// Describes whether the selection is to be removed when viewWillAppear has been reveived.
    open var clearsSelectionOnViewWillAppear = true
    
    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    /**
     Runs on viewWillAppear received. Additionaly removes the selection if clearsSelectionOnViewWillAppear
    flag is set to true. Notifies life cycle observer about viewDidLoad by calling super class
    viewWillAppear.
    - parameter animated: Describes whether the view will be animated.
    */
    override open func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if refreshWhenAppear {
            refresh()
            refreshWhenAppear = false
        }
        
        if clearsSelectionOnViewWillAppear {
            if let selection = tableView.indexPathForSelectedRow {
                tableView.deselectRow(at: selection, animated: animated)
            }
        }
    }

    /**
     Runs when viewDidLoad has been received.
     Prepare the collection of items.
    */
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        if refreshEnabled {
            let tableViewController = UITableViewController()
            tableViewController.tableView = tableView
            refreshControl = UIRefreshControl()
            refreshControl?.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
            tableViewController.refreshControl = refreshControl
        }
        
        lazySection = HKTableViewSection()
        itemsSource.removeAll()
        buildSource(lazySection)
    }

    /**
    - parameter someSection: Section to be tested.
    - Returns: True if the section is lazy section.
    */
    open func isLasySection(_ someSection : HKTableViewSection) -> Bool {
        return lazySection == someSection
    }
    
    /**
     Gets item from the lazy section.
     - parameter row: Row index the item to be found for.
     - Returns: The item found by the provided index.
     */
    open func lazyItem(row: Int) -> Any? {
        return lazySection.items[row].item;
    }

    /**
     Gets item from the lazy section.
    - parameter indexPath: Index path the item to be found for.
    - Returns: The item found by the provided index path or nil if index path belong not to the lazy section.
    */
    open func lazyItem(_ indexPath: IndexPath) -> Any? {
        if isLasySection(self.itemsSource[indexPath.section]) {
            return lazySection.items[indexPath.row].item;
        }
        return nil;
    }
    
    /**
     Gets number of items in the lazy section.
     - Returns: The number of items in the lazy section.
     */
    open func lazyItemsCount() -> Int {
        return lazySection.items.count;
    }

    /**
     Gets IndexPath of lazy section item.
    - parameter row: Row index the IndexPath to be found for.
    - Returns: The IndexPath found by the provided index.
    */
    open func lasyIndexPath(_ row: Int) -> IndexPath {
        return IndexPath(row: row, section: self.findSectionIndex(lazySection)!)
    }

    /**
    Customize displaying of cell.
    * Additionally reloads data if the data at indexPath should and can be updated.
    - parameter tableView: The table view which is displaying the data.
    - parameter cell: The table cell which is displaying the item.
    - parameter indexPath: The IndexPath of the displaying item.
    */
    open func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isShouldBeUpdated(indexPath) {
            querySkip += queryCount
            isUpdating = true
            loadList(buildListUpdater(), count: queryCount, skip: querySkip)
        }
    }

    /// Reloads data from the server.
    @objc open func refresh() {
        if !isUpdating {
            isUpdating = true;
            isCanBeUpdated = true;
            
            querySkip = 0;
            loadList(buildListLoader(), count: queryCount, skip: querySkip)
        }
    }

    /// Cleans the current data from the tableView and reloads new data from the server.
    open func clearList() {
        lazySection.items.removeAll()
        self.tableView.reloadData()
        
        refresh()
    }

    /**
    Build items source.
    - parameter lasySection: The lazy section that should be used in data source building.
    */
    open func buildSource(_ lazySection : HKTableViewSection) {
        itemsSource.append(lazySection)
    }

    /**
    Loads list of items from server.
    * Must be overriden.
     - parameter responseHandler: The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
    */
    open func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        preconditionFailure("Method loadList must be overridden")
    }

    /**
     Converts server response to list of list items.
    - Returns: Loaded items in response converted to List.
    - parameter response: The Response object to be converted to List.
    */
    open func toList(_ response : Response) -> [Any?] {
        preconditionFailure("Method toList must be overridden")
    }
    
    open func filterList(_ list : [Any?]) -> [Any?] {
        return list
    }
    
    /**
     Convert items list into the list of ViewController.
     - Returns: List to map.
     - parameter list: A mapped List. List can be modified or filtered in this function and returned as result.
     */
    open func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        
        for listItem in list {
            items.append(self.mapItem(listItem))
        }
        return items
    }
    
    /**
     Convert list item into the lazy list item.
     - Returns: Layzy list item.
     - parameter ietm: A List item.
     */
    open func mapItem(_ item : Any?) -> (HKTableViewItem) {
        preconditionFailure("Method mapItem must be overridden")
    }

    /**
     Check if new portion of data should be load.
    - Returns: True if new portion of data should be load.
    - parameter indexPathWilDisplay: The IndexPath to row to be tested for update necessity.
    */
    open func isShouldBeUpdated(_ indexPathWilDisplay: IndexPath) -> (Bool) {
        let section = itemsSource[(indexPathWilDisplay as IndexPath).section]
        return isLasySection(section) && ((indexPathWilDisplay as IndexPath).row >= (section.items.count - 1)) && !isUpdating && isCanBeUpdated
    }

    /// Should be called after load data is finished.
    open func loadFinished(_ queryCount : Int, querySkip : Int) {
        self.loadFinished()
    }
    
    /// Should be called after load data is finished.
    open func loadFinished() {
        self.tableView.reloadData()
    }

    /**
    Handles provided error.
    - parameter error: NSError object within the error to be handled.
    - Returns: True if error has been handled, otherwise false.
    */
    open func handleError(_ error : ApiError) -> Bool {
        return AppErrors.handleError(self, error: error)
    }

    /**
    Merges two lists.
    - parameter list1: The first list.
    - parameter list2: The second list.
    - Returns: The result of merge.
    */
    open func mergeList(_ list1 : [Any?], list2 : [Any?]?) -> ([Any?]?){
        if list2 != nil {
            return list1 + list2!
        }
        else {
            return list1
        }
    }
    
    /**
     - Returns: The loader object to be used on loadinga data from the server.
     */
    fileprivate func buildListLoader() -> (ResponseHandler) {
        
        return { (response) in
            
            self.isUpdating = false
            if self.refreshControl != nil {
                let formater = DateFormatter()
                formater.dateStyle = .medium
                formater.timeStyle = .short
                
                let title = NSString(format: self.refreshTitle as NSString, formater.string(from: Date()))
                let attrsDictionary = [NSAttributedStringKey.foregroundColor : UIColor.black]
                let attributedTitle = NSAttributedString(string: String(title), attributes: attrsDictionary)
                self.refreshControl?.attributedTitle = attributedTitle
                self.refreshControl?.endRefreshing()
            }
            
            if response.isStatusAccept() {
                let list = self.toList(response)
                let count = list.count
                
                self.lazyList = self.filterList(list)
                self.lazySection.items = self.mapList(self.lazyList)
                
                self.isCanBeUpdated = count >= self.queryCount && self.queryCount > 0
                self.loadFinished(count, querySkip: self.querySkip)
            }
            else {
                let _ = self.handleError(response.anyError())
            }
        }
    }
    
    /**
     - Returns: The loader object to be used on loading data from the server.
     */
    fileprivate func buildListUpdater() -> (ResponseHandler) {
        
        return { (response: Response) -> Void in
            self.isUpdating = false
            
            if response.isStatusAccept() {
                var list = self.toList(response)
                let count = list.count
                list = self.filterList(list)
                
                self.lazyList = self.mergeList(self.lazyList, list2: list)
                self.lazySection.items = self.mergeList(self.lazySection.items, list2: self.mapList(list))! as! [HKBaseTableViewController.HKTableViewItem]
                
                self.isCanBeUpdated = count >= self.queryCount && self.queryCount > 0
                self.loadFinished(count, querySkip: self.querySkip)
            }
        }
    }

    /**
    Finds the index of the section.
    - parameter someSection: The section the index should be found for.
    - Returns: Index for the provided section.
    */
    fileprivate func findSectionIndex(_ someSection : HKTableViewSection) -> Int? {
        for index in 0...self.itemsSource.count {
            if self.isLasySection(self.itemsSource[index]) {
                return index
            }
        }
        return nil;
    }
}


