//
//  HKAppControllerInstance.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
Describles methods to be implementer in HKAppViewControllerFactory implementation class.
*/
public protocol HKAppViewControllerFactory {

    /**
    - Returns: The ViewController for the storuboard found in the provided ViewController.
    - parameter controllerId: The identificator the story board identifier will be searched for.
    - parameter viewController: The view controller the story board identifier will be searched in.
    */
    func getViewControllerById(_ controllerId: Int, viewController: UIViewController) -> UIViewController?
    
    /**
     - Returns: The ViewController for the storuboard found in the provided ViewController.
     - parameter identifier: The story board identifier or bundle file name.
     - parameter viewController: The view controller the story board identifier will be searched in.
     */
    func getViewControllerByIdentifier(_ identifier: String, viewController: UIViewController) -> UIViewController?
    
    /**
     Checks if the Guest mode is available for the application.
     - Returns: True if Guest mode is available, otherwise false.
     */
    func isGuestModeAvailable() -> Bool
    
}
