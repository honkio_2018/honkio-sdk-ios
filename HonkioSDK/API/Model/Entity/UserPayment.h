//
//  UserPayment.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Data object that hold information about payment.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface UserPayment : NSObject

/** Time whet payment is executed. */
@property (nonatomic) NSDate*  timeStamp;
@property (nonatomic) NSString* amount;
@property (nonatomic) NSString* currency;

@end
