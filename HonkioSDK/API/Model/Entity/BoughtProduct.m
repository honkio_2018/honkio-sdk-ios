//
//  ProductTransaction.m
//  HonkioApi
//
//  Created by Alexandr Kolganov on 4/22/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import "BoughtProduct.h"

@interface BoughtProduct()

//for QRCode version 2 (more compact)
+(NSString*) hexeditToString:(NSString*)hexString;
+(NSString*) base64ToString:(NSString*)base64String;
+(NSString*) dateToByteString:(NSDate*)date;

@end

@implementation BoughtProduct

-(id)initFrom:(BoughtProduct*) product {
    self = [super initFrom:product];
    
    self.count = product.count;
    self.isValid = product.isValid;
    self.signature = product.signature;
    self.validFrom = product.validFrom;
    self.validTo = product.validTo;
    self.transaction = product.transaction;
    
    return self;
}

-(NSTimeInterval)validLeft:(NSDate*)time {
    return [[self validTo] timeIntervalSinceDate:time];
}

-(BOOL)isValid:(NSDate*)time {
    return [self isValid] && [self validLeft:time] > 0;
}

+(NSString*) hexeditToString:(NSString*)hexString {
    
    NSMutableData *buffer= [[NSMutableData alloc] init];
    unsigned char whole_byte;
    char byte_chars[3] = "xx";
    int i;
    for (i=0; i < [hexString length]/2; i++) {
        
        byte_chars[0] = [hexString characterAtIndex:i*2];
        byte_chars[1] = [hexString characterAtIndex:i*2+1];
        whole_byte = strtol(byte_chars, NULL, 16);
        [buffer appendBytes:&whole_byte length:1];
    }
    return [[NSString alloc] initWithData:buffer encoding:NSISOLatin1StringEncoding];
}

+(NSString*) base64ToString:(NSString*)base64String {
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    return [[NSString alloc] initWithData:decodedData encoding:NSISOLatin1StringEncoding];
}

+(NSString*) dateToByteString:(NSDate*)date {
    long time = [date timeIntervalSince1970];
    return [[NSString alloc] initWithBytes:&time length:8 encoding:NSISOLatin1StringEncoding];
}

@end
