//
//  HKPreferredUserAccountsTableViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/29/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

// TODO write docs
open class HKPreferredUserAccountsTableViewController: HKBaseTableViewController, HKUserAccountSelectionProtocol, HKViewControllerCompletionDelegate {

    public static let ITEM_CELL_IDENTIFIER = "HKPreferredUserAccountsTableViewController.ITEM_CELL_IDENTIFIER"
    
    private let TAG_ADD_CARD = "HKPreferredUserAccountsTableViewController.TAG_ADD_CARD"
    
    open var tableView: UITableView!
    
    open var merchant: MerchantSimple? = HonkioApi.mainShopInfo()?.merchant
    open var disallowedAccounts: Set<String>?
    open var minAmount : Double?
    open var currency : String?
    open var selectionDelegate: HKUserAccountSelectionProtocolDelegate!
    
    private var isAccountSelected = false

    override open func viewDidLoad() {
        self.buildViewProgrammaticaly()
        super.viewDidLoad()
        self.buildSource()
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(HKPreferredUserAccountsTableViewController.cancelAction))
    }
    
    open func buildSource() {
        self.itemsSource.removeAll()
        
        let section = HKTableViewSection()
        
        if let list = self.buildList() {
            let cellBinder = self.buildCellBunder()
            
            let dummyAccount = UserAccount(type: ACCOUNT_TYPE_CREDITCARD, andNumber:ACCOUNT_NUMBER_ZERO)!
            dummyAccount.isEnabled = true
            section.items.append((self.cellIdentifier(dummyAccount), dummyAccount, cellBinder))
            
            let sortedList = list.sorted(by: { (item1, item2) -> Bool in
                if item1.typeIs(ACCOUNT_TYPE_CREDITCARD) {
                    if item2.typeIs(ACCOUNT_TYPE_CREDITCARD) {
                        return false
                    }
                    return true;
                }
                return false;
            })
            
            for account in sortedList {
                section.items.append((self.cellIdentifier(account), account, cellBinder))
            }
        }
        self.itemsSource.append(section)
        self.tableView.reloadData()
    }
    
    open func cellIdentifier(_ item: UserAccount) -> String {
        return HKPreferredUserAccountsTableViewController.ITEM_CELL_IDENTIFIER
    }
    
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        return { (viewController, cell, item) in
            let accountCell = cell as! HKUserAccountsTableViewCell
            let account = item as! UserAccount
            
            accountCell.account = account
            accountCell.isEnabled = self.isEnabled(account)
            
            if accountCell.isEnabled {
                accountCell.cardWarning.text = nil
            }
            else {
                if (self.disallowedAccounts?.contains(account.type) ?? false)  {
                    accountCell.cardWarning.text = "pay_accounts_list_warn_disallowed".localized
                }
                else if !account.enoughLeft(self.minAmount ?? 0) {
                    accountCell.cardWarning.text = "pay_accounts_list_warn_not_enough_left".localized
                }
                else if !(self.merchant?.isAccountSupported(account.type) ?? true) {
                    accountCell.cardWarning.text = "pay_accounts_list_warn_not_supported_by_merchant".localized
                }
                else {
                    accountCell.cardWarning.text = nil
                }
            }
            
            if HKPreferredUserAccountsTableViewController.isDummyCard(account) {
                accountCell.cardTitle.text = "pay_accounts_list_btn_card_add".localized
                accountCell.cardDesc.text = nil
            }
        }
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let account = self.itemsSource[(indexPath as IndexPath).section].items[(indexPath as IndexPath).row].item as? UserAccount {
            if HKPreferredUserAccountsTableViewController.isDummyCard(account) {
                if let controller = AppController.getViewControllerById(AppController.SettingsUserAddCreditCard, viewController: self) as? HKBaseViewController {
                    
                    _ = HKCancelActionVCStrategy(parent: controller)
                    
                    controller.setCompletionDelegate(self, tag: TAG_ADD_CARD)
                    self.present(UINavigationController(rootViewController: controller), animated: true, completion: nil)
                }
            }
            else {
                if self.isEnabled(account) {
                    self.selectionDelegate.accountDidSelect(self, account: account)
                }
            }
        }
    }
    
    open func onComplete(_ viewController: UIViewController, tag: String?) {
        if tag == TAG_ADD_CARD {
            if let accounts = HonkioApi.activeUser()?.accounts {
                if accounts.count > 0 {
                    let account = accounts[accounts.count - 1]
                    if account.typeIs(ACCOUNT_TYPE_CREDITCARD) {
                        self.selectionDelegate.accountDidSelect(self, account: account)
                    }
                }
            }
        }
    }
    
    @objc open func cancelAction() {
        self.selectionDelegate.accountDidSelect(self, account: nil)
    }
    
    open func buildViewProgrammaticaly() {
        
        self.title = "settings_btn_set_pay_account".localized
        
        tableView  = self.makeTableView()
        
        view.backgroundColor = UIColor(netHex: 0xffffff)
        
        view.addSubview(tableView)
        
        let views : [String : AnyObject] = [
            "tableView" : tableView
        ]
        
        let formatArray : [String] = [
            "V:|-0-[tableView]-0-|",
            "H:|-0-[tableView]-0-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    open func buildList() -> [UserAccount]? {
        if HonkioApi.isConnected() {
            let merchant = (self.merchant ?? HonkioApi.mainShopInfo()?.merchant)!
            
            var list = HKPreferredUserAccountsManager.instance().list
            if list.count > 0 {
                list = self.filter(list, merchant, self.minAmount, self.currency)
            }
            if list.count == 0 {
                list = self.filter(HonkioApi.activeUser()!.accounts, merchant, self.minAmount, self.currency)
            }
            return list.sorted(by: { (account1, account2) -> Bool in
                return self.isEnabled(account1)
            })
        }
        return nil
    }
    
    private func filter(_ list: [UserAccount], _ merchant: MerchantSimple, _ amount: Double?, _ currency: String?) -> [UserAccount] {
        var newList: [UserAccount] = []
        for account in list {
            if !account.isDummy() {
                if account.typeIs(ACCOUNT_TYPE_INVOICE) || account.typeIs(ACCOUNT_TYPE_BONUS) {
                    if account.number == merchant.merchantId {
                        newList.append(account)
                    }
                }
                else {
                    newList.append(account)
                }
            }
        }
        return newList
    }
    
    private func isEnabled(_ account: UserAccount) -> Bool {
        if !account.isEnabled {
            return false
        }
        
        let merchant = (self.merchant ?? HonkioApi.mainShopInfo()?.merchant)!
        
        if !(self.disallowedAccounts?.contains(account.type) ?? false) {
            if merchant.isAccountSupported(account.type) {
                if account.enoughLeft(self.minAmount ?? 0) {
                    return true
                }
            }
        }
        return false
    }
    
    open func makeTableView() -> UITableView {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44.0
        tableView.backgroundColor = UIColor.clear
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.layoutMargins = UIEdgeInsets.zero
        tableView.separatorStyle = .singleLine
        tableView.allowsSelection = true
        tableView.allowsSelectionDuringEditing = false
        tableView.allowsMultipleSelection = false
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.keyboardDismissMode = .onDrag
        tableView.delegate = self
        tableView.dataSource = self
        
        //For iOS 9 and Above
        if #available(iOS 9, *) {
            tableView.cellLayoutMarginsFollowReadableWidth = false
        }
        
        tableView.register(HKUserAccountsTableViewCell.self,
                           forCellReuseIdentifier: HKPreferredUserAccountsTableViewController.ITEM_CELL_IDENTIFIER)
        
        return tableView
    }
    
    private static func isDummyCard(_ account: UserAccount) -> Bool {
        return account.typeIs(ACCOUNT_TYPE_CREDITCARD) && ACCOUNT_NUMBER_ZERO == account.number
    }
}
