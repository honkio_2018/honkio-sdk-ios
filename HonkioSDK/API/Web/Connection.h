//
//  Connection.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PIN_VALID_TIME 600000
#define MAX_PIN_FAIL_COUNT 3

/**
 * Class designed for holding authentication information.
 * As storage used StoreData.
 * To store OTP required PIN code which used for encryption and decryption.
 */
@interface Connection : NSObject

/**
 * Init new Connection. Login and password will get from StoreData.
 * @param pin PIN code. Will used to encrypt/decrypt OTP.
 */
-(id)initWithPin:(NSString*)pin;

/**
 * Init new Connection with given Login and password.
 * @param login User login.
 * @param password User password.
 */
-(id)initWithLogin:(NSString*)login password:(NSString*)password;

/**
 * Init new Connection with given Login and password.
 * @param login User login.
 * @param password User password.
 * @param pin PIN code. Will used to encrypt/decrypt OTP.
 */
-(id)initWithLogin:(NSString*)login password:(NSString*)password pin:(NSString*)pin;

/**
 * Removes Password from memory and StoreData, marks Connection as invalid.
 */
-(void)exit;

/**
 * Check if Connection is valid. New Connection object is valid by default. To set Connection invalid used setToInvalid.
 * @return True if Connection marked as valid, false otherwise.
 */
-(BOOL)isValid;

/**
 * Mark Connection as invalid.
 */
-(void)setToInvalid;

/**
 * Gets user login.
 * @return User login.
 */
-(NSString*) login;

/**
 * Gets One Time Password.
 * @return One Time Password.
 */
-(NSString*) otp;

/**
 * Sets new One Time Password. A password will stored into SStoreData if PIN code isn't nil.
 * @param otp One Time Password.
 */
-(void) setOtp:(NSString*)otp;

/**
 * Change Connection PIN to another.
 * @param newPin New PIN code.
 */
-(void)changePin:(NSString*)newPin;

/**
 * Change Connection PIN to another. PIN will be changed only if current PIN is correct.
 * @param currentPin Current PIN code.
 * @param newPin New PIN code.
 * @return YES if check of current PIN is success, NO otherwise.
 * @see checkPin:(NSString*)pin
 */
-(BOOL)changePin:(NSString*)currentPin newPin:(NSString*)newPin;

/**
 * Check if given PIN code is the PIN code of the Connection. If check will be failed increasePinFails will called.
 * @param pin PIN code.
 * @return YES if Connection has the same PIN code or Connection PIN code is nil, NO otherwise.
 */
-(BOOL)checkPin:(NSString*)pin;

/**
 * Increase PIN fails count. If amount of fails become equal or bigger that MAX_PIN_FAIL_COUNT method exit will called.
 */
-(BOOL)increasePinFails;

/**
 * Gets amount of invalid PIN checks.
 * @return Amount of invalid PIN checks.
 */
+(int)pinFailsCount;

/**
 * Check if StoreData has stored OTP.
 * @return YES if OTP stored into StoreData, NO otherwise.
 */
+(BOOL)isHasSoredOtp;

/**
 * Remove OTP from StoreData.
 */
+(void)removeSoredOtp;

@end
