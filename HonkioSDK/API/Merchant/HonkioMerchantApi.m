//
//  HonkioMerchantApi.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/7/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "HonkioMerchantApi.h"
#import "MerchantsListJsonParser.h"
#import "MerchantShopsListJsonParser.h"
#import "MerchantShopInfoJsonParser.h"
#import "TaskWrapper.h"
#import "WebServiceExecutor.h"
#import "MerchantProductsFilter.h"
#import "HKLinksList.h"

@implementation HonkioMerchantApi

+(nullable id<Task>)merchantGet:(nonnull NSString*)merchantId flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_GET
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:[HonkioApi mainShopIdentity]
                                             responseParser:[[HonkioApi protocolFactory] newParser:[Merchant class]]
                                                      flags:flags];
        
        [message addParameter:@"id" value:merchantId];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(nullable id<Task>)merchantSet:(nonnull Merchant*)merchant flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_SET
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:nil
                                             responseParser:[[HonkioApi protocolFactory] newParser:[Merchant class]]
                                                      flags:flags];
        
        [message addParameter:@"merchant" value:[merchant toParams]];
        [message addParameter:@"id" value:merchant.merchantId];
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    
    return nil;
}

+(nullable id<Task>)merchantGetList:(int)flags handler:(nullable ResponseHandler)responseHandler {
    return [self merchantGetList:nil flags:flags handler:responseHandler];
}

+(nullable id<Task>)merchantGetList:(NSDictionary *)params flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_GET_LIST
                                             connection:[HonkioApi activeConnection]
                                           shopIdentity:nil
                                         responseParser:[MerchantsListJsonParser new]
                                                  flags:flags];
    if (params) {
        [message addParameters:params];
    }
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(nullable id<Task>)merchantTransactionsList:(nullable TransactionFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    return [HonkioMerchantApi merchantTransactionsList:filter shopIdentity:nil flags:flags handler:responseHandler];
}

+(nullable id<Task>)merchantTransactionsList:(nullable TransactionFilter*)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_GET_TRANSACTIONS
                                            connection:[HonkioApi activeConnection]
                                          shopIdentity:shopIdentity
                                        responseParser:[[HonkioApi protocolFactory] newParser:[Transactions class]]
                                                 flags:flags];
    
    [filter apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(nullable id<Task>)merchantShopsList:(ShopFilter *)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_GET_SHOP_LIST
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:[HonkioApi mainShopIdentity]
                                             responseParser:[MerchantShopsListJsonParser new]
                                                      flags:flags];
        message.isIgnoreHash = YES; //fixme delete this
        if (filter)
            [filter apply: message];
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(nullable id<Task>)merchantShopGet:(Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_GET_SHOP
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[MerchantShopInfoJsonParser new]
                                                      flags:flags];
        message.isIgnoreHash = YES; //fixme delete this
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(nullable id<Task>)merchantGetOrders:(nullable OrderFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    return [HonkioMerchantApi merchantGetOrders:filter shopIdentity:nil flags:flags handler:responseHandler];
}

+(id<Task>)merchantGetOrders:(OrderFilter*)filter shopIdentity:(Identity *)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_GET_ORDERS
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[OrdersList class]]
                                                      flags:flags];
        [message setCacheFileName:MSG_COMMAND_USER_GET_ORDERS dependsOnShop:YES];
        
        if (filter) {
            [filter apply:message];
        }
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}
    
+(nullable id<Task>)merchantSetOrder:(nonnull Order*)order flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    Identity* identity = [HonkioApi mainShopIdentity];
    if ([identity.identityId isEqualToString:order.shopId]) {
        
        return [HonkioMerchantApi merchantSetOrder:order shopIdentity:[HonkioApi mainShopIdentity] flags:flags handler:responseHandler];
    }
    
    // FIXME: use merchantGetShop
    __block TaskWrapper* wrapper = [[TaskWrapper alloc] initWithTask: [HonkioApi shopFindById:order.shopId flags:0 handler:^(Response * _Nullable res) {
        if (res && [res isStatusAccept]) {
            ShopFind* shopsList = (ShopFind*) res.result;
            if (shopsList.list.count > 0) {
                Identity* shop = (Identity*) shopsList.list[0];
                [wrapper set:[HonkioMerchantApi merchantSetOrder:order shopIdentity:shop flags:flags handler:responseHandler]];
            }
            else {
                responseHandler([Response withError:ErrApiUnknown desc:nil]);
            }
        }
        else {
            responseHandler([Response withError:ErrApiUnknown desc:nil]);
        }
        
    }]];
    
    return wrapper;
}

+(nullable id<Task>)merchantSetOrder:(nonnull Order*)order shopIdentity:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_SET_ORDER
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[Order class]]
                                                      flags:flags];
        
        if (order) {
            [order apply:message];
        }
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(nullable id<Task>)merchantQrSet:(nonnull HKQrCode*)qrCode flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_QR_SET
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:[HonkioApi mainShopIdentity]
                                             responseParser:[[HonkioApi protocolFactory] newParser: nil]
                                                      flags:flags];
        
        [message addParameter:@"id" value:qrCode.qrId];
        [message addParameter:@"merchant" value:qrCode.merchantId];
        [message addParameter:@"object_type" value:qrCode.objectType];
        
        if (qrCode.object)
            [message addParameter:@"object" value:[qrCode.object getId]];
        else
            [message addParameter:@"object" value:@"null"];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(nullable id<Task>)merchantSetShop:(MerchantShop*)shop flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_SET_SHOP
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shop
                                             responseParser:[MerchantShopInfoJsonParser new]
                                                      flags:flags];
        
        [message addParameters:[shop toParams]];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(nullable id<Task>)userMerchantCreate:(Identity*)shopIdentity name:(NSString*)name email:(NSString*)adminEmail phone:(NSString*)adminPhone flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioMerchantApi buildMessage:@"usermerchantcreate"
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[Merchant class]]
                                                      flags:flags];
        
        NSMutableDictionary* merchant = [NSMutableDictionary new];
        merchant[@"str_name"] = name;
        merchant[@"str_email"] = adminEmail;
        merchant[@"str_telephone"] = adminPhone;
        
        [message addParameter:@"merchant" value:merchant];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

// MARK: - Asset

// TODO docs
+(nullable id<Task>)merchantAssetSet:(HKAsset *)asset shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_ASSET_SET
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[HKAsset class]]
                                                      flags:flags];
        [message addParameters:[asset toParams]];
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    
    return nil;
}
    
+(nullable id<Task>)merchantAssetDelete:(HKAsset *)asset shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_ASSET_SET
                                                connection:[HonkioApi activeConnection]
                                              shopIdentity:shopIdentity
                                            responseParser:[[HonkioApi protocolFactory] newParser:[HKAsset class]]
                                                     flags:flags];

        [message addParameter:@"id" value: asset.assetId];
        [message addParameter:@"delete" value: [NSNumber numberWithBool:YES]];
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    
    return nil;
}

+(nullable id<Task>)merchantAssetList:(AssetFilter *)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_ASSET_LIST
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[HKAssetList class]]
                                                      flags:flags];
        if (filter)
            [filter apply:message];
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

// MARK: - Products

+(Message *)merchantGetProducts:(MerchantProductsFilter *)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags {
    Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_GET_PRODUCT_LIST
                                             connection:[HonkioApi activeConnection]
                                           shopIdentity:shopIdentity
                                         responseParser:[[HonkioApi protocolFactory] newParser:[ShopProducts class]]
                                                  flags:flags];
    [message setCacheFileName:MSG_COMMAND_MERCHANT_GET_PRODUCT_LIST dependsOnShop:YES];
    [message addParameter:MSG_PARAM_QUERY_LANGUAGE value:[HonkioApi getSuitableLanguage]];
    
    if (filter)
        [filter apply:message];
    return message;
}

+(nullable id<Task>)merchantProductList:(MerchantProductsFilter *)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        return [WebServiceExecutor build:[HonkioMerchantApi merchantGetProducts:filter shopIdentity:shopIdentity flags: flags] handler:responseHandler];
    }
    return nil;
}

+(Message *)merchantProductSet:(Product *)product shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags {
    Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_SET_PRODUCT
                                             connection:[HonkioApi activeConnection]
                                           shopIdentity:shopIdentity
                                         responseParser:[[HonkioApi protocolFactory] newParser:[ShopProducts class]]
                                                  flags:flags];
    [message addParameter:@"product" value:[product toParams]];
    
    return message;
}

+(nullable id<Task>)merchantProductSet:(Product *)product shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        return [WebServiceExecutor build:[HonkioMerchantApi merchantProductSet:product shopIdentity:shopIdentity flags: flags] handler:responseHandler];
    }
    return nil;
}

+(id<Task>)merchantStructureSet:(HKStructure*)structure shopIdentity:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_SRUCTURE_SET
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[HKStructure class]]
                                                      flags:flags];
        
        [message addParameter:@"structure" value:[structure toParams]];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

// MARK: - Events

+(nullable id<Task>)merchantEventSet:(CalendarEvent *)calendarEvent
                            objectId:(NSString *)objectId
                          objectType:(NSString *)objectType
                        shopIdentity:(nullable Identity *)shopIdentity
                               flags:(int)flags
                             handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_SET_EVENT
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[CalendarEvent class]]
                                                      flags:flags];
        if (calendarEvent)
            [message addParameters:[calendarEvent toParams]];

        [message addParameter:@"object" value:objectId];
        [message addParameter:@"object_type" value:objectType];
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(nullable id<Task>)merchantEventDelete:(CalendarEvent *)calendarEvent
                           shopIdentity:(nullable Identity *)shopIdentity
                                  flags:(int)flags
                                handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_SET_EVENT
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[CalendarEvent class]]
                                                      flags:flags];
        if (calendarEvent)
            [message addParameters:[calendarEvent toParams]];
        
        [message addParameter:@"object" value:@""];
        [message addParameter:@"object_type" value:@""];
        [message addParameter:@"delete" value:@(YES)];
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

// MARK: - Chat

+(nonnull id<Task>)merchantGetChatMessages:(nullable ChatMessageFilter*)filter
                                 flags:(int)flags
                               handler:(nullable ResponseHandler)responseHandler {
    
    
    Message* message = [HonkioApi buildUserMessage:MSG_COMMAND_MERCHANT_GET_CHAT
                                    responseParser:[[HonkioApi protocolFactory] newParser:[ChatMessages class]]
                                             flags:flags];
    if (filter)
        [filter apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(nonnull id<Task>)merchantSendChatMessage:(nonnull ChatMessage*)chatMessage
                                 flags:(int)flags
                               handler:(nonnull ResponseHandler)responseHandler {
    Message *message = [HonkioApi buildUserMessage:MSG_COMMAND_MERCHANT_SEND_CHAT
                                    responseParser:[[HonkioApi protocolFactory] newParser:[ChatMessages class]]
                                             flags:flags];
    
    [chatMessage apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

// MARK: - Vote

+(Message *)merchantVote:(UserComment *)userComment flags:(int)flags {
    Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_VOTE
                                             connection:[HonkioApi activeConnection]
                                           shopIdentity:[HonkioApi mainShopIdentity]
                                         responseParser:[[HonkioApi protocolFactory] newParser:[UserComment class]]
                                                  flags:flags];

    [userComment apply:message];
    return message;
}

+(nullable id<Task>)merchantVote:(UserComment *)userComment flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        return [WebServiceExecutor build:[HonkioMerchantApi merchantVote: userComment flags: flags] handler: responseHandler];
    }
    return nil;
}

+(id<Task>)merchantSetLink:(HKLink*)link shopIdentity:(Identity *)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_SET_LINK
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[HKLink class]]
                                                      flags:flags];
        [message addParameters:[link toParams]];
        
        return [WebServiceExecutor build: message handler: responseHandler];
    }
    return nil;
}

+(id<Task>)merchantDeleteLink:(NSString*)linkId shopIdentity:(Identity *)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_SET_LINK
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser: nil]
                                                      flags:flags];
        
        [message addParameter:@"id" value:linkId];
        [message addParameter:@"delete" value:@"yes"];
        
        return [WebServiceExecutor build: message handler: responseHandler];
    }
    return nil;
}

+(id<Task>)merchantGetLinks:(HKLinkFilter*)filter shopIdentity:(Identity *)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler{
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_GET_LINK
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:shopIdentity
                                             responseParser:[[HonkioApi protocolFactory] newParser:[HKLinksList class]]
                                                      flags:flags];
        
        [filter apply:message];
        
        return [WebServiceExecutor build: message handler: responseHandler];
    }
    return nil;
}

+(id<Task>)merchantReportRun:(NSString*)reportId email:(NSString*)email params:(NSDictionary*)params flags:(int)flags handler:(ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioMerchantApi buildMessage:MSG_COMMAND_MERCHANT_REPORT_RUN
                                                 connection:[HonkioApi activeConnection]
                                               shopIdentity:[HonkioApi mainShopIdentity]
                                             responseParser:[[HonkioApi protocolFactory] newParser: nil]
                                                      flags:flags];
        
        [message addParameter:@"id" value:reportId];
        [message addParameter:@"parameters" value:params];
        if (email)
            [message addParameter:@"email" value:email];
        
        return [WebServiceExecutor build: message handler: responseHandler];
    }
    return nil;
}

+(Message*)buildMessage:(NSString*)command connection:(Connection*)connection shopIdentity:(Identity*)shopIdentity responseParser:(id<ResponseParser>)responseParser flags:(int)flags {
    if (!shopIdentity) {
        shopIdentity = [HonkioApi mainShopIdentity];
    }
    return [[Message alloc] initWithCommand:command url:[HonkioApi apiUrl] connection:connection device:[HonkioApi device] shopIdentity:shopIdentity requestBuilder:[[HonkioApi protocolFactory] newRequestBuilder] responseParser:responseParser flags:flags];
}

@end
