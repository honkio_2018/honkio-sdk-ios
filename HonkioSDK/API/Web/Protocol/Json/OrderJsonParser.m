//
//  OrderJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 12/2/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "UserRoleDescriptionJsonParser.h"
#import "OrderJsonParser.h"
#import "ApiDateFormatter.h"
#import "BookedProduct.h"

@implementation OrderJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [OrderJsonParser parseOrder:dict];
}

+(Order*)parseOrder:(NSDictionary*)dict {
    
    Order* order = [Order new];
    
    order.orderId = dict[@"order_id"];
    order.model = dict[@"model"];
    order.status = dict[@"order_status"];
    order.title = dict[@"title"];
    order.desc = dict[@"description"];
    order.asset = dict[@"asset"];
    
    order.userOwner = dict[@"user_owner"];
    order.thirdPerson = dict[@"third_person"];
    if ([@"None" isEqualToString:order.thirdPerson]) {
        order.thirdPerson = nil;
    }
    
    order.currency = dict[@"currency"];
    order.amount = [dict[@"amount"] doubleValue];
    
    NSArray* productsJson = dict[@"products"];
    if (productsJson) {
        NSMutableArray* products = [[NSMutableArray alloc] init];
        for(NSDictionary* productDict in productsJson) {
            Product* product = [[Product alloc] init: [productDict mutableCopy]];
            [BaseJsonResponseParser parseProduct:product dict:productDict];
            [products addObject:[[BookedProduct alloc] initWithProduct:product andCount:[(NSNumber*) productDict[@"count"] intValue]]];
        }
        order.products = products;
    }
    
    NSArray* rolesJsonArray = dict[@"roles"];
    if (rolesJsonArray) {
        NSMutableArray<UserRoleDescription*>* rolesArray = [[NSMutableArray alloc] init];
        for(NSDictionary* roleDict in rolesJsonArray) {
            UserRoleDescription* roleDesc = [UserRoleDescriptionJsonParser parseRoleDescription:roleDict];
            if (roleDesc)
                [rolesArray addObject: roleDesc];
        }
        order.limitedRoles = rolesArray;
    }
    
    if (dict[MSG_PARAM_ACCOUNT_TYPE]) {
        order.account = [[UserAccount alloc] initWithType:dict[MSG_PARAM_ACCOUNT_TYPE] andNumber:dict[MSG_PARAM_ACCOUNT_NUMBER]];
        
        if ([[[order account] type] isEqualToString:@"None"])
            order.account = nil;
    }
    
    order.latitude = [dict[@"latitude"] doubleValue];
    order.longitude = [dict[@"longitude"] doubleValue];
    
    NSString * date = dict[@"creation_date"];
    
    if (date) {
        
        order.creationDate = [ApiDateFormatter dateFromServerString:date];
    }
    
    date = dict[@"expire_date"];
    
   if (date) {
        
        order.expireDate = [ApiDateFormatter dateFromServerString:date];
    }
    
    date = dict[@"start_date"];
    
    if (date) {
        order.startDate = [ApiDateFormatter dateFromServerString:date];
    }
    
    date = dict[@"end_date"];
    
    if (date) {
        order.endDate = [ApiDateFormatter dateFromServerString:date];
    }

    NSDictionary* shop = dict[@"shop"];
    
    if (shop) {
        
        order.shopId = shop[@"id"];
        order.shopName = shop[@"name"];
        order.shopReceipt = shop[@"receipt"];
        order.shopReference = shop[@"reference"];
    }
    else {
        order.shopId = dict[@"shop_id"];
    }
    
    order.customFields = dict[@"custom_fields"];
    
    order.unreadChatMessagesCount = [dict[@"unread_chat_messages"] longValue];
    
    return order;
}

@end
