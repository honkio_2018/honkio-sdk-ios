//
//  HKUserAccountSelectionVCStrategy.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/29/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

// TODO write docs
open class HKUserAccountSelectionVCStrategy: HKViewControllerLifeCycleObserver, HKUserAccountSelectionProtocolDelegate {

    open var delegate : HKUserAccountSelectionVCStrategyDelegate?
    
    open var disallowedAccounts : Set<String>?
    
    open var removeOnCompletion : Bool = false
    
    open func selectAccount(_ shop: Shop?, _ amount: Double?, _ currency: String?) {
        var list = self.filter(HKPreferredUserAccountsManager.instance().list, shop, amount, currency)
        if list.count == 1 {
            self.accountDidSelect(list[0])
        }
        else {
            self.showAccountSelectionScreen(shop, amount, currency)
        }
    }
    
    open func accountDidSelect(_ sender: HKUserAccountSelectionProtocol, account: UserAccount?) {
        self.viewController.dismiss(animated: true, completion: nil)
        self.accountDidSelect(account)
    }
    
    open func accountDidSelect(_ account: UserAccount?) {
        self.getDelegate()?.acountDidSelect(self, account: account)
        if self.removeOnCompletion {
            self.parent?.removeLifeCycleObserver(self)
            self.delegate = nil
        }
    }
    
    private func showAccountSelectionScreen(_ shop: Shop?, _ amount: Double?, _ currency: String?) {
        let selectAccountViewController = AppController.getViewControllerById(AppController.UserAccountSelection,
                                                                              viewController: self.viewController) as! HKUserAccountSelectionProtocol

        selectAccountViewController.merchant = self.getMerchant(shop)
        selectAccountViewController.disallowedAccounts = self.disallowedAccounts
        selectAccountViewController.minAmount = amount;
        selectAccountViewController.currency = currency;
        selectAccountViewController.selectionDelegate = self
        
        let viewController = selectAccountViewController as! UIViewController
        viewController.title = "pay_accounts_list_title".localized
        
        self.viewController.present(UINavigationController(rootViewController: viewController), animated: true, completion: nil)
    }
    
    private func filter(_ list: [UserAccount], _ shop: Shop?, _ amount: Double?, _ currency: String?) -> [UserAccount] {
        let merchant = self.getMerchant(shop)
        var newList: [UserAccount] = []
        
        for account in list {
            if !(self.disallowedAccounts?.contains(account.type) ?? false) {
                if merchant.isAccountSupported(account.type) {
                    if account.typeIs(ACCOUNT_TYPE_INVOICE) {
                        if account.number == merchant.merchantId {
                            newList.append(account)
                        }
                    }
                    else if !account.isDummy() && account.enoughLeft(amount ?? 0) {
                        newList.append(account)
                    }
                }
            }
        }
        return newList
    }
    
    private func getDelegate() -> HKUserAccountSelectionVCStrategyDelegate? {
        var strategyDelegate = self.delegate
        
        if strategyDelegate == nil {
            strategyDelegate = self.parent as? HKUserAccountSelectionVCStrategyDelegate
        }
        
        return strategyDelegate
    }
    
    private func isCreditCardSupported(_ shop: Shop?) -> Bool {
        return self.getMerchant(shop).isAccountSupported(ACCOUNT_TYPE_CREDITCARD)
    }
    
    private func getMerchant(_ shop: Shop?) -> MerchantSimple {
        return shop?.merchant ?? (HonkioApi.mainShopInfo()?.merchant)!
    }
}
