//
//  HonkioAppTemplate.h
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 4/11/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HonkioApi.h"
#import "HonkioMerchantApi.h"
#import "BaseJsonResponseParser.h"
#import "ApiDateFormatter.h"
#import "BasePaymentProcessModel.h"
#import "AmountPaymentProcessModel.h"
#import "ProductPaymentProcessModel.h"
#import "AccountCreationProcessModel.h"
#import "MediaUploadProcessModel.h"
#import "PushParser.h"
#import "HonkioOauth.h"
#import "SimpleAsyncGet.h"
#import "UriUtils.h"

#import "ApiSecurity.h"
#import "AppInfoJsonParser.h"
#import "ApplicantsListJsonParser.h"
#import "AssetJsonParser.h"
#import "AssetListJsonParser.h"
#import "ChatMessageJsonParser.h"
#import "ChatMessageListJsonParser.h"
#import "ConsimerLoginProcessModel.h"
#import "HKAddress.h"
#import "HKLog.h"
#import "InventoryJsonParser.h"
#import "InventoryProductJsonParser.h"
#import "InvitationJsonParser.h"
#import "JsonProtocolFactory.h"
#import "JsonRequestBuilder.h"
#import "KeychainItemWrapper.h"
#import "LoadingProperties.h"
#import "LocationFinder.h"
#import "NSData+AESCrypt.h"
#import "NSData+Hexadecimal.h"
#import "NSString+AESCrypt.h"
#import "OperatorAccountCreationProcessModel.h"
#import "OrderJsonParser.h"
#import "OrdersListJsonParser.h"
#import "PriorityQueue.h"
#import "PushesListJsonParser.h"
#import "QrCodeJsonParser.h"
#import "ScheduleJsonParser.h"
#import "ServerFileJsonParser.h"
#import "ServerInfoJsonParser.h"
#import "ShopFindJsonParser.h"
#import "ShopInfoJsonParser.h"
#import "ShopProductsJsonParser.h"
#import "SimpleAsyncPost.h"
#import "SimpleResponseJsonParser.h"
#import "StructureJsonParser.h"
#import "TaskWrapper.h"
#import "TransactionsJsonParser.h"
#import "UIApplication+NetworkActivity.h"
#import "UserCommentsJsonParser.h"
#import "UserJsonParser.h"
#import "UserPaymentJsonParser.h"
#import "UserRoleDescriptionJsonParser.h"
#import "UserRoleDescriptionsListJsonParser.h"
#import "UserRoleJsonParser.h"
#import "UserRolesListJsonParser.h"
#import "WebServiceCache.h"
#import "MerchantJsonParser.h"
#import "MerchantsListJsonParser.h"
#import "SimpleListItem.h"
#import "CalendarEventJsonParser.h"
#import "CalendarScheduleJsonParser.h"
#import "HKMediaFileJsonParser.h"
#import "HKMediaFileListJsonParser.h"
#import "HKMediaUrlJsonParser.h"
#import "MerchantShopInfoJsonParser.h"
#import "MerchantShopsListJsonParser.h"
#import "RateListJsonParser.h"
#import "StringResponseJsonParser.h"
#import "StructureListJsonParser.h"
#import "TagsListJsonParser.h"
#import "UserAccessListJsonParser.h"
#import "HKUserRoleStructureJsonParser.h"

//! Project version number for HonkioAppTemplate.
FOUNDATION_EXPORT double HonkioAppTemplateVersionNumber;

//! Project version string for HonkioAppTemplate.
FOUNDATION_EXPORT const unsigned char HonkioAppTemplateVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HonkioAppTemplate/PublicHeader.h>


