//
//  AssetFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/1/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "AbsListFilter.h"
#import "HKPropertiesFilter.h"
#import "HKSortFilter.h"

@interface AssetFilter : AbsListFilter

@property (nonatomic, nullable) NSString* latitude;
@property (nonatomic, nullable) NSString* longitude;
@property (nonatomic, nullable) NSString* radius;
@property (nonatomic, nullable) NSString* merchantId;
@property (nonatomic, nullable) NSArray<NSString*>* structureIds;

@property (nonatomic, assign, getter=isQuerySchedule) BOOL querySchedule;
 
@property (nonatomic, nullable) HKPropertiesFilter *propertiesFilter;
@property (nonatomic, nullable) HKSortFilter* sortFilter;

@end
