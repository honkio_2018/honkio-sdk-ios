//
//  BookedProduct.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/25/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "BookedProduct.h"

@implementation BookedProduct

-(id)init {
    self = [super init];
    return self;
}

-(id)initFrom:(BookedProduct*) bookedProduct {
    self = [super init];
    
    if (bookedProduct.product) {
        self.product = [[Product alloc] initFrom:bookedProduct.product];
    }
    
    self.count = bookedProduct.count;
    
    return self;
}

-(id)initWithProduct:(Product*)product andCount:(int)count {
    self = [super init];
    if (self) {
        _product = product;
        _count = count;
    }
    return self;
}

-(id)initWithProductId:(NSString*)productId andCount:(int)count {
    self = [super init];
    if (self) {
        _product = [Product new];
        _product.productId = productId;
        _count = count;
    }
    return self;
}

-(double)getPrice:(NSString*)accountType {
    return [self count] * ([[self product] amount] * [[self product] supplusFor:accountType]);
}

-(double)getPrice {
    return [self count] * [[self product] amount];
}


@end
