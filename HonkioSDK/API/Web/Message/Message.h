//
//  Message.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Connection.h"
#import "Identity.h"
#import "RequestBuilder.h"
#import "ResponseParser.h"
#import "Response.h"
#import "HKDevice.h"

#define API_VERSION @"1"

/** Sending offline request. Use this flag for getting last online response from the cache. */
#define MSG_FLAG_OFFLINE_MODE           0x00000001
/** Sending online request. Use this flag when offline mode turned on. FLAG_OFFLINE_MODE will be ignored */
#define MSG_FLAG_ONLINE_MODE            0x00000002
/** Don't write response into cache. */
#define MSG_FLAG_NO_CACHE               0x00000004

#define MSG_FLAG_SIGN                   0x00000008
/** Don't send Broadcast messages for unhandled errors. */
#define MSG_FLAG_NO_ERROR_WARNING       0x00000010
/** Don't send Broadcast messages for unhandled responses. */
#define MSG_FLAG_NO_RESPONSE_WARNING    0x00000020
/** Don't send Broadcast messages for unhandled responses and errors. */
#define MSG_FLAG_NO_WARNING             0x00000030 // FLAG_NO_ERROR_WARNING | FLAG_NO_RESPONSE_WARNING
/** Request will be executed in the Low priority queue. */
#define MSG_FLAG_LOW_PRIORITY           0x00000040
/** Request will be executed in the High priority queue. */
#define MSG_FLAG_HIGH_PRIORITY          0x00000080

/** Prefix for identity data. Specified for each platform. */
#define IDENTITY_PREFIX     @"dA"
/** Length of the random string. */
#define RANDOM_LENGTH       40


// MSG_COMMAND_SERVER

#define MSG_COMMAND_SERVER_INFO             @"serverinfo"
#define MSG_COMMAND_SERVER_GET_TOU          @"servergettou"
#define MSG_COMMAND_SET_DEVICE              @"setdevice"

#define MSG_COMMAND_GET_SCHEDULE_PRODUCTS   @"getscheduleproducts"
#define MSG_COMMAND_GET_SCHEDULE            @"getschedule"
#define MSG_COMMAND_GET_SPLIT_SCHEDULE      @"getsplitschedule"

#define MSG_COMMAND_TRANSACTION_STATUS_SET  @"statusset"
#define MSG_COMMAND_APP_INFO                @"appinfo"
#define MSG_COMMAND_LOST_PASSWORD           @"consumerlostpassword"
#define MSG_COMMAND_QR_INFO                 @"qrinfo"
#define MSG_COMMAND_QUERY                   @"query"
#define MSG_COMMAND_GET_PRODUCT_TAGS        @"usergetproducttags"
#define MSG_COMMAND_LOGOUT                  @"logout"

// MSG_COMMAND_SHOP

#define MSG_COMMAND_SHOP_FIND               @"shopfind"
#define MSG_COMMAND_SHOP_INFO               @"shopinfo"
#define MSG_COMMAND_SHOP_TRANSACTIONS       @"shoptransaction"
#define MSG_COMMAND_SHOP_PRODUCTS           @"shopgetproducts"
#define MSG_COMMAND_SHOP_GET_RATE           @"shopgetrate"
#define MSG_COMMAND_SHOP_STAMP              @"shopstamp"

// #define MSG_COMMAND_USER

#define MSG_COMMAND_USER_GET_CHAT           @"usergetchat"
#define MSG_COMMAND_USER_SEND_CHAT          @"usersendchatmsg"
#define MSG_COMMAND_USER_SHOP_PRODUCTS      @"consumershopproduct"
#define MSG_COMMAND_USER_GET                @"consumerget"
#define MSG_COMMAND_USER_SET                @"consumerset"
#define MSG_COMMAND_USER_TRANSACTION        @"consumertransaction"
#define MSG_COMMAND_USER_GET_ORDERS         @"usergetorders"
#define MSG_COMMAND_USER_SET_ORDER          @"usersetorder"
#define MSG_COMMAND_USER_SET_ORDER_APPLICATION @"usersetapplicant"
#define MSG_COMMAND_USER_GET_COMMENTS       @"usergetcomments"
#define MSG_COMMAND_USER_GET_ROLE           @"usergetrole"
#define MSG_COMMAND_USER_SET_ROLE           @"usersetrole"
#define MSG_COMMAND_USER_GET_ROLES          @"usergetroles"
#define MSG_COMMAND_USER_APPLY_FOR_ORDER    @"userapplyfororder"
#define MSG_COMMAND_USER_VOTE               @"uservote"
#define MSG_COMMAND_USER_SET_PHOTO          @"usersetphoto"
#define MSG_COMMAND_USER_PAYMENT            @"consumerpayment"
#define MSG_COMMAND_USER_GET_FILE           @"consumergetfile"
#define MSG_COMMAND_USER_DISPUTE            @"consumerdispute"
#define MSG_COMMAND_USER_INVENTORY_LIST     @"userinventorylist"
#define MSG_COMMAND_USER_INVENTORY_CONVERT  @"userinventoryconvert"
#define MSG_COMMAND_USER_GET_APPLICANTS     @"usergetapplicants"
#define MSG_COMMAND_USER_GET_NOTIFICATIONS  @"usergetnotifications"
#define MSG_COMMAND_USER_SET_NOTIFICATION   @"usersetnotification"
#define MSG_COMMAND_USER_DONATE             @"userdonate"
#define MSG_COMMAND_USER_ACCEPT_INVITATION  @"useracceptinvitation"
#define MSG_COMMAND_USER_GET_ASSET          @"usergetasset"
#define MSG_COMMAND_USER_ASSET_LIST         @"userassetlist"
#define MSG_COMMAND_USER_MERCHANT_CREATE    @"usermerchantcreate"
#define MSG_COMMAND_USER_ASSET_STRUCTURE    @"userassetstructurelist"
#define MSG_COMMAND_USER_ACCESS_LIST        @"useraccesslist"
#define MSG_COMMAND_USER_GET_RATE           @"getrate"
#define MSG_COMMAND_USER_GET_MEDIA_FILES_LIST @"userfileslist"
#define MSG_COMMAND_USER_SET_MEDIA          @"userfileset"
#define MSG_COMMAND_USER_INVOICE            @"consumerinvoice"
#define MSG_COMMAND_USER_GET_ASSET          @"usergetasset"
#define MSG_COMMAND_USER_SET_ABUSE          @"usersetabuse"
#define MSG_COMMAND_USER_ROLES_DESCRIPTIONS @"usergetroledescriptions"
#define MSG_COMMAND_USER_ACCEPT_TOU         @"useraccepttou"

// MSG_COMMAND_MERCHANT

#define MSG_COMMAND_MERCHANT_ASSET_SET          @"merchantassetset"
#define MSG_COMMAND_MERCHANT_ASSET_LIST         @"merchantassetlist"
#define MSG_COMMAND_MERCHANT_GET_LIST           @"merchantlist"
#define MSG_COMMAND_MERCHANT_GET                @"merchantget"
#define MSG_COMMAND_MERCHANT_SET                @"merchantset"
#define MSG_COMMAND_MERCHANT_GET_ORDERS         @"merchantgetorders"
#define MSG_COMMAND_MERCHANT_SET_ORDER          @"merchantorderset"
#define MSG_COMMAND_MERCHANT_GET_PRODUCT_LIST   @"merchantproductlist"
#define MSG_COMMAND_MERCHANT_GET_PRODUCT        @"merchantproductget"
#define MSG_COMMAND_MERCHANT_SET_PRODUCT        @"merchantproductset"
#define MSG_COMMAND_MERCHANT_GET_SHOP           @"merchantshopget"
#define MSG_COMMAND_MERCHANT_SET_SHOP           @"merchantshopset"
#define MSG_COMMAND_MERCHANT_GET_SHOP_LIST      @"merchantshoplist"
#define MSG_COMMAND_MERCHANT_GET_TRANSACTIONS   @"merchantlisttransactions"
#define MSG_COMMAND_MERCHANT_INVENTORY_CONSUME  @"merchantinventoryconsume"
#define MSG_COMMAND_MERCHANT_ROLE               @"getmerchantroles"
#define MSG_COMMAND_MERCHANT_SET_EVENT          @"merchanteventset"
#define MSG_COMMAND_MERCHANT_UPDATE_TRANSACTION @"merchanttransactionupdate"
#define MSG_COMMAND_MERCHANT_QR_SET             @"merchantqrset"
#define MSG_COMMAND_MERCHANT_GET_CHAT           @"merchantgetchat"
#define MSG_COMMAND_MERCHANT_SEND_CHAT          @"merchantsendchatmsg"
#define MSG_COMMAND_MERCHANT_VOTE               @"merchantvote"
#define MSG_COMMAND_MERCHANT_SRUCTURE_SET       @"merchantassetstructureset"
#define MSG_COMMAND_MERCHANT_SET_LINK           @"merchantlinkset"
#define MSG_COMMAND_MERCHANT_GET_LINK           @"merchantlinklist"
#define MSG_COMMAND_MERCHANT_REPORT_RUN         @"merchantreportrun"


/**
 * Available parameters of the message.
 */

#define MSG_PARAM_ID                        @"id"
#define MSG_PARAM_VERSION                   @"version"
#define MSG_PARAM_COMMAND                   @"command"
#define MSG_PARAM_RANDOM                    @"random"
#define MSG_PARAM_HASH                      @"hash"
#define MSG_PARAM_IDENTITY_PLUGIN           @"identity_plugindata"
#define MSG_PARAM_IDENTITY_CLIENT           @"identity_client"
#define MSG_PARAM_SHOP_ID                   @"shop"
#define MSG_PARAM_SHOP_ID_FILTER            @"shopid"
#define MSG_PARAM_SHOP_QR_ID_FILTER         @"qrid"
#define MSG_PARAM_SHOP_SERVICE_TYPE         @"service_type"
#define MSG_PARAM_SHOP_MERCHANT_ID          @"merchantid"
#define MSG_PARAM_SHOP_QUERY_SUBMERCHANTS   @"query_submerchants"
#define MSG_PARAM_QUERY_CHILD_MERCHANTS     @"query_child_merchants"
#define MSG_PARAM_SHOP_QUERY_ALL_MERCHANTS  @"query_all_merchants"
#define MSG_PARAM_SHOP_QUERY_SHOW_RATINGS   @"query_show_ratings"
#define MSG_PARAM_SHOP_NETWORK_ID           @"network"
#define MSG_PARAM_SHOP_REFERENCE            @"shop_reference"
#define MSG_PARAM_LOGIN_IDENTITY            @"login_identity"
#define MSG_PARAM_LOGIN_PASSWORD            @"login_password"
#define MSG_PARAM_AUTH_TOKEN                @"access_token"
#define MSG_PARAM_USER_LOGIN                @"user_login"
#define MSG_PARAM_LOC_LATITUDE              @"html_latitude"
#define MSG_PARAM_LOC_LONGITUDE             @"html_longitude"
#define MSG_PARAM_LOC_ACCURACY              @"html_accuracy"
#define MSG_PARAM_QUERY_VALID               @"query_valid"
#define MSG_PARAM_QUERY_ID                  @"query_id"
#define MSG_PARAM_QUERY_COUNT               @"query_count"
#define MSG_PARAM_QUERY_SKIP                @"query_skip"
#define MSG_PARAM_QUERY_TIMESTAMP           @"query_timestamp"
#define MSG_PARAM_QUERY_LANGUAGE            @"query_language"
#define MSG_PARAM_QUERY_STATUS              @"query_status"
#define MSG_PARAM_QUERY_ORDER               @"query_order"
#define MSG_PARAM_QUERY_OWNER               @"query_owner"
#define MSG_PARAM_QUERY_THIRD_PERSON        @"query_third_person"
#define MSG_PARAM_QUERY_EXCLUDE_EXPIRED     @"query_exclude_expired"
#define MSG_PARAM_QUERY_START_DATE          @"query_start_date"
#define MSG_PARAM_QUERY_END_DATE            @"query_end_date"
#define MSG_PARAM_QUERY_TYPE                @"query_type"
#define MSG_PARAM_PRODUCT                   @"product"
#define MSG_PARAM_ACCOUNT_TYPE              @"account_type"
#define MSG_PARAM_ACCOUNT_NUMBER            @"account_number"
#define MSG_PARAM_ACCOUNT_SIGN              @"account_sign"
#define MSG_PARAM_ACCOUNT_AMOUNTH           @"account_amount"
#define MSG_PARAM_ACCOUNT_CURRENCY          @"account_currency"
#define MSG_PARAM_USER_EMAIL                @"consumer_str_email"
#define MSG_PARAM_USER_PASSWORD             @"consumer_str_password"
#define MSG_PARAM_USER_DICT_KEYSTORE        @"consumer_dict_keystore"
#define MSG_PARAM_USER_REGISTRATION         @"consumer_registration"
#define MSG_PARAM_USER_RE_REGISTRATION      @"consumer_reregistration"
#define MSG_PARAM_USER_TOU_KEY              @"tou_key"
#define MSG_PARAM_USER_INVOICERS            @"consumer_invoicers"
#define MSG_PARAM_USER_DEVICES              @"consumer_devices"
#define MSG_PARAM_MERCHANT_DEVICES          @"dict_devices"
#define MSG_PARAM_DEVICE_APP                @"app"
#define MSG_PARAM_DEVICE_TOKEN              @"token"
#define MSG_PARAM_DEVICE_TYPE               @"type"
#define MSG_PARAM_DEVICE_NAME               @"name"
#define MSG_PARAM_DEVICE_NOTIFY             @"notify"
#define MSG_PARAM_DEVICE_MANUFACTURER       @"manufacturer"
#define MSG_PARAM_DEVICE_MODEL              @"model"
#define MSG_PARAM_DEVICE_OS_VERSION         @"os_version"
#define MSG_PARAM_DISPUTE_TEXT              @"text"
#define MSG_PARAM_STATUS                    @"status"
#define MSG_PARAM_ROLE                      @"role_id"
#define MSG_PARAM_USERID                    @"user_id"
#define MSG_PARAM_ORDER                     @"order"
#define MSG_PARAM_ORDER_ID                  @"order_id"
#define MSG_PARAM_ORDER_STATUS              @"status"
#define MSG_PARAM_LANGUAGE                  @"language"

@interface MessageBinary : NSObject

@property (readonly, nonatomic) NSString* mime;
@property (readonly, nonatomic) NSData* binData;

-(id) initWithFile:(NSString*)filePath;
-(id) initWithData:(NSData*)data andMimeType:(NSString*)mimeType;

-(NSString*)fileName;

@end

/**
 * This class contains all requirement information to send requests and decode responses.
 */
@interface Message : NSObject

/** Command of the message. */
@property (readonly, nonatomic) NSString* command;

/** API url set fot this message. */
@property (readonly, nonatomic) NSURL* url;

/** Device info used in the message. */
@property (readonly, nonatomic) HKDevice* device;

/** Connection used in the message. */
@property (readonly, nonatomic) Connection* connection;

// TODO write docs
@property (nonatomic, nonnull) Identity* appIdentity;

/** Shop Identity that will used for the request. */
@property (nonatomic) Identity* shopIdentity;

/** YES if message support offline, NO otherwise. Default value is YES. */
@property (nonatomic) BOOL isSupportOfline;

/** YES if hash should be ignored in this request. Default value is NO. */
@property (nonatomic) BOOL isIgnoreHash;

/** Message flags. */
@property (nonatomic) int flags;

/** Command name that will used for sending local notifications. By default used command of the message. */
@property (nonatomic) NSString* notificationCommand;

@property (nonatomic) MessageBinary* binary;

/**
 * Init new Message.
 *
 * @param command
 *            Command of the message. See {@link Command}.
 * @param url
 *            Server url.
 * @param connection
 *            User authentication data.
 * @param device
 *            Device info.
 * @param shopIdentity
 *            Shop that will used for the request.
 * @param requestBuilder
 *            Builder for creating requests.
 * @param responseParser
 *            Builder for decode response.
 */
-(id)initWithCommand:(NSString*)command url:(NSURL*)url connection:(Connection*)connection device:(HKDevice*)device shopIdentity:(Identity*)shopIdentity requestBuilder:(id<RequestBuilder>)requestBuilder responseParser:(id<ResponseParser>)responseParser flags:(int)flags;

/**
 * Add a parameter for message.
 * @param key Key of parameter.
 * @param value Value of parameter.
 */
-(void)addParameter:(NSString*)key value:(NSObject*)value;

/**
 * Add a Dictionary of parameters for message.
 * @param params Dictionary of parameters.
 */
-(void)addParameters:(NSDictionary*)params;

/**
 * Gets parameter from message.
 * @param key Key of parameter.
 * @return Value of parameter or nil if parameter doesn't exists.
 */
-(NSObject*)parameter:(NSString*)key;

/**
 * Remove parameter from message by name.
 *
 * @param key Key of the removed parameter.
 */
-(void)removeParameter:(NSString*)key;

/**
 * Check if message has specified flag.
 * @param flag request flag.
 * @return YES if message has flag, NO otherwise;
 */
-(BOOL)isHasFlag:(int)flag;

/**
 * Check if message should be executed offline.
 * @return YES if is offline message, NO otherwise.
 */
-(BOOL)isOffline;

/**
 * Build request data.
 * @return Data of the request.
 */
-(NSData*)buildRequest;

/**
 * Parse response object from response string.
 *
 * @param responseString Response string.
 * @return Response object.
 */
-(Response*)parseResponse:(NSData*)responseString;

/**
 * Sets name of the cache file without extension.
 * @param name Name of the file that will used for caching response. If nil cache for this message will not created.
 * @param dependsOnShop If YES, shop id will be attached to cache file name.
 */
-(void)setCacheFileName:(NSString*)name dependsOnShop:(BOOL)dependsOnShop;

/**
 * Gets name of the cache file without extension.
 * @return Name of the file that used for caching response. If nill cache for this message will not created.
 */
-(NSString*)cacheFileName;


@end
