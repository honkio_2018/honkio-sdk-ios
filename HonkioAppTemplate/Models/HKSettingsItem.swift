//
//  HKSettingsItem.swift
//  WorkPilots
//
//  Created by developer on 1/13/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
Defines the item to be presented in SettingItemTableViewCell.
*/
open class HKSettingsItem: NSObject {
    /// Available items types.
    public static let ITEM_TYPE_UNDEFINED = "HonkioSettings.ITEM_TYPE_UNDEFINED"
    public static let ITEM_TYPE_USER_ACCOUNT = "HonkioSettings.ITEM_TYPE_USER_ACCOUNT"
    public static let ITEM_TYPE_PAYMENT_ACCOUNTS = "HonkioSettings.ITEM_TYPE_PAYMENT_ACCOUNTS"
    public static let ITEM_TYPE_CHANGE_PASSWORD = "HonkioSettings.ITEM_TYPE_CHANGE_PASSWORD"
    public static let ITEM_TYPE_CHANGE_PIN = "HonkioSettings.ITEM_TYPE_CHANGE_PIN"
    public static let ITEM_TYPE_LOST_PASSWORD = "HonkioSettings.ITEM_TYPE_LOST_PASSWORD"
    public static let ITEM_TYPE_TOU = "HonkioSettings.ITEM_TYPE_TOU"
    public static let ITEM_TYPE_GDPR = "HonkioSettings.ITEM_TYPE_GDPR"
    public static let ITEM_TYPE_ABOUT = "HonkioSettings.ITEM_TYPE_ABOUT"
    public static let ITEM_TYPE_LOGOUT = "HonkioSettings.ITEM_TYPE_LOGOUT"

    var iconName: String!
    
    var title: String!
    
    var type: String!

    /**
    Constructs as new item and call the action provided.
    - parameter initializer: The action to be performed on initialization of the item.
    */
    init(initializer: ((HKSettingsItem) -> Void)? = nil) {
        
        super.init()
        initializer?(self)
    }
}
