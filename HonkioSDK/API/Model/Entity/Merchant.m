//
//  Merchant.m
//  HonkioSDK
//
//  Created by Mikhail Li on 28/04/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "Merchant.h"
#import "AbsFilter.h"

@implementation Merchant

@synthesize bankIban, bankName, bankSwift, vat, vatNumber;

-(NSDictionary*)toParams {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:[super toParams]];
    
    [AbsFilter applyIfNotNil:@"str_businessid" value:self.businessId forParams:params];
    [AbsFilter applyIfNotNil:@"str_sepa" value:self.bankIban forParams:params];
    [AbsFilter applyIfNotNil:@"str_bankname" value:self.bankName forParams:params];
    [AbsFilter applyIfNotNil:@"str_swift" value:self.bankSwift forParams:params];
    [AbsFilter applyIfNotNil:@"str_vatnumber" value:self.vatNumber forParams:params];
    
    [AbsFilter applyIfNotNil:@"str_email" value:self.email forParams:params];
    [AbsFilter applyIfNotNil:@"str_telephone" value:self.phone forParams:params];
    
    [AbsFilter applyIfNotNil:@"parent" value:self.parentMerchant forParams:params];
    
    [AbsFilter applyIfNotNil:@"active" value: [NSNumber numberWithBool:self.isActive] forParams:params];
    
    return params;
}

@end
