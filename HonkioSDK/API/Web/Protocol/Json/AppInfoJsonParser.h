//
//  AppInfoJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/28/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"

/**
 * Parse AppInfo
 */
@interface AppInfoJsonParser : BaseJsonResponseParser

@end
