//
//  MerchantShopsListJsonParser.h
//  HonkioSDK
//
//  Created by Mikhail Li on 24/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"

@interface MerchantShopsListJsonParser : BaseJsonResponseParser

@end
