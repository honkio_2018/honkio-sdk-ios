//
//  JsonRequestBuilder.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestBuilder.h"

/**
 * The builder to create request string in JSON format.
 */
@interface JsonRequestBuilder : NSObject<RequestBuilder>

+(NSData*)toJson:(id)object;

@end
