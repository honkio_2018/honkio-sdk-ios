//
//  HKUserRoleStructureListJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseJsonResponseParser.h"

@interface HKUserRoleStructureListJsonParser : BaseJsonResponseParser

@end
