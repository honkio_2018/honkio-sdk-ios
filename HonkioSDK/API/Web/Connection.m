//
//  Connection.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "Connection.h"
#import "StoreData.h"
#import "ApiSecurity.h"

#define STORE_KEY_OTP         @"otp"
#define STORE_KEY_PIN_FAILS   @"pin_fails_count"

static int pinFailsCount = 0;

@implementation Connection {
    NSString* login;
    NSString* password;
    NSString* otp;
    NSString* pin;
    NSDate* lastTimePinChecked;
    BOOL isValid;
}

-(id)init {
    self = [super init];
    if (self) {
        isValid = NO;
    }
    return self;
}

-(id)initWithPin:(NSString*)pPin {
    self = [super init];
    if (self) {
        isValid = YES;
        pin = pPin;
        lastTimePinChecked = [NSDate new];
        pinFailsCount = [[StoreData loadFromStore:STORE_KEY_PIN_FAILS] intValue];
        login = [StoreData loadFromStore:STORE_KEY_LOGIN];
        if (!login)
            isValid = NO;
        otp = [ApiSecurity decryptString:[StoreData loadFromStore:STORE_KEY_OTP] withKey:pin];
        if (!otp)
            isValid = NO;
    }
    return self;
}

-(id)initWithLogin:(NSString*)pLogin password:(NSString*)pPassword {
    self = [self initWithLogin:pLogin password:pPassword pin:nil];
    return self;
}

-(id)initWithLogin:(NSString*)pLogin password:(NSString*)pPassword pin:(NSString*)pPin {
    self = [super init];
    if (self) {
        login = pLogin;
        [StoreData saveToStore:login forKey:STORE_KEY_LOGIN];
        password = pPassword;
        pin = pPin;
        lastTimePinChecked = [NSDate new];
        pinFailsCount = 0;
        isValid = YES;
    }
    return self;
}

-(void)exit {
    otp = nil;
    password = nil;
    [Connection removeSoredOtp];
    isValid = false;
}

-(BOOL)isValid {
    return login && [self otp] && isValid;
}

-(void)setToInvalid {
    isValid = false;
}

-(NSString*)login {
    return login;
}

-(NSString*)otp {
    if (!otp) {
        return password;
    }
    return otp;
}

-(void)setOtp:(NSString*)pOtp {
    otp = pOtp;
    if (pOtp && pin) {
        [StoreData saveToStore:[ApiSecurity encryptString:otp withKey:pin] forKey:STORE_KEY_OTP];
    }
}

-(void)changePin:(NSString*)newPin {
    pin = newPin;
    if (otp)
        [StoreData saveToStore:[ApiSecurity encryptString:otp withKey:pin] forKey:STORE_KEY_OTP];
}

-(BOOL)changePin:(NSString*)currentPin newPin:(NSString*)newPin {
    if ([self checkPin:currentPin]) {
        [self changePin:newPin];
        return YES;
    }
    return NO;
}

-(BOOL)checkPin:(NSString*)pPin {
    BOOL result = YES;
    if (pin) {
        result = [pin isEqualToString:pPin];
        if (result) {
            lastTimePinChecked = [NSDate new];
            pinFailsCount = 0;
        }
    }
    
    if (!result)
        [self increasePinFails];
    
    return result;
}

-(BOOL)increasePinFails {
    pinFailsCount++;
    if (pinFailsCount >= MAX_PIN_FAIL_COUNT) {
        [self exit];
    }
    return isValid;
}


+(BOOL)isHasSoredOtp {
    return [StoreData loadFromStore:STORE_KEY_OTP] != nil;
}

+(void)removeSoredOtp {
    [StoreData deleteFromStore:STORE_KEY_OTP];
}

+ (int)pinFailsCount {
    
    return pinFailsCount;
}

@end
