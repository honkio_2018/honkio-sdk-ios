//
//  UIExtensions.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 4/6/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
import Foundation

infix operator =~

func =~ (string: String, regex: String) -> Bool {
    
    return string.range(of: regex, options: .regularExpression) != nil
}

/**
 Extends UITextField with the custom fext validation and error reporting.
 */
extension UITextField {

    /**
    - Returns: True if error message is displaying, otherwise false.
    */
    public var hasError: Bool {
        
        return self.rightView != nil
    }

    /**
     Shows the custom alert within the button titled by UIControlState.Reserved localized value.
     - parameter button: The button to be added to CustomAlertView.
     * No special action to be performed, no title to be added to AlertView.
    */
    @objc public func showError(_ button: UIButton!) {
        
        CustomAlertView(okTitle: nil, okMessage: button.title(for: UIControlState.reserved)!.localized, okAction: nil).show()
    }

    /**
     Displays error pictire and error message at the right side of the text view.
     - parameter message: The text to be displayed as explanation of the error.
    */
    public func setErrorMessage(_ message:String) {
        
        let image = HKBundleUtils.image("hk_text_error")
        let buttonHeight = self.bounds.height - 2
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: buttonHeight, height: buttonHeight))
        button.setTitle(message, for: .reserved)
        button.setImage(image, for: UIControlState())
        button.addTarget(self, action: #selector(UITextField.showError(_:)), for: UIControlEvents.touchUpInside)
        self.rightViewMode = UITextFieldViewMode.always
        self.rightView = button
    }

    /**
     Hides (removes) the error message for the UITextView.
    */
    public func disableError() {
        
        self.rightView = nil
    }

    /**
     Checks the text entered in UITextView fits the criteria to be a password.
     - Returns: True if the value is ok to be a password, otherwise false.
     * Note: password is more then 6 chars. Whitespaces are trimmed from both ends.
    */
    public var checkPassword: Bool {
        
        self.text = self.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if self.text != nil && self.text!.count >= 6 {
            
            return true
        }
        
        return false
    }

    /**
     Checks the text entered in UITextView fits the criteria to be an email address.
     - Returns: True if the value is ok to be an email, otherwise false.
    */
    public var checkEmail: Bool {
        
        if self.text != nil && (self.text! =~ "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$") {
            
            return true
        }
        
        return false
    }

    /**
     Checks the text entered in UITextView fits the criteria to be a PIN code.
     - Returns: True if the value is ok to be a PIN, otherwise false.
     Note: only digits are allowed.
    */
    public var checkPin: Bool {
        
        if self.text != nil && (self.text! =~ "^\\d+$") {
            
            return true
        }
        
        return false
    }
}

/**
 Extends UIColor class with the custom initialization methods.
*/
extension UIColor {

    /**
     Checks the R,G,B values are in proper bounds. Construct UIColor class within provided RGB values.
     * Alpha is always 1.0
     - parameter red: 0-255 ranged value for Red channel of UIColor.
     - parameter green: 0-255 ranged value for Green channel of UIColor.
     - parameter blue: 0-255 ranged value for Blue channel of UIColor.
    */
    public convenience init(red: Int, green: Int, blue: Int) {
        
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }

    /**
     Construct UIColor class within provided hex value.
     - parameter netHex: The value to be converted to an RGB.
    */
    public convenience init(netHex:Int) {
        
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }

    /**
     Construct UIColor class within provided hex and alpha values.
     - parameter netHex: The value to be converted to an RGB.
     - parameter alpha: The value for Alpha channel.
    */
    public convenience init(hex: Int, alpha: CGFloat) {
        
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
