//
//  QueryPaymentProcessModel.m
//  HonkioApi
//
//  Created by Shurygin Denis on 8/4/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "QueryPaymentProcessModel.h"
#import "HonkioApi.h"

@implementation QueryPaymentProcessModel {
    ResponseHandler responseHandler;
    Identity* shopIdentity;
    NSString* queryId;
    long delay;
}

-(id)initWithHandler:(ResponseHandler)handler shopIdentity:(Identity *)identity transactionId:(NSString *)transactionId {
    self = [super init];
    if (self) {
        responseHandler = handler;
        shopIdentity = identity;
        queryId = transactionId;
    }
    return self;
}

-(id)initWithProcessModel:(BaseProcessModel*)processModel shopIdentity:(Identity *)identity transactionId:(NSString *)transactionId {
    return [self initWithHandler:^(Response *response) {
        if ([response isStatusAccept]) {
            [processModel onComplete:response];
        }
        else if ([response isStatusPending]) {
            // Do nothing
        }
        else {
            [processModel onError:response];
        }
    } shopIdentity:identity transactionId:transactionId];
}

-(void)query {
    if (queryId) {
        [HonkioApi queryPaymentRequest:queryId shop:[self shopIdentity] flags:0 handler:responseHandler];
    }
}

-(void)query:(long)loopDelay {
    delay = loopDelay;
    if (delay > 0) {
        [self dispatchQueryAfter:delay];
    }
    else
        [self query];
}

-(void)stopQuery {
    delay = 0;
}

-(BOOL)abort {
    _isAborted = true;
    if (queryId) {
        [HonkioApi transactionSetStatus:RESPONSE_STATUS_REJECT transactionId:queryId shop:[self shopIdentity] flags:0 handler:^(Response *response) {
            if ([response isStatusError])
                [self query];
            else {
                if ([response isStatusReject])
                    responseHandler(response);
                [self query];
            }
        }];
        return NO;
    }
    return YES;
}

-(void)dispatchQueryAfter:(long)queryDelay {
    if (![self isAborted]) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, queryDelay * NSEC_PER_MSEC), dispatch_get_main_queue(), ^{
            if (![self isAborted]) {
                
                __weak QueryPaymentProcessModel* weakSelf = self;
                __weak ResponseHandler weakHandler = responseHandler;
                long queryDelay = delay;
                
                ResponseHandler queryHandler = ^(Response* response) {
                    QueryPaymentProcessModel* strongSelf = weakSelf;
                    ResponseHandler strongHandler = weakHandler;
                    
                    if (response) {
                        if ([response isStatusPending]) {
                            if (strongSelf && queryDelay > 0) {
                                [strongSelf dispatchQueryAfter:queryDelay];
                            }
                        }
                        else if (strongHandler) {
                            strongHandler(response);
                        }
                    }
                };
                
                [HonkioApi queryPaymentRequest:queryId shop:[self shopIdentity] flags:0 handler:queryHandler];
            }
        });
    }
}

-(Identity*)shopIdentity {
    if (shopIdentity) {
        return shopIdentity;
    }
    return [HonkioApi mainShopIdentity];
}

@end
