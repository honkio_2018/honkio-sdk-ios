//
//  PriorityQueue.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "PriorityQueue.h"

@implementation PriorityQueue {
    NSOperationQueue *requestQueue;
}

CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(PriorityQueue);

-(id)init {
    self = [super init];
    if (self) {
        requestQueue = [[NSOperationQueue alloc] init];
        requestQueue.maxConcurrentOperationCount = 1;
    }
    return self;
}

+(void)addOperation:(NSOperation *)operation {
    [[[PriorityQueue shared] queue] addOperation:operation];
}

-(NSOperationQueue*)queue {
    return requestQueue;
}

@end
