//
//  HKGenericTabViewDataSource.swift
//  Honkio SDK
//
//  Created by developer on 12/23/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

//FIXME: not used!!!
/**
* HKGenericTabViewDataSource should be used for managing the data loaded from server in UITableView.
*/
open class HKGenericTabViewDataSource<TTableViewCell: UITableViewCell, TTableItem> : NSObject, UITableViewDataSource, UITableViewDelegate {

    /**
    Contains the list of sections.
    */
    open var sections: [HKCollectionSection<TTableItem>] = []

    /**
    The action Will be performed during the initialization of cell by the object.
    */
    open var initializeCellAction: (TTableViewCell, TTableItem) -> Void?

    /**
    Containes the identifier for reuse.
    */
    open var reusableIdentifier: String?

    /**
    Constructs the new instance of class.
    - parameter reusableIdentifier: The string value to be used for indentification for reuse.
    - parameter initializeCellAction: The action to be performed on initialization.
    */
    public init(reusableIdentifier: String?, initializeCellAction: @escaping (TTableViewCell, TTableItem) -> Void) {
        
        self.reusableIdentifier = reusableIdentifier
        self.initializeCellAction = initializeCellAction
        
        super.init()
    }

    /**
    - Returns: The number of row in the section.
    - parameter tableView: The UITableView the number of rows in section to be found.
    - parameter numberOfRowsInSection: The index of section the number of rows to be found in.
    */
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.sections[section].items.count
    }

    /**
    - Returns: The number of sections.
    - parameter tableView: The UITableView the number of sections are returned.
    */
    open func numberOfSections(in tableView: UITableView) -> Int {
        if self.sections.count == 0 {
            return 1
        } else {
            return self.sections.count
        }
    }

    /**
    - Returns: The cell from UITableView for provided IndexPath.
    - parameter tableView: The UITableView the cell will be found.
    - parameter indexPath: The IndexPath for the cell.
    */
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = self.sections[(indexPath as IndexPath).section].items[(indexPath as IndexPath).row]

        let cell = tableView.dequeueReusableCell(withIdentifier: self.reusableIdentifier ?? "", for: indexPath)
        
        self.initializeCellAction(cell as! TTableViewCell, item)
        
        return cell
    }
}
