//
//  ApplicantsListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/9/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "ApplicantsListJsonParser.h"
#import "UserRoleJsonParser.h"
#import "ApplicantsList.h"

@implementation ApplicantsListJsonParser {
    UserRoleJsonParser* roleParser;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        roleParser = [UserRoleJsonParser new];
    }
    return self;
}

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    ApplicantsList* result = [ApplicantsList new];
    
    NSMutableArray<Applicant*>* list = [NSMutableArray new];
    
    NSArray* applicants = dict[@"applicants"];
    if (applicants) {
        for (NSDictionary* applicant in applicants) {
            if([applicant isKindOfClass:[NSDictionary class]]) {
                [list addObject:[self parseApplicant:applicant]];
            }
        }
    }
    result.list = list;
    
    return result;
}

-(Applicant*)parseApplicant:(NSDictionary*)dict {
    Applicant* applicant = [Applicant new];
    
    applicant.orderId = dict[@"order_id"];
    applicant.applicationId = dict[@"id"];
    applicant.userId = dict[@"worker_id"];
    applicant.timeProposal = dict[@"time_proposal"];
    
    if ([applicant.timeProposal isKindOfClass:[NSNull class]]) {
        applicant.timeProposal = nil;
    }
    
    NSDictionary* roleDict = dict[@"role"];
    if (roleDict) {
        applicant.role = [roleParser parseDictionary:nil dict:roleDict];
        if (applicant.role) {
            applicant.userId = applicant.role.userId;
        }
    }
    
    return applicant;
}

@end
