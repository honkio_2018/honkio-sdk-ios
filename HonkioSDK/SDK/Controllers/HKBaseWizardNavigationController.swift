//
//  HKBaseWizardNavigationController.swift
//  WorkPilots
//
//  Created by developer on 12/14/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

/// HKBaseWizardNavigationController monitors the wizard step number and wizard step count.
/// Notifies the completion listener about completion of the wizard.
/// Dictates derrived classes to override the *pushViewControllerWithIdentifier* function,
/// initiates pushing of ViewController of current wizard step.

open class HKBaseWizardNavigationController: HKBaseNavigationController, HKWizardProtocol, HKWizardViewControllerCompletionDelegate {

    /// Containes functions that construct wizard view controllers.
    open var wizardViewControllers: [((_ parent:UINavigationController) -> UIViewController?)] = []

    /// Containes wizard instance object.
    open var wizardObject: AnyObject?
    
    fileprivate weak var completionDelegate : HKWizardViewControllerCompletionDelegate?
    fileprivate var tag: String?

    /**
    Sets the completion listener (delegate) to be notified when the wizard has been completed.
    - parameter completionDelegate: The listener (delegate) of HKWizardViewControllerCompletionDelegate type.
    - parameter tag: The String value the completion notification will be tagged with.
    */
    open func setCompletionDelegate(_ completionDelegate: HKWizardViewControllerCompletionDelegate?, tag: String?) {
        self.completionDelegate = completionDelegate
        self.tag = tag
    }
    
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        completionDelegate = self
    }
    
    public override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        completionDelegate = self
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        completionDelegate = self
    }
    
    public init() {
        super.init(nibName: nil, bundle:  nil)
        completionDelegate = self
    }

    /// Moves to next wizard step (by pushing next ViewController) if possible or completes wizard.
    open func nextViewController() {
        
        if self.wizardViewControllers.count == 0 {
            
            self.wizardComplete()
            return
        }

        let index = self.viewControllers.count
        if index > 0 {
            
            if index == self.wizardViewControllers.count {
                
                self.wizardComplete()
                return
            }
            
            let controller = self.wizardViewControllers[index](self)
            self.pushViewController(controller!, animated: true)
        }
        else {
            let controller = self.wizardViewControllers[0](self)
            self.pushViewController(controller!, animated: false)
        }
    }

    /// Notifies the listener (delegate) that the wizard is completed.
    open func wizardComplete() {
        if let delegate = self.completionDelegate {
            delegate.onWizardComplete(self, tag: self.tag, wizardObject: self.wizardObject)
        }
    }
    
    open func onWizardComplete(_ viewController: UIViewController, tag: String?, wizardObject: AnyObject?) {
        preconditionFailure("Method onWizardComplete must be overridden")
    }
}
