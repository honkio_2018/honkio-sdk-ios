//
//  AssetJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "HKAsset.h"

@interface AssetJsonParser : BaseJsonResponseParser

+(HKAsset*)parseAsset:(NSDictionary*)assetDict;

@end
