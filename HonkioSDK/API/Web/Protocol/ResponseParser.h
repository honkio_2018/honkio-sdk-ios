//
//  ResponseParser.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//
#import "Response.h"

@class Message;

/**
 * The Parser of the server response.
 */
@protocol ResponseParser <NSObject>

/**
 * Parse server response into Client Api object.
 * @param responseData Data which server returns.
 * @param password The password to check hash of the response.
 * @return Object that represent the server response.
 */
-(Response*)parse:(Message*)message response:(NSData*)responseData password:(NSString*)password;

@end