//
//  InvitationJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 11/29/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "InvitationJsonParser.h"
#import "Invitation.h"

@implementation InvitationJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    Invitation* invitation = nil;
    
    NSDictionary* invitationDict = dict[@"invitation"];
    
    if (invitationDict && [invitationDict isKindOfClass:[NSDictionary class]]) {
        invitation = [Invitation new];
        
        invitation.bonus = [invitationDict[@"bonus"] doubleValue];
        invitation.message = invitationDict[@"message"];
    }
    
    return invitation;
}

@end
