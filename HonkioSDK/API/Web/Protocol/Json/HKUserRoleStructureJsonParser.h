//
//  HKUserRoleStructureJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "HKUserRoleStructure.h"

@interface HKUserRoleStructureJsonParser : BaseJsonResponseParser

+(HKUserRoleStructure*)toStructure:(NSDictionary*)structureDict;

@end
