//
//  ChatMessageFilter.h
//  HonkioSDK
//
//  Created by Dev on 01.09.16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AbsListFilter.h"

@interface ChatMessageFilter : AbsListFilter

@property (nonatomic) NSString *orderId;

@end
