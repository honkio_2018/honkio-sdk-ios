//
//  UserRolesListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/26/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "UserRolesListJsonParser.h"
#import "UserRoleJsonParser.h"

@implementation UserRolesListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    UserRolesList* result = [UserRolesList new];
    NSMutableArray<UserRole*>* list = [NSMutableArray new];
    
    NSString* userId = dict[@"user_id"];
    NSString* userFirstName = dict[@"user_first_name"];
    NSString* userLastName = dict[@"user_last_name"];
    
    NSArray* rolesArray = dict[@"roles"];
    if (rolesArray) {
        for(NSDictionary* roleDict in rolesArray) {
            UserRole* userRole = [UserRoleJsonParser parseRole:roleDict];
            userRole.userId = userId;
            userRole.userFirstName = userFirstName;
            userRole.userLastName = userLastName;
            [list addObject:userRole];
        }
    }
    result.list = [[NSArray alloc] initWithArray:list];
    return result;
}

@end
