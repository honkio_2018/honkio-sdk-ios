//
//  HonkioOAuth.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/21/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import "HonkioOAuth.h"
#import "HonkioApi.h"

#define DEFAULT_AUTHORIZE_URL @"https://oauth.honkio.com/authorize"
#define DEFAULT_TOKEN_URL @"https://oauth.honkio.com/token"

@implementation HonkioOAuth

+(NSString*)authorisationUrl:(HKDevice*) device forClient:(NSString*)clientId redirect:(NSString*)redirectUrl {
    ServerInfo* serverInfo = [HonkioApi serverInfo];
    return [NSString stringWithFormat:
            @"%@?platform=%@&device=%@&response_type=code&client_id=%@&redirect_uri=%@&scope=email",
            ([[serverInfo oauthAuthorizeURL] length] > 0 ? [serverInfo oauthAuthorizeURL] : DEFAULT_AUTHORIZE_URL),
            device.platform, device.device_id, clientId, redirectUrl];
}

+(NSString*)accessTokenUrl:(HKDevice*) device forClient:(NSString*)clientId clientSecret:(NSString*)clientSecret redirect:(NSString*)redirectUrl token:(NSString*)authorizationToken {
    ServerInfo* serverInfo = [HonkioApi serverInfo];
    return [NSString stringWithFormat:
            @"%@?platform=%@&device=%@&grant_type=authorization_code&code=%@&client_id=%@&client_secret=%@&redirect_uri=%@",
            ([[serverInfo oauthTokenURL] length] > 0 ? [serverInfo oauthTokenURL] : DEFAULT_TOKEN_URL),
            device.platform, device.device_id, authorizationToken, clientId, clientSecret, redirectUrl];
}

@end
