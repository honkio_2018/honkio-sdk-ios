//
//  HKStructure.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HK_STRUCTURE_PROPERTY_TYPE_BOOLEAN @"boolean"
#define HK_STRUCTURE_PROPERTY_TYPE_INT @"int"
#define HK_STRUCTURE_PROPERTY_TYPE_STRING @"string"
#define HK_STRUCTURE_PROPERTY_TYPE_URL @"url"
#define HK_STRUCTURE_PROPERTY_TYPE_PHONE @"phone"
#define HK_STRUCTURE_PROPERTY_TYPE_EMAIL @"email"
#define HK_STRUCTURE_PROPERTY_TYPE_ENUM @"enum"
#define HK_STRUCTURE_PROPERTY_TYPE_ARRAY @"array"

// Property
@interface HKStructureProperty : NSObject

@property (nonatomic) NSString* type;
@property (nonatomic) NSString* name;
@property (nonatomic) NSString* title;
@property (nonatomic) BOOL isRequired;

-(NSDictionary*) toParams;

@end

// Numeric Property
@interface HKStructureNumericProperty : HKStructureProperty

@property (nonatomic) double min;
@property (nonatomic) double max;

@end

// Enum Property
@interface HKStructureEnumPropertyValue : NSObject

@property (nonatomic) NSString* name;
@property (nonatomic) NSString* value;
@property (nonatomic, readonly) NSMutableDictionary* props;

-(id) initWithProps:(NSDictionary*)props;

-(NSDictionary*) toParams;

@end

@interface HKStructureEnumProperty : HKStructureProperty

@property (nonatomic) NSArray<HKStructureEnumPropertyValue*>* values;

-(HKStructureEnumPropertyValue*)find:(NSString*)value;

@end

// Array property
@interface HKStructureArrayProperty : HKStructureProperty

@property (nonatomic) HKStructureProperty* subtype;

@end

// Structure
@interface HKStructure : NSObject

-(id)init;
-(id)initWithId:(NSString*) objectId;

@property (nonatomic) NSString* objectId;
@property (nonatomic) NSString* name;
@property (nonatomic) NSString* merchantId;

@property (nonatomic) NSDictionary<NSString*, HKStructureProperty*>* properties;

-(nullable HKStructureEnumPropertyValue *)findEnumPropertyValue:(NSString *)propertyName value:(NSString *)value;

-(NSDictionary*) toParams;

@end
