//
//  HKViewControllerLifeCycleObserver.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 4/1/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation
import UIKit


@objc public protocol HKLifeCycleObserverParentViewController {
    func addLifeCycleObserver(_ observer: HKViewControllerLifeCycleObserver)
    func getLifeCycleObserver(_ tag: String) -> HKViewControllerLifeCycleObserver?
    func removeLifeCycleObserver(_ observer: HKViewControllerLifeCycleObserver)
}

/**
The HKViewControllerLifeCycleObserver class should be used as a helper for any ViewController class as a
monitor for ViewController life cycle and notifier of life cycle state changes.
HKViewControllerLifeCycleObserver could be instatntiated within the param of parent ViewController or
could be attached to a ViewController by didAddToViewController call.
*/
open class HKViewControllerLifeCycleObserver : NSObject, HKPinProtocol {
    
    override public init() {
        super.init()
    }
    
    /**
     Instantiates the HKViewControllerLifeCycleObserver with provided tag
     - parameter tag: String tag.
     */
    public convenience init(_ tag:String) {
        self.init(tag, parent: nil)
    }
    
    /**
     Instantiates the HKViewControllerLifeCycleObserver and adds it to a provided ViewController
     - parameter parent: Provided ViewController the life cycle to be monitored of.
     */
    public convenience init(parent: HKLifeCycleObserverParentViewController) {
        self.init(nil, parent: parent)
    }

    /**
     Instantiates the HKViewControllerLifeCycleObserver with provided tag and adds it to a provided ViewController
     - parameter tag: String tag.
     - parameter parent: Provided ViewController the life cycle to be monitored of.
    */
    public init(_ tag:String? ,parent: HKLifeCycleObserverParentViewController?) {
        super.init()
        parent?.addLifeCycleObserver(self)
        self.tag = tag
    }
    
    fileprivate(set) open var parent : HKLifeCycleObserverParentViewController!
    fileprivate(set) open var tag : String?
    fileprivate(set) open var viewController : UIViewController!

    /**
     * Runs when containing view got viewDidLoad.
     */
    open func viewDidLoad() { }

    /**
     * Runs when containing view got viewWillAppear.
     */
    open func viewWillAppear(_ animated: Bool) { }

    /**
     * Runs when containing view got viewDidAppear.
     */
    open func viewDidAppear(_ animated: Bool) { }

    /**
     * Runs when containing view got viewWillDisappear.
     - parameter animated: Whether the animation is to be used.
     */
    open func viewWillDisappear(_ animated: Bool) { }

    /**
     * Runs when containing view got viewDidDisappear.
     - parameter animated: Whether the animation is to be used.
     */
    open func viewDidDisappear(_ animated: Bool) { }

    /**
     * Runs when instance has been added to desired ViewController.
     */
    open func didAddToViewController(_ controller: UIViewController?) {
        self.parent = controller as? HKLifeCycleObserverParentViewController
        self.viewController = controller
    }


    /**
    * Should be called when PIN entered.
    - parameter pinCode: Entered PIN code.
    - parameter reason: Defines the reason of entering pin code.
    */
    open func pinEntered(_ pinCode: String!, reason: Int!) {
        
    }


    /**
    * Should be called when PIN pad canceled without entering a PIN.
    - parameter reason: Defines the reason of entering pin code.
    - parameter view: Parent view.
    */
    open func pinCanceled(_ reason: Int!) {
        
    }

    func isViewControllerVisible() -> Bool {
        return self.viewController != nil && self.viewController!.isViewLoaded && self.viewController!.view.window != nil
    }
}
