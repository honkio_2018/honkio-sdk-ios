//
//  AmountPaymentProcessModel.m
//  HonkioApi
//
//  Created by Shurygin Denis on 11/27/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "AmountPaymentProcessModel.h"
#import "HonkioApi.h"

@implementation AmountPayParams

-(AmountPaymentProcessModel*) instantiateProcessModel {
    return [[AmountPaymentProcessModel alloc] init];
}

@end

@implementation AmountPaymentProcessModel

-(void)pay:(Shop*)shop orderId:(NSString*)orderId account:(UserAccount*)account amount:(double)amount currency:(NSString*)currency flags:(int)flags {
    AmountPayParams* payParams = [[AmountPayParams alloc] initWithShop:shop oirderId:orderId account:account flags:flags];
    payParams.amount = amount;
    payParams.currency = currency;
    [self pay:payParams];
}

-(void)pay:(UserAccount*)account amount:(double)amount currency:(NSString*)currency {
    [self pay:account amount:amount currency:currency flags:0];
}

-(void)pay:(UserAccount*)account amount:(double)amount currency:(NSString*)currency flags:(int)flags {
    [self pay:nil orderId:nil account:account amount:amount currency:currency flags:flags];
}

-(void)doPayment:(PayParams*)payParams handler:(ResponseHandler)handler {
    AmountPayParams* amountParams = (AmountPayParams*) payParams;
    [HonkioApi paymentRequest:[amountParams amount] currency:[amountParams currency] account:[payParams account] shop:[amountParams shop] order:[amountParams orderId] params:nil flags:[payParams flags] handler:handler];
}

@end
