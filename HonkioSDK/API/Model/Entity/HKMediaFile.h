//
//  HKMediaFile.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/13/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HK_MEDIA_FILE_TYPE_IMAGE @"image"
#define HK_MEDIA_FILE_TYPE_VIDEO @"video"

#define HK_MEDIA_FILE_ACCESS_PUBLIC @"public"
#define HK_MEDIA_FILE_ACCESS_PRIVATE @"private"

#define HK_MEDIA_FILE_METADATA_SIZE @"size"
#define HK_MEDIA_FILE_METADATA_TYPE @"type"

@interface HKMediaFile : NSObject

@property (nonatomic) NSString *objectId;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString *access;
@property (nonatomic) NSString *extension;
@property (nonatomic) NSString *userOwnerId;
@property (nonatomic) NSString *timestamp;
@property (nonatomic) NSDictionary<NSString*, NSObject*> *metaData;

@property (readonly) NSString *type;
@property (readonly, assign) int size;

-(id)initWithId:(NSString*) objectId url:(NSString*)url;

@end
