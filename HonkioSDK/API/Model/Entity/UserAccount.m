//
//  UserAccount.m
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/3/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import "UserAccount.h"

@implementation UserAccount

-(id)initFrom:(UserAccount*) account {
    self = [super init];
    
    self.type = account.type;
    self.number = account.number;
    self.index = account.index;

    self.isHasLeft = account.isHasLeft;
    self.left = account.left;
    self.limit = account.limit;
    self.isSign = account.isSign;
    self.accountDescription = account.accountDescription;
    self.logoUrl = account.logoUrl;
    
    return self;
}

-(id)initWithType:(NSString*)type andNumber:(NSString*)number {
    self = [super init];
    if (self) {
        _type = type;
        _number = number;
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _isEnabled = YES;
    }
    return self;
}

-(BOOL)typeIs:(NSString*)type {
    if (self.type) {
        return [self.type isEqualToString:type];
    }
    return !type;
}

-(BOOL)enoughLeft:(double)value {
    if (value != 0 && [self isHasLeft]) {
        return [self left] >= value;
    }
    return YES;
}

-(BOOL)isHiden {
    return [self isDummy];
}


-(BOOL)isDummy {
    return !self.type
    || !self.number
    || ([self typeIs:ACCOUNT_TYPE_CREDITCARD] && [@"0" isEqualToString:self.number])
    || ([self typeIs:ACCOUNT_TYPE_NETS] && [@"0" isEqualToString:self.number])
    || ([self typeIs:ACCOUNT_TYPE_SEITATECH] && [@"0" isEqualToString:self.number]);
}

-(NSDictionary*) toParams {
    return @{MSG_PARAM_ACCOUNT_TYPE : self.type,
             MSG_PARAM_ACCOUNT_NUMBER: self.number};
}

@end
