//
//  HKUserAccountsTableViewCell.swift
//  WorkPilots
//
//  Created by developer on 2/25/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
The table view cell representing the use accoount item.
*/
open class HKUserAccountsTableViewCell: HKBaseTableViewCell {
    
    fileprivate(set) open lazy var cardImage : UIImageView  = self.makeLeftImageView()
    fileprivate(set) open lazy var cardTitle : UILabel  = self.makeTitleLabel()
    fileprivate(set) open lazy var cardDesc : UILabel  = self.makeDescLabel()
    fileprivate(set) open lazy var cardWarning : UILabel  = self.makeWarningLabel()
    
    open var account: UserAccount? {
        didSet {
            if self.account != nil {
                self.fillCell(self.account!)
            }
        }
    }
    
    open var isEnabled: Bool = true {
        didSet {
            if isEnabled {
                self.cardImage.alpha = 1
                self.cardTitle.alpha = 1
                self.cardDesc.alpha = 1
                self.cardWarning.alpha = 1
            }
            else {
                let alpha : CGFloat = 0.5
                
                self.cardImage.alpha = alpha
                self.cardTitle.alpha = alpha
                self.cardDesc.alpha = alpha
                self.cardWarning.alpha = alpha
            }
        }
    }
    
    func fillCell(_ account: UserAccount) {
        self.isEnabled = account.isEnabled
        var imageCard = HKUserAccountFormatter.getAccountImage(account)
        if imageCard == nil {
            imageCard = HKBundleUtils.image("hk_ic_account_card")
        }
        
        if account.logoUrl != nil {
            self.cardImage.imageFromURL(link: account.logoUrl, errorImage: imageCard, contentMode: nil)
        }
        else {
            self.cardImage.image = imageCard
        }
        
        self.cardTitle.text = HKUserAccountFormatter.getAccountName(account)
        self.cardDesc.text = HKUserAccountFormatter.getAccountDesc(account)
    }
    
    // MARK: - Lifecycle
    /**
    Sets the cell UI style.
    * sectionStyle is defined as .None,
    * layoutMargins and separatorInset are defined as UIEdgeInsetsZero,
    * backgroundColor is defined as .None,
    * sectionStyle is defined as 0.0 white, 0.0 alpha.
    */
    override open func setupStyle() {
        super.setupStyle()
        
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
        
        selectedBackgroundView = UIView()
        backgroundColor = UIColor.clear
    }
    
    /**
    Add card image and card title into cell content.
    */
    override open func setupSubviews() {
        super.setupSubviews()
        
        contentView.addSubview(cardImage)
        contentView.addSubview(cardTitle)
        contentView.addSubview(cardDesc)
        contentView.addSubview(cardWarning)
    }
    /**
    Sets the card image and card title constraints.
    */
    override open func setupConstraints() {

        let views : [String : AnyObject] = [
            "cardImage" : cardImage,
            "cardTitle" : cardTitle,
            "cardDesc" : cardDesc,
            "cardWarning" : cardWarning
        ]
        
        let formatArray : [String] = [
            "H:|-8-[cardImage(30)]-5-[cardTitle]-8-|",
            "H:[cardImage]-5-[cardDesc(==cardTitle)]",
            "H:[cardImage]-5-[cardWarning(==cardTitle)]",
            "V:|-8-[cardTitle]-0-[cardDesc]-0-[cardWarning]-8-|",
            "V:|-8-[cardImage(30)]-(>=8)-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    // MARK: - Private methods
    fileprivate func makeLeftImageView() -> UIImageView {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.contentMode = .scaleAspectFit
        view.tintColor = UIColor.black
        
        return view
    }
    
    fileprivate func makeTitleLabel() -> UILabel {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.numberOfLines = 0
        view.textAlignment = .left
        view.font = UIFont(name: "OpenSans", size: CGFloat(14.0))
        view.text = "<label>"
        
        return view
    }
    
    fileprivate func makeDescLabel() -> UILabel {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.numberOfLines = 0
        view.textAlignment = .left
        view.font = UIFont(name: "OpenSans", size: CGFloat(9.0))
        view.text = "<label>"
        
        return view
    }
    
    fileprivate func makeWarningLabel() -> UILabel {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.numberOfLines = 0
        view.textAlignment = .left
        view.font = UIFont(name: "OpenSans", size: CGFloat(9.0))
        view.textColor = UIColor.red
        view.text = ""
        
        return view
    }
}
