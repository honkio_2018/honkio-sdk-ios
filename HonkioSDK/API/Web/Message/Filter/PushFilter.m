//
//  PushFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/18/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "PushFilter.h"

@implementation PushFilter

-(id) init {
    self = [super init];
    return self;
}

-(id) initWithId:(NSString*)pushId {
    self = [super init];
    
    if (self) {
        self.pushId = pushId;
    }
    return self;
}

-(NSDictionary*) toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [AbsFilter applyIfNotNil:@"query_id" value:self.pushId forParams:params];
    [AbsFilter applyIfNotNil:@"query_content" value:self.conten forParams:params];
    
    return params;
}

-(PushFilter*)addContent:(NSString*)key value:(NSString*)value {
    if (!self.conten) {
        self.conten = [NSMutableDictionary new];
    }
    self.conten[key] = value;
    return self;
}

@end
