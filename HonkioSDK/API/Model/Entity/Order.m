//
//  Order.m
//  HonkioApi
//
//  Created by Shurygin Denis on 12/1/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "Order.h"
#import "AbsFilter.h"
#import "ApiDateFormatter.h"
#import "BookedProduct.h"

@implementation Order

-(id)init {
    self = [super init];
    return self;
}

-(id)initFrom:(Order*) order {
    self = [super init];

    self.orderId = order.orderId;

    self.model = order.model;
    self.status = order.status;

    self.title = order.title;
    self.desc = order.desc;

    self.userOwner = order.userOwner;
    self.thirdPerson = order.thirdPerson;
    
    self.creationDate = order.creationDate;
    self.expireDate = order.expireDate;
    self.completionDate = order.completionDate;
    self.startDate = order.startDate;
    self.endDate = order.endDate;
    
    self.asset = order.asset;

    self.latitude = order.latitude;
    self.longitude = order.longitude;

    self.amount = order.amount;
    self.currency = order.currency;

    if (order.products) {
        NSMutableArray* products = [NSMutableArray new];
        for(BookedProduct* product in order.products) {
            [products addObject:[[BookedProduct alloc] initFrom:product]];
        }
        self.products = [products copy];
    }
    
    if (order.account)
        self.account = [[UserAccount alloc] initFrom:order.account];
    
    if (order.customFields)
        self.customFields = [NSDictionary dictionaryWithDictionary:order.customFields];
    
    if (order.limitedRoles)
        self.limitedRoles = [[NSArray alloc] initWithArray:order.limitedRoles];

    self.shopId = order.shopId;
    self.shopName = order.shopName;
    self.shopReceipt = order.shopReceipt;
    self.shopReference = order.shopReference;
    
    return self;
}

-(BOOL)isStatus:(NSString*) status {
    if (status == nil) {
        return self.status == nil;
    }
    
    return self.status != nil && [self.status isEqualToString:status];
}
-(BOOL)isStatusOneOf:(NSArray*) statuses {
    
    if (statuses == nil || statuses.count == 0) {
        return self.status == nil;
    }
    
    if (self.status == nil) {
        return NO;
    }
    
    for (NSString* status in statuses) {
        if ([self.status isEqualToString:status]) {
            return YES;
        }
    }
    
    return NO;
}

-(void)setCustom:(NSString *)key value:(nonnull id) value {
    if (self.customFields) {
        NSMutableDictionary* mutableDict = [NSMutableDictionary dictionaryWithDictionary:self.customFields];
        
        mutableDict[key] = value;
        
        self.customFields = mutableDict;
    }
    else {
        self.customFields = [ @{key : value} mutableCopy];
    }
}

-(nullable id)getCustom:(NSString *)key {
    if (self.customFields) {
        return self.customFields[key];
    }
    return nil;
}

-(void)removeCustom:(NSString *)key {
    if ([self getCustom:key]) {
        NSMutableDictionary* mutableDict = [NSMutableDictionary dictionaryWithDictionary:self.customFields];
        [mutableDict removeObjectForKey:key];
        self.customFields = mutableDict;
    }
}

-(NSDictionary*)toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [AbsFilter applyIfNotNil:MSG_PARAM_ORDER_ID value:self.orderId forParams:params];
    if (self.model) {
        [AbsFilter applyIfNotNil:@"model" value:self.model forParams:params];
    }
    [AbsFilter applyIfNotNil:MSG_PARAM_ORDER_STATUS value:self.status forParams:params];
    [AbsFilter applyIfNotNil:@"title" value:self.title forParams:params];
    [AbsFilter applyIfNotNil:@"description" value:self.desc forParams:params];
    [AbsFilter applyIfNotNil:@"third_person" value:self.thirdPerson forParams:params];
    [AbsFilter applyIfNotNil:@"asset" value:self.asset forParams:params];
    
    if (self.amount > 0) {
        [AbsFilter applyIfNotNil:@"amount" value:[NSString stringWithFormat:@"%f", self.amount] forParams:params];
        [AbsFilter applyIfNotNil:@"currency" value:self.currency forParams:params];
    }
    
    if (self.products) {
        NSMutableArray* productsArray = [NSMutableArray new];
        if(self.products) {
            for(BookedProduct* bookProduct in self.products) {
                NSDictionary* productDict = @{MSG_PARAM_ID : bookProduct.product.productId,
                                              @"count" : [NSNumber numberWithInt:bookProduct.count]};
                
                [productsArray addObject:productDict];
            }
        }
        
        [AbsFilter applyIfNotNil:@"products" value:productsArray forParams:params];
    }
    
    [AbsFilter applyIfNotNil:@"expire_date" value:[ApiDateFormatter serverStringFromDate:self.expireDate] forParams:params];
    [AbsFilter applyIfNotNil:@"completion_date" value:[ApiDateFormatter serverStringFromDate:self.completionDate] forParams:params];
    
    // Location
    [AbsFilter applyIfNotNil:@"longitude" value:[NSString stringWithFormat:@"%f", self.longitude] forParams:params];
    [AbsFilter applyIfNotNil:@"latitude" value:[NSString stringWithFormat:@"%f", self.latitude] forParams:params];
    
    [AbsFilter applyIfNotNil:@"custom_fields" value:self.customFields forParams:params];
    
    // New Start order date and end date
    [AbsFilter applyIfNotNil:@"start_date" value:[ApiDateFormatter serverStringFromDate:self.startDate] forParams:params];
    [AbsFilter applyIfNotNil:@"end_date" value:[ApiDateFormatter serverStringFromDate:self.endDate] forParams:params];
    
    if (self.limitedRoles) {
        NSMutableArray* array = [NSMutableArray new];
        for (UserRoleDescription* role in self.limitedRoles) {
            [array addObject:role.roleId];
        }
        [AbsFilter applyIfNotNil:@"roles" value:array forParams:params];
    }
    
    return params;
}

-(BOOL)isExpired {
    return [self isExpired:[NSDate new]];
}
-(BOOL)isExpired:(NSDate*) date {
    return [self.expireDate compare: date] != NSOrderedDescending;
}

-(BOOL)isAccountValid {
    if (self.account != nil && [self.products count] > 0) {
        NSMutableSet* disallowedAccounts = [NSMutableSet new];
        for (BookedProduct* bookedProduct in self.products) {
            if (bookedProduct.product.disallowedAccounts)
                [disallowedAccounts unionSet: bookedProduct.product.disallowedAccounts];
        }
        return ![disallowedAccounts containsObject:self.account.type];
    }
    return self.account != nil && [self.account enoughLeft:self.amount];
}

-(void) setProducts:(NSArray *)products {
    _products = products;
    
    if (products) {
        self.amount = 0;
        self.currency = nil;
        
        for (BookedProduct* bookedProduct in products) {
            
            self.amount += bookedProduct.getPrice;
            if (self.currency == nil)
                self.currency = bookedProduct.product.currency;
        }
    }
}

@end
