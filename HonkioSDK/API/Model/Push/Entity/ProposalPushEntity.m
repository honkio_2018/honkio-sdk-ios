//
//  ProposalPushEntity.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/3/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import "ProposalPushEntity.h"
#import "Push.h"

@implementation ProposalPushEntity

-(id)initWithContent:(NSDictionary*)content {
    return [super initWithType:PUSH_TYPE_ORDER_PROPOSAL andContent:content];
}

-(NSString*) value1 {
    return self.orderId;
}

-(NSString*) value2 {
    return nil;
}

@end
