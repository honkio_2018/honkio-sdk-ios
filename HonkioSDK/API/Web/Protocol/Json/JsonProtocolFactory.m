//
//  JsonProtocolFactory.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "JsonProtocolFactory.h"
#import "JsonRequestBuilder.h"
#import "ServerInfoJsonParser.h"
#import "ShopInfoJsonParser.h"
#import "UserJsonParser.h"
#import "SimpleResponseJsonParser.h"
#import "ServerFileJsonParser.h"
#import "ShopFindJsonParser.h"
#import "ShopProductsJsonParser.h"
#import "UserPaymentJsonParser.h"
#import "TransactionsJsonParser.h"
#import "OrderJsonParser.h"
#import "OrdersListJsonParser.h"
#import "UserRoleJsonParser.h"
#import "UserCommentsJsonParser.h"
#import "ScheduleJsonParser.h"
#import "AppInfoJsonParser.h"
#import "ChatMessageJsonParser.h"
#import "ChatMessageListJsonParser.h"
#import "InventoryProductJsonParser.h"
#import "InventoryJsonParser.m"
#import "ApplicantsListJsonParser.h"
#import "UserRoleDescriptionJsonParser.h"
#import "UserRoleDescriptionsListJsonParser.h"
#import "PushesListJsonParser.h"
#import "UserRolesListJsonParser.h"
#import "InvitationJsonParser.h"
#import "StructureJsonParser.h"
#import "StructureListJsonParser.h"
#import "QrCodeJsonParser.h"
#import "AssetJsonParser.h"
#import "AssetListJsonParser.h"
#import "TagsListJsonParser.h"
#import "StringResponseJsonParser.h"

#import "ShopFind.h"
#import "ShopInfo.h"
#import "ServerInfo.h"
#import "AppInfo.h"
#import "ShopProducts.h"
#import "Schedule.h"
#import "Transactions.h"
#import "OrdersList.h"
#import "Order.h"
#import "ServerFile.h"
#import "User.h"
#import "UserRole.h"
#import "UserPayment.h"
#import "UserComment.h"
#import "UserCommentsList.h"
#import "ChatMessage.h"
#import "ChatMessages.h"
#import "ApplicantsList.h"
#import "PushesList.h"
#import "UserRolesList.h"
#import "Invitation.h"
#import "HKStructure.h"
#import "HKStructureList.h"
#import "HKQrCode.h"
#import "HKAsset.h"
#import "HKAssetList.h"
#import "TagsList.h"
#import "HKMediaFile.h"
#import "HKMediaFileList.h"
#import "HKMediaFileJsonParser.h"
#import "HKMediaFileListJsonParser.h"
#import "UserAccessList.h"
#import "UserAccessListJsonParser.h"
#import "HKMediaUrl.h"
#import "HKMediaUrlJsonParser.h"
#import "UserCommentJsonParser.h"

#import "Merchant.h"
#import "MerchantJsonParser.h"
#import "RateList.h"
#import "RateListJsonParser.h"
#import "CalendarSchedule.h"
#import "CalendarScheduleJsonParser.h"
#import "CalendarEvent.h"
#import "CalendarEventJsonParser.h"
#import "HKUserRoleStructureJsonParser.h"
#import "HKUserRoleStructureListJsonParser.h"
#import "HKUserRoleStructureList.h"

#import "HKLinksListJsonParser.h"
#import "HKLinkJsonParser.h"
#import "HKLinksList.h"
#import "HKLink.h"

#import "HKDevice.h"
#import "HKDeviceJsonParser.h"


@implementation JsonProtocolFactory

-(id<RequestBuilder>)newRequestBuilder {
    return [JsonRequestBuilder new];
}

-(id<ResponseParser>)newParser:(Class)clazz {
    
    // Common
    if (clazz == nil) {
        return [SimpleResponseJsonParser new];
    }
    else if (clazz == [NSString class]) {
        return [StringResponseJsonParser new];
    }
    else if(clazz == [HKDevice class]) {
        return [HKDeviceJsonParser new];
    }
    else if(clazz == [ShopFind class]) {
        return [ShopFindJsonParser new];
    }
    else if(clazz == [ShopInfo class]) {
        return [ShopInfoJsonParser new];
    }
    else if(clazz == [ServerInfo class]) {
        return [ServerInfoJsonParser new];
    }
    else if(clazz == [AppInfo class]) {
        return [AppInfoJsonParser new];
    }
    else if(clazz == [ShopProducts class]) {
        return [ShopProductsJsonParser new];
    }
    else if(clazz == [TagsList class]) {
        return [TagsListJsonParser new];
    }
    else if(clazz == [Schedule class]) {
        return [ScheduleJsonParser new];
    }
    else if(clazz == [Transactions class]) {
        return [TransactionsJsonParser new];
    }
    else if(clazz == [OrdersList class]) {
        return [OrdersListJsonParser new];
    }
    else if(clazz == [Order class]) {
        return [OrderJsonParser new];
    }
    else if(clazz == [ServerFile class]) {
        return [ServerFileJsonParser new];
    }
    else if(clazz == [ApplicantsList class]) {
        return [ApplicantsListJsonParser new];
    }
    else if(clazz == [UserRoleDescription class]) {
        return [UserRoleDescriptionJsonParser new];
    }
    else if(clazz == [UserRoleDescriptionsList class]) {
        return [UserRoleDescriptionsListJsonParser new];
    }
    else if(clazz == [HKStructure class]) {
        return [StructureJsonParser new];
    }
    else if(clazz == [HKStructureList class]) {
        return [StructureListJsonParser new];
    }
    else if (clazz == [HKUserRoleStructure class]) {
        return [HKUserRoleStructureJsonParser new];
    }
    else if (clazz == [HKUserRoleStructureList class]) {
        return [HKUserRoleStructureListJsonParser new];
    }
    else if(clazz == [HKQrCode class]) {
        return [QrCodeJsonParser new];
    }
    else if(clazz == [HKAsset class]) {
        return [AssetJsonParser new];
    }
    else if(clazz == [HKAssetList class]) {
        return [AssetListJsonParser new];
    }
    else if(clazz == [HKMediaFile class]) {
        return [HKMediaFileJsonParser new];
    }
    else if(clazz == [HKMediaFileList class]) {
        return [HKMediaFileListJsonParser new];
    }
    else if(clazz == [User class]) {
        return [UserJsonParser new];
    }
    else if(clazz == [UserRole class]) {
        return [UserRoleJsonParser new];
    }
    else if(clazz == [UserRolesList class]) {
        return [UserRolesListJsonParser new];
    }
    else if(clazz == [UserPayment class]) {
        return [UserPaymentJsonParser new];
    }
    else if(clazz == [UserComment class]) {
        return [UserCommentJsonParser new];
    }
    else if(clazz == [UserCommentsList class]) {
        return [UserCommentsJsonParser new];
    }
    else if(clazz == [ChatMessage class]) {
        return [ChatMessageJsonParser new];
    }
    else if(clazz == [ChatMessages class]) {
        return [ChatMessageListJsonParser new];
    }
    else if(clazz == [InventoryProduct class]) {
        return [InventoryProductJsonParser new];
    }
    else if(clazz == [Inventory class]) {
        return [InventoryJsonParser new];
    }
    else if(clazz == [PushesList class]) {
        return [PushesListJsonParser new];
    }
    else if(clazz == [Invitation class]) {
        return [InvitationJsonParser new];
    }
    else if(clazz == [HKMediaUrl class]) {
        return [HKMediaUrlJsonParser new];
    }
    else if(clazz == [Merchant class]) {
        return [MerchantJsonParser new];
    }
    else if(clazz == [UserAccessList class]) {
        return [UserAccessListJsonParser new];
    }
    else if(clazz == [RateList class]) {
        return [RateListJsonParser new];
    }
    else if(clazz == [CalendarSchedule class]) {
        return [CalendarScheduleJsonParser new];
    }
    else if(clazz == [CalendarEvent class]) {
        return [CalendarEventJsonParser new];
    }
    else if(clazz == [HKLink class]) {
        return [HKLinkJsonParser new];
    }
    else if(clazz == [HKLinksList class]) {
        return [HKLinksListJsonParser new];
    }
    return nil;
}

@end
