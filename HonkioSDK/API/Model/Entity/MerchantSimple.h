//
//  ApiMerchantSimple.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Identity.h"

/**
 * Data object that hold base information about merchant.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface MerchantSimple : NSObject

@property (nonatomic) NSString* merchantId;
@property (nonatomic) NSString* name;
@property (nonatomic) NSString* businessId;
@property (nonatomic) NSString* currency;
@property (nonatomic) NSString* supportEmail;
@property (nonatomic) NSString* supportPhone;


/** Array of the Payment Account types which supported by the Merchant. */
@property (nonatomic) NSArray<NSString*>* supportedAccounts;

/** Login shop identity. This Identity should be used in the requests with login and password. */
@property (nonatomic) Identity* loginShopIdentity;

/** Registration shop identity. This Identity should be used in the registration requests. */
@property (nonatomic) Identity* registrationShopIdentity;

/** LostPassword shop identity. This Identity should be used in the lostPassword requests.
 * @see [HonkioApi userCreateTempPassword:handler login:lofin flags:flags]
 */
@property (nonatomic) Identity* lostPasswordShopIdentity;

/**
 * Merchant logo URL or file name in server assets. To build URL for server assets use [ServerInfo assetsUrl].<br>
 */
@property (nonatomic) NSString* logoUrl;

/**
 * Check if specified Payment Account type is supported by the Merchant.
 * @param accountType Payment Account type.
 * @return YES if account supported by the Merchant, NO otherwise.
 */
-(BOOL)isAccountSupported:(NSString*)accountType;

-(NSDictionary*)toParams;

- (BOOL)isEqual: (id)other;

@end
