//
//  MainViewController.swift
//  honkio-sdk-example
//
//  Created by Shurygin Denis on 8/17/16.
//  Copyright © 2016 RiskPointer. All rights reserved.
//

import Foundation
import HonkioAppTemplate

public class MainViewController: BaseViewController {
    
    @IBAction public func actionShowSettings(sender: AnyObject?) {
        self.navigationController?.pushViewController(AppController.getViewControllerById(AppController.SettingsMain, viewController: self)!, animated: true)
    }
}