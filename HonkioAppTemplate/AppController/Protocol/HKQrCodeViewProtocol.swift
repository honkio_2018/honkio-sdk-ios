//
//  HKQrCodeViewProtocol.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 2/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

public protocol HKQrCodeViewProtocol: class {
    
    var qrCode : HKQrCode? { get set }
    
    func loadQrCode(_ qrId: String)
    
}
