//
//  HKCountries.swift
//  Honkio SDK
//
//  Created by Shurygin Denis on 9/8/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

open class HKCountries: NSObject {
    
    private static var _list : [CountryInfo]?
    private static var _dict : [String : CountryInfo]?
    
    public static var List : [CountryInfo] {
        get {
            if _list == nil {
                HKCountries.initData()
            }
            return _list!
        }
    }
    
    public static var Dict : [String : CountryInfo] {
        get {
            if _dict == nil {
                HKCountries.initData()
            }
            return _dict!
        }
    }
    
    public static func findCounry(byIso: String) -> CountryInfo? {
        if let country = Dict[byIso] {
            return country;
        }
        else {
            let identifier = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue : byIso])
            if #available(iOS 10.0, *) {
                if let country = Dict[NSLocale(localeIdentifier: identifier).countryCode ?? ""] {
                    return country;
                }
            } else if byIso.count == 2 {
                for country in List {
                    let index = country.iso.index(country.iso.startIndex, offsetBy: 2)
                    if String(country.iso[..<index]) == byIso {
                        return country
                    }
                }
            }
        }
        return nil
    }
    
    public static func toIso3(_ iso: String?) -> String? {
        if iso == nil {
            return nil
        }
        
        if iso?.count == 3 {
            return iso
        }
        
        return HKCountries.findCounry(byIso: iso!)?.iso
    }
        
    private static func initData() {
        _list = []
        _dict = [:]
        
        let locale = NSLocale(localeIdentifier: NSLocale.current.identifier)
        for code in NSLocale.isoCountryCodes {
            let identifier = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue : code])
            if let name = locale.displayName(forKey: NSLocale.Key.identifier, value: identifier) {
                let country = CountryInfo()
                country.iso = code
                country.name = name
                country.nameTranslated = name
                
                _list?.append(country)
                _dict?[code] = country
            }
        }
        
        _list?.sort(by: { (item1, item2) -> Bool in
            item1.name < item2.name
        })
    }

}
