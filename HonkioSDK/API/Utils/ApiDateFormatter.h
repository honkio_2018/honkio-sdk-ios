//
//  ApiDateFormatter.h
//  HonkioApi
//
//  Created by Alexandr Kolganov on 4/22/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiDateFormatter : NSObject
+(NSDate*) dateFromServerString:(NSString*)str;
+(NSString*) serverStringFromDate:(NSDate*)date;
+(NSString*) barcodeStringFromDate:(NSDate*)date;
+(NSString*) stringFromDate:(NSDate*)date dateStyle:(NSDateFormatterStyle) dateStyle timeStyle:(NSDateFormatterStyle) timeStyle;
+(NSString *)stringFromDate:(NSDate*)date;
+(NSString*) secondsToHumanTime:(NSInteger)seconds;
+(long) stringDateToLong:(NSString *)dateString;
@end
