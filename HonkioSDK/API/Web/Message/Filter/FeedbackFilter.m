//
//  FeedbackFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 4/6/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "FeedbackFilter.h"

@implementation FeedbackFilter

-(void)fillParams:(NSMutableDictionary*)params {
    [AbsFilter applyIfNotNil:@"order_id" value:self.orderId forParams:params];
    [AbsFilter applyIfNotNil:@"user_id" value:self.userId forParams:params];
    [AbsFilter applyIfNotNil:@"role_id" value:self.roleId forParams:params];
    [AbsFilter applyIfNotNil:@"object" value:self.objectId forParams:params];
    [AbsFilter applyIfNotNil:@"object_type" value:self.objectType forParams:params];
    [super fillParams:params];
}

@end
