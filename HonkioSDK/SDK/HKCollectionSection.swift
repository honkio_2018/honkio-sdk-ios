//
//  HKCollectionSection.swift
//  Honkio SDK
//
//  Created by developer on 1/4/16.
//  Copyright © 2016 developer. All rights reserved.
//

/// Container class for table item.

open class HKCollectionSection<TTableItem> : NSObject {
    /// Represents the title of a section.
    open var title: String?
    /// Containes the collection of items.
    open var items: [TTableItem] = []

    /**
    Initializes the section item.
    - parameter title: The string value to be used as a title of the section item.
    */
    public init(title: String! = "") {
        
        self.title = title
    }
}
