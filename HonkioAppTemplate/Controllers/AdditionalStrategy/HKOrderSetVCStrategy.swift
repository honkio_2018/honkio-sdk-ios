//
//  HKOrderSetVCStrategy.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/27/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

// TODO: docs
open class HKOrderSetVCStrategy: HKSimpleOrderSetVCStrategy, HKUserAccountSelectionVCStrategyDelegate, HKPinRequestVCStrategyDelegate, HKUserAccountVerifyVCStrategyDelegate {
    
    open var isPinRequired : Bool = false
    open var isAccountRequired : Bool = false
    
    fileprivate var savedShop : Identity?
    fileprivate var savedOrder : Order?
    fileprivate var savedOrderID : String?
    fileprivate var savedApplication : String?
    fileprivate var savedStatus : String?
    fileprivate var savedBuildNewOrder : Bool = false
    fileprivate var savedFlags : Int32 = 0
    
    fileprivate var pinRequestStrategy : HKPinRequestVCStrategy?
    
    open override func userSetOrder(_ order: Order, shop: Identity?, flags: Int32) {
        self.resetSavedValues()
        
        savedShop = shop
        savedOrder = order
        savedFlags = flags
        
        if checkShop(order, shop, flags) && checkAccount(order, shop, flags) {
            if isPinRequired {
                self.requestPin()
            }
            else {
                self.resetSavedValues()
                super.userSetOrder(order, shop: shop, flags: flags)
            }
        }
    }
    
    open override func userSetOrder(_ orderId: String, status: String, shop: Identity?, flags: Int32) {
        self.resetSavedValues()
        
        savedShop = shop
        savedOrderID = orderId
        savedStatus = status
        savedFlags = flags
        
        // No account check. Cause it not possible do without order object
        if isPinRequired {
            self.requestPin()
        }
        else {
            self.resetSavedValues()
            super.userSetOrder(orderId, status: status, shop: shop, flags: flags)
        }
    }
    
    open override func userSetOrder(application: String, status: String, buildNewOrder: Bool, shop: Identity?, flags: Int32) {
        self.resetSavedValues()
        
        savedShop = shop
        savedApplication = application
        savedStatus = status
        savedBuildNewOrder = buildNewOrder
        savedFlags = flags
        
        // No account check. Cause it not possible do without order object
        if isPinRequired {
            self.requestPin()
        }
        else {
            self.resetSavedValues()
            super.userSetOrder(application: application, status: status, buildNewOrder: buildNewOrder, shop: shop, flags: flags)
        }
    }
    
    open func acountDidSelect(_ controller: HKUserAccountSelectionVCStrategy, account: UserAccount?){
        if account != nil {
            if let order = self.savedOrder {
                let flags = self.savedFlags
                let shop = self.savedShop
                self.resetSavedValues()
                
                order.account = account
                self.userSetOrder(order, shop: shop, flags: flags)
            }
        }
        else {
            self.onOrderSetError(ApiError(code: ErrApi.canceledByUser.rawValue))
            self.resetSavedValues()
        }
    }
    
    public func pinEntered(_ strategy: HKPinRequestVCStrategy, _ pinCode: String!, _ reason: Int!) {
        self.pinEntered(pinCode, reason: reason)
    }
    
    override open func pinEntered(_ pinCode: String!, reason: Int!) {
        if self.savedOrder != nil && reason == Int(PIN_REASON_PURCHASE){
            if HonkioApi.checkPin(pinCode) {
                if self.savedOrder != nil {
                    super.userSetOrder(self.savedOrder!, shop: self.savedShop, flags: self.savedFlags)
                }
                else if self.savedOrderID != nil {
                    super.userSetOrder(self.savedOrderID!, status: self.savedStatus!, shop: self.savedShop, flags: self.savedFlags)
                }
                else if self.savedApplication != nil {
                    super.userSetOrder(application: self.savedApplication!, status: self.savedStatus!, buildNewOrder: self.savedBuildNewOrder, shop: self.savedShop, flags: self.savedFlags)
                }
                else {
                    self.onOrderSetError(ApiError.unknown())
                }
            }
            else {
                self.onOrderSetError(ApiError(code: ErrApi.invalidPin.rawValue))
            }
        }
        self.resetSavedValues()
    }
    
    public func pinCanceled(_ strategy: HKPinRequestVCStrategy, _ reason: Int!) {
        self.pinCanceled(reason)
    }
    
    /**
     * Should be called when PIN pad canceled without entering a PIN.
     - parameter reason: Defines the reason of entering pin code.
     - parameter view: Parent view.
     */
    override open func pinCanceled(_ reason: Int!) {
        self.onOrderSetError(ApiError(code: ErrApi.canceledByUser.rawValue))
        self.resetSavedValues()
    }
    
    override func onOrderSetPending(_ response: Response) {
        let accountVerifyStrategy = HKUserAccountVerifyVCStrategy(parent: self.parent)
        accountVerifyStrategy.delegate = self
        accountVerifyStrategy.removeOnCompletion = true
        
        if !accountVerifyStrategy.handle(response) {
            super.onOrderSetPending(response)
        }
    }
    
    public func userAccountVerifyError(_ error: ApiError) {
        self.onOrderSetError(error)
    }
    
    public func userAccountVerifyComplete(_ response: Response) {
        if response.isStatusAccept() {
            self.orderDidSet(response.result as! Order)
        }
        else if response.isStatusPending() {
            self.onOrderSetPending(response)
        }
        else {
            self.userAccountVerifyError(response.anyError())
        }
    }
    
    fileprivate func checkShop(_ order: Order, _ shop: Identity?, _ flags: Int32) -> Bool {
        if order.shopId == nil || order.shopId.count == 0 || (shop as? Shop)?.merchant != nil {
            return true
        }
        
        self.orderWillSet()
        if order.shopId == shop?.identityId && shop != nil {
            loadShopByIdentity(shop!) {[weak self] (shop) in
                self?.userSetOrder(order, shop: shop, flags: flags)
            }
            return false;
        }
        
        HonkioApi.shopFind(byId: order.shopId, flags: 0) {[weak self] (response) in
            if response.isStatusAccept() {
                let list = (response.result as? ShopFind)?.list
                if (list?.count ?? 0) > 0 {
                    self?.loadShopByIdentity(list![0]) {[weak self] (shop) in
                        self?.userSetOrder(order, shop: shop, flags: flags)
                    }
                    return
                }
            }
            
            self?.onOrderSetError(response.anyError())
        }
        
        return false;
    }
    
    fileprivate func loadShopByIdentity(_ identity: Identity, _ callback: @escaping (_ shop: Shop) -> Void) {
        HonkioApi.shopGetInfo(identity, flags: 0) {[weak self] (response) in
            if response.isStatusAccept(), let shop = (response.result as? ShopInfo)?.shop, shop.merchant != nil {
                callback(shop)
            }
            else {
                self?.onOrderSetError(response.anyError())
            }
        }
    }
    
    fileprivate func checkAccount(_ order: Order, _ shop: Identity?, _ flags: Int32) -> Bool {
        if isAccountRequired {
            if !order.isAccountValid() {
                let accountSelectStrategy = HKUserAccountSelectionVCStrategy(parent: self.parent)
                accountSelectStrategy.delegate = self
                accountSelectStrategy.removeOnCompletion = true
                
                var disallowedAccounts = Set<String>()
                if order.products != nil {
                    for bookedProduct in order.products {
                        if bookedProduct.product.disallowedAccounts != nil {
                            disallowedAccounts.formUnion(bookedProduct.product.disallowedAccounts as! Set<String>)
                        }
                    }
                }
                accountSelectStrategy.disallowedAccounts = disallowedAccounts
                accountSelectStrategy.selectAccount(shop as? Shop, order.amount, order.currency)
                return false
            }
        }
        return true
    }
    
    fileprivate func requestPin() {
        if self.pinRequestStrategy == nil {
            self.pinRequestStrategy = HKPinRequestVCStrategy(parent: self.parent)
            self.pinRequestStrategy?.delegate = self
        }
        
        self.pinRequestStrategy?.requestPin(Int(PIN_REASON_PURCHASE), message: "pin_reason_purchare".localized)
    }
    
    fileprivate func resetSavedValues() {
        self.savedShop = nil
        self.savedOrder = nil
        self.savedOrderID = nil
        self.savedApplication = nil
        self.savedStatus = nil
        self.savedFlags = 0
    }
}
