//
//  HKLink.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKLink.h"
#import "AbsFilter.h"

@implementation HKLink

-(id) init {
    return [self initWithMetaData:[NSMutableDictionary new]];
}

-(id) initWithMetaData:(NSMutableDictionary<NSString*, NSObject*>*) metaData {
    self = [super init];
    
    _metaData = metaData;
    
    return self;
}

-(id) initFrom:(HKLink*)link {
    self = [super init];
    if (self) {
        _objectId = [link objectId];
        _type = [link type];
        
        _fromObject = [link fromObject];
        _fromType = [link fromType];
        
        _toObject = [link toObject];
        _toType = [link toType];
        
        if ([link metaData]) {
            _metaData = [NSMutableDictionary dictionaryWithDictionary: [link metaData]];
        }
        else {
            _metaData = [NSMutableDictionary new];
        }
    }
    return self;
}

-(void) setFrom:(NSString*)object type:(NSString*)type {
    _fromObject = object;
    _fromType = type;
}

-(void) setTo:(NSString*)object type:(NSString*)type {
    _toObject = object;
    _toType = type;
}

-(NSDictionary*) toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [AbsFilter applyIfNotNil: @"id" value:self.objectId forParams:params];
    [AbsFilter applyIfNotNil: @"type_id" value:self.type forParams:params];
    
    [AbsFilter applyIfNotNil: @"from_object" value:self.fromObject forParams:params];
    [AbsFilter applyIfNotNil: @"from_object_type" value:self.fromType forParams:params];
    [AbsFilter applyIfNotNil: @"to_object" value:self.toObject forParams:params];
    [AbsFilter applyIfNotNil: @"to_object_type" value:self.toType forParams:params];
    
    [AbsFilter applyIfNotNil: HK_LINK_FIELD_META_DATA value:self.metaData forParams:params];
    
    return params;
}

@end
