//
//  HKAddress.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/31/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

// TODO write docs
@interface HKAddress : NSObject

@property (nonatomic) NSString* address1;
@property (nonatomic) NSString* address2;

@property (nonatomic) NSString* city;
@property (nonatomic) NSString* admin;
@property (nonatomic) NSString* postalCode;

@property (nonatomic, readonly) NSString* countryIso;
@property (nonatomic, readonly) NSString* countryName;

-(id)init;
-(id)initFromDict:(NSDictionary*) dict;

-(void)setCountry:(NSString*)iso name:(NSString*)name;

-(NSDictionary*)toDict;
-(NSString*)fullAddress;

@end
