//
//  HKUserRoleStructure.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKUserRoleStructure.h"

@implementation HKUserRoleStructure

-(HKStructureProperty*)getProperty:(NSString*)name {
    HKStructureProperty* prop = self.publicProperties[name];
    
    if (prop)
        return prop;
    
    return self.privateProperties[name];
}

-(nullable HKStructureEnumPropertyValue *)findEnumPropertyValue:(nonnull NSString *)propertyName value:(nonnull NSString *)value {
    HKStructureProperty *property = [self getProperty:propertyName];
    if ([property isKindOfClass:[HKStructureEnumProperty class]]) {
        return [((HKStructureEnumProperty *)property) find:value];
    } else if ([property isKindOfClass:[HKStructureArrayProperty class]]) {
        HKStructureProperty *supProperty = ((HKStructureArrayProperty *)property).subtype;
        if ([supProperty isKindOfClass:[HKStructureEnumProperty class]]) {
            return [((HKStructureEnumProperty *)supProperty) find:value];
        }
    }
    return nil;
}

@end
