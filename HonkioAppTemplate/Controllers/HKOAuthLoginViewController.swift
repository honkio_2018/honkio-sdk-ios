//
//  HKOAuthLoginViewController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 4/7/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
Manages the OAuth type login.
*/

open class HKOAuthLoginViewController: HKBaseOAuthLoginViewController, HKPinRequestVCStrategyDelegate, HKLoginViewProtocol, HKLoginViewLoginDelegate {

    /**
    Describes whether the target is login or logout.
    */
    open var doLogin = true
    
    private var isInit = false
    private var pinRequestStrategy: HKPinRequestVCStrategy?

    /**
    If *doLogin* was set to false then logs out the user. Otherwise shows the splash screen, loads server information
    and tries to login.
    */
    override open func viewDidLoad() {
        
        if self.loginDelegate == nil {
            self.loginDelegate = self
        }
        
        super.viewDidLoad()
        
        self.title = "login_screen_title".localized
        webView.scalesPageToFit = true
        self.automaticallyAdjustsScrollViewInsets = false
        
        if #available(iOS 11.0, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.isInit {
            self.showSplashPage()
            self.loadServerData(doLogin: doLogin)
            
            self.isInit = true
        }
    }

    /**
    Checks whether the login routine completed successfully. If not shows the login page.
    If the user has been logged in navigates to main page.
    - parameter isLogedin: Describes whether the login routine was completed successfully.
    */
    open func loginFinished(_ view: HKLoginViewProtocol, _ isLogedIn: Bool) {
        
        if (!isLogedIn && !AppController.instance.isGuestModeAvailable()) || !doLogin {
            HonkioApi.logout(MSG_FLAG_OFFLINE_MODE)
            self.doLogin = true
            self.showLoginPage()
        }
        else {
            self.showMainScreen()
        }

    }
    
    open override func callLoginFinished(_ isLoggedin: Bool) {
        if isLoggedin, let appDelegate = UIApplication.shared.delegate as? HKBaseAppDelegate {
            appDelegate.reRegisterForRemoteNotifications()
        }
        super.callLoginFinished(isLoggedin)
    }

    /// Hides splash screen and starts load login page.
    override open func showLoginPage() {
        self.startWaitingOperation()
        super.showLoginPage()
    }
    
    open func showMainScreen() {
        if let viewController = AppController.getViewControllerById(AppController.Main, viewController: self) {
            
            let appDelegate = UIApplication.shared.delegate as! HKBaseAppDelegate
            appDelegate.changeRootViewController(viewController)
        }
    }

    /// Hides busy indicator.
    override open func stopWaitOperation() {
        
        BusyIndicator.shared.hideProgressView()
    }

    /// Shows busy indicator
    override open func startWaitingOperation() {
        if self.splashView == nil || self.splashView.isHidden {
            BusyIndicator.shared.showProgressView(self.view)
        }
    }

    /**
    Shows the alert when user account is in PENDING status.
    - parameter response: The Response object within the user account information.
    - parameter action: The action could be preformed. *Not yet used.*
    */
    override open func alertPendingStatus(_ response: Response, action: (() -> Void)?) {
        // TODO localize text
        let alert = CustomAlertView(title: "", message: "Can't get pending status".localized, cancelButtonTitle: "dlg_common_button_ok".localized, cancelButtonAction: nil)
        alert.show()
    }

    /**
    Shows the alert when server responded that the loign information is not valid.
    - parameter action: The action could be preformed. *Not yet used.*
    */
    override open func alertInvalidLogin(_ action: (() -> Void)?) {
        
        let message = "dlg_login_failed_message".localizedWithComment("Could not log you into service. Please check that you entered correct email address as username and typed password correctly.")
        
        let alert = CustomAlertView(title: "", message: message, cancelButtonTitle: "Ok", cancelButtonAction: nil)
        alert.show()
    }

    /**
    Shows the alert when server responded that the email verification is needed.
    - parameter action: The action will be performed when user pressed the button on alert dialog.
    */
    override open func alertEmailVerify(_ action: (() -> Void)?) {
        
        let message = "registration_p_activation_email_desc".localizedWithComment("You have been sent a email to validate your email address and activate your account but you haven't done the activation yet. You are not allowed to make new purchases until you activate your account.")
        
        let alert = CustomAlertView(title: "", message: message, cancelButtonTitle: "Ok", cancelButtonAction: action)
        alert.show()
    }

    /**
    Shows PinView
    - parameter reason: The reaon of PIN code entering.
    - parameter completionAction: The action is to be performed after PIN code is entered.
    */
    override open func showPinViewController(_ reason: Int, completionAction: (() -> Void)?) {
        if self.pinRequestStrategy == nil {
            self.pinRequestStrategy = HKPinRequestVCStrategy(parent: self)
            self.pinRequestStrategy?.delegate = self
        }
        
        self.pinRequestStrategy?.requestPin(reason, message: "pin_reason_new".localized);
    }
    
    open func pinEntered(_ strategy: HKPinRequestVCStrategy, _ pinCode: String!, _ reason: Int!) {
        self.pinEntered(pinCode, reason: reason)
    }
    
    open func pinCanceled(_ strategy: HKPinRequestVCStrategy, _ reason: Int!) {
        self.pinCanceled(reason)
    }

    /**
    Shows Terms of Use.
    - parameter completionAction: The action to be performed after accepting Terms of Use.
    */
    override open func showTouUpdateViewController(_ completionAction: (() -> Void)?) {
        CustomAlertView(okTitle: nil, okMessage: "terms_of_use_new_version_message".localized) {
            if let viewController = AppController.getViewControllerById(AppController.Tou, viewController: self) as? HKTouViewController {
                
                viewController.completeAction = completionAction
                
                self.presentViewControllerWithNavigation(viewController, completionAction: nil)
            }
        }.show()
    }
    
    open override func handleError(error: ApiError) {
        CustomAlertView(apiError: error, okAction: nil).show()
    }
}
