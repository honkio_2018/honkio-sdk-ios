//
//  MerchantShopsListJsonParser.m
//  HonkioSDK
//
//  Created by Mikhail Li on 24/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "MerchantShopsListJsonParser.h"
#import "MerchantShopsList.h"
#import "MerchantShop.h"

@implementation MerchantShopsListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    NSMutableArray<MerchantShop *> *list = [NSMutableArray new];
    
    NSArray *shops = dict[@"shops"];
    if (shops) {
        for (NSDictionary *jsonShop in shops) {
            MerchantShop *merchantShop = [[MerchantShop alloc] init:dict];
            merchantShop.identityId = jsonShop[@"id"];
            merchantShop.password = jsonShop[@"password"];
            merchantShop.settings = [[NSMutableDictionary alloc] initWithDictionary:jsonShop[@"settings"]];
            
            [list addObject:merchantShop];
        }
    }
    
    MerchantShopsList *shopList = [MerchantShopsList new];
    shopList.list = list;
    return shopList;;
}

@end
