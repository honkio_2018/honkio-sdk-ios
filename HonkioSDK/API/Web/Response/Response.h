//
//  Response.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiError.h"

/**
 * Available statuses of the response.
 */
 #define RESPONSE_STATUS_ACCEPT     @"accept"
 #define RESPONSE_STATUS_ERROR      @"error"
 #define RESPONSE_STATUS_PENDING    @"pending"
 #define RESPONSE_STATUS_REJECT     @"reject"

/**
 * Available pending statuses.
 */
 #define RESPONSE_PENDING_EMAIL_VERIFY      @"emailverify"
 #define RESPONSE_PENDING_SMS_VERIFY        @"smsverify"
 #define RESPONSE_PENDING_CREDITCARD_VERIFY @"creditcardverify"
 #define RESPONSE_PENDING_MANUAL_VERIFY     @"manualverify"

@class Message;

/**
 * Response of the API request.
 */
@interface Response : NSObject

/** Object of the request. */
@property (readonly, nonatomic, nullable) NSObject* result;

/** Status of the transaction. */
@property (nonatomic) NSString* status;

/** Transaction ID of the response. */
@property (nonatomic) NSString* transactoinId;

/** Description for the response. */
@property (nonatomic) NSString* respDescription;

/** New OTP(One time password) for the next request. */
@property (nonatomic) NSString* otp;

/** Shop ID that return this response. */
@property (nonatomic) NSString* shopId;

/** URL string or null if the response doesn't contains URL. */
@property (nonatomic) NSString* url;

/** Pending's list of the response. */
@property (nonatomic) NSArray* pendingsList;

/** YES if this response was created in offline mode from the cache, NO otherwise. */
@property (nonatomic) BOOL isOffline;

/** Shop reference from request. */
@property (nonatomic) NSString* shopReference;

/** Message that was used in the request. May be null. */
@property (nonatomic) Message* message;


@property (nonatomic) ApiError* error;

/**
 * Init new response with specified result.
 * @param result Result object.
 */
-(id)initWithResult:(NSObject*)result;

/**
 * Init new response based on template response object with specified result.
 *
 * @param templateResponse Template object that will be copied.
 * @param result Result object.
 */
-(id)initFromTemplate:(Response*)templateResponse withResult:(NSObject*)result;

/**
 * Check if status of response is RESPONSE_STATUS_ACCEPT.
 * @return YES if status of the response is RESPONSE_STATUS_ACCEPT, NO otherwise.
 */
-(BOOL)isStatusAccept;

/**
 * Check if status of response is RESPONSE_STATUS_ERROR.
 * @return YES if status of the response is RESPONSE_STATUS_ERROR, NO otherwise.
 */
-(BOOL)isStatusError;

/**
 * Check if status of response is RESPONSE_STATUS_PENDING.
 * @return YES if status of the response is RESPONSE_STATUS_PENDING, NO otherwise.
 */
-(BOOL)isStatusPending;

/**
 * Check if status of response is RESPONSE_STATUS_REJECT.
 * @return YES if status of the response is RESPONSE_STATUS_REJECT, NO otherwise.
 */
-(BOOL)isStatusReject;

/**
 * Check that response contains specified pending.
 * @param pending Pending for searching.
 * @return YES if the response has specified pending, NO otherwise.
 */
-(BOOL)hasPending:(NSString*)pending;

//TODO write docs
-(ApiError*)anyError;

//TODO write docs
+(Response*) withError:(int)code desc:(NSString*)desc;
+(Response*) withApiError:(ApiError*)error;

@end
