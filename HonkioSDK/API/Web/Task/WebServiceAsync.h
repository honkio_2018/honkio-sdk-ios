//
//  WebServiceAsync.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"
#import "ApiResponseHandler.h"
#import "Message.h"

/**
 * Async task that send requests on the server.<br>
 *  WebServiceAsync has Task protocol, so you have opportunity to abort task.
 */
@interface WebServiceAsync : NSObject<Task>

/**
 * Init new WebServiceAsync.
 * @param message Message object.
 * @param responseHandler Handler that will receive response.
 */
-(id)initWithMessage:(Message*)message responseHandler:(ResponseHandler)responseHandler;

/**
 * Execute task. Result will returned into ResponseHandler providet in the initialisation method.
*  @return Self instance
 */
-(WebServiceAsync*)execute;


@end
