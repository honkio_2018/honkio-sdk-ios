//
//  QueryPaymentProcessModel.h
//  HonkioApi
//
//  Created by Shurygin Denis on 8/4/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ApiResponseHandler.h"
#import "Identity.h"
#import "BaseProcessModel.h"

/**
 * Helper class that allow to schedule transaction status check.
 */
@interface QueryPaymentProcessModel : NSObject

@property (readonly) BOOL isAborted;

/**
 * Init new Query Model.
 * @param handler Payment handler.
 * @param shopIdentity Identity of the Shop which will process payment.
 * @param transactionId Id of the payment transaction that should be queried.
 */
-(id)initWithHandler:(ResponseHandler)handler shopIdentity:(Identity*)identity transactionId:(NSString*)transactionId;

//TODO doc
-(id)initWithProcessModel:(BaseProcessModel*)processModel shopIdentity:(Identity *)identity transactionId:(NSString *)transactionId;

/** Makes query for pending payment transaction. */
-(void)query;

/**
 * Starts loop query for pending payment transaction with specified delay.
 * @param loopDelay Delay for the next query.
 */
-(void)query:(long)loopDelay;

/** Stops loop query. */
-(void)stopQuery;

/**
 * Aborts query and sets payment transaction status to "reject".
 * @return YES if transaction aborted immediately, NO otherwise.
 */
-(BOOL)abort;

@end
