//
//  HKPaymentVCStrategyDelegate.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/1/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
 * Describes the set of functions to be implemented for an instance to be used in OrderLoaderVCStrategy.
 */
@objc public protocol HKPaymentVCStrategyDelegate: class {
    
    /**
     * Is to be used as notifier that the payment process has been started.
     - parameter payParams: Instance of payment parameters are to be used for payment.
     */
    func paymentWillstart(_ controller: HKBasePaymentVCStrategy, payParams: PayParams)
    
    /**
     * Is to be used as notifier that the payment process has been completed.
     - parameter response: The response from the server containing the payment detailes.
     - parameter payment: The detailes of performed payment.
     */
    func paymentDidComplete(_ controller: HKBasePaymentVCStrategy, response: Response, payment: UserPayment)
    
    
    func paymentPending(_ controller: HKBasePaymentVCStrategy, response: Response!, payment: UserPayment)
    
    /**
     * Called if the request to the server has been failed.
     - parameter model: ProcessModel which runs into error.
     - parameter response: Response object containing server response.
     - parameter error: Returned error.
     */
    func paymentError(_ controller: HKBasePaymentVCStrategy, response: Response!)

}
