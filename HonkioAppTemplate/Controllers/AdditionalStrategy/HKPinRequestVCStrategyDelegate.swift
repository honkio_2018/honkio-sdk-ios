//
//  HKPinRequestVCStrategyDelegate.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 11/14/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

//TODO write docs
public protocol HKPinRequestVCStrategyDelegate {

    func pinEntered(_ strategy: HKPinRequestVCStrategy, _ pinCode: String!, _ reason: Int!)

    func pinCanceled(_ strategy: HKPinRequestVCStrategy, _ reason: Int!)
    
}
