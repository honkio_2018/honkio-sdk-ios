//
//  HKMediaFile.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/13/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKMediaFile.h"

@implementation HKMediaFile

@synthesize type, size;

-(id)initWithId:(NSString*) objectId url:(NSString*)url {
    self = [super init];
    if (self) {
        _objectId = objectId;
        _url = url;
    }
    return self;
}

-(NSString*) type {
    NSString *result = (NSString *)[self.metaData valueForKey:HK_MEDIA_FILE_METADATA_TYPE];
    return result;
}

-(void) setType:(NSString*) type {
    [self.metaData setValue:type forKey:HK_MEDIA_FILE_METADATA_TYPE];
}

-(int) size {
    NSNumber *result = (NSNumber *)[self.metaData valueForKey:HK_MEDIA_FILE_METADATA_SIZE];
    return [result intValue];
}

-(void) setSize:(NSString*) type {
    [self.metaData setValue:type forKey:HK_MEDIA_FILE_METADATA_SIZE];
}

@end
