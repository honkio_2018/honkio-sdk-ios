//
//  HKBaseAppControllerInstance.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
The implementation of HKAppViewControllerFactory.
Finds or creates story boards and view controllers by identifiers.
*/

open class HKBaseAppViewControllerFactory: HKAppViewControllerFactory {
    
    fileprivate var storyboards: [UIStoryboard]?
    fileprivate var framewokBundle: Bundle
    
    public init(storyboards sBoards: [String]?) {
        if sBoards != nil && sBoards!.count > 0 {
            storyboards = []
            for storybord in sBoards! {
                storyboards?.append(UIStoryboard(name: storybord, bundle: nil))
            }
        }
        self.framewokBundle = Bundle(for: AppController.self)
    }

    /**
    - Returns: The ViewController for the storuboard found in the provided ViewController.
    - parameter controllerId: The identificator the story board identifier will be searched for.
    - parameter viewController: The view controller the story board identifier will be searched in.
    */
    open func getViewControllerById(_ controllerId: Int, viewController: UIViewController) -> UIViewController? {
        let viewController = buildViewControllerFromStoryboard(controllerId, viewController: viewController)
        
        if controllerId == AppController.Pin {
            if let pinCintroller = viewController as? HKBaseViewController {
                pinCintroller.addLifeCycleObserver(HKPinCancelActionVCStrategy())
            }
        }
        
        if viewController == nil {
            switch controllerId {
            case AppController.SettingsMain:
                return HKSettingsViewController()
            case AppController.ManageCards:
                return HKUserAccountsTableViewController()
            case AppController.UserAccountSelection:
                return HKPreferredUserAccountsTableViewController()
            case AppController.WebView:
                return HKWebPageViewController()
            default:
                break
            }
        }
        
        return viewController
    }
    
    open func getViewControllerByIdentifier(_ identifier: String, viewController: UIViewController) -> UIViewController? {
        if let storyBoard = viewController.storyboard {
            if (storyBoard.value(forKey: "identifierToNibNameMap") as! NSDictionary).object(forKey: identifier) != nil {
                return storyBoard.instantiateViewController(withIdentifier: identifier)
            }
        }
        
        if self.storyboards != nil {
            for storyBoard in self.storyboards! {
                if (storyBoard.value(forKey: "identifierToNibNameMap") as! NSDictionary).object(forKey: identifier) != nil {
                    return storyBoard.instantiateViewController(withIdentifier: identifier)
                }
            }
        }
        
        for bundle in HKBundleUtils.bundles {
            if let controller = self.getViewControllerFromBundle(bundle, identifier: identifier) {
                return controller
            }
        }
        
        if let controller = self.getViewControllerFromBundle(self.framewokBundle, identifier: identifier) {
            return controller
        }
        return nil
    }
    
    open func buildViewControllerFromStoryboard(_ controllerId: Int, viewController: UIViewController) -> UIViewController? {
        if let identifier = self.getStoryboardIdentifierByViewControllerId(controllerId) {
            return self.getViewControllerByIdentifier(identifier, viewController: viewController)
        }
        return nil
    }
    
    /**
     - Returns: The identificator of StoryBoard for the provided id of ViewController.
     - parameter identifier: The identificator of ViewController the StoryBoard id will be seached for.
     */
    open func getStoryboardIdentifierByViewControllerId(_ identifier: Int) -> String? {
        
        switch identifier {
            
        case AppController.Launch:
            return "HKLaunchViewController"
            
        case AppController.Login:
            return "LoginViewController"
            
        case AppController.Tou:
            return "TouViewController"
            
        case AppController.Pin:
            
            switch UIDevice.current.type {
                
            case .iPhone4S:
                return "PinViewController_4inch"
            default:
                return "PinViewController"
            }
            
        case AppController.ChangePin:
            return "UserChangePinViewController"
            
        case AppController.ForgotPassword:
            return "UserForgotPasswordViewController"
            
        case AppController.SettingsUserAccount:
            return "UserEditProfileViewController"
            
        case AppController.SettingsUserAddCreditCard,
             AppController.RegistrationCreditCard:
            return "UserAccountsAddCreditCardViewController"
            
        case AppController.ChangePassword:
            return "UserChangePasswordViewController"
            
        case AppController.Main:
            return "MainViewController"
            
        case AppController.QrCode:
            return "LazyQrCodeViewController"
        
        case AppController.SettingsAbout:
            return "AboutViewController"
            
        case AppController.Gdpr:
            return "GdprViewController"
            
        default:
            return nil
        }
    }
    
    func getViewControllerFromBundle(_ bundle: Bundle, identifier:String) -> UIViewController? {
        if bundle.path(forResource: identifier, ofType: "nib") != nil {
            if let controllers = bundle.loadNibNamed(identifier, owner: nil, options: nil) {
                if controllers.count > 0 {
                    if let controller = controllers[0] as? UIViewController {
                        return controller
                    }
                }
            }
        }
        return nil
    }
    
    /**
     Checks if the Guest mode is available for the application.
     - Returns: True if Guest mode is available, otherwise false.
     */
    open func isGuestModeAvailable() -> Bool {
        return false
    }
    
}
