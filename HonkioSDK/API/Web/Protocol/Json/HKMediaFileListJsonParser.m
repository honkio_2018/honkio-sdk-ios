//
//  HKMediaFileListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/13/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKMediaFileListJsonParser.h"
#import "HKMediaFileJsonParser.h"
#import "HKMediaFileList.h"

@implementation HKMediaFileListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    NSMutableArray<HKMediaFile*>* list = [NSMutableArray new];
    
    NSArray* filesArray = dict[@"files"];
    if (filesArray) {
        for (NSDictionary* fileDict in filesArray) {
            HKMediaFile* file = [HKMediaFileJsonParser parseMediaFile:fileDict];
            if (file)
                [list addObject:file];
        }
    }

    HKMediaFileList* result = [HKMediaFileList new];
    result.list = list;
    return result;
}


@end
