//
//  AppDelegate.swift
//  honkio-sdk-example
//
//  Created by Shurygin Denis on 4/22/16.
//  Copyright © 2016 RiskPointer. All rights reserved.
//

import UIKit
import HonkioAppTemplate

@UIApplicationMain
class AppDelegate: BaseAppDelegate {
    
    override func applicationShouldInit() {
        let appIdentity = Identity(id: "com/riskpointer/honkio-sdk-example",
                          andPassword: "pj3X5ujrz1ap5ICFVmsfrBaGmPhKEnqIui2cka9u")
        
        HonkioApi.initInstanceWithUrl("http://api.honkio2.webdev.softdev.com", app:appIdentity)
        AppController.initInstance(BaseAppController(mainStoryboard: "Main"))
        AppErrors.initInstance(BaseAppErrors())
    }
    
}