//
//  InventoryJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "InventoryJsonParser.h"
#import "Inventory.h"

@implementation InventoryJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    Inventory* inventory = [Inventory new];
    NSMutableArray* list = [NSMutableArray new];
    
    NSArray* products = dict[@"product"];
    
    if([products isKindOfClass:[NSArray class]]) {
        for (NSDictionary* productDict in products) {
            if([productDict isKindOfClass:[NSDictionary class]]) {
                InventoryProduct* product = [InventoryProduct new];
                [BaseJsonResponseParser parseInventoryProduct:product dict:productDict];
                [list addObject:product];
            }
        }
    }
    inventory.list = list;
    
    return inventory;
}

@end
