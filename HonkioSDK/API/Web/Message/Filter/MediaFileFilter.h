//
//  MediaFileFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/13/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsListFilter.h"

@interface MediaFileFilter : AbsListFilter

@property (nonatomic) NSString* mediaType;
@property (nonatomic) NSString* objectId;
@property (nonatomic) NSString* access;

@end
