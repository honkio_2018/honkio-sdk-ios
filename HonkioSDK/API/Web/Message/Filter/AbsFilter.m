//
//  AbsFilter.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "AbsFilter.h"

@implementation AbsFilter

-(void) apply:(Message*) message {
    [message addParameters:[self toParams]];
}

+(void) applyIfNotNil:(NSString*) key value:(NSObject*) value forMessage:(Message*) message {
    if (value) {
        [message addParameter:key value:value];
    }
}

+(void) applyIfNotNil:(NSString*) key value:(NSObject*) value forParams:(NSMutableDictionary*)params {
    if (value) {
        [params setValue:value forKey:key];
    }
}

+(void) applyOrRemove:(NSString*) key value:(NSObject*) value forParams:(NSMutableDictionary*)params {
    if(value)
        [params setValue:value forKey:key];
    else
        [params setValue:[NSNull null] forKey:key];
}

+(BOOL) isHasLocation:(NSDictionary *)params {
    return params && params[@"html_latitude"] && params[@"html_longitude"];
}

@end
