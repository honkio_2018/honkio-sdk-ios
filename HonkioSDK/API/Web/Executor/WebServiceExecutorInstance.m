//
//  WebServiceExecutorInstance.m
//  HonkioSDK
//
//  Created by Dash on 30.04.2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "WebServiceExecutorInstance.h"
#import "WebServiceAsync.h"

@implementation WebServiceExecutorInstance

-(id<Task>)build:(Message*)message handler:(ResponseHandler)responseHandler {
    return [[[WebServiceAsync alloc] initWithMessage:message responseHandler:responseHandler] execute];
}

@end
