//
//  BasePush.m
//  HonkioApi
//
//  Created by Shurygin Denis on 5/15/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "Push.h"

@implementation Push

-(NSString*)type {
    NSString* type = [self.entity type];
    return type != nil ? type : PUSH_TYPE_NONE;
}

@end
