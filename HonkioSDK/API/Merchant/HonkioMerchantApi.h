//
//  HonkioMerchantApi.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/7/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HonkioApi.h"
#import "HKMerchantsList.h"
#import "HKLink.h"
#import "HKLinkFilter.h"

@interface HonkioMerchantApi : NSObject
    
+(nullable id<Task>)merchantGet:(nonnull NSString*)merchantId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
+(nullable id<Task>)merchantSet:(nonnull Merchant*)merchant flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
+(nullable id<Task>)merchantGetList:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantTransactionsList:(nullable TransactionFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
+(nullable id<Task>)merchantTransactionsList:(nullable TransactionFilter*)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantShopsList:(ShopFilter *)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
+(nullable id<Task>)merchantShopGet:(Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantGetOrders:(nullable OrderFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
+(nullable id<Task>)merchantGetOrders:(nullable OrderFilter*)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
    
+(nullable id<Task>)merchantSetOrder:(nonnull Order*)order flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
+(nullable id<Task>)merchantSetOrder:(nonnull Order*)order shopIdentity:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantQrSet:(nonnull HKQrCode*)qrCode flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantSetShop:(MerchantShop*)shop flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)userMerchantCreate:(Identity*)shopIdentity name:(NSString*)name email:(NSString*)adminEmail phone:(NSString*)adminPhone flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantAssetSet:(HKAsset *)asset shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
+(nullable id<Task>)merchantAssetDelete:(HKAsset *)asset shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
+(nullable id<Task>)merchantAssetList:(AssetFilter *)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to load the list of products for specified Merchant.
 *
 * @param shopIdentity Identity of the shop.
 * @param flags Flags for request.                                  */
+(Message *)merchantGetProducts:(MerchantProductsFilter *)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags;

+(nullable id<Task>)merchantProductList:(MerchantProductsFilter *)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(Message *)merchantProductSet:(Product *)product shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags;

+(nullable id<Task>)merchantProductSet:(Product *)product shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantStructureSet:(nonnull HKStructure*)structure shopIdentity:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// MARK: - Calendar Event

+(nullable id<Task>)merchantEventSet:(CalendarEvent *)calendarEvent
                            objectId:(NSString *)objectId
                          objectType:(NSString *)objectType
                        shopIdentity:(nullable Identity *)shopIdentity
                               flags:(int)flags
                             handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantEventDelete:(CalendarEvent *)calendarEvent
                           shopIdentity:(nullable Identity *)shopIdentity
                                  flags:(int)flags
                                handler:(nullable ResponseHandler)responseHandler;

// MARK: - Chat

+(nonnull id<Task>)merchantGetChatMessages:(nullable ChatMessageFilter*)filter
                                     flags:(int)flags
                                   handler:(nullable ResponseHandler)responseHandler;

+(nonnull id<Task>)merchantSendChatMessage:(nonnull ChatMessage*)chatMessage
                                     flags:(int)flags
                                   handler:(nonnull ResponseHandler)responseHandler;

// MARK: - Vote

+(Message *)merchantVote:(UserComment *)userComment flags:(int)flags;
+(nullable id<Task>)merchantVote:(UserComment *)userComment flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// MARK: - Links

+(nullable id<Task>)merchantSetLink:(nonnull HKLink*)link
                        shopIdentity:(nullable Identity *)shopIdentity
                               flags:(int)flags
                             handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantDeleteLink:(nonnull NSString*)linkId
                        shopIdentity:(nullable Identity *)shopIdentity
                               flags:(int)flags
                             handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantGetLinks:(nonnull HKLinkFilter*)filter
                        shopIdentity:(nullable Identity *)shopIdentity
                               flags:(int)flags
                             handler:(nullable ResponseHandler)responseHandler;

+(nullable id<Task>)merchantReportRun:(nonnull NSString*)reportId
                                email:(nullable NSString*)email
                               params:(nonnull NSDictionary*)params
                                flags:(int)flags
                              handler:(nullable ResponseHandler)responseHandler;

@end
