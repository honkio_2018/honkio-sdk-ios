//
//  HKBaseOrdersListViewController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 1/21/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation


// TODO: doc describe 'filterUserIsOwner', 'filterUserIsThirdPerson', 'filterStatuses' and 'filterExcludeExpired'


/**
The base ViewController that provides functionality for loading and filtering user orders. ViewController adds observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages into NSNotificationCenter to automaticaly refresh list.
* Must not be instantiated.
*/

open class HKBaseOrdersListViewController: HKBaseLazyTableViewController {
    public var shopIdentity : Identity?
    public fileprivate(set) var filter : OrderFilter = OrderFilter()
    
    @IBInspectable public var filterUserIsOwner : Bool = false
    @IBInspectable public var filterUserIsThirdPerson : Bool = false
    
    @IBInspectable public var filterQueryProductDetails : Bool = false
    
    @IBInspectable public var filterStatuses : String?
    @IBInspectable public var filterExcludeExpired: Bool = false

    /**
     Adds observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages on loading controlled view.
    */
    override open func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseOrdersListViewController.refresh(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_PAYMENT)), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseOrdersListViewController.refresh(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_SET_ORDER)), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseOrdersListViewController.refresh(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_MERCHANT_SET_ORDER)), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseOrdersListViewController.reloadTableData(_:)), name: NSNotification.Name(rawValue: NOTIFY_NAME_ON_PUSH_RECEIVED), object: nil)
    }

    /**
     Removes observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages on moved to parent controlled view.
    */
    open override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self, name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_PAYMENT)), object: nil)
            NotificationCenter.default.removeObserver(self, name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_SET_ORDER)), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFY_NAME_ON_PUSH_RECEIVED), object: nil)
        }
    }
    
    public func filterStatuses(_ statuses: [String]) {
        self.filterStatuses = statuses.joined(separator: "|")
    }
    
    @objc func refresh(_ notification : Notification) {
        refresh()
    }
    
    @objc func reloadTableData(_ notification : Notification) {
        self.tableView.reloadData()
    }
    

    /**
    Loads the list of orders for current user and specified shop by specified filter.
     - parameter responseHandler: The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
    */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(filter, count: count, skip: skip)
        
        HonkioApi.userGetOrders(filter, shop:shopIdentity, flags: 0, handler: responseHandler)
    }

    /**
    Creates the list from the result of the Response
    - parameter responce: The Response instance within the result of processing.
    - Returns: A list of orders.
    */
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! OrdersList).list
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! Order), listItem, itemBinder))
        }
        return items
    }

    /**
     Gets the cell indetifier for provided Order.
    * Must be overriden.
    - parameter item: Order.
    - Returns: The identifier of cell.
    */
    open func cellIdentifier(_ item: Order) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }

    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }

    /**
     Sets the filter params for the current filter.
     - parameter filter: The filter to be updated.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
    */
    open func setupFilter(_ filter: OrderFilter, count: Int, skip: Int) {
        self.filter.status = filterStatuses
        self.filter.excludeExpired = filterExcludeExpired
        self.filter.queryProductDetails = filterQueryProductDetails
        
        if HonkioApi.activeUser() != nil {
            if filterUserIsOwner {
                self.filter.owner = HonkioApi.activeUser()!.userId
            }
            
            if filterUserIsThirdPerson {
                self.filter.thirdPerson = HonkioApi.activeUser()!.userId
            }
        }
        
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
}
