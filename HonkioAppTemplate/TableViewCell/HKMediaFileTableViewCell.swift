//
//  HKMediaFileTableViewCell.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 2/28/19.
//  Copyright © 2019 developer. All rights reserved.
//

import UIKit

open class HKMediaFileTableViewCell: UICollectionViewCell {
    
    @IBOutlet open var mediaImage: UIImageView!

    
    open func fillUi(_ item: Any) {
        if let mediaFile = item as? HKMediaFile {
            self.fillUi(mediaFile: mediaFile)
        }
        else if let image = item as? UIImage {
            self.fillUi(image: image)
        }
        else if let url = item as? URL {
            self.fillUi(url: url)
        }
    }
    
    open func fillUi(image: UIImage) {
        self.mediaImage.image = image
    }
    
    open func fillUi(url: URL) {
        self.mediaImage.imageFromURL(link: url.absoluteString, errorImage: UIImage(named: "ic_delete"), contentMode: .scaleAspectFit)
    }
    
    open func fillUi(mediaFile: HKMediaFile) {
        self.mediaImage.imageFromURL(link: mediaFile.url, errorImage: UIImage(named: "ic_delete"), contentMode: .scaleAspectFit)
    }
}
