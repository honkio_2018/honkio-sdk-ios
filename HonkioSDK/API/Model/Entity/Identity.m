//
//  Identity.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "Identity.h"

@implementation Identity

-(id)initWithId:(NSString*)identityId andPassword:(NSString*)password {
    self = [super init];
    if (self) {
        _identityId = identityId;
        _password = password;
    }
    return self;
}

-(id)initWithFrom:(Identity*)identity {
    return [self initWithId:[identity identityId] andPassword:[identity password]];
}

-(BOOL)isCorupt {
    return [self.identityId length] == 0 || [self.password length] == 0;
}

- (BOOL)isEqual: (id)other {
    if (self == other) {
        return YES;
    }
    
    if (!other || ![other isKindOfClass: [Identity class]]) {
        return NO;
    }
    
    Identity *identity = other;
    return ([_identityId isEqual:identity.identityId] && [_password isEqual:identity.password]);
}

@end
