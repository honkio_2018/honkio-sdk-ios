//
//  HKBaseLoginViewController.swift
//  WorkPilots
//
//  Created by developer on 12/11/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

/**
 HKBaseLoginViewController provides base functionality for view controllers what are supposed
 to work with login routine.
 Internally checks activation/pending status and rises corresponding routine.
 Supports login by PIN code and OAuth token if provided.
* Must not be instantiated.
*/

open class HKBaseLoginViewController: HKBaseViewController, HKPinProtocol {
    /// Contains a link to the login listener.
    open weak var loginDelegate : HKLoginViewLoginDelegate?

    /**
    Loads ServerInfo from the server and starts login process if succeed.
     Will try to reload data if error has occured.
     Will try to login with stored PIN if PIN and OTP are found, otherwise will run PIN request to as user.
     If no OTP was saved will run normal login routine.
    */
    open func loadServerData(doLogin: Bool) {
        
        self.startWaitingOperation()
        
        HonkioApi.loadServerData(0) {[weak self] err in
            
            if err != nil {
                self?.stopWaitOperation()
                
                let action = {() -> Void in
                    self?.loadServerData(doLogin: doLogin)
                }
                
                if self == nil || !AppErrors.handleError(self!, error: err, fallAction: action) {
                    self?.handleError(error: err!)
                }
            }
            else {
                self?.serverDataDidLoad(doLogin: doLogin)
            }
        }
    }
    
    open func serverDataDidLoad(doLogin: Bool) {
        if doLogin && Connection.isHasSoredOtp() {
            
            if let savedPin = StoreData.load(fromStore: STORE_KEY_PIN) as? String {
                
                self.doLoginForPin(savedPin)
                
            } else {
                
                self.showPinViewController(Int(PIN_REASON_REENTER), completionAction: nil)
            }
            
        }
        else {
            
            self.callLoginFinished(false)
        }
    }

    /**
    Starts login to server using the provided OAuth token.
    - parameter token: The token used for authorization.
    */
    open func doLoginForOAuthToken(_ token: String) {
        DispatchQueue.main.async{
            self.startWaitingOperation()
        }
        
        HonkioApi.login(withToken: token, flags: 0) {[weak self] (response) in
            
            self?.stopWaitOperation()

            if response.isStatusAccept() {
                
                if (response.result as? User) != nil {
                    self?.showPinViewController(Int(PIN_REASON_NEW), completionAction: nil)
                }
            }
            else if AppErrors.isError(response) {
                let error = response.anyError()
                if error!.code == ErrCommon.invalidUserLogin.rawValue {
                    
                    self?.alertInvalidLogin(nil)
                }
                else if self == nil || !AppErrors.handleError(self!, response: response) {
                    self?.handleError(response: response)
                }
            }
        }
    }

    /**
    Logins the application with the provided PIN code.
    - parameter pin: The PIN code to be used for login.
    */
    open func doLoginForPin(_ pin: String!) {
        
        HonkioApi.login(withPin: pin, flags: 0) {[weak self] (response) in
            
            if AppErrors.isError(response) {
                if self == nil || !AppErrors.handleError(self!, response: response) {
                    self?.handleError(response: response)
                }
                self?.callLoginFinished(false)
            }
            else if response.isStatusAccept() {
                    
                if !(self?.checkActivation() ?? false) {
                    
                    self?.callLoginFinished(false)
                    
                } else {
                    if self?.checkTouAfterLogin() ?? false {
                        self?.callLoginFinished(true)
                    }
                }
            }
            else {
                self?.callLoginFinished(false)
            }
        }
    }

    /**
     Is called when PIN entered.
    - parameter pinCode: Entered PIN code.
    - parameter reason: Defines the reason of entering pin code.
    */
    open func pinEntered(_ pinCode: String!, reason: Int!) {

        switch reason {

        case Int(PIN_REASON_REENTER):

            if HonkioApi.checkPin(pinCode) {

                self.startWaitingOperation()
                self.doLoginForPin(pinCode)
                break

            } else {

                if HonkioApi.activeConnection()!.isValid() {

                    self.alertInvalidPin() {
                        self.showPinViewController(reason, completionAction: nil)
                    }
                } else {

                    self.alertInvalidPin() {
                        self.callLoginFinished(false)
                    }
                }
            }

        case Int(PIN_REASON_NEW):

            HonkioApi.changePin(pinCode)
            StoreData.save(toStore: pinCode, forKey: STORE_KEY_PIN)

            if self.checkActivation() && self.checkTouAfterLogin() {
                self.callLoginFinished(true)
            }

            break

        default:
            break
        }
    }

    /**
    Is called when PIN pad canceled without entering a PIN.
    - parameter reason: Defines the reason of entering pin code.
    - parameter view: Parent view.
    */
    open func pinCanceled(_ reason: Int!) {
        // Do nothing
    }

    /**
    Should be called for error handling.
    * Must be overriden.
    - parameter response: The Responce object containing the error.
    - parameter error: The NSError object containing the error.
    */
    open func handleError(response: Response) {
        
        self.handleError(error: response.anyError())
    }
    
    open func handleError(error: ApiError) {
        
        self.riseMustOverrideException()
    }

    /**
    Shows PinView
    * Must be overriden.
    - parameter reason: The reaon of PIN code entering.
    - parameter completionAction: The action is to be performed after PIN code is entered.
    */
    open func showPinViewController(_ reason: Int, completionAction: (() -> Void)?) {
        
        self.riseMustOverrideException()
    }

    /**
    Shows Terms of Use.
    * Must be overriten.
    - parameter completionAction: The action to be performed after accepting Terms of Use.
    */
    open func showTouUpdateViewController(_ completionAction: (() -> Void)?) {
        
        self.riseMustOverrideException()
    }

    /**
    Should be called when the application comes back from busy state.
    * Must be overriden.
    - Example: Hide busy/progress indicator.
    */
    open func stopWaitOperation() {
        
        self.riseMustOverrideException()
    }

    /**
    Should be called when the application goes to busy state.
    * Must be overriden.
    - Example: Show busy/progress indicator.
    */
    open func startWaitingOperation() {

        self.riseMustOverrideException()
    }
    
    /**
     Call this function for notification of login state listener that the login procedure is completed.
     - parameter isLoggedIn: Should be set to true if the user has successfully logeed in, otherwise should ne set to false.
     */
    open func callLoginFinished(_ isLoggedin: Bool) {
        self.loginDelegate?.loginFinished(self as! HKLoginViewProtocol, isLoggedin)
    }
    
    // Alerts ======================================== \/

    /**
    Will be called when the user login status is PENDING.
    * Must be overriden.
    - parameter response: The response within the data.
    - parameter action: The action is to be performed in alert processing.
    */
    open func alertPendingStatus(_ response: Response, action: (() -> Void)?) {
        
        self.riseMustOverrideException()
    }

    /**
    Will be called when server responded that the email verification is needed.
    * Must be overriden.
    - parameter action: The action is to be performed in alert processing.
    */
    open func alertEmailVerify(_ action: (() -> Void)?) {
        
        self.riseMustOverrideException()
    }

    /**
    Will be called when server responded that the loign information is not valid.
    * Must be overriden.
    - parameter action: The action is to be performed in alert processing.
    */
    open func alertInvalidLogin(_ action: (() -> Void)?) {
        
        self.riseMustOverrideException()
    }

    /**
    Will be called when server responded that the PIN code is not valid.
    * Must be overriden.
    - parameter action: The action is to be performed in alert processing.
    */
    open func alertInvalidPin(_ action: (() -> Void)?) {
        
        self.riseMustOverrideException()
    }
    
    // Alerts ======================================== /\
    
    func checkActivation() -> Bool {
        
        if HonkioApi.activeUser()!.isActive {
            
            return true;
        }
        
        self.checkPendingStatus {[weak self] (response) in
            
            if AppErrors.isError(response) {
                if self == nil || !AppErrors.handleError(self!, response: response) {
                    self?.handleError(response: response)
                }
                return
            }
            
            if !response.isStatusPending() {
                
                return
            }
            else if response.hasPending(RESPONSE_PENDING_EMAIL_VERIFY) {
                
                self?.alertEmailVerify(nil)
                return
            }
            else {
                
                self?.alertPendingStatus(response, action: nil)
            }
        }
        
        return false;
    }
    
    func checkTouAfterLogin() -> Bool {
        if HonkioApi.activeUser()!.isTouVersionObsolete(HonkioApi.appInfo()) {
            showTouUpdateViewController({ () -> Void in
                self.callLoginFinished(true)
            })
            return false
        }
        else {
            return true
        }
    }
    
    func checkPendingStatus(_ responseHandler: ResponseHandler?) {
        
        if let transactionId = HonkioApi.activeUser()!.registrationId {
            
            HonkioApi.queryUserRegister(transactionId, flags: 0) { (response) in
                
                if response.isStatusError() {
                    
                    responseHandler?(response)
                    return
                }
                
                if response.isStatusReject() {
                    
                    HonkioApi.userReRegister(HonkioApi.activeUser()!, flags: 0){ (userResponse) -> Void in
                        
                        if userResponse.isStatusAccept() {
                            
                            if let transaction = userResponse.transactoinId {
                                
                                HonkioApi.userUpdateDictValues(["app_reg_transaction_id" : transaction], flags: 0, handler: nil)
                                HonkioApi.queryUserRegister(transaction, flags: 0, handler: responseHandler!)
                            }
                            else {
                                
                                responseHandler?(Response.withError(ErrApi.unknown.rawValue, desc: "cann't get transaction id after re-registration"))
                            }
                        }
                        else {
                            
                            responseHandler?(userResponse)
                        }
                        
                    }
                }
                
            }
        }
        else {
            
            responseHandler?(Response.withError(ErrApi.unknown.rawValue, desc: "can't get transaction ID"))
        }
    }
    
    func riseMustOverrideException() {
        
        preconditionFailure("WARNING: You must override this method in your custom class")
    }
}
