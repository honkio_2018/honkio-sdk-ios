//
//  PinCancelActionController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 4/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
Provides the functionality for cancelling PIN entering routine.
*/

open class HKPinCancelActionVCStrategy : HKCancelActionVCStrategy {

    /**
    Creates cancel button and attaches cancellation action to this button on viewWillAppear event.
    - parameter animated: Describe whether the view will appear animated.
    */
    override open func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAction))
    }

    /**
    Describes the action to be performed on cancell button click event.
    */
    override open func cancelAction() {
        super.cancelAction()
        if let perrent = self.viewController as? HKBasePinViewController {
            perrent.cancelAction()
        }
    }
}
