//
//  ApplicantFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/9/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "ApplicantFilter.h"

@implementation ApplicantFilter

-(void)fillParams:(NSMutableDictionary *)params {
    [AbsFilter applyIfNotNil:@"order_id" value: self.orderId forParams:params];
    [AbsFilter applyIfNotNil:@"query_role" value:self.queryRole forParams:params];
    
    [super fillParams:params];
}

@end
