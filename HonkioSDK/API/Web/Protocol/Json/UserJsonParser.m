//
//  UserJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "UserJsonParser.h"
#import "User.h"
#import "ApiDateFormatter.h"

@implementation UserJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    User* user = [User new];
    
    user.userId = dict[@"id"];
    
    user.login      = dict[USER_EMAIL_KEY];
    if (!user.login) {
        user.login = dict[MSG_PARAM_LOGIN_IDENTITY];
    }
    
    user.firstname  = dict[USER_FIRSTNAME_KEY];
    user.lastname   = dict[USER_LASTNAME_KEY];
    user.phone      = dict[USER_TELEPHONE_KEY];
    user.country    = dict[USER_COUNTRY_KEY];
    user.city       = dict[USER_CITY_KEY];
    user.address1   = dict[USER_ADDRESS1_KEY];
    user.address2   = dict[USER_ADDRESS2_KEY];
    user.ssn        = dict[USER_SSN_KEY];
    user.zip        = dict[USER_ZIP_KEY];
    user.timezone   = dict[USER_TIMEZONE_KEY];
    user.language   = dict[USER_LANGUAGE_KEY];
    
    user.accounts = [self parseAccounts:dict];
    
    user.isActive = YES;
    id active = dict[USER_ACTIVE_KEY];
    if(active && ![active boolValue])
        user.isActive = NO;
    
    user.isTempPasswordUsed = NO;
    id lostPassword = dict[USER_LOSTPASSWORD_KEY];
    if(lostPassword && [lostPassword boolValue])
        user.isTempPasswordUsed = YES;
    
    user.isOperatorBillingEnabled = NO;
    id opertorEnabled = dict[OPERATOR_BILLING];
    if(opertorEnabled && [opertorEnabled boolValue])
        user.isActive = YES;
    
    user.userKeystore = dict[USER_KEYSTORE_KEY];
    if(![user.userKeystore isKindOfClass:[NSDictionary class]])
        user.userKeystore = nil;
    
    user.registrationId = dict[USER_REGISTRATION];
    
    NSDictionary* touAppsDict = dict[USER_TOU_VERSION_KEY];
    if (touAppsDict) {
        NSMutableDictionary* touApps = [NSMutableDictionary new];
        for (NSString* appId in [touAppsDict allKeys]) {
            NSDictionary* touAppDict = touAppsDict[appId];
            if (touAppDict) {
                NSMutableDictionary* appTou = [NSMutableDictionary new];
                
                NSString* defaultTimeStamp = touAppDict[@"timestamp"];
                NSString* defaultVersion = touAppDict[@"version"];
                if (defaultTimeStamp && defaultVersion) {
                    appTou[TOU_KEY_DEFAULT] = [[HKTouVersion alloc] initWithVersion:[defaultVersion intValue]
                                                                          timestamp: [ApiDateFormatter dateFromServerString: defaultTimeStamp]];
                }
                
                for (NSString* touKey in [touAppDict allKeys]) {
                    NSDictionary* touDict = touAppDict[touKey];
                    if (touDict && [touDict isKindOfClass:NSDictionary.class]) {
                        appTou[touKey] = [[HKTouVersion alloc] initWithVersion:[touDict[@"version"] intValue]
                                                                     timestamp: [ApiDateFormatter dateFromServerString: touDict[@"timestamp"]]];
                    }
                }
                
                touApps[appId] = appTou;
            }
        }
        user.touVersions = touApps;
    }
    
    return user;
}

-(NSArray*)parseAccounts:(NSDictionary*)dict {
    NSMutableArray* accountsArray = [NSMutableArray new];
    id accountsObject = dict[USER_ACCOUNTS_KEY];
    if( [accountsObject isKindOfClass:[NSArray class] ]) {
        NSArray* accounts = accountsObject;
        for(NSUInteger i=0; i < accounts.count; i++) {
            NSDictionary* property = accounts[i];
            UserAccount* accountInfo = [self parseAccount:property];
            accountInfo.index = i;
            
            [accountsArray addObject:accountInfo];
        }
    }
    return accountsArray;
}

-(UserAccount*)parseAccount:(NSDictionary*)dict {
    UserAccount* accountInfo = [UserAccount new];
    
    accountInfo.type = dict[@"type"];
    accountInfo.number = dict[@"number"];
    
    NSString* isSignString = dict[@"sign"];
    accountInfo.isSign = (isSignString != nil) && [isSignString boolValue];
    
    NSString* leftString = dict[@"left"];
    accountInfo.isHasLeft = (leftString != nil);
    if (accountInfo.isHasLeft) {
        accountInfo.left = [leftString floatValue];
    }
    accountInfo.limit = [dict[@"limit"] floatValue];
    accountInfo.accountDescription = dict[@"description"];
    
    accountInfo.logoUrl = dict[@"logo_url"];
    
    return accountInfo;
}
@end
