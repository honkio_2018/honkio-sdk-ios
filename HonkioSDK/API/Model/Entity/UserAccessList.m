//
//  UserAccessList.m
//  HonkioSDK
//
//  Created by Mikhail Li on 15/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "UserAccessList.h"

@implementation UserAccessList

-(nullable UserAccessItem *)findByMerchant:(nullable NSString *)merchantId {
    if (!self.list || !merchantId) {
        return nil;
    }
    for (UserAccessItem *item in self.list) {
        if ([merchantId isEqualToString:item.merchant])
            return item;
    }
    return nil;
}

-(BOOL)isHasAccess:(NSString *)merchantId access:(NSArray *)access {
    UserAccessItem *item = [self findByMerchant:merchantId];
    return item != nil && [item isHasAccess:access];
}

@end
