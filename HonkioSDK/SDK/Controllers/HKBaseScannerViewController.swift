//
//  HKBaseScannerViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/6/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit
import AVFoundation

open class Barcode {
    open var metadataObject : AVMetadataMachineReadableCodeObject?
    open var barcodeType : String?
    open var barcodeData : String?
    open var cornersPath : UIBezierPath?
    open var boundingBoxPath : UIBezierPath?
    
    public static func processMetadataObject(_ code : AVMetadataMachineReadableCodeObject) -> Barcode {
        // 1 create the obj
        let barcode = Barcode()
        
        // 2 store code type and string
        barcode.barcodeType = code.type.rawValue;
        barcode.barcodeData = code.stringValue;
        barcode.metadataObject = code;
        
        // 3 Create the path joining code's corners
        let cornersPath = CGMutablePath();
        
        cornersPath.move(to: code.corners[0])
        
        // 4 Make path
        for i in 1...(code.corners.count - 1) {
            cornersPath.move(to: code.corners[i])
        }
        // 5 Finish box
        cornersPath.closeSubpath()
        
        // 6 Set path
        barcode.cornersPath = UIBezierPath(cgPath: cornersPath)
        
        // 7 Create the path for the code's bounding box
        barcode.boundingBoxPath = UIBezierPath(rect: code.bounds)
        
        return barcode
    }
    
}

open class HKBaseScannerViewController: HKBaseViewController, AVCaptureMetadataOutputObjectsDelegate {

    @IBOutlet open weak var scanView : UIView!
    
    public let allowedBarcodeTypes : [String] = ["org.iso.QRCode"]
    
    /* Here’s a quick rundown of the instance variables (via 'iOS 7 By Tutorials'):
     
     1. _captureSession – AVCaptureSession is the core media handling class in AVFoundation. It talks to the hardware to retrieve, process, and output video. A capture session wires together inputs and outputs, and controls the format and resolution of the output frames.
     
     2. _videoDevice – AVCaptureDevice encapsulates the physical camera on a device. Modern iPhones have both front and rear cameras, while other devices may only have a single camera.
     
     3. _videoInput – To add an AVCaptureDevice to a session, wrap it in an AVCaptureDeviceInput. A capture session can have multiple inputs and multiple outputs.
     
     4. _previewLayer – AVCaptureVideoPreviewLayer provides a mechanism for displaying the current frames flowing through a capture session; it allows you to display the camera output in your UI.
     5. _running – This holds the state of the session; either the session is running or it’s not.
     6. _metadataOutput - AVCaptureMetadataOutput provides a callback to the application when metadata is detected in a video frame. AV Foundation supports two types of metadata: machine readable codes and face detection.
     7. _backgroundQueue - Used for showing alert using a separate thread.
     */
    private var _captureSession : AVCaptureSession!
    private var _videoDevice : AVCaptureDevice!
    private var _videoInput : AVCaptureDeviceInput!
    private var _previewLayer : AVCaptureVideoPreviewLayer!
    private var _running : Bool = false
    private var _metadataOutput : AVCaptureMetadataOutput!
    private var isCaptureSessionSetuped = false
    
    override public init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        if (_captureSession == nil) {
            
            _videoDevice = AVCaptureDevice.default(for: AVMediaType.video)
            
            if _videoDevice == nil {
                HKLog.e("No video camera on this device!")
            }
            else {
                
                _captureSession = AVCaptureSession()
                _captureSession.sessionPreset = AVCaptureSession.Preset.high
                do {
                    try _videoInput = AVCaptureDeviceInput(device: _videoDevice)
                }
                catch {
                    // Do nothing
                }
                
                if _captureSession.canAddInput(_videoInput) {
                    _captureSession.addInput(_videoInput)
                }
                
                _previewLayer = AVCaptureVideoPreviewLayer(session: _captureSession)
                _previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
                
                _metadataOutput = AVCaptureMetadataOutput()
                
                let metadataQueue = DispatchQueue(label: "com.riskpointer.camera_queue")
                _metadataOutput.setMetadataObjectsDelegate(self, queue: metadataQueue)
                
                if _captureSession.canAddOutput(_metadataOutput) {
                    _captureSession.addOutput(_metadataOutput)
                }
                
                _previewLayer.frame = self.view.layer.frame;
                self.scanView.layer.addSublayer(_previewLayer)
                
                isCaptureSessionSetuped = true
            }
        }
        
        // listen for going into the background and stop the session
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseScannerViewController.applicationWillEnterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseScannerViewController.applicationDidEnterBackground(_:)), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.startRunning()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.stopRunning()
    }
    
    @objc public func applicationWillEnterForeground(_ note : NSNotification) {
        self.startRunning()
    }
    
    @objc public func applicationDidEnterBackground(_ note : NSNotification) {
        self.stopRunning()
    }
    
    public func startRunning() {
        if _running || !isCaptureSessionSetuped {
            return
        }
        
        _captureSession.startRunning()
        _metadataOutput.metadataObjectTypes = _metadataOutput.availableMetadataObjectTypes
        _running = true
    }
    
    public func stopRunning() {
        if !_running || !isCaptureSessionSetuped {
            return
        }
        
        _captureSession.stopRunning()
        _running = false
    }
    
    open func barcodeDidRecognized(_ barcode: Barcode) {
        preconditionFailure("Method barcodeDidRecognized must be overridden")
    }
    
    public func metadataOutput(_ captureOutput: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        for object in metadataObjects {
            HKLog.d("QR code scanner recognize object")
            if let metaDataObject = object as? AVMetadataMachineReadableCodeObject {
                if !_running {
                    return
                }
                
                self.stopRunning()
                
                let code = _previewLayer.transformedMetadataObject(for: metaDataObject) as! AVMetadataMachineReadableCodeObject
                let barCode = Barcode.processMetadataObject(code)
                
                DispatchQueue.main.async { [unowned self] in
                    self.barcodeDidRecognized(barCode)
                }
            }
        }
    }
}
