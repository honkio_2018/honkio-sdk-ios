//
//  ApiShop.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "Shop.h"
#import "Tag.h"

@interface Shop ()

@end


@implementation Shop

@synthesize description;

-(BOOL)checkDistanceToLat:(double)latitude lon:(double)longitude {
    return [self checkDistanceToLocation:[[CLLocation alloc] initWithLatitude:latitude longitude:longitude]];
}

-(BOOL)checkDistanceToLocation:(CLLocation*)location {
    CLLocation* shopLoc = [[CLLocation alloc] initWithLatitude:self.latitude longitude:self.longitude];
    CLLocationDistance distance = [shopLoc distanceFromLocation:location];
    return distance < self.maxDistance;
}

-(BOOL)isHasTagWithId:(NSString*)tagId {
    if (self.tags && tagId) {
        for (Tag* tag in self.tags) {
            if ([tagId isEqualToString:[tag tagId]]) {
                return YES;
            }
        }
    }
    return NO;
}

-(BOOL)isHasTagWithName:(NSString*)tagName {
    if (self.tags && tagName) {
        for (Tag* tag in self.tags) {
            if ([tagName isEqualToString:[tag name]]) {
                return YES;
            }
        }
    }
    return NO;
}

@end
