//
//  CalendarSchedule.h
//  HonkioSDK
//
//  Created by Mikhail Li on 27/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CalendarEvent.h"
#import "ScheduleFilter.h"

@interface CalendarSchedule : NSObject

@property (nonatomic) NSArray<CalendarEvent *> *list;
@property (nonatomic) ScheduleFilter *filter;

@end
