//
//  MerchantShopInfoJsonParser.m
//  HonkioSDK
//
//  Created by Mikhail Li on 24/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "MerchantShopInfoJsonParser.h"
#import "MerchantShopInfo.h"
#import "ApiDateFormatter.h"
#import "MerchantShop.h"

@implementation MerchantShopInfoJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary *)dict {
    MerchantShopInfo *merchantShopInfo = [MerchantShopInfo new];
    merchantShopInfo.timeStamp = [ApiDateFormatter stringDateToLong: dict[@"timestamp"]];
    
    NSDictionary* shopDict = dict[@"shop"];
    if (shopDict) {
        MerchantShop *merchantShop = [[MerchantShop alloc] init:shopDict];
        merchantShop.identityId = shopDict[@"id"];
        merchantShop.password = shopDict[@"password"];
        merchantShop.settings = shopDict[@"settings"];
        merchantShopInfo.shop = merchantShop;
    }
    return merchantShopInfo;
}
@end
