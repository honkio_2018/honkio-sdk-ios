//
//  PriorityQueue.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"

@interface PriorityQueue : NSObject

CWL_DECLARE_SINGLETON_FOR_CLASS(PriorityQueue);

-(NSOperationQueue*)queue;

+(void)addOperation:(NSOperation *)operation;

@end
