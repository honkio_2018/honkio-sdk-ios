//
//  HKUserForgotPasswordViewController.swift
//  WorkPilots
//
//  Created by developer on 1/15/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
The view controller manages the user forgot password view.
*/
open class HKUserForgotPasswordViewController: HKBaseViewController {
    /// The login field
    @IBOutlet open weak var loginField: UITextField!
    /**
    The action to be performed on send button.
    Gets the current user login value from the API, shows busy indicator, sends the request to tthe server for
    creating as temporary password for a user. After the request has been reveived hides progress indicator.
    If the response is accepted notifies user by the CustomALertView within the "lost_password_dlg_success_message".
    If the response contains error and this error was not handled by application then notifies the user by
    CustomAlertView within the error description.
    - parameter sender: Not used.
    */
    @IBAction open func sendAction(_ sender: AnyObject) {
        
        let login = HonkioApi.activeUser()!.login
        
        BusyIndicator.shared.showProgressView(self.view)
        
        HonkioApi.userCreateTempPassword(login!, flags: 0) { (response) in
            
            BusyIndicator.shared.hideProgressView()

            if response.isStatusAccept() {
                
                CustomAlertView(okTitle: nil, okMessage: "lost_password_dlg_success_message".localized, okAction: { self.dismiss(animated: true, completion: nil) }).show()
                return
            }
            if !AppErrors.handleError(self, response: response) {
                CustomAlertView(apiError: response.anyError(), okAction: nil).show()
            }
            
        }
    }
    /**
    On viewDidLoad attaches the LiginField delegate and fills the loginField value from the result of API call.
    */
    override open func viewDidLoad() {
        super.viewDidLoad()

        self.loginField.text = HonkioApi.activeUser()!.login
        self.loginField.delegate = self
    }
    /**
    Sets the UITextField (loginField) in readonly mode.
    - parameter textField: The UITextField calls the method.
    - Returns: Whether the field is editable (always false).
    */
    open func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return false
    }
}
