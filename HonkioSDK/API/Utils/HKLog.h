//
//  HKLog.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/26/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HKLog : NSObject

+(void)d:(NSString*)string;

+(void)e:(NSString*)string;

@end
