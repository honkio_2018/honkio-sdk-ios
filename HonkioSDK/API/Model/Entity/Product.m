//
//  Product.m
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/3/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import "Product.h"
#import "UserAccount.h"

@implementation PicInfo

@end

@implementation Target

@end


@implementation Product

-(instancetype)init:(NSDictionary *) propertiesDict {
    self = [super init:propertiesDict];
    return self;
}

-(instancetype)initFrom:(Product*) product {
    self = [super initFromEntity:product];
    
    self.productId = product.productId;
    self.name = product.name;
    self.desc = product.desc;
    self.currency = product.currency;
    self.amount = product.amount;
    self.amount_vat = product.amount_vat;
    self.amount_no_vat = product.amount_no_vat;
    self.amount_vat_percent = product.amount_vat_percent;
    self.isActive = product.isActive;
    self.visible = product.isVisible;
    self.typeId = product.typeId;
    self.duration = product.duration;
    
    self.validityTime = product.validityTime;
    self.validityTimeType = product.validityTimeType;
    
    if (product.surplus)
        self.surplus = [NSDictionary dictionaryWithDictionary:product.surplus];
    
    // TODO clone pics
    if (product.pics)
        self.pics = [NSArray arrayWithArray:product.pics];
    
    if (product.tags)
        self.tags = [NSArray arrayWithArray:product.tags];
    
    if (product.targetProducts) {
        self.targetProducts = [NSArray arrayWithArray:product.targetProducts];
    }
    
    return self;
}

-(NSDictionary *)toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [AbsFilter applyIfNotNil:@"id" value:self.productId forParams:params];
    [AbsFilter applyIfNotNil:@"name" value:self.name forParams:params];
    [AbsFilter applyIfNotNil:@"description" value:self.desc forParams:params];
    [AbsFilter applyIfNotNil:@"currency" value:self.currency forParams:params];
    [AbsFilter applyIfNotNil:@"amount" value:[NSString stringWithFormat:@"%.2f", self.amount] forParams:params];
    [AbsFilter applyIfNotNil:@"amount_vat" value:[NSString stringWithFormat:@"%.2f", self.amount_vat] forParams:params];
    [AbsFilter applyIfNotNil:@"amount_no_vat" value:[NSString stringWithFormat:@"%.2f", self.amount_no_vat] forParams:params];
    [AbsFilter applyIfNotNil:@"amount_vat_percent" value:[NSString stringWithFormat:@"%.2f", self.amount_vat_percent] forParams:params];
    [AbsFilter applyIfNotNil:@"int_validity_time" value:[NSNumber numberWithLong:self.validityTime] forParams:params];
    [AbsFilter applyIfNotNil:@"str_validity_time_type" value:self.validityTimeType forParams:params];
    [AbsFilter applyIfNotNil:@"id_type" value:self.typeId forParams:params];
    [AbsFilter applyIfNotNil:@"active" value:[NSNumber numberWithBool:self.isActive] forParams:params];
    
    if (self.duration > 0)
        [AbsFilter applyIfNotNil:@"int_duration" value:@(self.duration) forParams:params];
    if (self.targetProducts && self.targetProducts.count > 0) {
        NSMutableArray *targetProducts = [NSMutableArray array];
        for (Target *targetItem in self.targetProducts) {
            [targetProducts addObject:@{@"id": targetItem.productId, @"required_count": @(targetItem.count)}];
        }
        [AbsFilter applyIfNotNil:@"target_products" value:targetProducts forParams:params];
    }
    
    if (self.tags && self.tags.count > 0) {
        [AbsFilter applyIfNotNil:@"list_tags" value:self.tags forParams:params];
    }

    if (self.pics && self.pics.count > 0) {
        NSMutableArray *pics = [NSMutableArray array];
        for (PicInfo* picsItem in self.pics) {
            [pics addObject:@{@"url": picsItem.url,
                              @"x": picsItem.width,
                              @"y": picsItem.heigth}];
        }
        [AbsFilter applyIfNotNil:@"list_pics" value:pics forParams:params];
    }
    
    return params;
}

-(BOOL)isHasTag:(NSString*)tagId {
    if (!tagId) {
        return NO;
    }
    
    for (NSString* tag in self.tags) {
        if ([tagId isEqualToString:tag]) {
            return YES;
        }
    }
    
    return NO;
}

-(BOOL)isHasTimeValidity {
    return self.validityTimeType && self.validityTimeType.length > 0 && self.validityTime > 0;
}

-(double)supplusFor:(NSString*)accountType
{
    if(accountType) {
        NSString* accountSurplus = self.surplus[accountType];
        if(accountSurplus)
            return [accountSurplus floatValue];
    }
    return 0.0;
}


-(BOOL)isCanBeExchanged {
    return [self.targetProducts count] > 0;
}

-(Target*)findTarget:(NSString*)productId {
    if (!productId) {
        return nil;
    }
    
    if (self.targetProducts) {
        for(Target* target in self.targetProducts) {
            if ([productId isEqualToString:target.productId]) {
                return target;
            }
        }
    }
    return nil;
}

@end


