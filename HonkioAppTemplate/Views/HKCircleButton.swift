//
//  HKCircleButton.swift
//  WorkPilots
//
//  Created by developer on 2/20/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
The subclass for UIButton. Represents the UIButton within the rounded corners.
*/
open class HKCircleButton: UIButton {
    /**
    The property for the color of button border.
    */
    @objc open var borderColor: UIColor? {
        set {
            self.layer.borderWidth = 1
            self.layer.borderColor = newValue?.cgColor
        }
        
        get {
            if self.layer.borderColor != nil {
                return UIColor(cgColor: self.layer.borderColor!)
            }
            return nil
        }
    }

    @objc open var highlightedColor: UIColor!
    /**
    Performs the counding of corners ans clips the button content to its bounds.
    */
    override open func layoutSubviews() {
    
        super.layoutSubviews()
        self.layer.cornerRadius = min(self.frame.width, self.frame.height) / 2
        self.clipsToBounds = true
    }
//    
//    public override func didMoveToSuperview() {
//        super.didMoveToSuperview()
//        
//        if self.highlightedColor != nil {
//            self.setBackgroundImage(UIImage.imageFromColor(self.highlightedColor, rect: CGRect(x: 0, y: 0, width: 1, height: 1)), forState: UIControlState.Highlighted)
//        }
//    }
    
}
