//
//  SimpleAsyncGet.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/25/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "SimpleAsyncGet.h"
#import "PriorityQueue.h"

@implementation SimpleAsyncGet {
    NSURL* url;
    UrlResponseHandler respHandler;
    NSDictionary* headers;
    
    NSError* error;
}

-(id)initWithUrl:(NSURL*)requestUrl heders:(NSDictionary*)headers responseHandler:(UrlResponseHandler)responseHandler {
    self = [super init];
    if (self) {
        url = requestUrl;
        respHandler = responseHandler;
    }
    return self;
}

-(void)execute {
    [NSURLConnection sendAsynchronousRequest:[self buildRequest]
                                       queue:[PriorityQueue shared].queue
                           completionHandler:respHandler];
}

-(NSMutableURLRequest*)buildRequest {
    return [self buildRequestForMethod:@"GET"];
}

-(NSMutableURLRequest*)buildRequestForMethod:(NSString*)method {
     NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:30.0f];
    [urlRequest setHTTPMethod:method];
    if(headers) {
        for(NSString* headerKey in headers) {
            [urlRequest setValue:headers[headerKey]  forHTTPHeaderField:headerKey];
        }
    }
    return urlRequest;
}

-(void)abort {
    //Do nothing
}

-(BOOL)isAborted {
    return false;
}

@end
