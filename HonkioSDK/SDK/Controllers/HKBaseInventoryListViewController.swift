//
//  HKBaseInventoryListViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/19/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

/**
 The base ViewController that provides functionality for loading and filtering user inventory. ViewController adds observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages into NSNotificationCenter to automaticaly refresh list.
 * Must not be instantiated.
 */

open class HKBaseInventoryListViewController: HKBaseLazyTableViewController {
    var shopIdentity : Identity?
    
    @IBInspectable open var groupByProduct : Bool = false
    
    /**
     Adds observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages on loading controlled view.
     */
    override open func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseInventoryListViewController.refresh(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_PAYMENT)), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseInventoryListViewController.refresh(_:)), name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_SET_ORDER)), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseInventoryListViewController.reloadTableData(_:)), name: NSNotification.Name(rawValue: NOTIFY_NAME_ON_PUSH_RECEIVED), object: nil)
    }
    
    /**
     Removes observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages on moved to parent controlled view.
     */
    open override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self, name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_PAYMENT)), object: nil)
            NotificationCenter.default.removeObserver(self, name: Notification.Name(NotificationHelper.notifyName(onComplete: MSG_COMMAND_USER_SET_ORDER)), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFY_NAME_ON_PUSH_RECEIVED), object: nil)
        }
    }
    
    @objc func refresh(_ notification : Notification) {
        refresh()
    }
    
    @objc func reloadTableData(_ notification : Notification) {
        self.tableView.reloadData()
    }
    
    /**
     Loads the inventory for current user and specified shop.
     - parameter responseHandler: The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
     - parameter count: The number of items to be loaded from server (IGNORED).
     - parameter skip: The number of items to be skipped on loading (If skip > 0 execution will breaked).
     */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        if skip > 0 {
            responseHandler(Response.withError(ErrApi.canceledByUser.rawValue, desc: nil))
            return
        }
        HonkioApi.userGetInventory(self.shopIdentity, flags: 0, handler: responseHandler)
    }
    
    /**
     Creates the list from the result of the Response
     - parameter responce: The Response instance within the result of processing.
     - Returns: A list of inventory.
     */
    open override func toList(_ response : Response) -> [Any?] {
        let inventory = response.result as! Inventory
        return self.groupByProduct ? inventory.groupByProduct() : inventory.list
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! InventoryProduct), listItem, itemBinder))
        }
        return items
    }
    
    /**
     Gets the cell indetifier for provided Inventory product.
     * Must be overriden.
     - parameter item: Inventory product.
     - Returns: The identifier of cell.
     */
    open func cellIdentifier(_ item: InventoryProduct) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }
    
    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }
}
