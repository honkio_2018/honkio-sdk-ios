//
//  UIApplication+NetworkActivity.h
//  mePay
//
//  Created by Alexandr Kolganov on 2/5/14.
//
// source: http://stackoverflow.com/questions/3032192/networkactivityindicatorvisible
//

#import <UIKit/UIKit.h>

@interface UIApplication (NetworkActivity)
- (void)showNetworkActivityIndicator;
- (void)hideNetworkActivityIndicator;
@end
