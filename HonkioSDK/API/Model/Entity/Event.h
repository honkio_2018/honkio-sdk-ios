//
//  Event.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define EVENT_TYPE_BUSY         @"busy"
#define EVENT_TYPE_TENTATIVE    @"free"

#define EVENT_META_SHOP         @"shop"

@interface Event : NSObject

@property (nonatomic, copy) NSString *type;
@property (nonatomic) NSDate* start;
@property (nonatomic) NSDate* end;

/**
 * Array of dictionaries with meta data.
 */
@property (nonatomic) NSArray* metaData;

-(BOOL)isBusy;

@end
