//
//  HKDeviceJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/12/19.
//  Copyright © 2019 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseJsonResponseParser.h"

@interface HKDeviceJsonParser : BaseJsonResponseParser

@end
