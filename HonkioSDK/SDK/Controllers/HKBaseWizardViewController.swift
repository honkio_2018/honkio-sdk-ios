//
//  HKBaseWizardViewController.swift
//  WorkPilots
//
//  Created by developer on 12/14/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

/// HKBaseWizardViewController is a top level (container) wizard view controller.
/// Contains HKBaseWizardNavigationController for navigation between wizard steps.

open class HKBaseWizardViewController: HKBaseViewController {

    /// Connaines the navigation controller for the wizard.
    open var wizardNavigationController: HKBaseWizardNavigationController? {
        
        get {
            let controller = self.navigationController
            return controller as? HKBaseWizardNavigationController
        }
    }

    override open func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }


    /**
    Overrides the basic touchesBeganWithEvent for resigning the first view responder.
    - parameter touches: Set of UITouches
    - parameter event: The corresponding UIEvent.
    */
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true);
    }

    /// Moves to next wizard step (by pushing next ViewController) if possible or completes wizard.
    open func nextViewController() {
        if let wizard = self.wizardNavigationController {
            wizard.nextViewController()
        }
    }
}
