//
//  Order.h
//  HonkioApi
//
//  Created by Shurygin Denis on 12/1/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsFilter.h"
#import "UserAccount.h"
#import "UserRoleDescription.h"
#import "BookedProduct.h"

/**
 *  Describes loaded order.
 */
@interface Order : AbsFilter

// TODO write docs
-(id)init;

// TODO write docs
-(id)initFrom:(Order*) order;

/**
 *  Gets or sets Order id that is contain this transaction.
 */
@property (nonatomic) NSString* orderId;

/**
 *  Gets or sets Order model.
 */
@property (nonatomic) NSString* model;

/**
 *  Gets or sets Order status that is contain this transaction.
 */
@property (nonatomic) NSString* status;

/**
 *  Gets or sets Order title.
 */
@property (nonatomic) NSString* title;

/**
 *  Gets or sets Order description.
 */
@property (nonatomic) NSString* desc;

/**
 *  Gets or sets owner of current order.
 */
@property (nonatomic) NSString* userOwner;

/**
 *  Gets or sets third person.
 */
@property (nonatomic) NSString* thirdPerson;

/**
 *  Gets or sets creation date of current order.
 */
@property (nonatomic) NSDate* creationDate;

/**
 *  Gets or sets expire date of current order.
 */
@property (nonatomic) NSDate* expireDate;

/**
 *  Gets or sets completion date.
 */
@property (nonatomic) NSDate* completionDate;

/**
 *  Gets or sets start date of current order.
 */
@property (nonatomic) NSDate *startDate;

/**
 *  Gets or sets end date of current order.
 */
@property (nonatomic) NSDate *endDate;

/**
 *  Gets or sets order latitude.
 */
@property (nonatomic, assign) double latitude;

/**
 *  Gets or sets order longitude.
 */
@property (nonatomic, assign) double longitude;

/**
 *  Gets or sets amount.
 */
@property (nonatomic, assign) double amount;


/**
 *  Gets or sets type of currency.
 */
@property (nonatomic) NSString* currency;


/**
 *  Gets or sets list of products.
 */
@property (nonatomic) NSArray<BookedProduct*>* products;


/**
 *  Gets or sets payment account for payments.
 */
@property (nonatomic) UserAccount* account;


/**
 *  Gets or sets custom fields.
 */
@property (nonatomic) NSDictionary* customFields;

/**
 *  Gets or sets Shop id that is contain this transaction.
 */
@property (nonatomic) NSString* shopId;

/**
 *  Gets or sets Shop name that is contain this transaction.
 */
@property (nonatomic) NSString* shopName;

@property (nonatomic) NSString* asset;

/**
 *  Gets or sets Shop receipt for the Transaction.
 */
@property (nonatomic) NSString* shopReceipt;

/**
 *  Gets or sets reference string that was set in the payment request.
 */
@property (nonatomic) NSString* shopReference;

// TODO write docs
@property (nonatomic) NSArray<UserRoleDescription*>* limitedRoles;

// TODO write docs
@property (nonatomic) long unreadChatMessagesCount;

-(NSDictionary*)toParams;


/**
 *  Check whether the order is expired.
 *
 *  @return YES / NO
 */
-(BOOL)isExpired;


/**
 *
 *  Check whether the order is expired.
 *
 *  @param date Specified date.
 *
 *  @return YES / NO
 */
-(BOOL)isExpired:(NSDate*) date;

// TODO write docs
-(BOOL)isStatus:(NSString*) status;
// TODO write docs
-(BOOL)isStatusOneOf:(NSArray*) statuses;

// TODO write docs
-(void)setCustom:(NSString *)key value:(nonnull id) value;
// TODO write docs
-(nullable id)getCustom:(NSString *)key;

-(void)removeCustom:(NSString *)key;

// TODO write docs
-(BOOL)isAccountValid;

@end
