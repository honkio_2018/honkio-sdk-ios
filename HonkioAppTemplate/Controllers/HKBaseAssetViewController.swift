//
//  HKBaseAssetViewController.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 12/10/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

open class HKBaseAssetViewController: HKBaseLazyContentViewController<HKAsset>, HKAssetViewProtocol {
    
    open var asset: HKAsset? {
        get { return self.contentItem }
        set { self.contentItem = newValue }
    }
    
    open func loadAsset(_ assetId: String) {
        HonkioApi.userGetAsset(assetId, flags: 0)  { [weak self] (response) in
            if response.isStatusAccept() {
                self?.asset = response.result as? HKAsset
            }
            else {
                self?.loadingError(response.anyError())
            }
        }
    }
    
    open func loadingError(_ error: ApiError) {
        preconditionFailure("Method buildViewController must be overridden")
    }
    
}
