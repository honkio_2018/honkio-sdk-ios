//
//  HKAsset.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKStructure.h"
#import "HKQrCode.h"

// TODO write docs
@interface HKAsset : NSObject<HKQrObject>

@property (nonatomic, copy) NSString* assetId;
@property (nonatomic, copy) NSString* name;

@property (nonatomic, assign, getter=isVisible) BOOL visible;

/** Coordinate latitude of real position. */
@property (nonatomic, assign) double latitude;

/** Coordinate longitude of real position. */
@property (nonatomic, assign) double longitude;

@property (nonatomic, copy) NSString* merchantId;
@property (nonatomic, copy) NSString* group;

@property (nonatomic) NSDictionary* properties;

@property (nonatomic) HKStructure* stricture;

@property (nonatomic, assign, getter=isHasValidSchedule) BOOL hasValidSchedule;

-(id)init;

// TODO write docs
-(id)initFrom:(HKAsset*) asset;


-(NSDictionary *)toParams;

-(BOOL) structureIs:(NSString*) structureId;

-(BOOL) structureIsOneOf:(NSArray<NSString*>*) structures;

@end
