//
//  AbsProcessModel.h
//  HonkioApi
//
//  Created by Shurygin Denis on 8/3/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Task.h"
#import "Response.h"

/**
 Available statuses of the Process model.
 */
typedef enum {
    /** Process just created or reset. */
    CREATED,
    /** Process started. */
    STARTED,
    /** Process await for special condition to start again. */
    PENDING,
    /** Process finish work. */
    FINISHED,
    /** Process aborted. */
    ABORTED,
} ProcessModelStatus;

@class BaseProcessModel;

//TODO doc
@protocol ProcessProtocol <NSObject>

-(void)onComplete:(BaseProcessModel*)model response:(Response*)response;

@optional

-(void)onPending:(BaseProcessModel*)model response:(Response*)response;
-(void)onError:(BaseProcessModel*)model response:(Response*)response;

@end

/**
 * Base Process model. Not tested!!!
 */
@interface BaseProcessModel : NSObject

/** Status of the model. */
@property (readonly, nonatomic) ProcessModelStatus status;
@property (weak, nonatomic) id<ProcessProtocol> processDelegate;

/** Reset model and set status CREATED. */
-(void)reset;

/** Starting the process. Process can be started only in status CREATED. */
-(void)start;

/** Abortion of the process. */
-(void)abort;

/**
 * Check if process is aborted.
 * @return True if process aborted, false otherwise.
 */
-(BOOL)isAborted;

/**
 * Check if process is completed.
 * @return True if process completed, false otherwise.
 */
-(BOOL)isCompleted;

-(void)onComplete:(Response*)response;
-(void)onPending:(Response*)response;
-(void)onError:(Response*)response;

@end
