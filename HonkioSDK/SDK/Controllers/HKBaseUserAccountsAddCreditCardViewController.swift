//
//  HKBaseUserAccountsAddCreditCardViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/10/16.
//  Copyright © 2016 developer. All rights reserved.
//
import UIKit

/// The base ViewController class to be used for creating new Credit Card type payment account.

open class HKBaseUserAccountsAddCreditCardViewController: HKBaseViewController, UIWebViewDelegate, HKUserAccountAddViewProtocol {
    
    var accountCreator: AccountCreationProcessModel?

    /// The account type to be used for creating as new payment account.
    open var accountType : String = ACCOUNT_TYPE_CREDITCARD
    /// The shop to be used for creating as new payment account.
    open var shop : Shop? = nil
    
    @IBOutlet open var webView: UIWebView!

    /// Subscribes to web view UI changes, setup the web view to be fit to page, disables the scroll.
    /// Performs attache to LifeCycleObserver by calling super class viewDidLoad.
    override open func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.webView.delegate = self
        self.webView.scalesPageToFit = true
        self.webView.scrollView.isScrollEnabled = false
        
    }

    /**
    Runs on viewWillAppear received. Starts the account creation process
    and shows progress indicator. Notifies LifeCycleObserver by calling super class viewWillAppear.
    - parameter animated: Describes whether the view will be animated.
    */
    override open func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
//        self.webView.scrollView.contentInset = UIEdgeInsetsMake(18, 0, 0, 0)
        
        if self.accountCreator == nil {
            self.accountCreator = AccountCreationProcessModel(account: UserAccount(type: accountType, andNumber: ACCOUNT_NUMBER_ZERO), shop: shop, handler: { (response) -> Void in
                if response.isStatusPending() {
                    if let url = response.url {
                        self.webView.loadRequest(URLRequest(url: URL(string: url)!))
                        return
                    }
                }
                else if response.isStatusAccept() {
                    // show a good message
                    self.accountCreator = nil
                    self.notifyCompletionDelegate()
                    
                    return
                }
                
                let _ = self.handleError(response)
                
                })
            
            self.accountCreator!.start()
            showProgress(self.view)
        }
    }

    /**
    Runs on viewWillDisappear received. Aborts the account creation process
    and shows progress indicator.
    Notifies LifeCycleObserver by calling super class viewWillDisappear.
    - parameter animated: Describes whether the view will be animated.
    */
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.accountCreator?.abort()
    }

    /**
    Is sent after a web view starts loading a frame.
    Hides progress indicator, checks the status of new payment account creation process.
    - parameter webView: The web view that has begun loading a new frame.
    */
    open func webViewDidFinishLoad(_ webView: UIWebView) {
        hideProgress()
        self.accountCreator?.checkStatus()
    }

    /**
    Handles error if occured.
    * Must be overriden.
    - parameter response: The Respoonce object containing error.
    - parameter error: The NSError object containing error.
    */
    open func handleError(_ response: Response) -> Bool {
        preconditionFailure("Method cellIdentifier must be overridden")
    }

    /**
    Shows progress indicator.
    * Must be overriden.
    - parameter view: The view the progress indicator is attached to.
    */
    open func showProgress(_ view: UIView) {
        preconditionFailure("Method showProgress must be overridden")
    }

    /**
    Hides progress indicator.
    * Must be overriden.
    */
    open func hideProgress() {
        preconditionFailure("Method hideProgress must be overridden")
    }
}
