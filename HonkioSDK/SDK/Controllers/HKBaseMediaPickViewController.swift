//
//  HKBaseMediaPickViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

public protocol HKMediaPickViewControllerDelegate: class {
    
    func imageDidPicked(_ viewController: HKBaseMediaPickViewController, _ image: UIImage)
    
    func mediaDidPicked(_ viewController: HKBaseMediaPickViewController, _ type: String, _ url: NSURL)
    
}

open class HKBaseMediaPickViewController: HKBaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    public var mediaType: String = HK_MEDIA_FILE_TYPE_IMAGE
    public var pickDelegate: HKMediaPickViewControllerDelegate?
    
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var previewVideo: UIView!
    
    @IBAction public func fromAlbumAction(_ sender: AnyObject) {
        self.pickAction(.photoLibrary)
    }
    
    @IBAction public func fromCameraAction(_ sender: AnyObject) {
        self.pickAction(.camera)
    }
    
    open func pickAction(_ sourceType: UIImagePickerControllerSourceType) {
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let viewController = UIImagePickerController()
            viewController.delegate = self
            viewController.sourceType = sourceType
            
            switch self.mediaType {
            case HK_MEDIA_FILE_TYPE_IMAGE:
                viewController.allowsEditing = true
                viewController.mediaTypes = [kUTTypeImage as String]
                break
            case HK_MEDIA_FILE_TYPE_VIDEO:
                viewController.allowsEditing = false
                viewController.mediaTypes = [kUTTypeMovie as String]
                break
            default:
                preconditionFailure("Unknown media type: " + self.mediaType)
            }
            
            self.present(viewController, animated: true, completion: nil)
            
        } else {
            switch sourceType {
            case .camera:
                // TODO translate strings
                CustomAlertView(okTitle: "Error!".localized, okMessage: "Camera Unavailable".localized, okAction: nil).show()
                break
            case .photoLibrary:
                // TODO translate strings
                CustomAlertView(okTitle: "Error!".localized, okMessage: "Unable to find a photo album on your device".localized, okAction: nil).show()
                break
            default:
                preconditionFailure("Provided source type not supported")
                break
            }
        }
    }
    
    @objc open func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        
        if let _ = info[UIImagePickerControllerMediaType] as? String {
            if let image = info[UIImagePickerControllerEditedImage] as? UIImage {
                self.previewImage.image = image
                self.imageDidPicked(image)
            }
            else if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
                self.previewImage.image = image
                self.imageDidPicked(image)
            }
            else {
                self.previewImage.isHidden = true
                self.previewVideo.isHidden = false
                let url = info[UIImagePickerControllerMediaURL] as! URL
                let player = AVPlayer(url: url)
                
                let playerLayer = AVPlayerLayer(player: player)
                playerLayer.frame = self.previewVideo.bounds
                self.previewVideo.layer.addSublayer(playerLayer)
                player.play()
                self.mediaDidPicked(self.mediaType, url as NSURL)
            }
        }
    }
    
    open func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    open func imageDidPicked(_ image: UIImage) {
        // Delegate must be not nil. Let it crush if nil.
        self.pickDelegate!.imageDidPicked(self, image)
    }
    
    open func mediaDidPicked(_ type: String, _ url: NSURL) {
        // Delegate must be not nil. Let it crush if nil.
        self.pickDelegate!.mediaDidPicked(self, type, url)
    }
}
