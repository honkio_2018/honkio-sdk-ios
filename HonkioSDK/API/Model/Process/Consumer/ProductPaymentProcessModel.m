//
//  ProductPaymentProcessModel.m
//  HonkioApi
//
//  Created by Shurygin Denis on 11/27/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "ProductPaymentProcessModel.h"
#import "HonkioApi.h"

@implementation ProductPayParams

-(ProductPaymentProcessModel*) instantiateProcessModel {
    return [[ProductPaymentProcessModel alloc] init];
}

-(void)setProducts:(NSArray *)products {
    _products = products;
    
    if ([products count] > 0) {
        NSMutableSet* disallowedAccounts = [NSMutableSet new];
        for (BookedProduct* bookedProduct in self.products) {
            if (bookedProduct.product.disallowedAccounts)
                [disallowedAccounts unionSet: bookedProduct.product.disallowedAccounts];
        }
        self.disallowedAccounts = [[NSSet alloc] initWithSet:disallowedAccounts];
    }
    else {
        self.disallowedAccounts = nil;
    }
}

-(double)getAmount {
    if(self.products) {
        double amount = 0;
        for (BookedProduct* bookedProduct in self.products) {
            amount += bookedProduct.getPrice;
        }
        return amount;
    }
    return 0;
}

-(void)setAmount:(double)amount {
    [NSException raise:@"Unsupported selector" format:@"This selector is not supported in class ProductPayParams. Please, use products list to manage payment."];
}

-(NSString*)getCurrency {
    if(self.products && self.products.count > 0) {
        BookedProduct* bookedProduct = self.products[0];
        return [bookedProduct.product currency];
    }
    return nil;
}

-(void)setCurrency:(NSString *)currency {
    [NSException raise:@"Unsupported selector" format:@"This selector is not supported in class ProductPayParams. Please, use products list to manage payment."];
}

@end

@implementation ProductPaymentProcessModel

-(void)pay:(Shop*)shop orderId:(NSString*)orderId account:(UserAccount*)account products:(NSArray*)products flags:(int)flags {
    ProductPayParams* payParams = [[ProductPayParams alloc] initWithShop:shop oirderId:orderId account:account flags:flags];
    payParams.products = products;
    [self pay:payParams];
}

-(void)pay:(UserAccount*)account products:(NSArray*)products {
    [self pay:account products:products flags:0];
}

-(void)pay:(UserAccount*)account products:(NSArray*)products flags:(int)flags {
    [self pay:nil orderId:nil account:account products:products flags:flags];
}

-(void)doPayment:(PayParams*)payParams handler:(ResponseHandler)handler {
    ProductPayParams* params = (ProductPayParams*) payParams;
    [HonkioApi paymentRequest:[params products]
                      account:[payParams account]
                         shop:[payParams shop]
                        order:[params orderId]
                       params:nil
                        flags:[payParams flags]
                      handler:handler];
}

@end
