//
//  ApiShop.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Identity.h"
#import "MerchantSimple.h"
#import "Tag.h"
#import "Rate.h"

/** Known types of the shops. */
#define SHOP_TYPE_PRODUCT       @"product"
#define SHOP_TYPE_PAYMENT       @"payment"
#define SHOP_TYPE_LOGIN         @"login"
#define SHOP_TYPE_REGISTRATIONT @"registration"
#define SHOP_TYPE_LOSTPASSWORD  @"lostpassword"

/** Known service types of the shops. */
#define SHOP_SERVICE_TYPE_GAS_STATION   @"gas_station"
#define SHOP_SERVICE_TYPE_CAR_WASH      @"car_wash"
#define SHOP_SERVICE_TYPE_VENDING       @"vending"
#define SHOP_SERVICE_TYPE_GATE_PARKING  @"gate_parking"
#define SHOP_SERVICE_TYPE_OPEN_PARKING  @"open_parking"
#define SHOP_SERVICE_TYPE_ACCESS_FREE   @"access_fee"
#define SHOP_SERVICE_TYPE_E_COMMERCE    @"ecommerce"

/**
 * Data object that holds information about shop.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface Shop : Identity

/** Shop name. */
@property (nonatomic) NSString* name;

/** Shop description. */
@property (nonatomic, readwrite) NSString* description;

/** Coordinate latitude of real position. */
@property (nonatomic, assign) double latitude;

/** Coordinate longitude of real position. */
@property (nonatomic, assign) double longitude;

//TODO docs
@property (nonatomic, assign) double maxDistance;

/** Address string. */
@property (nonatomic) NSString* address;

/** Public SSH key. The Key can be used to check transaction or product signatures. */
@property (nonatomic) NSString* publicKey;

/** Shop type. */
@property (nonatomic) NSString* type;

/** Shop service type. */
@property (nonatomic) NSString* serviceType;

//TODO write docs
@property (nonatomic) NSArray<Rate*>* ratings;

/**
 * Small logo URL or file name in server assets. To build URL for server assets use [ServerInfo assetsUrl].<br>
 * The Small logo should be used in the shop lists.
 */
@property (nonatomic) NSString* logoSmall;

/**
 * Large logo URL or file name in server assets. To build URL for server assets use [ServerInfo assetsUrl].<br>
 * The Large logo should be used in the shop detail pages.
 */
@property (nonatomic) NSString* logoLarge;

/** Merchant for the Shop. */
@property (nonatomic, weak) MerchantSimple* merchant;

/** Shop tags. */
@property (nonatomic) NSArray<Tag*>* tags;

//TODO write docs
@property (nonatomic) NSDate* closingTime;

//TODO write docs
@property (nonatomic, assign) long closingOffset;

//TODO write docs
@property (nonatomic, assign) double preAuthorisationAmount;

-(BOOL)checkDistanceToLat:(double)latitude lon:(double)longitude;
-(BOOL)checkDistanceToLocation:(CLLocation*)location;

-(BOOL)isHasTagWithId:(NSString*)tagId;
-(BOOL)isHasTagWithName:(NSString*)tagName;

@end
