//
//  SimpleListItem.m
//  HonkioSDK
//
//  Created by Mikhail Li on 03/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "SimpleListItem.h"

@implementation SimpleListItem

@synthesize itemId, string, name;

/**
 * Creates new Item with specified params.
 * @param itemId ID of the Item
 * @param string String value of the Item
 * @param name Name of the Item
 */
-(instancetype)initWithId:(NSString *)itemId string:(NSString *)string name:(NSString *)name {
    self = [super init];
    if (self) {
        self.itemId = itemId;
        self.string = string;
        self.name = name;
    }
    return self;
}

-(NSString *)description {
    return !self.name ? name : self.string;
}

@end
