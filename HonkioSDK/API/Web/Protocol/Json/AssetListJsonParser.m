//
//  AssetListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/1/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "AssetListJsonParser.h"
#import "AssetJsonParser.h"
#import "HKAsset.h"
#import "HKAssetList.h"

@implementation AssetListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    HKAssetList* assets = [HKAssetList new];
    
    NSMutableArray* list = [NSMutableArray new];
    
    NSArray* assetsArray = dict[@"assets"];
    if (assetsArray) {
        for (NSDictionary* assetDict in assetsArray) {
            if([assetDict isKindOfClass:[NSDictionary class]]) {
                [list addObject:[AssetJsonParser parseAsset:assetDict]];
            }
        }
    }
    
    assets.list = list;
    
    return assets;
}

@end
