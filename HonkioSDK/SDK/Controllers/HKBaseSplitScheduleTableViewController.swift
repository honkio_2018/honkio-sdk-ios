//
//  HKBaseSplitScheduleTableViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/8/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

open class HKBaseSplitScheduleTableViewController: HKBaseLazyTableViewController {
    
    open var shopIdentity : Identity?
    public fileprivate(set) var filter : SplitScheduleFilter = SplitScheduleFilter()
    
    /**
     Loads list of shedule products from server.
     - parameter responseHandler: The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
     - parameter count: The number of products to be loaded from server.
     - parameter skip: The number of products to be skipped on loading.
     */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(filter, count: count, skip: skip)
        
        // FIXME Shedule list currently not support paging
        if skip > 0 {
            responseHandler(Response(result: Schedule()))
        }
        else {
            HonkioApi.getSplitSchedule(self.filter, shop: self.shopIdentity, flags: 0, handler: responseHandler)
        }
    }
    
    /**
     Creates the list from the result of the Response
     - Returns: Loaded items in response converted to List.
     - parameter response: The Response object to be converted to List.
     */
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! Schedule).list
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! Event), listItem, itemBinder))
        }
        return items
    }
    
    /**
     Gets the cell indetifier for provided Event.
     * Must be overriden.
     - parameter item: Event of Schedule.
     - Returns: The identifier of cell.
     */
    open func cellIdentifier(_ item: Event) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }
    
    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }
    
    /**
     Sets the filter params for the current filter.
     - parameter filter: The filter to be updated.
     - parameter count: The number of events to be loaded from server.
     - parameter skip: The number of events to be skipped on loading.
     */
    open func setupFilter(_ filter: SplitScheduleFilter, count: Int, skip: Int) {
        // FIXME Shedule list currently not support paging
        // Do nothing
    }
}
