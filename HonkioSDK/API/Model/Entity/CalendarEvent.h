//
//  CalendarEvent.h
//  HonkioSDK
//
//  Created by Mikhail Li on 26/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event.h"
#import "Product.h"

#define EVENT_REPEATING_ONCE          @"once"
#define EVENT_REPEATING_DAILY         @"daily"
#define EVENT_REPEATING_WEEKLY        @"weekly"
#define EVENT_REPEATING_MONTHLY       @"monthly"

@interface CalendarEvent : Event

@property (nonatomic, copy) NSString *eventId;
@property (nonatomic) NSDate *initialStart;
@property (nonatomic) NSDate *initialEnd;
@property (nonatomic, copy) NSString *repeating;
@property (nonatomic) NSArray<Product *> *products;

- (instancetype)initWithEvent:(CalendarEvent *)event;

-(NSDictionary *)toParams;

-(NSString *)description;

@end
