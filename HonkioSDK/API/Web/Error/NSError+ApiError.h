//
//  NSError+ApiError.h
//  Honkio API
//
//  Created by Alexandr Kolganov on 4/15/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
#import "ApiError.h"

@interface NSError (ApiError)

FOUNDATION_EXPORT NSString* const ApiErrorDomain;
FOUNDATION_EXPORT NSString* const ApiErrorFields;

//-(BOOL)isHonkioApiError;
//-(BOOL)isHonkioApiError:(int)errorCode;
//
-(ApiError*)honkioApiError;

//+(NSError*)errorWithGroup:(int)group subItem:(int)subItem string:(NSString*)string;
//+(NSError*)errorWithCode:(int)code string:(NSString*)string;
//+(NSError*)errorWithError:(ApiError*)apiError string:(NSString*)string;
//+(NSError*)errorWithString:(NSString*)string;
//
//+(NSError*)errorFromResponse:(Response*)response error:(NSError*)error;
@end
