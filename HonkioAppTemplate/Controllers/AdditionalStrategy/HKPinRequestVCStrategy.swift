//
//  HKPinRequestVCStrategy.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 11/14/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit
import LocalAuthentication

open class HKPinRequestVCStrategy: HKViewControllerLifeCycleObserver {

    private static let BiometricAuth = "HKPinRequestVCStrategy_BimetricAuth"
    
    open var delegate : HKPinRequestVCStrategyDelegate?
    
    private var isBiometricAuthOn = false
    
    open func requestPin(_ reason: Int, message: String) {
        let savedPin = StoreData.load(fromStore: STORE_KEY_PIN) as? String
        let biometricAuth = (StoreData.load(fromStore: HKPinRequestVCStrategy.BiometricAuth) != nil)
        
        if reason != PIN_REASON_NEW && savedPin != nil && biometricAuth {
            let context = LAContext()
            var error: NSError?

            if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                context.localizedFallbackTitle = "dlg_biometric_auth_btn_fallback".localized
                context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: message, reply: {[unowned self] (success, evalPolicyError) in
                    DispatchQueue.main.async { [unowned self] in
                        if success {
                            self.pinEntered(savedPin, reason: reason)
                        }
                        else {
                            switch (evalPolicyError! as NSError).code {
                            case LAError.userCancel.rawValue, LAError.systemCancel.rawValue:
                                self.pinCanceled(reason)
                                break
                            case LAError.userFallback.rawValue:
                                self.openPinScreen(reason, message: message)
                                break
                            case LAError.authenticationFailed.rawValue:
                                CustomAlertView(okTitle: "dlg_biometric_auth_reject_title".localized, okMessage: "dlg_biometric_auth_reject_message".localized, okAction: {
                                    [unowned self] in
                                    self.openPinScreen(reason, message: message)
                                }).show()
                                break
                            default:
                                CustomAlertView(okTitle: "dlg_biometric_auth_error_title".localized, okMessage: "dlg_biometric_auth_error_message".localized, okAction: {
                                    [unowned self] in
                                    self.openPinScreen(reason, message: message)
                                }).show()
                                break
                            }
                        }
                    }
                })
                
                return
            }
        }
        
        self.openPinScreen(reason, message: message)
    }
    
    override open func pinEntered(_ pinCode: String!, reason: Int!) {
        if reason == PIN_REASON_NEW {
            StoreData.delete(fromStore: HKPinRequestVCStrategy.BiometricAuth)
            
            let context = LAContext()
            var error: NSError?
            
            if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &error) && isBiometricAuthOn {
                // Currently only TouchID allowed.
                if #available(iOS 11.0, *), context.biometryType == .touchID {
                    context.localizedFallbackTitle = ""
                    context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "dlg_biometric_auth_message".localized, reply: {[unowned self] (success, evalPolicyError) in
                        DispatchQueue.main.async { [unowned self] in
                            if success {
                                StoreData.save(toStore: "active", forKey: HKPinRequestVCStrategy.BiometricAuth)
                                self.invokeDelegatePinEntered(pinCode, reason)
                            }
                            else {
                                switch (evalPolicyError! as NSError).code {
                                case LAError.userCancel.rawValue:
                                    self.invokeDelegatePinEntered(pinCode, reason)
                                    break
                                default:
                                    CustomAlertView(okTitle: "dlg_biometric_auth_setup_error_title".localized, okMessage: "dlg_biometric_auth_setup_error_message".localized, okAction: {
                                        [unowned self] in
                                        self.invokeDelegatePinEntered(pinCode, reason)
                                    }).show()
                                    break
                                }
                            }
                        }
                    })
                    return
                }
            }
        }
        
        self.invokeDelegatePinEntered(pinCode, reason)
    }

    override open func pinCanceled(_ reason: Int!) {
        self.invokeDelegatePinCanceled(reason)
    }
    
    private func openPinScreen(_ reason: Int, message: String) {
        if let pin = AppController.getViewControllerById(AppController.Pin, viewController: self.viewController) as? HKPinViewController {
            
            pin.reason = reason
            pin.messageString = message
            pin.delegate = self
            
            let naviController = UINavigationController(rootViewController: pin)
            self.viewController.present(naviController, animated: true, completion: nil)
        }
    }
    
    private func invokeDelegatePinEntered(_ pinCode: String!, _ reason: Int!) {
        self.delegate?.pinEntered(self, pinCode, reason)
    }
    
    private func invokeDelegatePinCanceled(_ reason: Int!) {
        self.delegate?.pinCanceled(self, reason)
    }
}
