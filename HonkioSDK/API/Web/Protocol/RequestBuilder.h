//
//  RequestBuilder.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

/**
 * The builder to build request string.
 */
@protocol RequestBuilder <NSObject>

/**
 * Reset builder. All entity values will cleared.
 */
-(void)reset;

/**
 * Put name value pair in to the builder entity.
 * @param key Key of value.
 * @param value Value.
 */
-(void)putKey:(NSString*)key value:(NSObject*) value;

/**
 * Calculate hash for the current values set and put it in to the builder entity as name value pair.
 * @param key Key for calculate hash.
 */
-(void)calculateHash:(NSString*)key;

/**
 * Build a new Array object specified for current protocol.
 * @param values Collection of the objects which represent entities specified for current protocol.
 * @return An object that represent values array.
 */
-(NSData*)newEntitiesArray:(NSArray*)values;

/**
 * Gets Builder entity specified for the current protocol.
 * @return Builder entity specified for the current protocol.
 */
-(NSData*)entity;

@end
