//
//  HKBaseOAuthLoginViewController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 3/21/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

// TODO: Add detailed description of the class.
/**
 The base ViewController that provides support for web OAuth authorization routine.
 */

open class HKBaseOAuthLoginViewController: HKBaseLoginViewController, UIWebViewDelegate {
    
    fileprivate let DEFAULT_REDIRECT_URL = "http%3A%2F%2Flocalhost%3A5000%2Fauthorized"
    fileprivate let DEFAULT_REDIRECT_URL_FOR_CHECK = "http://localhost:5000/authorized"
    
    @IBOutlet open var webView: UIWebView!
    @IBOutlet open var splashView: UIView!

    /**
     Subscribes to web view UI changes, setup the web view fit to page, disables the scroll
     and shows the splash screen until the web view is loaded.
    */
    override open func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.webView.delegate = self
        self.webView.scalesPageToFit = true
        
        showSplashPage()
    }

    /**
    Sets the contentInset for the webView when the viewWillAppear event is rised.
    - parameter animated: Describes whether the view will appear animated.
    */
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.webView.scrollView.contentInset = UIEdgeInsetsMake(18, 0, 0, 0)
    }

    /**
    Sent before a web view begins loading a frame.
    - If there is no way to authorize then calls normal login process routine.
    - parameter webView: The web view that is about to load a new frame.
    - parameter request: The content location.
    - parameter navigationType: Indicates the action (linkClicked, formSubmitted, backForward, reload, formResubmitted, other).
    - Returns: true if the web view should begin loading content; otherwise, false.
    */
    open func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let url = request.url!.absoluteString
        if (url.hasPrefix(DEFAULT_REDIRECT_URL_FOR_CHECK)) {
            
            var params : [String : String] = [:]
            for param in (request.url?.query?.components(separatedBy: "&"))! {
                let elts = param.components(separatedBy: "=");
                if elts.count < 2 {
                    continue
                }
                params[elts.first!] = elts.last
            }
            
            if let authorizationToken = params["code"] {
                weak var weakSelf = self
                let oauthIdentity = getOAuthIdentity()
                let accessTokenUrl = HonkioOAuth.accessTokenUrl(HonkioApi.device(), forClient: oauthIdentity.identityId, clientSecret: oauthIdentity.password, redirect: DEFAULT_REDIRECT_URL, token: authorizationToken)
                
                self.startWaitingOperation()
                SimpleAsyncGet(url: URL(string: accessTokenUrl!)!, heders: nil, responseHandler: { (response, data, error) -> Void in
                    if error != nil || data == nil {
                        weakSelf!.authorisationNotAllowed()
                    }
                    else {
                        do {
                            let dict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as? Dictionary<String, AnyObject>
                            if let token = dict?["access_token"] as? String {
                                weakSelf?.tokenGet(token)
                            }
                        } catch {
                            self.authorisationNotAllowed()
                        }
                    }
                })!.execute()
            }
            else {
                self.authorisationNotAllowed()
            }
            return false
        }
        return true
    }

    /**
     Is sent after a web view stops loading a frame.
    - May be used for hiding progress indicators.
    - parameter webView: The web view that has begun loading a new frame.
    */
    open func webViewDidFinishLoad(_ webView: UIWebView) {
        self.stopWaitOperation()
    }

    /**
    Sent if a web view failed to load a frame.
    - parameter webView: The web view that failed to load a frame.
    - parameter error: The error that occurred during loading.
    */
    open func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        print("%@", error)
        if (error as NSError).code == -1008 { // Dirty fix. Some times web page doesn't load. Lets try agane if error code = -1008 (resource unavailable).
            self.showLoginPage()
        }
    }

    /**
    Gets the application identity from the server.
    - Returns: The Identity object containing identification credentials.
    */
    open func getOAuthIdentity() -> Identity {
        return HonkioApi.appInfo()!.oauthIdentity!
    }

    /**
    Calls when authorisation not allowed. Shows login page.
    */
    open func authorisationNotAllowed() {
        self.showLoginPage()
    }

    /**
    Calls when OAuth token gets from the server. Starts login to server using the provided OAuth token.
    - parameter token: The token used for authorization.
    */
    open func tokenGet(_ token: String) {
        self.doLoginForOAuthToken(token)
    }

    /**
    Hides splash screen and starts load login page.
    */
    open func showLoginPage() {
        if self.splashView != nil {
            self.splashView!.isHidden = true
        }
        self.webView.isHidden = false
        
        let storage = HTTPCookieStorage.shared
        if let cookies = storage.cookies {
            for cookie in cookies {
                storage.deleteCookie(cookie)
            }
            UserDefaults.standard.synchronize()
        }
        
        let authorisationUrl = HonkioOAuth.authorisationUrl(HonkioApi.device(), forClient: getOAuthIdentity().identityId, redirect: DEFAULT_REDIRECT_URL)
        HKLog.d("OAuth url = \(authorisationUrl)")
        self.webView.loadRequest(URLRequest(url: URL(string: authorisationUrl!)!))
    }

    /** Shows the splash screen. */
    open func showSplashPage() {
        if self.splashView != nil {
            self.splashView!.isHidden = false
        }
        self.webView.isHidden = true
    }
    
}
