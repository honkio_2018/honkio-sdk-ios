//
//  HKAssetViewProtocol.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 2/20/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

public protocol HKAssetViewProtocol: class {

    var asset: HKAsset? { get set }
    
    func loadAsset(_ assetId: String)
    
}
