//
//  HKSystemMessagePushEntity.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/12/19.
//  Copyright © 2019 developer. All rights reserved.
//

#import "HKSystemMessagePushEntity.h"
#import "Push.h"

@implementation HKSystemMessagePushEntity

-(id)initWithContent:(NSDictionary*)content {
    return [super initWithType:PUSH_TYPE_SYSYEM andContent:content];
}

- (NSString*)subject {
    return [self getContent:@"subject"];
}

- (void)setSubject:(NSString*)value {
    [self setContent:@"subject" value:value];
}

- (NSString*)message {
    return [self getContent:@"message"];
}

- (void)setMessage:(NSString*)value {
    [self setContent:@"message" value:value];
}

@end
