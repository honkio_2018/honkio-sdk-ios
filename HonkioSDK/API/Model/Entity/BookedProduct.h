//
//  BookedProduct.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/25/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "Product.h"

/**
 * BookedProduct used for purchasing. Is a data object which contains information about product for purchase.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface BookedProduct : NSObject

// TODO write docs
-(id)init;

// TODO write docs
-(id)initFrom:(BookedProduct*) bookedProduct;

/** Booked product. */
@property (nonatomic) Product* product;

/** Count of the products. */
@property (nonatomic) int count;

/**
 * @param product Product for booking.
 * @param count Count of the products.
 */
-(id)initWithProduct:(Product*)product andCount:(int)count;

/**
 * @param productId Product id for booking.
 * @param count Count of the products.
 */
-(id)initWithProductId:(NSString*)productId andCount:(int)count;

/**
 * Calculate price of the product for specified account type. To original price
 * will be added surplus for selected account.
 *
 * @param accountType Account that will used for surplus calculation.
 * @return Total price for this product.
 */
-(double)getPrice:(NSString*)accountType;

/**
 * Calculate price of the product.
 *
 * @return Total price for this product.
 */
-(double)getPrice;

@end
