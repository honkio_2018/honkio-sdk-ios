//
//  HKUserRoleStructureListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKUserRoleStructureListJsonParser.h"
#import "HKUserRoleStructureJsonParser.h"
#import "HKUserRoleStructureList.h"

@implementation HKUserRoleStructureListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    NSMutableArray<HKUserRoleStructure*>* list = [NSMutableArray new];
    
    NSArray* structuresArray = dict[@"descriptions"];
    if (structuresArray) {
        for (NSDictionary* structureDict in structuresArray) {
            HKUserRoleStructure* structure = [HKUserRoleStructureJsonParser toStructure:structureDict];
            if (structure)
                [list addObject:structure];
        }
    }
    
    HKUserRoleStructureList* result = [HKUserRoleStructureList new];
    result.list = list;
    
    return result;
}


@end
