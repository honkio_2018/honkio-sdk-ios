//
//  HKBaseOrderChatViewController.swift
//  HonkioSDK
//
//  Created by Dev on 01.09.16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

/**
The base ViewController for loading and manage chat messages.
*/

open class HKBaseOrderChatViewController: HKBaseLazyTableViewController {
    
    open var order : Order! {
        didSet {
            if self.filter == nil {
                self.filter = ChatMessageFilter()
            }
            self.filter?.orderId = self.order.orderId
        }
    }
    
    fileprivate var filter : ChatMessageFilter?
    
    open override func viewDidLoad() {
        self.refreshEnabled = false
        self.refreshWhenAppear = false
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseOrderChatViewController.pushReceived(_:)), name: NSNotification.Name(rawValue: NOTIFY_NAME_ON_PUSH_RECEIVED), object: nil)
    }
    
    open override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFY_NAME_ON_PUSH_RECEIVED), object: nil)
        }
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.order != nil {
            HonkioApi.userSetPush(true, filter: PushFilter()
                .addContent(PUSH_FILTER_QUERY_CONTENT_ORDER, value: self.order.orderId)
                .addContent(PUSH_FILTER_QUERY_CONTENT_TYPE, value: PUSH_TYPE_ORDER_CHAT_MESSAGE), flags: 0, handler: nil)
            HKBaseAppDelegate.addPushToIgnore(PUSH_TYPE_ORDER_CHAT_MESSAGE, value1: self.order.orderId, value2: nil)
        }
        refresh()
    }
    
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.order != nil {
            HKBaseAppDelegate.removePushFromIgnore(PUSH_TYPE_ORDER_CHAT_MESSAGE, value1: self.order.orderId, value2: nil)
        }
    }
    
    @objc open func pushReceived(_ notification : Notification) {
        if notification.name.rawValue == NOTIFY_NAME_ON_PUSH_RECEIVED && (notification as NSNotification).userInfo != nil {
            if let push = (notification as NSNotification).userInfo![NOTIFY_EXTRA_PUSH] as? Push {
                if push.type == PUSH_TYPE_ORDER_CHAT_MESSAGE {
                    if let chatPush = push.entity as? OrderChatMessagePushEntity {
                        if chatPush.orderId == self.filter?.orderId {
                            let message = ChatMessage()
                            message?.senderId = chatPush.senderId
                            message?.senderRole = chatPush.senderRole
                            message?.senderFirstName = chatPush.senderFirstName
                            message?.senderLastName = chatPush.senderLastName
                            message?.receiverId = chatPush.receiverId
                            message?.receiverRole = chatPush.receiverRole
                            message?.orderId = chatPush.orderId
                            message?.orderThirdPerson = chatPush.orderThirdPerson
                            message?.shopName = chatPush.shopName
                            message?.shopId = chatPush.shopId
                            message?.text = chatPush.text
                            message?.time = chatPush.time
                            
                            self.addMessageToList(message!)
                        }
                    }
                }
            }
        }
    }
    
    open func sendMessage(_ message: ChatMessage) {
        message.time = Date()
        self.addMessageToList(message)
        
        weak var weakSelf = self;
        HonkioApi.userSend(message, flags: 0) { (response) in
            if let strongSelf = weakSelf {
                if response.isStatusAccept() {
                    // Handle message sending
                }
                else {
                    let _ = AppErrors.handleError(strongSelf, response: response)
                }
            }
        }
    }
    
    open override func loadFinished(_ queryCount: Int, querySkip: Int) {
        self.tableView.reloadData()
        if querySkip == 0 && queryCount > 0{
            self.tableView.scrollToRow(at: self.lasyIndexPath(queryCount - 1), at: .top, animated: false)
        }
        else if querySkip > 0 {
            self.tableView.scrollToRow(at: self.lasyIndexPath(queryCount), at: .top, animated: false)
        }
        
        super.loadFinished(queryCount, querySkip: querySkip)
    }
    
    open override func loadFinished() {
        // Do nothing
    }
    
    open override func isShouldBeUpdated(_ indexPathWilDisplay: IndexPath) -> (Bool) {
        let section = itemsSource[(indexPathWilDisplay as IndexPath).section]
        return isLasySection(section) && ((indexPathWilDisplay as IndexPath).row <= 0) && !isUpdating && isCanBeUpdated
    }
    
    
    /**
    Loads the list of ChatMessages.
     - parameter responseHandler: The handler to be used for managing the response.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
    */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        if self.filter == nil {
            self.filter = ChatMessageFilter()
//            self.filter?.orderId = self.order.orderId
        }
        self.setupFilter(filter!, count: count, skip: skip)
        
        HonkioApi.userGetChatMessages(filter, flags: 0 , handler: responseHandler)
    }

    /**
    Creates a list of ChatMessages from the Responce object.
    - parameter response: The Response object to be converted.
    - Returns: The lost of ChatMessages.
    */
    open override func toList(_ response : Response) -> [Any?] {
        return ((response.result as! ChatMessages).list?.reversed().map({Optional($0)}))!
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! ChatMessage), listItem, itemBinder))
        }
        return items
    }
    
    open override func mergeList(_ list1: [Any?], list2: [Any?]?) -> ([Any?]?) {
        if list2 != nil {
            return super.mergeList(list2!, list2: list1)
        }
        return list1
    }

    /**
    Gets the cell indetifier for provided ChatMessage item.
    * Must be overriden.
    - parameter item: Chat message.
    - Returns: The identifier of cell.
    */
    open func cellIdentifier(_ item: ChatMessage) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }

    /**
     Gets cell bunder that define how cell should be filled from list item.
    * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
    */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }

    /**
    Sets up the filter.
     - parameter filter: The ChatMessageFilter object to be changed.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
    */
    open func setupFilter(_ filter: ChatMessageFilter, count: Int, skip: Int) {
        
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
    
    fileprivate func addMessageToList(_ message: ChatMessage) {
        self.lazySection.items.append((cellIdentifier(message), message, buildCellBunder()))
        self.tableView.reloadData()
        
//        let newCell = self.tableView(self.tableView, cellForRowAt: self.lasyIndexPath(self.lazySection.items.count - 1))
//        newCell.layoutIfNeeded()
//        let verticalContentOffset = self.tableView.contentOffset.y + newCell.frame.height
//        self.tableView.setContentOffset(CGPoint(x: 0, y: verticalContentOffset), animated: true)
        
        self.tableView.scrollToRow(at: self.lasyIndexPath(self.lazySection.items.count - 1), at: .bottom, animated: false)
    }
}
