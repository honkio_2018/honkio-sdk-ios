//
//  PushParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 2/12/15.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import "PushParser.h"
#import "ApiDateFormatter.h"

@implementation PushParser


+(Push*)parse:(NSDictionary*)dict {
    Push* push = [Push new];
    
    NSString* type = dict[@"type"];
    if (type) {
        if ([type isEqualToString:PUSH_TYPE_SYSYEM]) {
            push.entity = [[HKSystemMessagePushEntity alloc] initWithContent:dict];
        }
        if ([type isEqualToString:PUSH_TYPE_TRANSACTION]) {
            push.entity = [PushParser parseTransaction:dict];
        }
        else if([type isEqualToString:PUSH_TYPE_ORDER_STATUS_CHANGED]) {
            push.entity = [PushParser parseOrderStatusChanged:dict];
        }
        else if([type isEqualToString:PUSH_TYPE_ORDER_CHAT_MESSAGE]) {
            push.entity = [PushParser parseOrderChatMessage:dict];
        }
        else if([type isEqualToString:PUSH_TYPE_ORDER_PROPOSAL]) {
            push.entity = [PushParser parseProposal:dict];
        }
        else if([type isEqualToString:PUSH_TYPE_ROLE_CHANGED]) {
            push.entity = [PushParser parseRoleAssigned:dict];
        }
        else if([type isEqualToString:PUSH_TYPE_ASSET_CHANGED]) {
            push.entity = [PushParser parseAssetChanged:dict];
        }
    }
    
    push.body = dict;
    push.date = [NSDate date];
    [PushParser parsePush:push dict:dict];
    
    return push;
}

+(void)parsePush:(Push*)push dict:(NSDictionary*)dict {
    push.message = dict[@"message"];
    if (!push.message) {
        NSDictionary* aps = dict[@"aps"];
        if (aps) {
            push.message = aps[@"alert"];
        }
    }
}

+(TransactionPushEntity*)parseTransaction:(NSDictionary*)dict {
    TransactionPushEntity* pushEntity = [[TransactionPushEntity alloc] initWithContent:dict];
    
    pushEntity.type = dict[@"type"];
    pushEntity.shopId = dict[@"shop"];
    pushEntity.transactionId = dict[@"id"];
    pushEntity.status = dict[@"status"];
    
    return pushEntity;
}

+(OrderStatusChangedPushEntity*)parseOrderStatusChanged:(NSDictionary*)dict {
    OrderStatusChangedPushEntity* pushEntity = [[OrderStatusChangedPushEntity alloc] initWithContent:dict];
    
    pushEntity.orderId = dict[@"order_id"];
    pushEntity.orderTitle = dict[@"order_title"];
    pushEntity.orderStatus = dict[@"order_status"];
    pushEntity.orderOwnerId = dict[@"order_owner_id"];
    pushEntity.userId = dict[@"user_id"];
    pushEntity.userName = dict[@"user_name"];
    
    return pushEntity;
    
}

+(OrderChatMessagePushEntity*)parseOrderChatMessage:(NSDictionary*)dict {
    OrderChatMessagePushEntity* pushEntity = [[OrderChatMessagePushEntity alloc] initWithContent:dict];
    
    pushEntity.senderId = [PushParser notNull:dict[@"sender_id"]];
    pushEntity.senderRole = [PushParser notNull:dict[@"sender_role_id"]];
    pushEntity.senderFirstName = [PushParser notNull:dict[@"sender_first_name"]];
    pushEntity.senderLastName = [PushParser notNull:dict[@"sender_last_name"]];
    pushEntity.receiverId = [PushParser notNull:dict[@"receiver_id"]];
    pushEntity.receiverRole = [PushParser notNull:dict[@"receiver_role_id"]];
    pushEntity.orderId = [PushParser notNull:dict[@"order_id"]];
    pushEntity.orderThirdPerson = [PushParser notNull:dict[@"third_person"]];
    pushEntity.text = [PushParser notNull:dict[@"text"]];
    pushEntity.shopName = [PushParser notNull:dict[@"shop_name"]];
    pushEntity.shopId = [PushParser notNull:dict[@"shop_id"]];
    pushEntity.assetId = [PushParser notNull:dict[@"asset_id"]];
    pushEntity.assetName = [PushParser notNull:dict[@"asset_name"]];
    pushEntity.time = [ApiDateFormatter dateFromServerString:dict[@"timestamp"]];
    
    return pushEntity;
}

+(ProposalPushEntity*)parseProposal:(NSDictionary*)dict {
    ProposalPushEntity* pushEntity = [[ProposalPushEntity alloc] initWithContent:dict];
    
    pushEntity.orderId = dict[@"order_id"];
    pushEntity.orderTitle = dict[@"order_title"];
    pushEntity.userId = dict[@"user_id"];
    pushEntity.userName = dict[@"user_name"];
    pushEntity.proposalDesc = dict[@"proposal_time"];
    
    return pushEntity;
}

+(BasePushEntity*)parseRoleAssigned:(NSDictionary*)dict {
    return [[RoleChangedPushEntity alloc] initWithContent:dict];
}

+(BasePushEntity*)parseAssetChanged:(NSDictionary*)dict {
    return [[AssetChangedPushEntity alloc] initWithContent:dict];
}
    
+(id)notNull:(id)object {
    if ([object isEqual:[NSNull null]]) {
        return nil;
    }
    return object;
}

@end
