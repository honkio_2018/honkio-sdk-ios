//
//  HKBaseMerchantProductListTableViewController.swift
//  HonkioSDK
//
//  Created by Mikhail Li on 21/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

import Foundation

/**
 The base ViewController for working with merchant products.
 Loads merchant products from server.
 */

open class HKBaseMerchantProductListTableViewController: HKBaseLazyTableViewController {
    
    public var shopIdentity : Identity?
    public fileprivate(set) var filter: MerchantProductsFilter = MerchantProductsFilter()
    
    /**
     Loads list of merchant products from server.
     - parameter responseHandler: The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
     - parameter count: The number of products to be loaded from server.
     - parameter skip: The number of products to be skipped on loading.
     */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(filter, count: count, skip: skip)
        
        HonkioMerchantApi.merchantProductList(filter, shop:shopIdentity, flags:0, handler:responseHandler)
    }
    
    /**
     Creates the list from the result of the Response
     - Returns: Loaded items in response converted to List.
     - parameter response: The Response object to be converted to List.
     */
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! ShopProducts).list
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! Product), listItem, itemBinder))
        }
        return items
    }
    
    /**
     Gets the cell indetifier for provided Product.
     * Must be overriden.
     - parameter item: Shop product.
     - Returns: The identifier of cell.
     */
    open func cellIdentifier(_ item: Product) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }
    
    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }
    
    /**
     Sets the filter params for the current filter.
     - parameter filter: The filter to be updated.
     - parameter count: The number of merchant products to be loaded from server.
     - parameter skip: The number of merchant products to be skipped on loading.
     */
    open func setupFilter(_ filter: MerchantProductsFilter, count: Int, skip: Int) {
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
}
