//
//  UserAccessListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/15/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "UserAccessListJsonParser.h"
#import "UserAccessList.h"

@implementation UserAccessListJsonParser

-(id)parseDictionary:(Message *)message dict:(NSDictionary *)dict {
    UserAccessList *accessList = [UserAccessList new];
    
    NSArray *userAccessJson = dict[@"user_access"];
    if (userAccessJson) {
        NSMutableArray<UserAccessItem *> *list = [NSMutableArray arrayWithCapacity:userAccessJson.count];
        for(NSDictionary *accessJsonItem in userAccessJson) {
            
            NSMutableArray<NSString *> *accessArray = [NSMutableArray new];
            NSArray<NSString *> *accessJsonArray = accessJsonItem[@"access"];
            if (accessJsonArray) {
                for (NSString *access in accessJsonArray) {
                    [accessArray addObject:access];
                }
            }
            
            UserAccessItem *item = [UserAccessItem new];
            item.access = accessArray;
            item.merchant = accessJsonItem[@"merchant"];
            
            [list addObject:item];
        }
        accessList.list = list;
    }
    
    return accessList;
}

@end
