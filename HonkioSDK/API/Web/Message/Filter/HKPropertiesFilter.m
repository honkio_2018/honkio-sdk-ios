//
//  HKPropertiesFilter
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/26/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKPropertiesFilter.h"

@implementation HKPropertiesFilter {
    NSMutableDictionary<NSString*, id<HKPropertiesFilterItem>>* items;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        items = [NSMutableDictionary new];
    }
    return self;
}

-(void)addItem:(id<HKPropertiesFilterItem>)item forKey:(NSString*)key {
    [items setValue:item forKey:key];
}

-(void)remove:(NSString*)key {
    [items removeObjectForKey:key];
}

-(void)removeAll {
    [items removeAllObjects];
}

- (NSDictionary *)toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    for (NSString* key in items.allKeys) {
        id<HKPropertiesFilterItem> item = items[key];
        if (item) {
            [params setValue:[item toParams] forKey:key];
        }
    }
    return params;
}

@end

@implementation HKPropertiesFilterEqual

- (id)initWithValue:(id)value {
    self = [super init];
    if (self) {
        _value = value;
    }
    return self;
}

-(NSDictionary<NSString*, NSObject*>*)toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    [params setValue:self.value forKey:@"eq"];
    return params;
}

@end

@implementation HKPropertiesFilterNotEqual

-(NSDictionary<NSString*, NSObject*>*)toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    [params setValue:self.value forKey:@"ne"];
    return params;
}

@end

@implementation HKPropertiesFilterRange

-(id)initWithStart:(id)start end:(id)end {
    return [self initWithStart:start includeStart:YES end:end includeEnd:YES];
}
-(id)initWithStart:(id)start includeStart:(BOOL)includeStart end:(id)end includeEnd:(BOOL)includeEnd {
    self = [super init];
    if (self) {
        _start = start;
        _end = end;
        _includeStart = includeStart;
        _includeEnd = includeEnd;
    }
    return self;
}

-(NSDictionary<NSString*, NSObject*>*)toParams {
    if (!self.start && !self.end)
        return nil;
    
    NSMutableDictionary* params = [NSMutableDictionary new];
    if (self.start) {
        if (self.includeStart) {
            [params setValue:self.start forKey:@"gte"];
        }
        else {
            [params setValue:self.start forKey:@"gt"];
        }
    }
    
    if (self.end) {
        if (self.includeEnd) {
            [params setValue:self.end forKey:@"lte"];
        }
        else {
            [params setValue:self.end forKey:@"lt"];
        }
    }
    
    return params;
}

@end

@implementation HKPropertiesFilterInList

-(id)initWithList:(NSArray<id>*)list {
    self = [super init];
    if (self) {
        _list = list;
    }
    return self;
}

-(id)initWithItems:(id)items, ... {
    self = [super init];
    if (self) {
        NSMutableArray<id>* list = [NSMutableArray new];
        
        [list addObject:items];
        
        va_list args;
        va_start(args, items);
        
        id value;
        value = va_arg(args, id);
        while (value) {
            [list addObject:value];
            value = va_arg(args, id);
        }
        
        va_end(args);
        
        _list = list;
    }
    return self;
}

-(NSDictionary<NSString*, NSObject*>*)toParams {
    if (!self.list || [self.list count] == 0)
        return nil;
    
    NSMutableDictionary* params = [NSMutableDictionary new];
    [params setValue:self.list forKey:@"in"];
    return params;
}

@end

@implementation HKPropertiesFilterAllInList

-(NSDictionary<NSString*, NSObject*>*)toParams {
    if (!self.list || [self.list count] == 0)
        return nil;
    
    NSMutableDictionary* params = [NSMutableDictionary new];
    [params setValue:self.list forKey:@"all"];
    return params;
}

@end

@implementation HKPropertiesFilterNotInList

-(NSDictionary<NSString*, NSObject*>*)toParams {
    if (!self.list || [self.list count] == 0)
        return nil;
    
    NSMutableDictionary* params = [NSMutableDictionary new];
    [params setValue:self.list forKey:@"nin"];
    return params;
}

@end
