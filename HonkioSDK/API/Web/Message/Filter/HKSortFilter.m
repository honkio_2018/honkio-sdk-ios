//
//  HKSortFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKSortFilter.h"

@implementation HKSortFilterItem

-(id) initWith:(NSString*)field order:(HKSortFilterOrder)order {
    self = [super init];
    if (self) {
        _field = field;
        _order = order;
    }
    return self;
}

@end

@implementation HKSortFilter

-(id) initWithItems:(NSArray<HKSortFilterItem*>*)items {
    self = [super init];
    if (self) {
        if ([items isKindOfClass: [NSMutableArray class]]) {
            _items = (NSMutableArray<HKSortFilterItem*>*) items;
        }
        else {
            _items = [[NSMutableArray alloc] initWithArray:items];
        }
    }
    return self;
}

-(NSArray*) toParams {
    if (self.items == nil || [self.items count] == 0) {
        return nil;
    }
    
    NSMutableArray* array = [NSMutableArray new];
    for (HKSortFilterItem* item in self.items) {
        if (item) {
            [array addObject:@{
               item.field : [NSNumber numberWithInteger: item.order == asc ? 1 : -1]
            }];
        }
    }
    
    return array;
}

@end
