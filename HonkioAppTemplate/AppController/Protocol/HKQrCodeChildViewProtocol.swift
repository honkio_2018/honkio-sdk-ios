//
//  HKQrCodeChildViewProtocol.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 3/2/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

public protocol HKQrCodeChildViewProtocol: class {
        
    var qrCode : HKQrCode? { get set }
        
}
