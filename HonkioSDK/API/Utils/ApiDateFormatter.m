//
//  ApiDateFormatter.m
//  HonkioApi
//
//  Created by Alexandr Kolganov on 4/22/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import "ApiDateFormatter.h"

//DateFormatters
//read timestamp from server
static NSDateFormatter* staticServerDateFormatter = nil;

//write utc time to barcode
static  NSDateFormatter* staticBarcodeDateFormatter = nil;

static NSDateFormatter* staticDateFormatter = nil;


@implementation ApiDateFormatter

+(NSDateFormatter*)serverDateFormatter
{
    if(!staticServerDateFormatter) {
        staticServerDateFormatter = [[NSDateFormatter alloc] init];
        [staticServerDateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        [staticServerDateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [staticServerDateFormatter setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];

    }
    return staticServerDateFormatter;
}

+(NSDateFormatter*)barcodeDateFormatter
{
    if(!staticBarcodeDateFormatter) {
        staticBarcodeDateFormatter = [[NSDateFormatter alloc] init];
        [staticBarcodeDateFormatter setDateFormat:@"yyyyMMddHHmmss"];
        [staticBarcodeDateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [staticBarcodeDateFormatter setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    }
    return staticBarcodeDateFormatter;
}

+(NSDateFormatter*)dateFormatter
{
    if(!staticDateFormatter) {
        staticDateFormatter = [[NSDateFormatter alloc] init];
        [staticDateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    }
    return staticDateFormatter;
}

+(NSDate*) dateFromServerString:(NSString*)str
{
    return [[ApiDateFormatter serverDateFormatter] dateFromString:str];
}

+(NSString*) serverStringFromDate:(NSDate*)date
{
    return [[ApiDateFormatter serverDateFormatter] stringFromDate:date];
}

+(NSString*) barcodeStringFromDate:(NSDate*)date
{
    return [[ApiDateFormatter barcodeDateFormatter] stringFromDate:date];
}

+(NSString*) stringFromDate:(NSDate*)date dateStyle:(NSDateFormatterStyle) dateStyle timeStyle:(NSDateFormatterStyle) timeStyle
{
    NSDateFormatter* formater = [ApiDateFormatter dateFormatter];
    [formater setDateStyle: dateStyle];
    [formater setTimeStyle: timeStyle];

    if (dateStyle == NSDateFormatterShortStyle) {
        if (timeStyle == NSDateFormatterNoStyle) {
            [formater setDateFormat:@"MM/dd/yyyy"];
        } else {
            [formater setDateFormat:@"MM/dd/yyyy h:mm a"];
        }
    }

    return [formater stringFromDate:date];
}

+(NSString *)stringFromDate:(NSDate*)date {
    return [[ApiDateFormatter serverDateFormatter] stringFromDate:date];
}

+(NSString*) secondsToHumanTime:(NSInteger)seconds
{
    NSMutableString* ret = [NSMutableString new];
    NSInteger days = seconds / (3600*24);
    NSInteger hours = (seconds/3600)%24;
    NSInteger mins = (seconds/60)%60;
    NSInteger secs = seconds % 60;
    
    if(days > 0)
        [ret appendString:[NSString stringWithFormat:@"%ldd ",(long)days]];
    if(hours > 0)
        [ret appendString:[NSString stringWithFormat:@"%ldh ",(long)hours]];
    if(mins > 0)
        [ret appendString:[NSString stringWithFormat:@"%2ldm ",(long)mins]];
    if(secs > 0)
        [ret appendString:[NSString stringWithFormat:@"%2lds ",(long)secs]];
    return [NSString stringWithString:ret];
}

+(long) stringDateToLong:(NSString *)dateString {
    NSDate *date = [[ApiDateFormatter serverDateFormatter] dateFromString:dateString];
    return (long)[date timeIntervalSince1970];
}


@end
