//
//  RatingsFilter.m
//  HonkioSDK
//
//  Created by Mikhail Li on 07/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "RatingsFilter.h"

#define QUERY_OBJECT        @"query_object"
#define QUERY_OBJECT_TYPE   @"query_object_type"

@implementation RatingsFilter

-(NSDictionary *)toParams {
    NSMutableDictionary *result = [[NSMutableDictionary alloc] initWithCapacity:3];
    
    [self fillParams:result];
    
    return result;
}

-(void)fillParams:(NSMutableDictionary *)params {
    if (self.object)
        [AbsFilter applyIfNotNil:QUERY_OBJECT value:self.object forParams:params];
    if (self.objectType)
        [AbsFilter applyIfNotNil:QUERY_OBJECT_TYPE value:self.objectType forParams:params];
    [super fillParams:params];
}

@end
