//
//  BasePushEntity.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/19/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "BasePushEntity.h"

@implementation BasePushEntity {
    NSMutableDictionary* _content;
}

-(id)initWithType:(NSString*)type {
    return [self initWithType:type andContent:[NSMutableDictionary new]];
}

-(id)initWithType:(NSString*)type andContent:(NSDictionary*)content {
    self = [super init];
    if (self) {
        _type = type;
        
        if ([content isKindOfClass: NSMutableDictionary.class]) {
            _content = content;
        }
        else {
            _content = [[NSMutableDictionary alloc] initWithDictionary:content];
        }
    }
    return self;
}

-(NSString*)uid {
    return nil;
}

-(void)setContent:(NSString*)key value:(NSObject*)value {
    _content[key] = value;
}

-(NSObject*)getContent:(NSString*)key {
    return _content[key];
}

@end
