//
//  TagsList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 5/15/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tag.h"

@interface TagsList : NSObject

@property (nonatomic, strong) NSDictionary<NSString*, Tag*>*  dict;

@end
