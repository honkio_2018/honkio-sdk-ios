//
//  HKBaseLasyMapViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

open class HKBaseLasyMapViewController: HKBaseMapViewController {
    
    private(set) open var isUpdating = false
    
    private var defferedRequest = false
    private var defferedLatitude: CLLocationDegrees?
    private var defferedLongitude: CLLocationDegrees?
    private var defferedRadius: CLLocationDistance?
    
    open func addItemAnnotation(_ item: Any?) {
        preconditionFailure("Method toList must be overridden")
    }
    
    /**
     Loads list of items from server.
     * Must be overriden.
     - parameter responseHandler: The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
     */
    open func loadList(_ responseHandler: @escaping ResponseHandler, _ latitude: CLLocationDegrees, _ longitude: CLLocationDegrees, _ radius: CLLocationDistance) {
        preconditionFailure("Method loadList must be overridden")
    }
    
    /**
     Converts server response to list of list items.
     - Returns: Loaded items in response converted to List.
     - parameter response: The Response object to be converted to List.
     */
    open func toList(_ response : Response) -> [Any?] {
        preconditionFailure("Method toList must be overridden")
    }
    
    open func filterList(_ list : [Any?]) -> [Any?] {
        return list
    }
    
    open override func cameraDidMove(latitude: CLLocationDegrees, longitude: CLLocationDegrees, radius: CLLocationDistance) {
        if !self.isUpdating {
            self.defferedRequest = false
            self.updateAnnotations(latitude, longitude, radius)
        }
        else {
            self.defferedLatitude = latitude
            self.defferedLongitude = longitude
            self.defferedRadius = radius
            self.defferedRequest = true
        }
    }
    
    /**
     Handles provided error.
     - parameter error: NSError object within the error to be handled.
     - Returns: True if error has been handled, otherwise false.
     */
    open func handleError(_ error : ApiError) -> Bool {
        return AppErrors.handleError(self, error: error)
    }
    
    private func listDidLoad(_ list: [Any?]) {
        self.removeAllAnnotations()
        for item in list {
            if item != nil {
                self.addItemAnnotation(item)
            }
        }
    }
    
    private func updateAnnotations(_ latitude: CLLocationDegrees, _ longitude: CLLocationDegrees, _ radius: CLLocationDistance) {
        self.isUpdating = true
        self.loadList(self.buildListLoader(), latitude, longitude, radius)
    }
    
    /**
     - Returns: The loader object to be used on loadinga data from the server.
     */
    private func buildListLoader() -> (ResponseHandler) {
        
        return {[weak self] (response) in
            
            self?.isUpdating = false
            
            if response.isStatusAccept(), let strongSelf = self {
                strongSelf.listDidLoad(strongSelf.filterList(strongSelf.toList(response)))
            }
            else {
                let _ = self?.handleError(response.anyError())
            }
            
            if self?.defferedRequest ?? false {
                self?.defferedRequest = false
                self?.updateAnnotations(self?.defferedLatitude ?? 0.0, self?.defferedLatitude ?? 0.0, self?.defferedRadius ?? 0.0)
            }
        }
    }
}
