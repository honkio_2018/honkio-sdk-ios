//
//  ErrorExtensions.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 11/29/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

extension Error {
    
    public func isHonkioApiError() -> Bool {
        return (self as NSError).isHonkioApiError()
    }
    
    public func isHonkioApiError(_ errorCode: Int32) -> Bool {
        return (self as NSError).isHonkioApiError(errorCode)
    }
    
    public func honkioApiError() -> ApiError? {
        return (self as NSError).honkioApiError()
    }
    
}
