//
//  MerchantJsonParser.h
//  HonkioSDK
//
//  Created by Mikhail Li on 03/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "Merchant.h"

@interface MerchantJsonParser : BaseJsonResponseParser

+(nullable Merchant *)parseMerchant:(NSDictionary *)dict;

@end
