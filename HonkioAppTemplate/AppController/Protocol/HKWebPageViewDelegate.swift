//
//  HKWebPageViewDelegate.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 7/11/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

protocol HKWebPageViewDelegate: class {
    
    var webViewDelegate: UIWebViewDelegate? { get set }
    
    func showUrl(_ url: URL)
    func showUrl(string: String)
    
}
