//
//  PinReason.h
//  HonkioApi
//
//  Created by Shurygin Denis on 11/30/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//


#define PIN_REASON_NEW 1
#define PIN_REASON_REENTER 2
#define PIN_REASON_PURCHASE 3
#define PIN_REASON_TIMEOUT 4
