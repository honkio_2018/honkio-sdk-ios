//
//  NSString + Regex.m
//  HonkioApi
//
//  Created by developer on 1/22/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import "NSString + Regex.h"

@implementation NSString (Regex)

- (NSString *)rightStrip:(NSString *)character {
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern: [NSString stringWithFormat:@"%@$", character] options:NSRegularExpressionCaseInsensitive error:&error];
    return [regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, [self length]) withTemplate:@""];
}

@end
