//
//  HKBaseAssetsMapViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/29/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

// TODO write docs
open class HKBaseAssetsMapViewController: HKBaseLasyMapViewController {
    
    public var shopIdentity : Identity?
    public fileprivate(set) var filter : AssetFilter = AssetFilter()
    
    open override func addItemAnnotation(_ item: Any?) {
        if let asset = item as? HKAsset {
            let annotation = self.toAnnotation(asset)
            annotation.item = asset
            self.addAnnotation(annotation)
        }
    }
    
    open override func loadList(_ responseHandler: @escaping ResponseHandler, _ latitude: CLLocationDegrees, _ longitude: CLLocationDegrees, _ radius: CLLocationDistance) {
        self.setupFilter(self.filter, latitude, longitude, radius)
        HonkioApi.userAssetList(self.filter, shop: self.shopIdentity, flags: 0, handler: responseHandler)
    }
    
    /**
     Converts server response to list of list items.
     - Returns: Loaded items in response converted to List.
     - parameter response: The Response object to be converted to List.
     */
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! HKAssetList).list
    }
    
    open func toAnnotation(_ asset: HKAsset) -> HKMapViewAnnotation {
        let annotation = HKMapViewAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: asset.latitude, longitude: asset.longitude)
        annotation.title = asset.name
        return annotation
    }

    open func setupFilter(_ filter: AssetFilter, _ latitude: CLLocationDegrees, _ longitude: CLLocationDegrees, _ radius: CLLocationDistance) {
        filter.latitude = String(latitude)
        filter.longitude = String(longitude)
        filter.radius = String(radius)
        
        filter.queryCount = 0
        filter.querySkip = 0
    }

}
