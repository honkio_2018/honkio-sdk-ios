//
//  BaseOrderUpdaterController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/28/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
* HKSimpleOrderSetVCStrategy the class which processes the setting of order.
*/

open class HKSimpleOrderSetVCStrategy: HKViewControllerLifeCycleObserver {
    
    /**
     Represents the instance to be called when status of setting process has changed.<br>
     If delegat is nil the parrent view controller will be used as delegate if it response for delegate protocol.
    */
    open var delegate : HKOrderSetVCStrategyDelegate? {
        set {
            _delegate = newValue
        }
        
        get {
            if _delegate == nil {
                return self.parent as? HKOrderSetVCStrategyDelegate
            }
            return _delegate
        }
    }
    
    // TODO write docs
    open var removeOnCompletion : Bool = false
    
    // TODO write docs
    open var useMerchantApi : Bool = false
    
    fileprivate var _delegate : HKOrderSetVCStrategyDelegate?
    
    fileprivate var orderSetProcessModel : AnyObject? // TODO implement OrderSetProcessModel

    /**
     Sets the user's Order.
     - parameter order: The Order instance to be set.
     - parameter flags: Additional Request flags.
     * if listener represented by delegate is not nil it will be notified that order will now be set.
     * if listener represented by delegate is not nil it will be notified after order was set by orderDidSet or onOrderSetError.
     */
    open func userSetOrder(_ order: Order, flags:Int32) {
        self.userSetOrder(order, shop: nil, flags: flags)
    }

    /**
     Sets the user's Order.
     - parameter shopIdentity: The Identity inctance to be used for setting the order.
     - parameter order: The Order instance to be set.
     - parameter flags: Additional Request flags.
     * if listener represented by delegate is not nil it will be notified that order will now be set.
     * if listener represented by delegate is not nil it will be notified after order was set by orderDidSet or onOrderSetError.
     */
    open func userSetOrder(_ order: Order, shop: Identity?, flags:Int32) {
        self.orderWillSet()
        self.sendRequest(order, shop: shop, flags: flags, handler: self.orderSetHandler())
    }
    
    open func sendRequest(_ order: Order, shop: Identity?, flags:Int32, handler: ResponseHandler?) {
        if useMerchantApi {
            if shop != nil {
                HonkioMerchantApi.merchantSetOrder(order, shop: shop, flags: flags, handler:handler)
            }
            else {
                HonkioMerchantApi.merchantSetOrder(order, flags: flags, handler:handler)
            }
        }
        else {
            if shop != nil {
                HonkioApi.userSetOrder(order, shop: shop, flags: flags, handler:handler)
            }
            else {
                HonkioApi.userSetOrder(order, flags: flags, handler:handler)
            }
        }
    }
    
    // TODO write docs
    open func userSetOrder(_ orderId: String, status: String, shop: Identity?, flags:Int32) {
        self.orderWillSet()
        self.sendRequest(orderId: orderId, status: status, shop: shop, flags: flags, handler: self.orderSetHandler())
    }
    
    open func sendRequest(orderId: String, status: String, shop: Identity?, flags: Int32, handler: ResponseHandler?) {
        HonkioApi.userSetOrderStatus(status, orderId: orderId, shop: shop, flags: flags, handler: handler)
    }
    
    // TODO write docs
    open func userSetOrder(application: String, status: String, buildNewOrder:Bool, shop: Identity?, flags:Int32) {
        self.orderWillSet()
        self.sendRequest(application: application, status: status, buildNewOrder: buildNewOrder, shop: shop, flags: flags, handler: self.orderSetHandler())
    }
    
    open func sendRequest(application: String, status: String, buildNewOrder:Bool, shop: Identity?, flags: Int32, handler: ResponseHandler?) {
        HonkioApi.userSetOrderApplication(application, status: status, buildNewOrder: buildNewOrder, shop: shop, flags: flags, handler: handler)
    }
    
    func orderWillSet() {
        if self.delegate != nil {
            self.delegate?.orderWillSet(self)
        }
    }
    
    func orderDidSet(_ order: Order) {
        if self.delegate != nil {
            self.delegate?.orderDidSet(self, order: order)
        }
        if self.removeOnCompletion {
            self.parent?.removeLifeCycleObserver(self)
        }
    }
    
    func onOrderSetPending(_ response: Response) {
        self.onOrderSetError(response.anyError())
    }
    
    func onOrderSetError(_ error: ApiError) {
        if self.delegate != nil {
            self.delegate?.onOrderSetError(self, error: error)
        }
        if self.removeOnCompletion {
            self.parent?.removeLifeCycleObserver(self)
        }
    }
    
    private func orderSetHandler() -> ResponseHandler {
        return {[weak self] (response) in
            if response.isStatusAccept() {
                self?.orderDidSet(response.result as! Order)
            }
            else if response.isStatusPending() {
                self?.onOrderSetPending(response)
            }
            else {
                self?.onOrderSetError(response.anyError())
            }
        }
    }
}
