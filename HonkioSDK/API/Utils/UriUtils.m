//
//  UriUtils.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "UriUtils.h"

@implementation UriUtils


+(NSString*)buildInventoryProductUri:(NSString*)authorityUri id:(NSString*)inventoryProductId {
    NSURLComponents* components = [NSURLComponents componentsWithString:authorityUri];
    components.queryItems = @[[NSURLQueryItem queryItemWithName:HONKIO_QUERY_INVENTORY value:inventoryProductId]];
    return components.string;
}

@end
