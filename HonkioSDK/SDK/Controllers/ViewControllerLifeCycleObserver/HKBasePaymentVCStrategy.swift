///
///  HKBasePaymentVCStrategy.swift
///  WorkPilots
///
///  Created by Shurygin Denis on 4/1/16.
///  Copyright © 2016 developer. All rights reserved.
///

import Foundation
import UIKit

/**
* HKBasePaymentVCStrategy is a base class containing base payment functionlaty and should be used for
* any payment performing classes.
* It hooks up PIN enter/cancel events, and starts payments.
* This class should not be instaciated due to prevent incorrect or unintended error processing.
*/

open class HKBasePaymentVCStrategy: HKViewControllerLifeCycleObserver, ProcessProtocol, RequestPinProtocol {
    
    open var isPinRequired : Bool = false
    
    open var delegate : HKPaymentVCStrategyDelegate? {
        set {
            _delegate = newValue
        }
        
        get {
            if _delegate == nil {
                return self.parent as? HKPaymentVCStrategyDelegate
            }
            return _delegate
        }
    }
    fileprivate var _delegate : HKPaymentVCStrategyDelegate?
    
    fileprivate var paymentProcessModel: BasePaymentProcessModel?
    fileprivate var payParams: PayParams?
    
    /**
     * It to be called when the PIN code has to be requested.
     - parameter reason: Defines the reason of entering pin code.
     * Param _reason_ has the value from the following list:
     1. PIN_REASON_NEW = 1
     2. PIN_REASON_REENTER = 2
     3. PIN_REASON_PURCHASE = 3
     4. PIN_REASON_TIMEOUT = 4
     */
    open func requestPin(_ reason: Int) {
        preconditionFailure("WARNING: You must override this method in your custom class")
    }
    
    /**
     * Is to be used as notifier that the user has no payment accounts.
     * Must be overriden.
     */
    open func noPaymentAccount(_ payParams: PayParams) {
        preconditionFailure("WARNING: You must override this method in your custom class")
    }

    /**
    * Should be called when PIN entered.
    - parameter pinCode: Entered PIN code.
    - parameter view: Parent view.
    - parameter reason: Defines the reason of entering pin code.
    * Param _reason_ has the value from the following list:
     1. PIN_REASON_NEW = 1
     2. PIN_REASON_REENTER = 2
     3. PIN_REASON_PURCHASE = 3
     4. PIN_REASON_TIMEOUT = 4
    */
    override open func pinEntered(_ pinCode: String!, reason: Int!) {
        if paymentProcessModel != nil && reason == Int(PIN_REASON_PURCHASE){
            paymentProcessModel?.pinEntered(pinCode, reason: Int32(reason))
        }
    }

    /**
    * Should be called when PIN pad canceled without entering a PIN.
    - parameter reason: Defines the reason of entering pin code.
    - parameter view: Parent view.
     * Param _reason_ has the value from the following list:
     1. PIN_REASON_NEW = 1
     2. PIN_REASON_REENTER = 2
     3. PIN_REASON_PURCHASE = 3
     4. PIN_REASON_TIMEOUT = 4
    */
    override open func pinCanceled(_ reason: Int!) {
        if paymentProcessModel != nil && reason == Int(PIN_REASON_PURCHASE) {
            paymentProcessModel?.pinCanceled(Int32(reason))
        }
    }

    /**
    * onComplete is called if the request to the server has been completed successfully.
    - parameter model: ProcessModel which runs the request.
    - parameter response: Response object containing server response.
    */
    open func onComplete(_ model: BaseProcessModel!, response: Response!) {
        if model === paymentProcessModel {
            self.paymentDidComplete(response!, payParams: response.result as! UserPayment)
        }
    }

    /**
    * It to be called when the PIN code has to be requested.
    - parameter reason: Defines the reason of entering pin code.
    - parameter model: PaymentModel requesting the PIN code.
     * Param _reason_ has the value from the following list:
     1. PIN_REASON_NEW = 1
     2. PIN_REASON_REENTER = 2
     3. PIN_REASON_PURCHASE = 3
     4. PIN_REASON_TIMEOUT = 4
    */
    open func requestPin(_ model: BasePaymentProcessModel!, reason: Int32) {
        self.requestPin(Int(reason))
    }

    /**
    - parameter orderId: Defines the order identificator for the payment request.
    - parameter products: List of products to be purchased.
    - parameter flags: Additional flags for request.
    */
    open func paymentRequest(_ orderId: String?, products: [BookedProduct], flags: Int) {
        self.paymentRequest(nil, orderId: orderId, products: products, flags: flags)
    }

    /**
    - parameter shop: Shop which will process payment.
    - parameter orderId: Order identificator which will process payment.
    - parameter products: List of products to be purchased.
    - parameter flags: Additional flags for request.
    */
    open func paymentRequest(_ shop: Shop?, orderId: String?, products: [BookedProduct], flags: Int) {
        let productPayParams = ProductPayParams(shop: shop, oirderId: orderId, account: nil, flags: Int32(flags))
        productPayParams?.products = products
        payParams = productPayParams
        paymentProcessModel = nil
        self.runPaymentIfOk()
    }
    
    /**
     - parameter orderId: Defines the order identificator for the payment request.
     - parameter amount: Amount of money to pay.
     - parameter currency: Currency of the payment.
     - parameter flags: Additional flags for request.
     */
    open func paymentRequest(_ orderId: String?, amount: Double, currency: String?, flags: Int) {
        self.paymentRequest(nil, orderId: orderId, amount: amount, currency: currency, flags: flags)
    }
    
    /**
     - parameter shop: Shop which will process payment.
     - parameter orderId: Order identificator which will process payment.
     - parameter amount: Amount of money to pay.
     - parameter currency: Currency of the payment.
     - parameter flags: Additional flags for request.
     */
    open func paymentRequest(_ shop: Shop?, orderId: String?, amount: Double, currency: String?, flags: Int) {
        let productPayParams = AmountPayParams(shop: shop, oirderId: orderId, account: nil, flags: Int32(flags))
        productPayParams?.amount = amount
        productPayParams?.currency = currency
        payParams = productPayParams
        paymentProcessModel = nil
        self.runPaymentIfOk()
    }

    /**
        Checks the payment account is set and calls payment or notifies the account is not set.
        - parameter account: Payment account which will be used for the payment.
    */
    open func runPaymentIfOk(_ account: UserAccount?) {
        if payParams != nil {
            payParams?.account = account
            self.runPaymentIfOk()
        }
    }
    
    /**
     Checks the payment account is set and calls payment or notifies the account is not set.
     - parameter account: Payment account which will be used for the payment.
     */
    fileprivate func runPaymentIfOk() {
        if payParams != nil {
            if payParams!.isAccountValid() {
                runPayment()
            }
            else {
                self.noPaymentAccount(payParams!)
            }
        }
    }

    /**
    * Runs payment process by instantiation new process model and subscription
    * this to corresponding delegates.
    */
    open func runPayment() {
        if payParams != nil {
            self.paymentWillStart(payParams!)
            paymentProcessModel = payParams!.instantiateProcessModel()
            paymentProcessModel!.processDelegate = self
            paymentProcessModel!.pinDelegate = self.isPinRequired ? self : nil
            paymentProcessModel!.pay(payParams)
            payParams = nil
        }
    }
    
    /** Makes single query for pending payment transaction. */
    open func query() {
        paymentProcessModel?.query()
    }
    
    /**
     * Starts loop query for pending payment transaction with specified delay.
     * @param loopDelay Delay for the next query.
     */
    open func queryLoop(_ loopDelay: Int) {
        paymentProcessModel?.queryLoop(loopDelay)
    }
    
    open func stopQuery() {
        paymentProcessModel?.stopQuery()
    }
    
    /** Abortion of the payment process. */
    open func abort() {
        paymentProcessModel?.abort()
    }
    
    /** Check if payment process is aborted. */
    open func isAborted() -> Bool {
        return paymentProcessModel?.isAborted() ?? true
    }
    
    /**
     * onError is called if the request to the server has been failed.
     - parameter model: ProcessModel which runs into error.
     - parameter response: Response object containing server response.
     - parameter error: Returned error.
     */
    open func onError(_ model: BaseProcessModel!, response: Response!) {
        if delegate != nil {
            delegate?.paymentError(self, response: response)
        }
    }
    
    /**
     * onPending is called if the request to the server has pending status.
     - parameter model: ProcessModel which runs into error.
     - parameter response: Response object containing server response.
     */
    open func onPending(_ model: BaseProcessModel!, response: Response!) {
        if delegate != nil {
            delegate?.paymentPending(self, response: response, payment: response.result as! UserPayment)
        }
    }
    
    /**
     * Is to be used as notifier that the payment process has been started.
     - parameter payParams: Instance of payment parameters are to be used for payment.
     */
    func paymentWillStart(_ payParams: PayParams) {
        if delegate != nil {
            delegate?.paymentWillstart(self, payParams: payParams)
        }
    }
    
    /**
     Is to be used as notifier that the payment process has been completed.
     - parameter response: The response from the server containing the payment detailes.
     - parameter payment: The detailes of performed payment.
     */
    func paymentDidComplete(_ response: Response, payParams: UserPayment) {
        if delegate != nil {
            delegate?.paymentDidComplete(self, response: response, payment: payParams)
        }
    }
}
