//
//  ShopFilter.h
//  HonkioApi
//
//  Created by Shurygin Denis on 8/4/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsListFilter.h"

/**
 * Filter for shopFind request.
 */
@interface ShopFilter : AbsListFilter

/** Shop service type. Only shops with the specified service type will be found.*/
@property (nonatomic) NSString* serviceType;

/** Merchant Id. Only shops of the specified merchant will be found. */
@property (nonatomic) NSString* merchantId;

/** Network Id. Only shops from the specified Network will be found. */
@property (nonatomic) NSString* networkId;

@property (nonatomic) NSString* latitude;
@property (nonatomic) NSString* longitude;

/** Radius of the search. */
@property (nonatomic) NSString* radius;

// TODO write docs
@property (nonatomic) BOOL isQuerySubMerchants;

// TODO write docs
@property (nonatomic) BOOL isQueryAllMerchants;

// TODO write docs
@property (nonatomic) BOOL isQueryRatings;

/**
 * Check if filter has set location parameters.
 * @return YES if filter has set location parameters, false otherwise.
 */
-(BOOL)isHasLocation;

@end
