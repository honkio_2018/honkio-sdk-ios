//
//  UserRoleDescriptionsListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/5/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "UserRoleDescriptionsListJsonParser.h"
#import "UserRoleDescriptionJsonParser.h"

@implementation UserRoleDescriptionsListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    UserRoleDescriptionsList* result = [UserRoleDescriptionsList new];
    NSMutableArray* list = [NSMutableArray new];
    
    NSArray* rolesArray = dict[@"roles"];
    if (rolesArray) {
        for(NSDictionary* roleDict in rolesArray) {
            [list addObject:[UserRoleDescriptionJsonParser parseRoleDescription:roleDict]];
        }
    }
    result.list = [[NSArray alloc] initWithArray:list];
    return result;
}

@end
