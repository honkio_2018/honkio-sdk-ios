//
//  HKMediaFileJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/13/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKMediaFileJsonParser.h"

@implementation HKMediaFileJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [HKMediaFileJsonParser parseMediaFile:dict];
}

+(HKMediaFile*) parseMediaFile:(NSDictionary*) dict {
    HKMediaFile* result = [HKMediaFile new];
    if (dict) {
        result.objectId = dict[@"id"];
        result.url = dict[@"url"];
        result.timestamp = dict[@"timestamp"];
        result.access = dict[@"access"];
        result.userOwnerId = dict[@"user_owner"];
        result.extension = dict[@"extension"];
        result.metaData = dict[@"metadata"];
    }
    return result;
}

@end
