//
//  FeedBack.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 4/6/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "UserComment.h"

@implementation UserComment

-(NSDictionary*) toParams {
    
    NSMutableDictionary* params = [NSMutableDictionary new];
    [AbsFilter applyIfNotNil:@"order_id" value:self.orderId forParams:params];
    [AbsFilter applyIfNotNil:@"voter_role_id" value:self.voter.roleId forParams:params];
    [AbsFilter applyIfNotNil:@"mark" value:[NSNumber numberWithInt:self.mark] forParams:params];
    [AbsFilter applyIfNotNil:@"comment" value:self.text forParams:params];
    
    if (self.object && self.objectType) {
        [AbsFilter applyIfNotNil:@"object" value:self.object forParams:params];
        [AbsFilter applyIfNotNil:@"object_type" value:self.objectType forParams:params];
    }
    else {
        [AbsFilter applyIfNotNil:@"user_id" value:self.userId forParams:params];
        [AbsFilter applyIfNotNil:@"user_role_id" value:self.userRoleId forParams:params];
    }
    
    return [NSDictionary dictionaryWithDictionary:params];
}

@end
