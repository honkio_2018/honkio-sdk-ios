//
//  HKSortFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

// TODO docs

typedef NS_ENUM(NSUInteger, HKSortFilterOrder) {
    asc,
    desc
};

@interface HKSortFilterItem : NSObject

@property (nonatomic) NSString* field;
@property (nonatomic) HKSortFilterOrder order;

-(id) initWith:(NSString*)field order:(HKSortFilterOrder)order;

@end

@interface HKSortFilter : NSObject

@property (nonatomic, readonly) NSMutableArray<HKSortFilterItem*>* items;

-(id) initWithItems:(NSArray<HKSortFilterItem*>*)items;

-(NSArray*) toParams;

@end
