//
//  OrderChatMessagePushEntity.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/19/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasePushEntity.h"

@interface OrderChatMessagePushEntity : BasePushEntity

@property (nonatomic, nullable) NSString *senderId;
@property (nonatomic, nullable) NSString *senderRole;
@property (nonatomic, nullable) NSString *senderFirstName;
@property (nonatomic, nullable) NSString *senderLastName;
@property (nonatomic, nullable) NSString *receiverId;
@property (nonatomic, nullable) NSString *receiverRole;
@property (nonatomic, nullable) NSString *orderId;
@property (nonatomic, nullable) NSString *orderThirdPerson;
@property (nonatomic, nullable) NSString *text;
@property (nonatomic, nullable) NSString *shopName;
@property (nonatomic, nullable) NSString *shopId;
@property (nonatomic, nullable) NSString *assetId;
@property (nonatomic, nullable) NSString *assetName;

@property (nonatomic, nullable) NSDate* time;

-(id)initWithContent:(NSDictionary*)content;

@end
