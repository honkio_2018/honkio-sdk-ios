//
//  BaseJsonResponseBuilder.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponseParser.h"
#import "Shop.h"
#import "MerchantSimple.h"
#import "Product.h"
#import "BoughtProduct.h"
#import "InventoryProduct.h"

/**
 * This class parse server responses in JSON format.
 */
@interface BaseJsonResponseParser : NSObject<ResponseParser>

/**
 * Parse NSDictionary object into Result object.
 * @param message Message used for request.
 * @param dict NSDictionary object to parse.
 * @return Result object.
 */
-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict;

/**
 * Parse NSDictionary object into Response object.
 * @param message Message used for request.
 * @param dict NSDictionary object to parse.
 * @return Response object.
 */
-(Response*)parseResponse:(Message*)message dict:(NSDictionary*)dict;

/**
 * Parse Shop object from the JSON object.
 * @param shop Shop object that would be filled from NSDictionary object.
 * @param dict NSDictionary object which contains shop params.
 */
+(void)parseShop:(Shop*)shop dict:(NSDictionary*)dict;

/**
 * Parse SimpleMerchant object from the NSDictionary object.
 * @param merchant SimpleMerchant object that would be filled from NSDictionary object.
 * @param dict NSDictionary object which contains shop params.
 */
+(void)parseMerchantSimple:(MerchantSimple*)merchant dict:(NSDictionary *)dict;

/**
 * Parse Product object from the NSDictionary object.
 * @param product Product object that would be filled from NSDictionary object.
 * @param dict NSDictionary object which contains shop params.
 */
+(void)parseProduct:(Product*)product dict:(NSDictionary *)dict;

//TODO write docs
+(void)parseBoughtProduct:(BoughtProduct*)product dict:(NSDictionary *)prodData;

//TODO write docs
+(void)parseInventoryProduct:(InventoryProduct*)product dict:(NSDictionary *)prodData;

@end
