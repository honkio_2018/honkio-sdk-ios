//
//  HKAsset.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKAsset.h"
#import "AbsFilter.h"

@implementation HKAsset


-(id)init {
    self = [super init];
    return self;
}

-(id)initFrom:(HKAsset*) asset {
    self = [super init];
    
    _assetId = [asset assetId];
    _name = [asset name];
    
    _latitude = [asset latitude];
    _longitude = [asset longitude];
    
    _stricture = [asset stricture];
    _merchantId = [asset merchantId];
    _group = [asset group];
    
    _visible = [asset isVisible];
    _hasValidSchedule = [asset isHasValidSchedule];
    
    if ([asset properties]) {
        _properties = [[NSMutableDictionary alloc] initWithDictionary: [asset properties]];
    }
    
    return self;
}

- (NSString *)getId {
    return self.assetId;
}

-(NSDictionary *)toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    
    [AbsFilter applyIfNotNil:@"id" value:self.assetId forParams:params];
    [AbsFilter applyIfNotNil:@"merchant" value:self.merchantId forParams:params];
    [AbsFilter applyIfNotNil:@"name" value:self.name forParams:params];
    [AbsFilter applyIfNotNil:@"group" value:self.group forParams:params];
    if (self.latitude)
        [AbsFilter applyIfNotNil:@"latitude" value:[NSString stringWithFormat:@"%f", self.latitude] forParams:params];
    if (self.longitude)
        [AbsFilter applyIfNotNil:@"longitude" value:[NSString stringWithFormat:@"%f", self.longitude] forParams:params];
    [AbsFilter applyIfNotNil:@"visible" value:@(self.visible) forParams:params];
    
    if (self.properties)
        [AbsFilter applyIfNotNil:@"properties" value:self.properties forParams:params];
    if (self.stricture)
        [AbsFilter applyIfNotNil:@"structure" value:self.stricture.objectId forParams:params];
    
    return params;
}

-(BOOL) structureIs:(NSString*) structureId {
    if ([self.stricture objectId] == nil ) {
        return structureId == nil;
    }
    return [[self.stricture objectId] isEqualToString:structureId];
}

-(BOOL) structureIsOneOf:(NSArray<NSString*>*) structures {
    for (NSString* structureId in structures) {
        if ([self structureIs:structureId]) {
            return YES;
        }
    }
    return NO;
}

@end
