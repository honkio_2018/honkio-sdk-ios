//
//  UriUtils.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HONKIO_URI_AUTHORITY @"https://honkio.com"

#define HONKIO_QUERY_QRID @"shop"
#define HONKIO_QUERY_SHOP_ID @"shopId"
#define HONKIO_QUERY_PRODUCT @"product"
#define HONKIO_QUERY_INVENTORY @"inventory"

@interface UriUtils : NSObject

+(NSString*)buildInventoryProductUri:(NSString*)authorityUri id:(NSString*)inventoryProductId;

@end
