//
//  HKLazyQrCodeViewController.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 2/20/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

open class HKLazyQrCodeViewController : HKBaseViewController, HKQrCodeViewProtocol {
    
    @IBOutlet open weak var progressView: UIView!
    
    public var qrCode : HKQrCode? {
        didSet {
            if qrCode != nil {
                self.childViewController = self.getViewController(qrCode!)
                if let qrChild = childViewController as? HKQrCodeChildViewProtocol {
                    qrChild.qrCode = qrCode
                }
                if self.childViewController != nil {
                    self.addChildViewController(self.childViewController!)
                    self.view.addSubview(self.childViewController!.view)
                    self.childViewController!.didMove(toParentViewController: self)
                }
            }
        }
    }
    
    private var qrCodeLoadTask: Task?
    
    private var childViewController: UIViewController?
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    open func loadQrCode(_ qrId: String) {
        self.progressView.isHidden = false
        self.qrCodeLoadTask = HonkioApi.qrInfo(qrId, flags: 0) {[unowned self] (response) in
            if (response.isStatusAccept()) {
                self.qrCode = response.result as? HKQrCode
                self.progressView.isHidden = true
            }
            else if (!AppErrors.handleError(self, error: response.anyError())){
                CustomAlertView(apiError: response.anyError(), okAction: {
                    self.dismissScreen()
                }).show()
            }
        }
    }
    
    open func getViewController(_ qrCode: HKQrCode) -> UIViewController? {
        if (qrCode.objectType == nil || qrCode.objectType.count == 0 || qrCode.object == nil) {
            if let controller = AppController.getViewControllerById(AppController.QrCodeNoType, viewController: self) {
                return controller
            }
        }
        else if (qrCode.objectType == HK_QR_TYPE_ASSET) {
            if let controller = AppController.getViewControllerById(AppController.Asset, viewController: self) as? HKAssetViewProtocol {
                controller.loadAsset((qrCode.object as! HKAsset).assetId)
                return controller as? UIViewController
            }
        }
        else if (qrCode.objectType == HK_QR_TYPE_SHOP) {
            return AppController.getViewControllerById(AppController.Shop, viewController: self)
        }
        
        return UIViewController()
    }
    
    open func dismissScreen() {
        self.navigationController?.popViewController(animated: true)
    }
}

