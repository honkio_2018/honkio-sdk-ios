//
//  OrderControllerDelegate.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/28/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
* Describes the set of functions to be implemented for an instance to be used in OrderLoaderVCStrategy.
*/

public protocol HKOrderLoaderVCStrategyDelegate {
    /// Should be called before Orders start loading.
    func orderWillLoad(_ controller: HKOrderLoaderVCStrategy)
    /// Shoud be called when the Order has been loaded.
    func orderDidLoad(_ controller: HKOrderLoaderVCStrategy, order: Order)
    /// Should be called when the error occures on loading of Orders.
    func onOrderLoadingError(_ controller: HKOrderLoaderVCStrategy, error: ApiError)
    
}
