//
//  LocalizationExtensions.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/26/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation
import UIKit

/**
The set of extensions define useful functonality for localization of string values in the application.
Extends the String object with helpers for finding localized resource string value in a bundle.
*/
extension String {
    
    public static let MISSING_TRANSLATION_NOTIFICATION = "LocalizationExtensions.missing_translation";

    /**
    Containes the localized value of the extended String object
    */
    public var localized: String {
        return self.localizedWithComment("")
    }

    /**
    Searches the localized string resource value in application bundles.
    - parameter comment: Not used.
    */
    public func localizedWithComment(_ comment: String) -> String {
        if let string = self.localizedWithComment(comment, bundles: HKBundleUtils.bundles) {
            return string
        }
        
        return self
    }

    fileprivate func localizedWithComment(_ comment: String, bundles: [Bundle]) -> String? {
        for bundle in bundles {
            if let string = self.localizedWithComment(comment, bundle: bundle, recursion: 1) {
                return string
            }
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: String.MISSING_TRANSLATION_NOTIFICATION), object: self)
        
        return nil
    }
    
    fileprivate func localizedWithComment(_ comment: String, bundle: Bundle, recursion: Int) -> String? {
        let string = NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: comment)
        
        if self != string {
            return string
        }
        
        if recursion > 0 {
            if let urls = bundle.urls(forResourcesWithExtension: "bundle", subdirectory: nil) {
                for subURL in urls {
                    if let subBundle = Bundle(url: subURL) {
                        if let subString = self.localizedWithComment(comment, bundle: subBundle, recursion: recursion - 1) {
                            return subString
                        }
                    }
                }
                
            }
        }
        
        return nil;
    }
}

/**
Wraps the UILabel *text* property.
The "localized" extention function of a String object will be called
on setting the "text" property of the UILabel.
*/
extension UILabel {
    
    @IBInspectable public var localizedText : String? {
        set {
            if newValue != nil {
                self.text = newValue?.localized
            }
            else {
                self.text = nil
            }
        }
        
        get {
            return self.text
        }
    }
    
    @IBInspectable public var localizedAtrText : String? {
        set {
            if newValue != nil {
                let attrStr = try! NSAttributedString(
                    data: newValue!.localized.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
                    options: [ NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html],
                    documentAttributes: nil)
                
                self.attributedText = attrStr
            }
            else {
                self.text = nil
            }
        }
        
        get {
            return self.text
        }
    }
}

/**
Wraps the UITextField *text* and *placeholder* property.
The "localized" extention function of a String object will be called
on setting the "text" and "placeholder" properties of the UITextField.
*/
extension UITextField {
    
    @IBInspectable public var localizedText : String? {
        set {
            if newValue != nil {
                self.text = newValue?.localized
            }
            else {
                self.text = nil
            }
        }
        
        get {
            return self.text
        }
    }
    
    @IBInspectable public var localizedPlaceholder : String? {
        set {
            if newValue != nil {
                self.placeholder = newValue?.localized
            }
            else {
                self.placeholder = nil
            }
        }
        
        get {
            return self.placeholder
        }
    }
}

/**
Wraps the UIButton *setTitle* function.
The "localized" extention function of a String object will be called
on "setTitle" function call of the UIButton.
*/
extension UIButton {
    
    @IBInspectable public var localizedTitle : String? {
        set {
            if newValue != nil {
                self.setTitle(newValue!.localized, for: UIControlState())
            }
            else {
                self.setTitle(nil, for: UIControlState())
            }
        }
        
        get {
            if let title = self.titleLabel {
                return title.text
            }
            return nil
        }
    }
}

/**
Wraps the UIBarItem *title* property.
The "localized" extention function of a String object will be called
on setting the "title" property of the UIBarItem.
*/
extension UIBarItem {
    
    @IBInspectable public var localizedTitle : String? {
        set {
            if newValue != nil {
                self.title = newValue?.localized
            }
            else {
                self.title = nil
            }
        }
        
        get {
            return self.title
        }
    }
}

/**
Wraps the UINavigationItem *title* and *prompt* property.
The "localized" extention function of a String object will be called
on setting the "title" and "prompt" properties of the UINavigationItem.
*/
extension UINavigationItem {
    
    @IBInspectable public var localizedTitle : String? {
        set {
            if newValue != nil {
                self.title = newValue?.localized
            }
            else {
                self.title = nil
            }
        }
        
        get {
            return self.title
        }
    }
    
    @IBInspectable public var localizedPrompt : String? {
        set {
            if newValue != nil {
                self.prompt = newValue?.localized
            }
            else {
                self.prompt = nil
            }
        }
        
        get {
            return self.prompt
        }
    }
}

/**
Wraps the UIViewController *title* property.
The "localized" extention function of a String object will be called
on setting the "title" property of the UIViewController.
*/
extension UIViewController {
    
    @IBInspectable public var localizedTitle : String? {
        set {
            if newValue != nil {
                self.title = newValue?.localized
            }
            else {
                self.title = nil
            }
        }
        
        get {
            return self.title
        }
    }
}
