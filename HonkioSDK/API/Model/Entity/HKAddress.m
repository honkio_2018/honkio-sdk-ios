//
//  HKAddress.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/31/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "HKAddress.h"

@implementation HKAddress

- (instancetype)init
{
    self = [super init];
    if (self) {
        // init code
    }
    return self;
}

-(id)initFromDict:(NSDictionary*) dict {
    self = [super init];
    if (self) {
        self.address1 = dict[@"address_1"];
        self.address2 = dict[@"address_2"];
        self.city = dict[@"city"];
        self.admin = dict[@"admin"];
        self.postalCode = dict[@"postal_code"];
        _countryIso = dict[@"country_iso"];
        _countryName = dict[@"country_name"];
    }
    return self;
}

-(void)setCountry:(NSString*)iso name:(NSString*)name {
    _countryIso = iso;
    _countryName = name;
}

-(NSDictionary*)toDict {
    return @{
             @"address_1" : self.address1 ? self.address1 : @"",
             @"address_2" : self.address2 ? self.address2 : @"",
             @"city" : self.city ? self.city : @"",
             @"admin" : self.admin ? self.admin : @"",
             @"postal_code" : self.postalCode ? self.postalCode : @"",
             @"country_iso" : self.countryIso ? self.countryIso : @"",
             @"country_name" : self.countryName ? self.countryName : @""
             };
}

-(NSString*)fullAddress {
    NSMutableString* string = [NSMutableString new];
    
    [self appendAddressPart:string part:self.address1];
    [self appendAddressPart:string part:self.address2];
    [self appendAddressPart:string part:self.city];
    
    if ([self.city isEqualToString:self.admin]) {
        [self appendAddressPart:string part:self.admin];
    }
    
    [self appendAddressPart:string part:self.postalCode];
    [self appendAddressPart:string part:self.countryName];
    
    return string;
}

-(void)appendAddressPart:(NSMutableString*) string part:(NSString*)part {
    if (part && part.length > 0) {
        if (string.length > 0)
            [string appendString:@", "];
        [string appendString:part];
    }
}

@end
