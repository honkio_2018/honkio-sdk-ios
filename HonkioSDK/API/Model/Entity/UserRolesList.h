//
//  UserRolesList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/26/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserRole.h"

@interface UserRolesList : NSObject

@property (nonatomic, strong) NSArray<UserRole*>*  list;

@end
