//
//  OrderJsonParser.h
//  HonkioApi
//
//  Created by Shurygin Denis on 12/2/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "Order.h"

@interface OrderJsonParser : BaseJsonResponseParser

+(Order*)parseOrder:(NSDictionary*)dict;

@end
