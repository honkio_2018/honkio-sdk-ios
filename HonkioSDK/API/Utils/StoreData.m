//
//  StoreData.m
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/9/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import "StoreData.h"

@implementation StoreData

+(void)saveToStore:(id)data forKey:(NSString*)key
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:data forKey:key];
    [defaults synchronize];
}

+(id)loadFromStore:(NSString*)key
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:key];
}

+(void)deleteFromStore:(NSString*)key
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}

@end


