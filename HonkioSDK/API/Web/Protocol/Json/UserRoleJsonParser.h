//
//  UserRoleJsonParser.h
//  HonkioApi
//
//  Created by Shurygin Denis on 12/8/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "UserRole.h"

/**
 *  The parser of user role from a JSON response.
 */
@interface UserRoleJsonParser : BaseJsonResponseParser

+(UserRole*)parseRole:(NSDictionary*) dict;

@end
