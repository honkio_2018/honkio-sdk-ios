//
//  HKUserRoleStructureJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKUserRoleStructureJsonParser.h"
#import "StructureJsonParser.h"

@implementation HKUserRoleStructureJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [HKUserRoleStructureJsonParser toStructure: dict];
}

+(HKUserRoleStructure*)toStructure:(NSDictionary*)dict {
    HKUserRoleStructure* structure = [HKUserRoleStructure new];
    
    structure.objectId = dict[@"_id"];
    structure.merchant = dict[@"merchant"];
    structure.name = dict[@"name"];
    structure.title = dict[@"display_name"];
    structure.desc = dict[@"description"];
    structure.roleId = dict[@"role_id"];
    
    NSDictionary* extraFields = dict[@"extra_fields"];
    if (extraFields) {
        NSDictionary* privateDict = extraFields[@"private_properties"];
        if (privateDict)
            structure.privateProperties = [StructureJsonParser toPropertyiesDict: privateDict];
        
        NSDictionary* publicDict = extraFields[@"public_properties"];
        if (publicDict)
            structure.privateProperties = [StructureJsonParser toPropertyiesDict: publicDict];
    }
    
    if (structure.publicProperties == nil)
        structure.publicProperties = [NSMutableDictionary new];
    if (structure.privateProperties == nil)
        structure.privateProperties = [NSMutableDictionary new];
    
    return structure;
}

@end
