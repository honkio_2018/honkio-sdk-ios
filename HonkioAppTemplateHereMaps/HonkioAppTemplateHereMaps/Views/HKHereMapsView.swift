//
//  HKHereMapsView.swift
//  HonkioAppTemplateHereMaps
//
//  Created by Shurygin Denis on 12/13/18.
//  Copyright © 2018 developer. All rights reserved.
//

import NMAKit
import HonkioAppTemplate

open class HKHereMapsView: NSObject, HKMapViewProtocol, NMAMapGestureDelegate, NMAMapViewDelegate {

    private let NormalZoomLevel : Float = 0.04
    private weak var mapView: NMAMapView!
    
    private var annotations : [ UInt : (mapPin: NMAMapMarker, data: HKMapViewAnnotation)] = [:]
    
    public init(_ mapView: NMAMapView) {
        super.init()
        self.mapView = mapView
        self.mapView.delegate = self
        self.mapView.gestureDelegate = self
    }
    
    open var viewController: HKBaseMapViewController? {
        didSet {
            self.mapViewDidEndMovement(self.mapView)
        }
    }
    
    public var showsUserLocation: Bool {
        get {
            return self.mapView.positionIndicator.isVisible
        }
        set {
            self.mapView.positionIndicator.isVisible = newValue
        }
    }
    
    public var center: CLLocationCoordinate2D {
        get {
            return CLLocationCoordinate2D(latitude: self.mapView.geoCenter.latitude, longitude: self.mapView.geoCenter.longitude)
        }
    }
    
    public var radius: CLLocationDistance {
        get {
            return CLLocationDistance(self.mapView.zoomLevel * 10.0)
        }
    }
    
    public func mapView(_ mapView: NMAMapView, didReceiveTapAt location: CGPoint) {
        let geoLocation = mapView.geoCoordinates(from: location)
        let latitude = CLLocationDegrees(geoLocation?.latitude ?? 0.0)
        let longitude = CLLocationDegrees(geoLocation?.longitude ?? 0.0)
        self.viewController?.mapDidTap(latitude: latitude, longitude: longitude)
        mapView.defaultGestureHandler?.mapView?(mapView, didReceiveTapAt: location)
    }
    
    public func mapView(_ mapView: NMAMapView, didReceiveDoubleTapAt location: CGPoint) {
        let geoLocation = mapView.geoCoordinates(from: location)
        let latitude = CLLocationDegrees(geoLocation?.latitude ?? 0.0)
        let longitude = CLLocationDegrees(geoLocation?.longitude ?? 0.0)
        self.viewController?.mapDidDoubleTap(latitude: latitude, longitude: longitude)
        mapView.defaultGestureHandler?.mapView?(mapView, didReceiveDoubleTapAt: location)
    }
    
    public func mapViewDidEndMovement(_ mapView: NMAMapView) {
        let lat = self.mapView.geoCenter.latitude
        let lon = self.mapView.geoCenter.longitude
        
        self.viewController?.cameraDidMove(latitude: lat, longitude: lon, radius: self.radius)
    }
    
    public func mapView(_ mapView: NMAMapView, didSelect objects: [NMAViewObject]) {
        if objects.count > 0, let annotation = self.annotations[(objects[0] as? NMAMapObject)?.uniqueId() ?? 0] {
            self.viewController?.annotationDidSelect(annotation.data)
        }
    }
    
    public func addAnnotation(_ annotation: HKMapViewAnnotation) {
        let mapPin: NMAMapMarker!
        
        let location = NMAGeoCoordinates(latitude: annotation.coordinate.latitude, longitude: annotation.coordinate.longitude)
        if annotation.icon == nil {
            mapPin = NMAMapMarker(geoCoordinates: location)
        }
        else {
            mapPin = NMAMapMarker(geoCoordinates: location, image: annotation.icon!)
            mapPin.anchorOffset = CGPoint(x: 0.0, y: -annotation.icon!.size.height / 2.0)
        }
        mapPin.isVisible = true
        
        self.mapView.add(mapObject: mapPin)
        
        annotations[mapPin.uniqueId()] = (mapPin: mapPin, data: annotation)
    }
    
    open func removeAllAnnotations() {
        self.annotations.values.forEach { (mapPin: NMAMapMarker, data: HKMapViewAnnotation) in
            self.mapView.remove(mapObject: mapPin)
        }
    }
    
    open func moveToLocation(_ coordinate: CLLocationCoordinate2D, animated: Bool) {
        var zoomLevel = self.mapView.zoomLevel
        if zoomLevel < NormalZoomLevel {
            zoomLevel = NormalZoomLevel
        }
        self.mapView.set(geoCenter: NMAGeoCoordinates(latitude: coordinate.latitude, longitude: coordinate.longitude), zoomLevel: zoomLevel, animation: animated ? .linear : .none)
    }
}
