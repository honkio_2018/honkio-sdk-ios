//
//  HKDevice.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/12/19.
//  Copyright © 2019 developer. All rights reserved.
//

#import "HKDevice.h"
#import "AbsFilter.h"

@implementation HKDevice


-(id)init {
    self = [super init];
    return self;
}

-(id)initFrom:(HKDevice*)device {
    self = [super init];
    if (self) {
        _device_id = device.device_id;
        _app = device.app;
        _token = device.token;
        _platform = device.platform;
        _name = device.name;
        _notifyType = device.notifyType;
        _manufacturer = device.manufacturer;
        _model = device.model;
        _osVersion = device.osVersion;
    }
    return self;
}

-(BOOL)isNotifyType:(NSString*)type {
    return [self.notifyType isEqualToString:type];
}

-(NSDictionary*) toParams {
    NSMutableDictionary* params = [NSMutableDictionary new];
    [AbsFilter applyIfNotNil:DEVICE_ID value:self.device_id forParams:params];
    [AbsFilter applyIfNotNil:DEVICE_APP_KEY value:self.app forParams:params];
    [AbsFilter applyIfNotNil:DEVICE_TOKEN_KEY value:self.token forParams:params];
    [AbsFilter applyIfNotNil:DEVICE_TYPE_KEY value:self.platform forParams:params];
    [AbsFilter applyIfNotNil:DEVICE_NAME_KEY value:self.name forParams:params];
    [AbsFilter applyIfNotNil:DEVICE_NOTIFY_KEY value:self.notifyType forParams:params];
    
    NSMutableDictionary* properties = [NSMutableDictionary new];
    [AbsFilter applyIfNotNil:DEVICE_MANUFACTURER_KEY value:self.manufacturer forParams:properties];
    [AbsFilter applyIfNotNil:DEVICE_MODEL_KEY value:self.model forParams:properties];
    [AbsFilter applyIfNotNil:DEVICE_OS_VERSION_KEY value:self.osVersion forParams:properties];
    
    params[@"properties"] = properties;
    
    return [NSDictionary dictionaryWithDictionary:params];
}

- (NSString *)description {
    return [NSString stringWithFormat: @"Device[id=%@,app=%@,token=%@,notify=%@,name=%@,type=%@]", self.device_id, self.app, self.token, self.notifyType, self.name, self.platform];
}

@end
