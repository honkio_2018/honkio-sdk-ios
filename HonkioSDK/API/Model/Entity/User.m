//
//  User.m
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/3/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import "User.h"
#import <UIKit/UIDevice.h>
#import "HKTouInfo.h"
#import "HKLog.h"

// ------------- User ---------------------------
@implementation User {
    NSString* currentDeviceIdString;
}

@synthesize defaultAccount;

-(UserAccount*) defaultAccount {
    if(self.accounts.count > 0)
        for (int i = 0; i < self.accounts.count; i++) {
            UserAccount* account = self.accounts[i];
            if (![account isHiden])
                return account;
        }
    return nil;
}

- (id)init
{
    self = [super init];
    if (self) {
        _isActive = YES;
        _isTempPasswordUsed = NO;
        
        NSUUID *deviceId = [[UIDevice currentDevice] identifierForVendor];
        currentDeviceIdString = [deviceId UUIDString];
        
        [HKLog d: [NSString stringWithFormat: @"Current device is %@", currentDeviceIdString]];
    }
    return self;
}

-(NSDictionary*)toParams
{
    NSMutableDictionary* params = [NSMutableDictionary new];
    [AbsFilter applyIfNotNil:USER_FIRSTNAME_KEY value:self.firstname forParams:params];
    [AbsFilter applyIfNotNil:USER_FIRSTNAME_KEY value:self.firstname forParams:params];
    [AbsFilter applyIfNotNil:USER_LASTNAME_KEY value:self.lastname forParams:params];
    [AbsFilter applyIfNotNil:USER_TELEPHONE_KEY value:self.phone forParams:params];
    [AbsFilter applyIfNotNil:USER_COUNTRY_KEY value:self.country forParams:params];
    [AbsFilter applyIfNotNil:USER_CITY_KEY value:self.city forParams:params];
    [AbsFilter applyIfNotNil:USER_ADDRESS1_KEY value:self.address1 forParams:params];
    [AbsFilter applyIfNotNil:USER_ADDRESS2_KEY value:self.address2 forParams:params];
    [AbsFilter applyIfNotNil:USER_SSN_KEY value:self.ssn forParams:params];
    [AbsFilter applyIfNotNil:USER_ZIP_KEY value:self.zip forParams:params];
    [AbsFilter applyIfNotNil:USER_TIMEZONE_KEY value:self.timezone forParams:params];
    [AbsFilter applyIfNotNil:USER_LANGUAGE_KEY value:self.language forParams:params];
    [AbsFilter applyIfNotNil:USER_KEYSTORE_KEY value:self.userKeystore forParams:params];
    [AbsFilter applyIfNotNil:USER_RECEIPT_FLAG_KEY value:self.isReceipt forParams:params];
    
    [AbsFilter applyIfNotNil:USER_PASSWORD_KEY value:self.password forParams:params];
    [AbsFilter applyIfNotNil:USER_EMAIL_KEY value:self.login forParams:params];
    
    return [NSDictionary dictionaryWithDictionary:params];
}

-(NSString*)name {
    NSString* firstName = [self firstname];
    NSString* lastName = [self lastname];
    
    return [NSString stringWithFormat:@"%@ %@", (firstName != nil ? firstName : @""), (lastName != nil ? lastName : @"")];
}

-(void)setDictValue:(NSString*)value forKey:(NSString*)key {
    if (!self.userKeystore)
        self.userKeystore = [NSMutableDictionary new];
    
    [self.userKeystore setValue:value forKey:key];
}
-(NSString*)getDictValue:(NSString*)key {
    if (self.userKeystore)
        return self.userKeystore[key];
    
    return nil;
}

-(BOOL)isHasAddress {
    return self.zip && self.zip.length > 0
            && self.address1 && self.address1.length > 0
            && self.city && self.city.length > 0
            && self.country && self.country.length > 0;
}

-(BOOL)isHasAnyAccount {
    for (UserAccount* account in self.accounts)
        if (account && ![account isHiden])
            return YES;
    
    return NO;
}

-(BOOL)isTouVersionObsolete:(AppInfo*)appInfo {
    return [self isTouVersionObsolete:appInfo touUid:TOU_KEY_DEFAULT];
}

-(BOOL)isTouVersionObsolete:(AppInfo*)appInfo touUid:(NSString*) touUid {
    return appInfo != nil && [self isTouVersionObsolete:[appInfo identityId] touUid:(touUid != nil ? touUid : TOU_KEY_DEFAULT) touInfo:[appInfo getTouInfo:touUid]];
}

-(BOOL)isTouVersionObsolete:(NSString*)appId touUid:(NSString*) touUid touInfo:(HKTouInfo*)touInfo {
    if (touInfo == nil || appId == nil || touInfo.version == 0) {
        return NO;
    }
    
    HKTouVersion* touVersion = [self getTouVersion:appId touUid:touUid];
    return touVersion == nil || touVersion.version < touInfo.version;
}

-(UserAccount*)emptyAccountNumber:(NSString*)type {
    for(UserAccount* accountInfo in self.accounts) {
        if([accountInfo typeIs:type] && [accountInfo.number isEqualToString:@"0"])
            return accountInfo;
    }
    
    return nil;
}

-(UserAccount*)findAccountByType:(NSString*)type
{
    for(UserAccount* accountInfo in self.accounts) {
        if([accountInfo typeIs:type])
            return accountInfo;
    }
    return nil;
}

-(UserAccount*)findAccountByType:(NSString*)type number:(NSString*)number {
    if (number) {
        for(UserAccount* accountInfo in self.accounts) {
            if([accountInfo typeIs:type] && [number isEqualToString:accountInfo.number])
                return accountInfo;
        }
        return nil;
    }
    else
        return [self findAccountByType:type];
}

-(void)setTouVersion:(NSString*)appId touUid:(NSString*)touUid version:(HKTouVersion*) version {
    if (_touVersions == nil) {
        _touVersions = [NSMutableDictionary new];
    }
    
    NSMutableDictionary<NSString*, HKTouVersion*>* appTou = _touVersions[appId];
    if (appTou == nil) {
        appTou = [NSMutableDictionary new];
        _touVersions[appId] = appTou;
    }
    
    appTou[touUid] = version;
}

-(HKTouVersion*)getTouVersion:(NSString*)appId touUid:(NSString*)touUid {
    return [[_touVersions objectForKey:appId] objectForKey:touUid];
}
-(HKTouVersion*)getTouVersion:(NSString*)appId {
    return [self getTouVersion:appId touUid:TOU_KEY_DEFAULT];
}

-(UserAccount*)bonus
{
    for(UserAccount* accountInfo in self.accounts) {
        if([accountInfo typeIs:ACCOUNT_TYPE_BONUS])
            return accountInfo;
    }
    return nil;
}

@end
