//
//  UserRoleDescriptionJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/5/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "UserRoleDescriptionJsonParser.h"

@implementation UserRoleDescriptionJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [UserRoleDescriptionJsonParser parseRoleDescription:dict];
}

+(UserRoleDescription*) parseRoleDescription:(NSDictionary*) dict {
    NSString* roleId = dict[@"_id"];
    NSString* name = dict[@"name"];
    
    if (roleId && name) {
        UserRoleDescription* description = [UserRoleDescription new];
        description.roleId = roleId;
        description.name = name;
        description.displayName = dict[@"display_name"];;
        description.desc = dict[@"description"];
        description.totalUsers = [dict[@"users_count"] intValue];
        return description;
    }
    return nil;
}

@end
