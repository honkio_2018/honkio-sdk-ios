//
//  ProductPaymentProcessModel.h
//  HonkioApi
//
//  Created by Shurygin Denis on 11/27/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "BasePaymentProcessModel.h"

@interface ProductPayParams : PayParams

@property (nonatomic) NSArray* products;

@end

@interface ProductPaymentProcessModel : BasePaymentProcessModel

-(void)pay:(Shop*)shop orderId:(NSString*)orderId account:(UserAccount*)account products:(NSArray*)products flags:(int)flags;
-(void)pay:(UserAccount*)account products:(NSArray*)products flags:(int)flags;
-(void)pay:(UserAccount*)account products:(NSArray*)products;

@end
