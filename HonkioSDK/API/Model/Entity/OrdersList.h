//
//  OrdersList.h
//  HonkioApi
//
//  Created by Shurygin Denis on 12/2/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderFilter.h"
#import "Order.h"

@interface OrdersList : NSObject

@property (nonatomic) NSArray<Order*>* list;
@property (nonatomic) long totalCount;

@end
