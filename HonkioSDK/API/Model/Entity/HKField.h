//
//  HKField.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/13/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HK_FIELD_TYPE_STRING  @"str"
#define HK_FIELD_TYPE_BOOLEAN @"bool"

@interface HKField : NSObject

@property (nonatomic) NSString* label;
@property (nonatomic) NSObject* value;
@property (nonatomic) NSString* valueLabel;

@property (nonatomic) NSString* type;
@property (nonatomic) BOOL isVisible;

@end
