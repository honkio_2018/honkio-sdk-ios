//
//  InventoryProduct.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "InventoryProduct.h"

@implementation InventoryProduct


-(id)initFrom:(InventoryProduct*) product {
    self = [super initFrom:product];
    
    self.inventoryId = product.inventoryId;
    self.stamp = product.stamp;
    
    return self;
}

@end
