//
//  HKUserAccountAddViewProtocol.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/4/18.
//  Copyright © 2018 developer. All rights reserved.
//

public protocol HKUserAccountAddViewProtocol: class {
    
    /// The account type to be used for creating as new payment account.
    var accountType : String { get set }
    
    /// The shop to be used for creating as new payment account.
    var shop : Shop? { get set }
}
