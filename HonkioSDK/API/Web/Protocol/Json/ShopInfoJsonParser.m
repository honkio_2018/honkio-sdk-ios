//
//  ShopInfoJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "ShopInfoJsonParser.h"
#import "ShopInfo.h"

@implementation ShopInfoJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    ShopInfo* shopInfo = [ShopInfo new];
    
    NSDictionary* shopDict = dict[@"shop"];
    shopInfo.shop = [[Shop alloc] init:shopDict];
    [BaseJsonResponseParser parseShop:shopInfo.shop dict:shopDict];
    
    shopInfo.merchant = [MerchantSimple new];
    [BaseJsonResponseParser parseMerchantSimple:shopInfo.merchant dict:dict[@"merchant"]];
    
    shopInfo.shop.merchant = shopInfo.merchant;
    
    return shopInfo;
}

@end
