//
//  HKBaseAssetsListViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/1/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

// TODO write docs
open class HKBaseAssetsListViewController: HKBaseLazyTableViewController {
    public var shopIdentity : Identity?
    public fileprivate(set) var filter : AssetFilter = AssetFilter()
    
    /**
     Loads the list of assets for specified shop by specified filter.
     - parameter responseHandler: The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
     - parameter count: The number of assets to be loaded from server.
     - parameter skip: The number of assets to be skipped on loading.
     */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(filter, count: count, skip: skip)
        
        HonkioApi.userAssetList(filter, shop:shopIdentity, flags: 0, handler: responseHandler)
    }
    
    /**
     Creates the list from the result of the Response
     - parameter responce: The Response instance within the result of processing.
     - Returns: A list of assets.
     */
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! HKAssetList).list
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! HKAsset), listItem, itemBinder))
        }
        return items
    }
    
    /**
     Gets the cell identifier for provided Asset.
     * Must be overriden.
     - parameter item: Order.
     - Returns: The identifier of cell.
     */
    open func cellIdentifier(_ item: HKAsset) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }
    
    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }
    
    /**
     Sets the filter params for the current filter.
     - parameter filter: The filter to be updated.
     - parameter count: The number of shops to be loaded from server.
     - parameter skip: The number of shops to be skipped on loading.
     */
    open func setupFilter(_ filter: AssetFilter, count: Int, skip: Int) {
        
        if let location = HonkioApi.deviceLocation() {
            filter.latitude = String(location.coordinate.latitude)
            filter.longitude = String(location.coordinate.longitude)
            filter.radius = nil
        }
        
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
}
