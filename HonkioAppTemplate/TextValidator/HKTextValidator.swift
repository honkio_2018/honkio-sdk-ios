//
//  HKTextValidator.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 2/19/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation
/**
The helper class containing the set of functions for check the correspondence with the certain rules.
*/
open class HKTextValidator {
    
    public typealias RuleType = (_ text: String?) -> Bool
    
    fileprivate let errorMessage : String!
    fileprivate let rool: RuleType
    
    /**
    Initialize the HKTextValidator with the provided error message and rule to be used.
    - parameter errorMessage: The string message to be shown if the rule is not satisfied.
    */
    public init(errorMessage: String, rool: @escaping RuleType) {
        self.errorMessage = errorMessage
        self.rool = rool
    }
    
    
    
    /**
     Validates the value of string
     - parameter text: The string value should be validated of.
     - Returns: True if the value satisfies the rule.
     */
    open func validate(string: String?) -> Bool {
        return self.rool(string)
    }
    
    /**
    Validates the value of text of the provided UITextField.
    - parameter textField: The UITextField the text value should be validated of.
    - Returns: True if the value satisfies the rule.
    */
    open func validate(_ textField: UITextField?) -> Bool {
        return validate(textField, errorMessage: self.errorMessage)
    }
    
    /**
    Validates the value of text of the provided UITextField and sets the provided error message text if validation failed.
    - parameter textField: The UITextField the text value should be validated of.
    - parameter errorMessage: The error message text to be set.
    - Returns: True if the value satisfies the rule.
    */
    open func validate(_ textField: UITextField?, errorMessage: String) -> Bool {
        let valid = self.validate(string: textField?.text)
        
        if valid {
            textField?.disableError()
        }
        else {
            textField?.setErrorMessage(errorMessage)
        }
        return valid
    }
    
    /**
    Validates the string value is not white space.
    - Returns: True if rule is satisfied.
    */
    public static func ruleNotEmpty() -> RuleType {
        return {(text) -> Bool in
            return text != nil && text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0
        }
    }
    
    /**
    Validates the string value is integer number.
    - Returns: True if rule is satisfied.
    */
    public static func ruleIntNumber() -> RuleType {
        return ruleIntNumber(min: nil, max: nil)
    }
    
    /**
     Validates the string value is integer number.
     - Returns: True if rule is satisfied.
     */
    public static func ruleIntNumber(min: Int?, max: Int?) -> RuleType {
        return {(text) -> Bool in
            if text != nil {
                if let value = Int(text!) {
                    if min != nil && value < min! {
                        return false
                    }
                    
                    if max != nil && value > max! {
                        return false
                    }
                    
                    return true;
                }
            }
            return false
        }
    }
    
    /**
    Validates the string value is Double number.
    - Returns: True if rule is satisfied.
    */
    public static func ruleDoubleNumber() -> RuleType {
        return { (text) -> Bool in
            if text != nil {
                // check (.) or (,) from local region formats
                if let _ = NumberFormatter().number(from: text!) {
                    return true;
                }
            }
            return false
        }
    }
    
    /**
     Validates the string value is Double number.
     - Returns: True if rule is satisfied.
     */
    public static func ruleDoubleNumber(min: Double?, max: Double?) -> RuleType {
        return {(text) -> Bool in
            if text != nil {
                if let value = NumberFormatter().number(from: text!)?.doubleValue {
                    if min != nil && value < min! {
                        return false
                    }
                    
                    if max != nil && value > max! {
                        return false
                    }
                    
                    return true;
                }
            }
            return false
        }
    }
    
    /**
    Validates the string value is not shorter then provided length.
    - parameter minLength: The number the string should be not shorter then.
    - Returns: True if rule is satisfied.
    */
    public static func ruleTextLength(_ minLength: Int) -> RuleType {
        return {(text) -> Bool in
            return text != nil && text!.count >= minLength
        }
    }
    
    /**
    Validates the string value is not shorter 2 symbols.
    - Returns: True if rule is satisfied.
    */
    public static func ruleName() -> RuleType {
        return HKTextValidator.ruleTextLength(2)
    }
    
    /**
    Validates the string value is phone number.
    - Returns: True if rule is satisfied.
    */
    public static func rulePhoneNumber() -> RuleType {
        return {(text) -> Bool in
            return text != nil && (text! =~ "(\\+[0-9]+[\\- \\.]*)?(\\([0-9]+\\)[\\- \\.]*)?([0-9][0-9\\- \\.]+[0-9])")
        }
    }
    
    /**
     Validates the string value is email.
     - Returns: True if rule is satisfied.
     */
    public static func ruleEmail() -> RuleType {
        return {(text) -> Bool in
            return text != nil && (text! =~ ".+@.+\\.[a-z]+")
        }
    }
    
    /**
    Validates the string value is SSN.
    - Returns: True if rule is satisfied.
    */
    public static func ruleSsn() -> RuleType {
        return {(text) -> Bool in
            return ValueUtils.checkFinlandSsn(text) || ValueUtils.checkItalianSsn(text)
        }
    }
    
    public static func ruleIban() -> RuleType {
        return {(text) -> Bool in
            return ValueUtils.checkIban(text)
        }
    }
}
