//
//  HKUserAccountVerifyVCStrategyDelegate.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 7/11/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

public protocol HKUserAccountVerifyVCStrategyDelegate: class {
    
    func userAccountVerifyError(_ error: ApiError)
    func userAccountVerifyComplete(_ response: Response)
    
}
