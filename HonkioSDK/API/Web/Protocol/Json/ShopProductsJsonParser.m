//
//  ShopProductsJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "ShopProductsJsonParser.h"
#import "ShopProducts.h"
#import "Product.h"
#import "Message.h"
#import "HonkioApi.h"

@implementation ShopProductsJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    ShopProducts* shopProducts = [ShopProducts new];
    shopProducts.shopId = [[message shopIdentity] identityId];
    if (!shopProducts.shopId)
        shopProducts.shopId = [[HonkioApi mainShopIdentity] identityId];
    
    NSMutableArray* products = [NSMutableArray new];
    NSArray* productList = dict[@"product"];
    if(productList && [productList isKindOfClass:[NSArray class]]) {
        for (NSDictionary* prodData in productList) {
            if([prodData isKindOfClass:[NSDictionary class]]) {
                id isActiveValue = prodData[@"active"];
                if(isActiveValue == nil || [isActiveValue boolValue]) {
                    Product* product= [[Product alloc] init:[prodData mutableCopy]];
                    
                    [BaseJsonResponseParser parseProduct:product dict:prodData];
                    
                    [products addObject:product];
                }
            }
        }
    }
    shopProducts.list = products;
    
    return shopProducts;
}

@end
