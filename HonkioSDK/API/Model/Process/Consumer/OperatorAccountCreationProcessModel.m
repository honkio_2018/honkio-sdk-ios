//
//  OperatorAccountCreationProcessModel.m
//  HonkioApi
//
//  Created by Shurygin Denis on 8/4/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "OperatorAccountCreationProcessModel.h"

@implementation OperatorAccountCreationProcessModel

-(id)initWithHandler:(ResponseHandler)handler {
    self = [super initWithAccount:[[UserAccount alloc] initWithType:ACCOUNT_TYPE_OPERATOR andNumber:ACCOUNT_NUMBER_ZERO] shop:nil handler:handler];
    self.isPaymentCheckEnabled = false;
    return self;
}

@end
