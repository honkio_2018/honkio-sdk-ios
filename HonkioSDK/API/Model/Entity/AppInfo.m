//
//  AppInfo.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/28/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "AppInfo.h"

@implementation AppInfo {
    
    NSMutableDictionary<NSString*, HKTouInfo*>* touInfoDict;
    
}

-(id)init {
    self = [super init];
    if (self) {
        [self instantiateProperties];
    }
    return self;
}

-(id)initWithId:(NSString*)identityId andPassword:(NSString*)password {
    self = [super initWithId:identityId andPassword:password];
    if (self) {
        [self instantiateProperties];
    }
    return self;
}

-(id)initFrom:(Identity*)identity {
    self = [super initWithFrom:identity];
    if (self) {
        [self instantiateProperties];
    }
    return self;
}

-(HKTouInfo*) getTouInfo {
    return [self getTouInfo: TOU_KEY_DEFAULT];
}

-(HKTouInfo*) getTouInfo:(NSString*) key {
    return touInfoDict[key != nil ? key : TOU_KEY_DEFAULT];
}

-(void) setTouInfo:(HKTouInfo*) touInfo {
    touInfoDict[touInfo.key != nil ? touInfo.key : TOU_KEY_DEFAULT] = touInfo;
}

-(void) instantiateProperties {
    touInfoDict = [NSMutableDictionary new];
}

@end
