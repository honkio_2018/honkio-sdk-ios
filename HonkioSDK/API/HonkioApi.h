//
//  HonkioApiNew.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "CWLSynthesizeSingleton.h"
#import "NSError+ApiError.h"
#import "StoreData.h"
#import "Task.h"
#import "ApiResponseHandler.h"
#import "Identity.h"
#import "ServerInfo.h"
#import "Shop.h"
#import "ShopInfo.h"
#import "Connection.h"
#import "User.h"
#import "ServerFile.h"
#import "UserPayment.h"
#import "ShopFilter.h"
#import "ShopFind.h"
#import "ShopProducts.h"
#import "BookedProduct.h"
#import "TransactionFilter.h"
#import "RPReachability.h"
#import "BoughtProduct.h"
#import "Product.h"
#import "ShopProducts.h"
#import "Transaction.h"
#import "Transactions.h"
#import "PushStorage.h"
#import "NotificationHelper.h"
#import "ProtocolFactory.h"
#import "OrderFilter.h"
#import "Order.h"
#import "OrdersList.h"
#import "UserRole.h"
#import "PinReason.h"
#import "NSString + Regex.h"
#import "ApiError.h"
#import "FeedbackFilter.h"
#import "UserComment.h"
#import "UserCommentsList.h"
#import "ValueUtils.h"
#import "ScheduleFilter.h"
#import "ChatMessage.h"
#import "ChatMessages.h"
#import "ChatMessageFilter.h"
#import "Event.h"
#import "Schedule.h"
#import "SplitScheduleFilter.h"
#import "AppInfo.h"
#import "Rate.h"
#import "Inventory.h"
#import "InventoryProduct.h"
#import "Tag.h"
#import "Applicant.h"
#import "ApplicantsList.h"
#import "ApplicantFilter.h"
#import "UserRoleDescription.h"
#import "UserRoleDescriptionsList.h"
#import "PushesList.h"
#import "PushFilter.h"
#import "UserRoleDescriptionsFilter.h"
#import "UserRolesList.h"
#import "ShopProductsFilter.h"
#import "Invitation.h"
#import "HKField.h"
#import "HKQrCode.h"
#import "HKStructure.h"
#import "HKAsset.h"
#import "HKAssetList.h"
#import "AssetFilter.h"
#import "WebServiceAsync.h"
#import "WebServiceExecutorInstanceProtocol.h"
#import "WebServiceExecutorInstance.h"
#import "WebServiceExecutor.h"
#import "UserAccessItem.h"
#import "UserAccessList.h"
#import "TagsList.h"
#import "MerchantShop.h"
#import "MerchantShopInfo.h"
#import "HKStructureList.h"
#import "Merchant.h"
#import "MerchantShopsList.h"
#import "RatingsFilter.h"
#import "RateList.h"
#import "HKMediaFileList.h"
#import "MediaFileFilter.h"
#import "MerchantProductsFilter.h"
#import "HKMediaUrl.h"
#import "CalendarEvent.h"
#import "CalendarSchedule.h"
#import "HKUserRoleStructure.h"
#import "HKUserRoleStructureList.h"
#import "HKTouInfo.h"
#import "HKLinksList.h"
#import "HKDevice.h"


#define HK_OBJECT_TYPE_ASSET @"asset"
#define HK_OBJECT_TYPE_SHOP @"shop"
#define HK_OBJECT_TYPE_USER_ROLE @"user_role"
#define HK_OBJECT_TYPE_ORDER @"order"

/**
 * This class contains a lot of static methods to work with
 * server. All methods for the requests has parameter "handler".<br>
 * Handler is an implementation of the <b>ResponseHandler</b>.
 * This protocol contains two parameters:<br>
 * <b>Response</b> not nil when the server returns response and this
 * response is not error;<br>
 * <b>NSError</b> not nil when server returns an error or not
 * all conditions are satisfied for sending the request.<br>
 * <br>
 * Requests execute in series but asynchronous for UI thread.<br>
 * <br>
 * This is helps don't lose OTP and don't freeze UI. The apps that used this
 * library don't care about OTP and other service parameters. OTP will saved and
 * used automatically.<br>
 * <br>
 * <b>Warning: </b><br>
 * Despite the fact that the tasks are executed sequentially, the order of
 * callbacks can be broken.
 */
@interface HonkioApi : NSObject

CWL_DECLARE_SINGLETON_FOR_CLASS(nonnull HonkioApi);

/**
 * Initialization of instance of API.<br>
 *
 * Without initialization all requests will return an error
 * <b>NO_INITIALIZED</b>.<br>
 *
 *  @param apiUrl          Url address of the server.
 *  @param appIdentity     Application identity.
 *  @param shop            Identity of the shop that will used as main shop
 */
+ (void)initInstanceWithUrl:(nonnull NSString*)apiUrl app:(nonnull Identity*)appIdentity;

/**
 * Initialization of instance of API with default url.<br>
 *
 * Without initialization all requests will return an error
 * <b>NO_INITIALIZED</b>.<br>
 *
 *  @param apiUrl          Url address of the server.
 *  @param appIdentity     Application identity.
 *  @param shop            Identity of the shop that will used as main shop
 */
+ (void)initInstanceWithApp:(nonnull Identity*)appIdentity;

/**
 * Initialization of API.<br>
 * <p>
 * On this request the API will load <b>"serverinfo"</b> and <b>"shopinfo"</b>.
 * <p>
 * Without initialization all requests will return an error
 * <b>NO_INITIALIZED</b>.<br>
 *
 *  @param flags           Flags for request. If contains FLAG_OFFLINE_MODE API will be used in offline mode.
 *  @param responseHandler The object that implement SimpleResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 *  @return Task protocol implementation.
 */
+ (nonnull id<Task>)loadServerData:(int)flags handler:(nonnull SimpleResponseHandler)responseHandler;

/**
 *  The request to load server info.
 *
 *  @param flags           Flags for request.
 *  @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 *  @return Task protocol implementation.
 */
+(nonnull id<Task>)serverGetInfo:(int)flags handler:(nonnull ResponseHandler)responseHandler;


/**
 *  The request to load server TOU.
 *
 *  @param flags           Flags for request.
 *  @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 *  @return Task protocol implementation.
 */
+(nonnull id<Task>)serverGetTou:(int)flags handler:(nonnull ResponseHandler)responseHandler;

/**
 *  The request to load app info.
 *
 *  @param flags           Flags for request.
 *  @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 *  @return Task protocol implementation.
 */
+(nonnull id<Task>)appGetInfo:(int)flags handler:(nonnull ResponseHandler)responseHandler;

/**
 * The request to load Shop info.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 * @param shopIdentity Identity of the Shop that will loaded.
 *
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)shopGetInfo:(nonnull Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to find shops by specified filter.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 * @param filter Filter to query specified shops.
 *
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)shopFind:(nullable ShopFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to find shops by filter and specified params.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param filter Filter to query specified shops.
 * @param params Special params that will insert into request.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)shopFind:(nullable ShopFilter*)filter params:(nullable NSDictionary*) params flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to find shops by id.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param id Id of the Shop.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)shopFindById:(nonnull NSString *)shopId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to find shops by QR-id.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param id QR-id of the Shop.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)shopFindBuyQrId:(nonnull NSString *)qrId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to load the list of products for main Shop.
 *
 * @param context Application context
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)shopGetProducts:(nullable ShopProductsFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to load the list of products for specified Shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shopIdentity Identity of the shop or nil of default shor should be used.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)shopGetProducts:(nullable ShopProductsFilter*)filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nonnull id<Task>)shopGetProductTags:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nonnull id<Task>)getScheduleProducts:(nullable ScheduleFilter*)filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(id<Task>)getSchedule:(nullable ScheduleFilter *)filter shop:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nonnull id<Task>)getSplitSchedule:(nullable SplitScheduleFilter*)filter
                                   shop:(nullable Identity*)shopIdentity
                                  flags:(int)flags
                                handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+ (nonnull id<Task>)shopGetComments:(nullable FeedbackFilter*)filter
                               shop:(nullable Identity*)shopIdentity
                              flags:(int)flags
                            handler:(nullable ResponseHandler)responseHandle;

/**
 * DEPRECATED!!!
 * Get comments for shop
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shopIdentity Identity of the shop which contains transaction or nil of default shor should be used.
 * @param flags Flags for request.
 *
 * @param params request params for keys [shop_id, role_id] 
 *
 *
 * @return Task protocol implementation.
 */
+ (nonnull id<Task>)shopGetComments:(nullable NSDictionary*)params
                              flags:(int)flags
                            handler:(nullable ResponseHandler)responseHandler;


/**
 * The request to set another status of transaction.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shopIdentity Identity of the shop which contains transaction or nil of default shor should be used.
 * @param transactionId Transaction ID which status would be changed.
 * @param status New status. {@link Response.Status}
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)transactionSetStatus:(nonnull NSString*)status transactionId:(nonnull NSString*)transactionId shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Check if API initialized.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 * @return True if API initialized, otherwise false.
 */
+(BOOL)checkInitialisation:(nullable ResponseHandler)handler;

/**
 * Check if action connection is valid.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 * @return YES/NO.
 */
+(BOOL)checkConnection:(nullable ResponseHandler)handler;

/**
 * Check if API initialized.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 * @return YES if API initialized, otherwise NO.
 */
+(BOOL)checkApi:(nullable ResponseHandler)handler;

//============================================================
// Chat     ==================================================
//============================================================
+(nonnull id<Task>)userGetChatMessages:(nullable ChatMessageFilter*)filter
                                 flags:(int)flags
                               handler:(nullable ResponseHandler)responseHandler;

+(nonnull id<Task>)userSendChatMessage:(nonnull ChatMessage*)chatMessage
                                 flags:(int)flags
                               handler:(nonnull ResponseHandler)responseHandler;

//============================================================
// User     ==================================================
//============================================================

/**
 * The request to load the products list of the Main Shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userGetProducts:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to load the products list of the Specified Shop.
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shopIdentity Identity of the specified shop or nil of default shor should be used.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userGetProducts:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 *  Checking opportunity for login with PIN code, using cached OTP.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 *  @return YES/NO
 */
+(BOOL)checkReLogin:(nullable ResponseHandler)handler;

/**
 * Request Logout. Request will clear cache and OTP.
 * @param flags Flag for the request.
 */
+(nullable id<Task>)logout:(int)flags handler:(nullable SimpleResponseHandler)responseHandler;

/**
 * The request to re-login via data from cache. After login will created new Connection object.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param pin PIN code to decode encrypted OTP.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)loginWithPin:(nonnull NSString*)pin flags:(int)flags handler:(nullable ResponseHandler)responseHandler;


/**
 * The request to login via OAuth token. After login will created new Connection object.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param token OAuth token.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)loginWithToken:(nonnull NSString*)token flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to repeated registration. Send this request only when previous request of the registration has status "reject".
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param user Personal information of the user for registration.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userReRegister:(nonnull User*)user flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to get access list for provided merchant ant its child merchants.
 *
 * @param parentMerchant Merchant id.
 * @param flags    Flags for request.
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 * @return Task protocol implementation.
 */
+(nullable id<Task>)userAccessList:(NSString *)parentMerchant flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to get access list only for provided merchant without its child merchants.
 *
 * @param merchant Merchant id.
 * @param flags    Flags for request.
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 * @return Task protocol implementation.
 */
+(nullable id<Task>)userMerchantAccessList:(NSString *)merchant flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to get personal information of the user.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userGet:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * The request to update personal information of the user.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param user User object that contains information to change.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userUpdate:(nonnull User*) user flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nullable id<Task>)userClearMediaUrl:(nonnull NSString*) urlId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nullable id<Task>)userGetMediaUrl:(nonnull NSString*) objectId type:(nonnull NSString*)objectType flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nullable id<Task>)userUpdateMedia:(nullable HKMediaUrl*)url data:(nonnull MessageBinary*)data flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nullable id<Task>)userMediaFilesList:(nullable MediaFileFilter*) filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to add user payment account into accounts list. This method adds account without any
 * initialization, use {@link AccountCreationProcessModel}
 * to correct account adding.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param account Account object with params to adding.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userAddAccount:(nonnull UserAccount*) account flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to editing user accounts.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param account The action with edited params.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userEditAccount:(nonnull UserAccount*) account flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to move user account.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param account The account that will moved.
 * @param newPosition New position for the account.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userMoveAccount:(nonnull UserAccount*) account position:(NSInteger)position flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to delete user accounts.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param account The action that will deleted.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userDeleteAccount:(nonnull UserAccount*) account flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 *  Request to get specified user role of the current user.
 *
 *  @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *  @param roleId          User's role id.
 *  @param flags           Flags of request.
 *
 *  @return Task protocol implementation.
 */
+(nonnull id<Task>)userGetRole:(nonnull NSString*)roleId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 *  Request to get specified user role by user id.
 *
 *  @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *  @param roleId          User's role id.
 *  @param userId          User's id.
 *  @param flags           Flags of request.
 *
 *  @return Task protocol implementation.
 */
+(nonnull id<Task>)userGetRole:(nonnull NSString *)roleId userId:(nullable NSString*)userId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
    
// TODO write docs
+(nonnull id<Task>)userGetRole:(nonnull NSString*)roleId userId:(nullable NSString*)userId shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+(nonnull id<Task>)userGetRoles:(nullable NSArray *)roleIds userId:(nullable NSString*)userId shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nonnull id<Task>)userGetRoles:(nullable NSArray *)roleIds userId:(nullable NSString*)userId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nonnull id<Task>)userGetRoles:(nullable NSArray *)roleIds flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 *  Request to set specified user role of the current user.
 *
 *  @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *  @param role            User's role id.
 *  @param flags           Flags of request.
 *
 *  @return Task protocol implementation.
 */
+(nonnull id<Task>)userSetRole:(nonnull UserRole*)role flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nonnull id<Task>)userSetRole:(nonnull UserRole*)role shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nonnull id<Task>)userDeleteRole:(nonnull NSString*) roleId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO write docs
+(nonnull id<Task>)userDeleteRole:(nonnull NSString*) roleId shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 *  Upload user photo.
 *
 *  @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *  @param data            Binary data of current photo.
 *  @param flags           Flags of request.
 *
 *  @return Task protocol implementation.
 */
+(nonnull id<Task>)userUpdatePhoto:(nonnull MessageBinary*)data flags:(int)flags responseHandler:(nullable ResponseHandler)responseHandler;

/**
 *  Check user password.
 *
 *  @param password        User password.
 *  @param flags           Flags of request.
 *  @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 *  @return Task protocol implementation.
 */
+ (nonnull id<Task>)userCheckPassword:(nonnull NSString*)password flags:(int)flags andRespone:(nonnull ResponseHandler)responseHandler;

/**
 * Request to change password of the current User. If old password is
 * wrong it will be handled like wrong pin.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param oldPassword Old password.
 * @param newPassword New password.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userUpdatePassword:(nonnull NSString*)oldPassword newPassword:(nonnull NSString*)newPassword flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to put or edit values form the special dictionary map of the User.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param dict HashMap that contains a key value pairs to write in to user dictionary map.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userUpdateDictValues:(nonnull NSDictionary*)dict flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to update device info or add one.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param device Device that will be added or updated.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userSetDevice:(nonnull HKDevice*)device flags:(int)flags handler:(nonnull ResponseHandler)responseHandler;

/**
 * Request to agree current version of Terms of Use.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)userTouAgree:(nullable NSString*)touUid flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to load some file from the server.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param fileName
 *            Name of file that will be loaded.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)userGetFile:(nonnull NSString*)fileName flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to ask server for new temporary password when user forgot his password.
 * Password will send on the User email.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param email User mail that used for login.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)userCreateTempPassword:(nonnull NSString*)login flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to re-query pending registration request.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param queryId
 *            Registration request ID.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)queryUserRegister:(nonnull NSString*)queryId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to get list of transactions for current user and main Shop by specified filter.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param filter
 *            Filter for request.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)userGetTransactions:(nullable TransactionFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to get list of transactions for current user and specified shop by specified filter.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shopIdentity
 *            Identity of the specified shop or nil of default shop should be used.
 * @param filter
 *            Filter for request.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)userGetTransactions:(nullable TransactionFilter*)filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to get list of orders for current user and main Shop by specified filter.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param filter
 *            Filter for request.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)userGetOrders:(nullable OrderFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to get list of transactions for current user and specified shop by specified filter.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shopIdentity
 *            Identity of the specified shop or nil of default shop should be used.
 * @param filter
 *            Filter for request.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+ (nonnull id<Task>)userGetOrders:(nullable OrderFilter*)filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;


/**
 *  Request to add the new order made by current user.
 *
 *  responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *  @param order           Order innstance will be set.
 *  @param flags           Flags for request.
 *
 *  @return Task protocol implementation.
 */
+ (nonnull id<Task>)userSetOrder:(nonnull Order*)order flags:(int)flags handler:(nullable ResponseHandler)responseHandler;


/**
 *  Request to add the new order made by current user.
 *
 *  @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *  @param shopIdentity    Identity of specified shop or nil of default shop should be used.
 *  @param order           Order instance will be set.
 *  @param flags           Flags for request.
 *
 *  @return Task protocol implementation.
 */
+ (nonnull id<Task>)userSetOrder:(nonnull Order*)order shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

+ (nullable id<Task>)userSetOrderApplication:(nonnull NSString*)applicationId status:(nonnull NSString*)status buildNewOrder:(BOOL)buildNewOrder shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO doc
+ (nonnull id<Task>)userSetOrderStatus:(nonnull NSString*)orderStatus orderId:(nonnull NSString*)orderId shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

// TODO doc
+ (nonnull id<Task>)userGetComments:(nullable FeedbackFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
+ (nonnull id<Task>)userGetComments:(nullable FeedbackFilter*)filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;
/**
 *  Request to vote for user and how he or she has done his or her task.
 *
 *  @param comment     User comment.
 *  @param flags       Flags for request.
 *  @param handler     The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 *  @return Task protocol implementation.
 */
+ (nonnull id<Task>)userAddComment:(nonnull UserComment*)comment flags:(int)flags handler:(nullable ResponseHandler)handler;
+ (nonnull id<Task>)userAddComment:(nonnull UserComment*)comment shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;

// TODO doc
+ (nullable id<Task>)userDonate:(nonnull UserAccount*)account amount:(double)amount email:(nonnull NSString*)userEmail message:(nonnull NSString*)desc shop:(nullable Identity*)shopIdentity flags:(int)flags response:(nullable ResponseHandler)handler;

// TODO doc
+ (nullable id<Task>)userAcceptInvitation:(nonnull NSString*)invitationid flags:(int)flags response:(nullable ResponseHandler)handler;

// TODO doc
+ (nullable id<Task>)getRate:(RatingsFilter *)filter flags:(int)flags response:(nullable ResponseHandler)handler;

// TODO doc
+ (nullable id<Task>)userVote:(UserComment *)userComment flags:(int)flags handler:(nullable ResponseHandler)handler;

/**
 *  Request to submit user to do specified task.
 *
 *  @param orderId      Order identifier.
 *  @param proposedTime Proposal time.
 *  @param flags        Flags for request.
 *
 *  @param handler      The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 *  @return Task protocol implementation.
 */
// TODO use another structure
+ (nonnull id<Task>)userApplyForOrder:(nonnull NSString *)orderId proposedTime:(nullable NSString *)proposedTime flags:(int)flags response:(nullable ResponseHandler)handler;


/**
 *  Request to cancel current user to do specified task.
 *
 *  @param orderId Order identifier.
 *  @param flags   Flags for request.
 *  @param handler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 *
 *  @return Task protocol implementation.
 */
// TODO use another structure
+ (nonnull id<Task>)userCancelForOrder:(nonnull NSString *)orderId flags:(int)flags response:(nullable ResponseHandler)handler;

//TODO write docs
+(nonnull id<Task>)userGetInventory:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nonnull id<Task>)userGetInventory:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nonnull id<Task>)userInventoryConvert:(nonnull NSString*)productId target:(nonnull NSString*)targetId flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nonnull id<Task>)userInventoryConvert:(nonnull NSString*)productId target:(nonnull NSString*)targetId shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;

/**
 * Request to verify SMS code. The request will not return result of
 * validation, you should re-query registration request via queryUserRegister.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param smsCode
 *            SMS code to validate phone number.
 *
 * @return
 *      Task protocol implementation.
 */
+(nullable id<Task>)smsVerify:(nonnull NSString*)smsCode handler:(nullable SimpleResponseHandler)responseHandler;

//TODO write docs
+(nullable id<Task>)orderGetApplicants:(nonnull ApplicantFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)handler;
//TODO write docs
+(nullable id<Task>)orderGetApplicants:(nonnull ApplicantFilter*)filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nullable id<Task>)userRoleDescriptionsList:(nonnull UserRoleDescriptionsFilter*) filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;
//TODO write docs
+(nullable id<Task>)userRoleDescription:(nonnull NSString*) code shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nullable id<Task>)structureList:(nonnull NSArray<NSString*>*) structureIds shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nullable id<Task>)assetStructureList:(nonnull NSArray<NSString*>*) structureIds shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nullable id<Task>)userStructureDescriptionList:(nonnull NSArray<NSString*>*) roleIds shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nullable id<Task>)userGetPushes:(nullable AbsListFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)handler;
//TODO write docs
+(nullable id<Task>)userSetPush:(BOOL)isReaded filter:(nullable PushFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nullable id<Task>)qrInfo:(nonnull NSString*)qrId flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(id<Task>)userSendAbuse:(NSString *)merchant linkId:(NSString *)linkId linkType:(NSString *)linkType comment:(NSString *)comment flags:(int)flags handler:(ResponseHandler)handler;

//TODO write docs
+(nullable id<Task>)userGetAsset:(nonnull NSString*)assetId flags:(int)flags handler:(nullable ResponseHandler)handler;

//TODO write docs
+(nullable id<Task>)userAssetList:(nonnull AssetFilter*)filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler;


// Payments =============\/=============

/**
 * Request for a payment in the main Shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param account
 *            User account that would be used for purchasing. <b>Not null</b>.
 * @param bookProducts
 *            List of the products that would to be buy.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)paymentRequest:(nullable NSArray*)bookProducts account:(nonnull UserAccount*)account flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request for a payment in the specified shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shop
 *            Shop that will be used for request or nil of default shop should be used.
 * @param account
 *            User account that would be used for purchasing. <b>Not null</b>.
 * @param bookProducts
 *            List of the products that would to be buy.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)paymentRequest:(nullable NSArray*)bookProducts account:(nonnull UserAccount*)account shop:(nullable Shop*)shop flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request for a payment in the specified shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shop
 *            Shop that will be used for request or nil of default shop should be used.
 * @param account
 *            User account that would be used for purchasing. <b>Not null</b>.
 * @param bookProducts
 *            List of the products that would to be buy.
 * @param params
 *            Additional params that will insert into request.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)paymentRequest:(nullable NSArray*)bookProducts account:(nonnull UserAccount*)account shop:(nullable Shop*)shop params:(nullable NSDictionary*)params flags:(int)flags handler:(nullable ResponseHandler)responseHandler;


/**
 *  Starts payment process.
 *
 *  @param responseHandler Instance of json server response parser.
 *  @param shop            Identity of the specified shop or nil of default shop should be used.
 *  @param orderId         Identifier of specified order.
 *  @param account         UserAccount of specified user.
 *  @param bookProducts    Array of booked products.
 *  @param params          Additional params that will insert into request.
 *  @param flags           Flags for request.
 *
 *  @return Task instance.
 */
+(nonnull id<Task>)paymentRequest:(nullable NSArray*)bookProducts account:(nonnull UserAccount*)account shop:(nullable Shop*)shop order:(nullable NSString*)orderId params:(nullable NSDictionary*)params flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Reques for a payment in the main Shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param account User account that would be used for purchasing. <b>Not null</b>.
 * @param amount Amount of money that would be spend.
 * @param currency Currency of the payment.
 * @param flags Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)paymentRequest:(double)amount currency:(nullable NSString*)currency account:(nonnull UserAccount*)account flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Requesting a payment on the specified shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shop Shop that will be used for request or nil of default shop should be used.
 * @param account User account that would be used for purchasing. <b>Not null</b>.
 * @param amount Amount of money that would be spend.
 * @param currency Currency of the payment.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)paymentRequest:(double)amount currency:(nullable NSString*)currency account:(nonnull UserAccount*)account shop:(nullable Shop*)shop flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Requesting a payment on the specified shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shop Shop that will be used for request or nil of default shop should be used.
 * @param account User account that would be used for purchasing. <b>Not null</b>.
 * @param amount Amount of money that would be spend.
 * @param currency Currency of the payment.
 * @param params Additional params that will insert into request.
 * @param flags Flags for request.
 *
 * @return Task protocol implementation.
 */
+(nonnull id<Task>)paymentRequest:(double)amount currency:(nullable NSString*)currency account:(nonnull UserAccount*)account shop:(nullable Shop*)shop params:(nullable NSDictionary*)params flags:(int)flags handler:(nullable ResponseHandler)responseHandler;


/**
 *  Requesting a payment on the specified shop.
 *
 *  @param responseHandler Instance of json server response parser.
 *  @param shop            Identity of the specified shop or nil of default shop should be used.
 *  @param orderId         Identifier of specified order.
 *  @param account         UserAccount of specified user.
 *  @param amount          Amount of money that would be spend.
 *  @param currency        Currency of the payment.
 *  @param params          Additional params that will insert into request.
 *  @param flags           Flags for request.
 *
 *  @return Task instance.
 */
+(nonnull id<Task>)paymentRequest:(double)amount currency:(nullable NSString*)currency account:(nonnull UserAccount*)account shop:(nullable Shop*)shop order:(nullable NSString*)orderId params:(nullable NSDictionary*)params flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to re-query pending payment request on the main Shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param queryId
 *            Payment request ID.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)queryPaymentRequest:(nonnull NSString*)queryId flags:(int)flags handler:(nullable ResponseHandler)responseHandler;

/**
 * Request to re-query pending payment request on the specified shop.
 *
 * @param responseHandler The object that implement ResponseHandler protocol. This object will be used to send callbacks in to UI thread.
 * @param shopIdentity
 *            Identity of the specified shop. <b>Not null</b>.
 * @param queryId
 *            Payment request ID.
 * @param flags
 *            Flags for request.
 *
 * @return
 *      Task protocol implementation.
 */
+(nonnull id<Task>)queryPaymentRequest:(nonnull NSString*)queryId shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler;


/**
 *  Check opportunity of the payment.
 *
 *  @param handler Instance of json server response parser.
 *  @param shop    Identity of the specified shop.
 *  @param account User account that would be used for purchasing.
 *
 *  @return True if payment is available, false otherwise.
 */
+(BOOL)checkPaymentAvailable:(nonnull UserAccount*)account shop:(nonnull Shop*)shop handler:(nullable ResponseHandler)responseHandler;

//============================================================
// Other   ==================================================
//============================================================

/**
 *  Method to build user request.
 *
 *  @param command        Name of command you want to execute.
 *  @param responseParser Instance of json server response parser.
 *  @param flags          Message flags.
 *
 *  @return Message instance.
 */
+(nonnull Message*)buildUserMessage:(nonnull NSString*)command responseParser:(nonnull id<ResponseParser>)responseParser flags:(int)flags;

/**
 * Gets Url address of the server defined in the initialization.
 * @return Url address of the server.
 */
+(nullable NSURL*)apiUrl;

/**
 * Ges App id defined in the initialization.
 * @return App identity.
 */
+(nullable Identity*)appIdentity;
    
/**
 * Gets Device info.
 * @return Device info or null if API initialization was no completed.
 */
+(nullable HKDevice*)device;
    
/**
 * Gets main shop Identity defined in the initialization.
 * @return Main shop Identity.
 */
+(nullable Identity*)mainShopIdentity;

/**
 * Gets the factory that define format of the requests and responses.
 * @return Implementation of ProtocolFactory protocol.
 * Default JsonProtocolFactory.
 */
+(nullable id<ProtocolFactory>)protocolFactory;

/**
 * Change Connection pin to another. PIN will be changed only if current PIN is correct.
 * @param newPin New PIN code.
 * @return YES if check of current PIN is success, NO otherwise.
 */
+(void)changePin:(nonnull NSString*)newPin;

/**
 * Change Connection pin to another. PIN will be changed only if current PIN is correct.
 * @param currentPin Current PIN code.
 * @param newPin New PIN code.
 * @return YES if check of current PIN is success, NO otherwise.
 */
+(BOOL)changePin:(nullable NSString*)currentPin newPin:(nonnull NSString*)newPin;

/**
 * Check if given PIN code is the PIN code of the Connection. If check will be failed [Connection increasePinFails] will called.
 * @param pin PIN code.
 * @return YES if Connection has the same PIN code or Connection PIN code is nil, NO otherwise.
 */
+(BOOL)checkPin:(nonnull NSString*)pin;

/**
 * Check API initialization.
 * @return True if API is initialized, false otherwise.
 */
+(BOOL)isInitialised;

/**
 * Check active Connection.
 * @return True if Active Connection not nil and is valid, false otherwise.
 */
+(BOOL)isConnected;

/**
 * Gets active Connection.
 * @return Active Connection.
 */
+(nullable Connection*)activeConnection;

/**
 * Gets server info.
 * @return Server info or nil if API initialization was no completed.
 */
+(nullable ServerInfo*)serverInfo;

// class property
@property (class, nonatomic, copy, readonly, getter=getSuitableLanguage) NSString *suitableLanguage;

/**
 * Gets app info.
 * @return App info or nil if API initialization was no completed.
 */
+(nullable AppInfo*)appInfo;

/**
 * Gets Main shop info.
 * @return Shop info or nil if API initialization was no completed.
 */
+(nullable ShopInfo*)mainShopInfo;

/**
 * Method to get last User data that was returned from the server
 * @return Last User data that was returned from the server or nil if cache doesn't contain User object.
 */
+(nullable User*)activeUser;

//+(NSTimeInterval)deltaServerTime;

/**
 * Gets Location of Device from LocationFinder.
 * @return Location of Device.
 */
+(nullable CLLocation*)deviceLocation;

/**
 *  Check a remote host for reachability.
 *
 *  @return NetworkStatus instance.
 */
+ (NetworkStatus)networkStatus;

/**
 *  Method to get the user's photo url by user identifier.
 *
 *  @param userId User identifier.
 *
 *  @return NSString instance.
 */
+ (nullable NSString *)getUserPhotoURL:(nonnull NSString *)userId;

@end
