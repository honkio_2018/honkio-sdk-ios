//
//  TransactionsJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/26/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "TransactionsJsonParser.h"
#import "OrderJsonParser.h"
#import "BoughtProduct.h"
#import "Transaction.h"
#import "Transactions.h"
#import "ApiDateFormatter.h"
#import "HKField.h"

@implementation TransactionsJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    Transactions* transactions = [Transactions new];
    
    transactions.timeStamp = [ApiDateFormatter dateFromServerString:dict[@"timestamp"]];
    
    NSArray* transactions_properties = dict[@"transaction"];
    if([transactions_properties isKindOfClass:[NSArray class]]) {
        NSMutableArray* list = [NSMutableArray new];
        for (NSDictionary* transaction_properties in transactions_properties) {
            if([transaction_properties isKindOfClass:[NSDictionary class]]) {
                [list addObject:[self parseTransaction:transaction_properties]];
            }
        }
        transactions.list = [NSArray arrayWithArray:list];
        
    }
    NSDictionary* ordersProperty = dict[@"orders"];
    if (ordersProperty != nil) {
        NSMutableDictionary* ordersDict = [NSMutableDictionary new];
        for (NSString* key in ordersProperty.allKeys) {
            NSDictionary* orderProperty = ordersProperty[key];
            if (orderProperty) {
                Order* order = [OrderJsonParser parseOrder:orderProperty];
                if (order) {
                    ordersDict[key] = order;
                }
            }
        }
        transactions.orders = ordersDict;
    }
    return transactions;
}

-(Transaction*)parseTransaction:(NSDictionary*)properties {
    Transaction* newTransaction = [Transaction new];
    
    newTransaction.type = properties[@"type"];
    newTransaction.transaction_id = properties[@"id"];
    newTransaction.orderId = properties[@"order"];
    newTransaction.status = properties[@"status"];
    NSDictionary* shop_properties = properties[@"shop"];
    if([shop_properties isKindOfClass:[NSDictionary class]]) {
        newTransaction.shopId = shop_properties[@"id"];
        newTransaction.shopName= shop_properties[@"name"];
        newTransaction.shopReference= shop_properties[@"reference"];
        newTransaction.shopReceipt= shop_properties[@"receipt"];
    }
    
    NSDictionary* merchant = properties[@"merchant"];
    if (merchant) {
        newTransaction.merchantName = merchant[@"name"];
    }
    
    NSArray* products_properties = properties[@"product"];
    if([products_properties isKindOfClass:[NSArray class]]) {
        NSMutableArray<BoughtProduct*>* products = [NSMutableArray new];
        for(NSDictionary* product_properties in products_properties) {
            if([product_properties isKindOfClass:[NSDictionary class]]) {
                BoughtProduct* product = [BoughtProduct new];
                
                [BaseJsonResponseParser parseBoughtProduct:product dict:product_properties];
                
                product.transaction = newTransaction;
                [products addObject:product];
            }
        }
        newTransaction.products = [NSArray arrayWithArray:products];
    }
    
    NSArray* messages_properties = properties[@"messages"];
    if([messages_properties isKindOfClass:[NSArray class]]) {
        NSMutableArray<MessageTransaction*>* messages = [NSMutableArray new];
        for(NSDictionary* messags_properties in messages_properties) {
            if([messags_properties isKindOfClass:[NSDictionary class]]) {
                MessageTransaction* message = [MessageTransaction new];
                
                message.from = messags_properties[@"from"];
                message.text = messags_properties[@"text"];
                message.timeStamp = [ApiDateFormatter dateFromServerString:messags_properties[@"timestamp"]];
                
                [messages addObject:message];
            }
        }
        newTransaction.messages = [NSArray arrayWithArray:messages];
    }
    
    newTransaction.account = [[UserAccount alloc] initWithType:properties[@"account_type"] andNumber:properties[@"account_number"]];
    newTransaction.currency = properties[@"currency"];
    newTransaction.amount = [properties[@"amount"] doubleValue];
    newTransaction.time =  [ApiDateFormatter dateFromServerString:properties[@"timestamp"]];
    
    newTransaction.accountExtra = [self parseFields:properties[@"account_extra"]];
    
    return newTransaction;
}

-(NSDictionary*)parseFields:(NSDictionary*)dict {
    if (!dict)
        return nil;
    
    NSMutableDictionary* fieldsDict = [NSMutableDictionary new];
    
    for (NSString* key in [dict allKeys]) {
        HKField* field = [self parseField:key dict:dict[key]];
        if (field) {
            fieldsDict[key] = field;
        }
    }
    
    return [NSDictionary dictionaryWithDictionary:fieldsDict];
}

-(HKField*)parseField:(NSString*)name dict:(NSDictionary*)dict {
    if (!name || !dict)
        return nil;
    
    NSString* type = dict[@"type"];
    if ([type length] == 0) {
        return nil;
        // TODO get type from name
    }
    
    if ([type length] == 0)
        type = HK_FIELD_TYPE_STRING;
    
    HKField* field = [HKField new];
    
    if ([type isEqualToString:HK_FIELD_TYPE_BOOLEAN]) {
        field.value = [NSNumber numberWithBool:[dict[@"value"] boolValue]];
    }
    else {
        field.value = dict[@"value"];
    }
    
    field.type = type;
    field.valueLabel = dict[@"value_label"];
    field.label = dict[@"label"];
    field.isVisible = dict[@"visible"] != nil ? [dict[@"visible"] boolValue] : true;
    
    return field;
}

@end
