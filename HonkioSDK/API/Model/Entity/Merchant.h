//
//  Merchant.h
//  HonkioSDK
//
//  Created by Mikhail Li on 28/04/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MerchantSimple.h"

@interface Merchant : MerchantSimple

@property (nonatomic) NSString* bankIban;
@property (nonatomic) NSString* bankName;
@property (nonatomic) NSString* bankSwift;

@property (nonatomic) double vat;
@property (nonatomic) NSString* vatNumber;

@property (nonatomic) NSString* email;
@property (nonatomic) NSString* phone;

@property (nonatomic) NSString* parentMerchant;

@property (nonatomic) BOOL isActive;

-(NSDictionary*)toParams;

@end
