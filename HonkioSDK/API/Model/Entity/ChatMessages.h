//
//  ChatMessages.h
//  HonkioSDK
//
//  Created by Dev on 01.09.16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChatMessage.h"

@class ChatMessageFilter;
@interface ChatMessages : NSObject

@property (nonatomic) NSString *text;

@property (nonatomic) NSArray<ChatMessage*> *list;
@property (nonatomic) ChatMessageFilter *filter;

@end
