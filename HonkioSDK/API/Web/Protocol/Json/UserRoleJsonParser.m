//
//  UserRoleJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 12/8/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "UserRoleJsonParser.h"
#import "HonkioApi.h"

@implementation UserRoleJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return [UserRoleJsonParser parseRole:dict];
}

+(UserRole*)parseRole:(NSDictionary*) dict {
    UserRole* role = [UserRole new];
    role.roleId = dict[@"role_id"];
    role.objectId = dict[@"id"];
    role.merchantId = dict[@"merchant"];
    role.roleName = dict[@"name"];
    role.roleDisplayName = dict[@"display_name"];
    role.roleDesc = dict[@"description"];
    
    role.userId = dict[@"user_id"];
    role.userFirstName = dict[@"user_first_name"];
    role.userLastName = dict[@"user_last_name"];
    
    role.userJobsTotal = dict[@"user_jobs_total"];
    role.userRating = dict[@"user_rating"];
    
    role.privateProperties = [[NSMutableDictionary alloc] initWithDictionary:dict[@"private_properties"]];
    role.publicProperties = [[NSMutableDictionary alloc] initWithDictionary:dict[@"public_properties"]];
    role.userContactData = [[NSMutableDictionary alloc] initWithDictionary:dict[@"user_contact_data"]];
    
    if (dict[@"active"])
        role.isActive = [dict[@"active"] boolValue];
    
    return role;
}

@end
