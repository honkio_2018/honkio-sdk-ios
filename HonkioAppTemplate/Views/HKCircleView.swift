//
//  HKCircleView.swift
//  WorkPilots
//
//  Created by developer on 2/20/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
The subclass for UIView. Represents the UIView within the rounded corners.
*/
open class HKCircleView: UIView {
    /**
    The property for the color of view border.
    */
    @objc open var borderColor: UIColor? {
        set {
            self.layer.borderWidth = 1
            self.layer.borderColor = newValue?.cgColor
        }
        
        get {
            if self.layer.borderColor != nil {
                return UIColor(cgColor: self.layer.borderColor!)
            }
            return nil
        }
    }
    /**
    Performs the counding of corners ans clips the view content to its bounds.
    */
    override open func layoutSubviews() {
        
        super.layoutSubviews()
        self.layer.cornerRadius = min(self.frame.width, self.frame.height) / 2
        self.clipsToBounds = true
    }
}
