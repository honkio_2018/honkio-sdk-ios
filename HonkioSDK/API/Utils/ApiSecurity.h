//
//  ApiSecurity.h
//  Honkio API
//
//  Created by Alexandr Kolganov on 2/5/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  The helper to work with encryption and decryption of strings.
 */
@interface ApiSecurity : NSObject

/**
 *  Get a random string.
 *
 *  @return Randomly generated string.
 */
+ (NSString*)getRandomString;


/**
 *  Convert all value items in dictionary to hash string
 *
 *  @param dict Dictionary of key-value items.
 *
 *  @return Hash string based on value items in dictionary.
 */
+ (NSString*)dictionaryToHashString:(NSDictionary*)dict;

/**
 *  Calculate hash for the current values set and put it in to the builder entity as name value pair.
 *
 *  @param key     Key for calculate hash.
 *  @param strData Calculatable data.
 *
 *  @return Calculated hash.
 */
+ (NSString*)calculateHashForKey:(NSString*)key andString:(NSString*)strData;

/**
 *  Calculate hash for the current values set and put it in to the builder entity as name value pair.
 *
 *  @param key      Key for calculate hash.
 *  @param dictData Dictionary of key-value items.
 *
 *  @return Hash string based on value items in dictionary.
 */
+ (NSString*)calculateHashForKey:(NSString*)key andDictionary:(NSDictionary*)dictData;

/**
 *  Encrypt a string with using a key.
 *
 *  @param string Current string.
 *  @param key    Current key.
 *
 *  @return Encrypted string.
 */
+ (NSString*)encryptString:(NSString*)string withKey:(NSString*)key;

/**
 *  Decrypt a string with using a key.
 *
 *  @param string Current string.
 *  @param key    Current key.
 *
 *  @return Decrypted string.
 */
+ (NSString*)decryptString:(NSString*)string withKey:(NSString*)key;

@end