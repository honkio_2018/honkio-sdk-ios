//
//  UserAccessItem.m
//  HonkioSDK
//
//  Created by Mikhail Li on 15/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "UserAccessItem.h"

@implementation UserAccessItem

-(BOOL)isHasAccess:(NSArray *)accessList {
    if (self.access == nil)
        return false;
    
    for (NSString* item in accessList) {
        if (![self.access containsObject:item])
            return false;
    }
    
    return true;
}

@end
