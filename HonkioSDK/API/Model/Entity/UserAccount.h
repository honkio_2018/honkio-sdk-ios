//
//  UserAccount.h
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/3/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsFilter.h"

/** Types of the User Account. */
#define ACCOUNT_TYPE_NETS       @"CREDITCARD"
#define ACCOUNT_TYPE_SEITATECH  @"SEITATECH"
#define ACCOUNT_TYPE_CREDITCARD @"CREDITCARD"
#define ACCOUNT_TYPE_OPERATOR   @"OPERATOR"
#define ACCOUNT_TYPE_INVOICE    @"MERCHANTINVOICE"
#define ACCOUNT_TYPE_BONUS      @"BONUS"
#define ACCOUNT_TYPE_VISA_TEST  @"VISA"
#define ACCOUNT_TYPE_X_BILLING  @"X_BILLING"

/** Number of account with zero value. */
#define ACCOUNT_NUMBER_ZERO   @"0"

/**
 * Data object that hold information about user payment account.
 * <br>Also this class extent AbsFilter and can be used like a Filter. typeIs:
 * <br> <br>Note: Changes in properties of this object does not affect the server data.
 */
@interface UserAccount : AbsFilter

// TODO write docs
-(id)initFrom:(UserAccount*) account;

@property (nonatomic) NSString* type;
@property (nonatomic) NSString* number;
@property (nonatomic, assign) NSUInteger index;

/**
 * YES if account has left, NO otherwise.
 */
@property (nonatomic) BOOL isHasLeft;
@property (nonatomic) double left;
@property (nonatomic) double limit;
@property (nonatomic) BOOL isSign;
@property (nonatomic) NSString* accountDescription;
@property (nonatomic) NSString* logoUrl;

@property (nonatomic) BOOL isEnabled;

/**
 * Init Account with specified type and number.
 *
 * @param type Type of account.
 * @param number Number of account.
 */
-(id)initWithType:(NSString*)type andNumber:(NSString*)number;

/**
 * Check if type of the account equal specified type from parameter.
 * @param type Specified type.
 * @return YES if type of the account equal specified type from parameter, NO otherwise.
 */
-(BOOL)typeIs:(NSString*)type;

/**
 * Check if account has enough left.
 * @param value Minimum left value.
 * @return True if account has enough left or account doesn't has left value, false otherwise.
 */
-(BOOL)enoughLeft:(double)value;

/**
 * Check if account shouldn't be shown for user.
 * YES if account shouldn't be shown for user, NO otherwise.
 */
-(BOOL)isHiden;

/**
 * Check if account is dummy account with zero number.
 * YES if account is dummy, NO otherwise.
 */
-(BOOL)isDummy;

@end
