//
//  ServerInfoJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "ServerInfoJsonParser.h"
#import "ServerInfo.h"
#import "ApiDateFormatter.h"

@implementation ServerInfoJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict{
    
    ServerInfo* serverInfo = [[ServerInfo alloc] init];
    
    NSDictionary* serverProp = dict[@"server"];
    
    if (serverProp) {
        
        serverInfo.version = [serverProp[@"version"] intValue];
        serverInfo.verifyURL = serverProp[@"verify_url"];
        serverInfo.timeZones = serverProp[@"timezone"];
        serverInfo.languages = serverProp[@"language"];
        serverInfo.touVersion = [serverProp[@"version_termsofuse"] intValue];
        serverInfo.userURL = serverProp[@"consumer_url"];
        serverInfo.oauthAuthorizeURL = serverProp[@"oauth_authorize_url"];
        serverInfo.oauthTokenURL = serverProp[@"oauth_token_url"];
        serverInfo.serviceFee = [serverProp[@"service_fee"] doubleValue];
        
        NSMutableArray* countriesArr = [NSMutableArray new];
        NSArray* countriesProp = serverProp[@"country"];
        
        if (countriesProp) {
            for(NSDictionary* countryProp in countriesProp) {
                CountryInfo* countryInfo = [CountryInfo new];
                
                countryInfo.name = countryProp[@"name"];
                countryInfo.locale = countryProp[@"locale"];
                countryInfo.phonePrefix = countryProp[@"telephone"];
                countryInfo.iso = countryProp[@"iso"];
                countryInfo.timezone = countryProp[@"timezone"];
                countryInfo.nameTranslated = countryProp[@"name2"];
                
                [countriesArr addObject:countryInfo];
            }
        }
        
        serverInfo.countries = [NSArray arrayWithArray:countriesArr];
        
        NSString * serverTime = dict[@"timestamp"];
        if(serverTime) {
            serverInfo.serverDate = [ApiDateFormatter dateFromServerString:serverTime];
        }
    }
    
    return serverInfo;
}

@end
