//
//  FeedbackJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 4/6/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "UserCommentsJsonParser.h"
#import "UserCommentJsonParser.h"
#import "UserRoleJsonParser.h"
#import "UserComment.h"
#import "UserRole.h"

@implementation UserCommentsJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {

    NSMutableDictionary* rolesDict = [self parseRolesArray:dict[@"user_roles"]];
    
    NSMutableArray* comments = [NSMutableArray new];
    NSArray* commentsJsonArray = dict[@"user_comments"];
    
    if (!commentsJsonArray) {
        commentsJsonArray = dict[@"rates"];
    }
    
    for (NSDictionary* commentJson in commentsJsonArray) {
        UserComment* comment = [UserCommentJsonParser parseComment:commentJson];
        if (comment) {
            comment.voter = [self getRole:commentJson[@"voter_role_id"] forUser:commentJson[@"voter_id"] from:rolesDict withDefaultFirstName:commentJson[@"voter_first_name"] andLastName:commentJson[@"voter_last_name"]];
            
            [comments addObject:comment];
        }
    }
    
    return comments;
}

-(NSMutableDictionary*)parseRolesArray:(NSArray*)array {
    
    NSMutableDictionary* rolesDict = [NSMutableDictionary new];
    
    if (array) {
        UserRoleJsonParser* roleParser = [UserRoleJsonParser new];
        
        for (NSDictionary* roleJson in array) {
            if (roleJson) {
                UserRole* role = [roleParser parseDictionary:nil dict:roleJson];
                if (role && role.userId && role.roleId) {
                    rolesDict[[self keyForRole:role.roleId ofUser:role.userId]] = role;
                }
            }
        }
    }
    
    return rolesDict;
}

-(UserRole*) getRole:(NSString*) roleId forUser:(NSString*) userId from:(NSMutableDictionary*) dict withDefaultFirstName:(NSString*)firstName andLastName:(NSString*)lastName {
    UserRole* role = dict[[self keyForRole:roleId ofUser:userId]];
    if (!role) {
        role = [UserRole new];
        role.roleId = roleId;
        role.userId = userId;
        role.userFirstName = firstName;
        role.userLastName = lastName;
        
        dict[[self keyForRole:role.roleId ofUser:role.userId]] = role;
    }
    return  role;
}

-(NSString*)keyForRole:(NSString*)roleId ofUser:(NSString*)userId {
    return [NSString stringWithFormat:@"%@|%@", roleId , userId];
}

@end
