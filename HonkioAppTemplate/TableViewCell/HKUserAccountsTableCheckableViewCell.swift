//
//  HKUserAccountsTableCheckableViewCell.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/29/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

class HKUserAccountsTableCheckableViewCell: HKUserAccountsTableViewCell {
    
    fileprivate(set) open lazy var switchView : UISwitch  = self.makeSwitchView()
    
    open override var isEnabled: Bool {
        didSet {
            if isEnabled {
                self.switchView.alpha = 1
                
                self.switchView.isEnabled = true
            }
            else {
                let alpha : CGFloat = 0.5
                
                self.switchView.alpha = alpha
                
                self.switchView.isEnabled = false
            }
        }
    }

    override func fillCell(_ account: UserAccount) {
        super.fillCell(account)
        self.switchView.isOn = HKPreferredUserAccountsManager.instance().isPreferred(account)
    }
    
    @objc func switchChanged(sender: UISwitch!) {
        if sender.isOn {
            HKPreferredUserAccountsManager.instance().add(self.account)
        }
        else {
            HKPreferredUserAccountsManager.instance().remove(self.account)
        }
    }
    
    override open func setupStyle() {
        super.setupStyle()
        
        selectionStyle = .none
    }
    
    override open func setupSubviews() {
        super.setupSubviews()
        
        contentView.addSubview(self.switchView)
    }
    
    override open func setupConstraints() {
        
        let views : [String : AnyObject] = [
            "cardImage" : self.cardImage,
            "cardTitle" : self.cardTitle,
            "cardDesc" : self.cardDesc,
            "switchView" : self.switchView
        ]
        
        let formatArray : [String] = [
            "H:|-8-[cardImage(30)]-5-[cardTitle]-5-[switchView(49)]-8-|",
            "H:[cardImage]-5-[cardDesc(==cardTitle)]",
            "V:|-8-[cardTitle]-0-[cardDesc]-8-|",
            "V:|-8-[cardImage(30)]-(>=8)-|"
        ]
        
        var constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        constraints.append(NSLayoutConstraint(item: self.switchView, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1, constant: 1))
        
        NSLayoutConstraint.activate(constraints)
    }
    
    fileprivate func makeSwitchView() -> UISwitch {
        let view = UISwitch()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.addTarget(self, action: #selector(switchChanged(sender:)), for: .valueChanged)
        
        return view
    }
}
