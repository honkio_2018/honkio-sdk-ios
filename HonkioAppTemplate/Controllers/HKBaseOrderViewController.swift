//
//  HKBaseOrderViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/11/19.
//  Copyright © 2019 developer. All rights reserved.
//

import UIKit

open class HKBaseOrderViewController: HKBaseLazyContentViewController<Order>, HKOrderViewProtocol {
    
    public var order: Order! {
        get { return self.contentItem }
        set { self.contentItem = newValue }
    }

    public func load(buId orderId: String!, shop: Identity? = nil, asMerchant: Bool = false) {
        BusyIndicator.shared.showProgressView(self.view)
        
        let filter = OrderFilter()
        filter.queryId = orderId
        filter.queryUnreadMessages = true
        filter.queryProductDetails = true
        
        let handler: ResponseHandler = {[weak self] (response) in
            if response.isStatusAccept() {
                if let ordersList = response.result as? OrdersList {
                    if ordersList.list.count > 0 {
                        self?.contentItem = ordersList.list[0]
                    }
                    else {
                        self?.loadingError(ApiError(code: ErrUserOrder.notFound.rawValue))
                    }
                }
            }
            else {
                self?.loadingError(response.anyError())
            }
        }
        
        if !asMerchant {
            HonkioApi.userGetOrders(filter, shop: shop, flags: 0, handler: handler)
        } else {
            HonkioMerchantApi.merchantGetOrders(filter, shop: shop, flags: 0, handler: handler)
        }
    }
    
    open func loadingError(_ error: ApiError) {
        preconditionFailure("Method buildViewController must be overridden")
    }
}
