//
//  ChatMessage.h
//  HonkioSDK
//
//  Created by Dev on 01.09.16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsFilter.h"

@interface ChatMessage : AbsFilter

@property (nonatomic) NSString *senderId;
@property (nonatomic) NSString *senderRole;
@property (nonatomic) NSString *senderFirstName;
@property (nonatomic) NSString *senderLastName;

@property (nonatomic) NSString *receiverId;
@property (nonatomic) NSString *receiverName;
@property (nonatomic) NSString *receiverRole;

@property (nonatomic) NSString *orderId;
@property (nonatomic) NSString *orderOwner;
@property (nonatomic) NSString *orderThirdPerson;

@property (nonatomic) NSString *text;

@property (nonatomic) NSString *shopId;
@property (nonatomic) NSString *shopName;

@property (nonatomic) NSDate* time;

// TODO write docs
-(instancetype)init;

// TODO write docs
-(instancetype)initFrom:(ChatMessage*)message;

// TODO write docs
-(NSString*) senderName;

@end
