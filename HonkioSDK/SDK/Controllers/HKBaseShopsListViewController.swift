//
//  HKBaseShopsListViewController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/1/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
 The base ViewController that provides base functionality for processing of ShopList
 */

open class HKBaseShopsListViewController: HKBaseLazyTableViewController {
    /// Represents the filter to be applied when shop list is to be loaded.
    open var filter : ShopFilter?

    /**
    Initiates loading of shop list.
    - parameter responseHandler: The handler to loading shop list.
    - parameter count: The number of shops to be loaded from server.
    - parameter skip: The number of shops to be skipped on loading.
    */
    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        if self.filter == nil {
            self.filter = ShopFilter()
        }
        self.setupFilter(filter!, count: count, skip: skip)
        
        HonkioApi.shopFind(filter, flags: 0, handler: responseHandler)
    }

    /**
     Creates the list from the result of the Response
    - Returns: Loaded list of shops in response converted to List.
    - parameter response: The Response object to be converted to List.
    */
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! ShopFind).list
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! Shop), listItem, itemBinder))
        }
        return items
    }

    /**
     Gets the cell indetifier for provided Shop.
     * Must be overriden.
     - parameter item: Shop.
     - Returns: The identifier of cell.
     */
    open func cellIdentifier(_ item: Shop) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }
    
    /**
     Gets cell bunder that define how cell should be filled from list item.
     * Must be overriden.
     - Returns: The cellBuilder instance that define how cell should be filled from list item.
     */
    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }

    /**
    Sets the filter for the shop list.
    - parameter filter: The filter is to be updated.
    - parameter count: The number of shops to loaded from server.
    - parameter skip: The number of shops to be skipped on loading.
    */
    open func setupFilter(_ filter: ShopFilter, count: Int, skip: Int) {
        
        if let location = HonkioApi.deviceLocation() {
            filter.latitude = String(location.coordinate.latitude)
            filter.longitude = String(location.coordinate.longitude)
            filter.radius = nil
        }
        
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
}
