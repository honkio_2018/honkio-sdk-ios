//
//  HKForgetConfirmAlertView.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 2/6/19.
//  Copyright © 2019 developer. All rights reserved.
//

import UIKit

class HKForgetConfirmAlertView: UIView {
    
    var backgroundView = UIView()
    var dialogView = UIView()
    var okButton: UIButton!
    
    private var okAction: (() -> Void)!
    
    convenience init(okAction: @escaping (() -> Void)) {
        self.init(frame: UIScreen.main.bounds)
        self.okAction = okAction
        initialize()
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize() {
        dialogView.clipsToBounds = true
        
        backgroundView.frame = frame
        backgroundView.backgroundColor = UIColor.black
        backgroundView.alpha = 0.6
        backgroundView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancelAction(_:))))
        addSubview(backgroundView)
  
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
        titleLabel.text = "hk_screen_settings_gdpr_dlg_forget_title".localized
        titleLabel.textAlignment = .center
        dialogView.addSubview(titleLabel)
        
        let separator1LineView = UIView()
        separator1LineView.translatesAutoresizingMaskIntoConstraints = false
        separator1LineView.backgroundColor = UIColor.groupTableViewBackground
        dialogView.addSubview(separator1LineView)
        
        let messageLabel = UILabel()
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.lineBreakMode = .byWordWrapping
        messageLabel.numberOfLines = 0
        messageLabel.font = UIFont.systemFont(ofSize: 14)
        messageLabel.text = "hk_screen_settings_gdpr_dlg_forget_message".localized
        messageLabel.textAlignment = .left
        dialogView.addSubview(messageLabel)
        
        let agreeSwitch = UISwitch()
        agreeSwitch.translatesAutoresizingMaskIntoConstraints = false
        agreeSwitch.addTarget(self, action: #selector(agreeSwitchChanged(_:)), for: .valueChanged)
        dialogView.addSubview(agreeSwitch)
        
        let agreeLabel = UILabel()
        agreeLabel.translatesAutoresizingMaskIntoConstraints = false
        agreeLabel.lineBreakMode = .byWordWrapping
        agreeLabel.numberOfLines = 0
        agreeLabel.font = UIFont.systemFont(ofSize: 14)
        agreeLabel.text = "hk_screen_settings_gdpr_dlg_forget_check".localized
        agreeLabel.textAlignment = .left
        dialogView.addSubview(agreeLabel)
        
        let separator2LineView = UIView()
        separator2LineView.translatesAutoresizingMaskIntoConstraints = false
        separator2LineView.backgroundColor = UIColor.groupTableViewBackground
        dialogView.addSubview(separator2LineView)
        
        okButton = UIButton(type: .system)
        okButton.translatesAutoresizingMaskIntoConstraints = false
        okButton.setTitle("hk_screen_settings_gdpr_btn_forget".localized, for: .normal)
        okButton.contentHorizontalAlignment = .center
        okButton.addTarget(self, action: #selector(okAction(_:)), for: .touchUpInside)
        
        dialogView.addSubview(okButton)
        
        let cancelButton = UIButton(type: .system)
        cancelButton.translatesAutoresizingMaskIntoConstraints = false
        cancelButton.setTitle("dlg_common_button_cancel".localized, for: .normal)
        cancelButton.addTarget(self, action: #selector(cancelAction(_:)), for: .touchUpInside)
        dialogView.addSubview(cancelButton)


        dialogView.frame.origin = CGPoint(x: 32, y: frame.height)
        dialogView.frame.size = CGSize(width: frame.width-64, height: 200)
        dialogView.backgroundColor = UIColor.white
        dialogView.layer.cornerRadius = 6
        addSubview(dialogView)
        
        let views : [String : AnyObject] = [
            "titleLabel"        : titleLabel,
            "separator1LineView" : separator1LineView,
            "separator2LineView" : separator2LineView,
            "messageLabel"      : messageLabel,
            "agreeSwitch"       : agreeSwitch,
            "agreeLabel"        : agreeLabel,
            "okButton"          : okButton,
            "cancelButton"      : cancelButton
        ]
        
        let formatArray : [String] = [
            "H:|-16-[titleLabel]-16-|",
            "H:|-8-[separator1LineView]-8-|",
            "H:|-16-[messageLabel]-16-|",
            "H:|-16-[agreeSwitch]-16-[agreeLabel]-8-|",
            "H:|-8-[separator2LineView]-8-|",
            "H:|-16-[okButton]-8-[cancelButton]-16-|",
            "V:|-8-[titleLabel]-8-[separator1LineView(2)]-8-[messageLabel]-16-[agreeSwitch]-(>=8)-[separator2LineView(2)]-8-[okButton]-8-|",
            "V:[cancelButton(==okButton)]-8-|"
        ]
        
        var constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        constraints.append(NSLayoutConstraint.constraintsCenterY(agreeLabel, toItem: agreeSwitch, multiplier: 1.0, constant: 1.0))
        constraints.append(NSLayoutConstraint.init(item: okButton, attribute: .width, relatedBy: .equal, toItem: cancelButton, attribute: .width, multiplier: 1.0, constant: 1.0))
        
        NSLayoutConstraint.activate(constraints)
        
        
        agreeSwitchChanged(agreeSwitch)
    }
    
    @IBAction func cancelAction(_ sender: Any?){
        dismiss(animated: true)
    }
    
    @IBAction func agreeSwitchChanged(_ sender: UISwitch?) {
        if sender?.isOn == true {
//            self.okButton.alpha = 1.0
            self.okButton.isEnabled = true
        }
        else {
//            self.okButton.alpha = 0.5
            self.okButton.isEnabled = false
        }
    }
    
    @IBAction func okAction(_ sender: Any?) {
        self.okAction()
        dismiss(animated: true)
    }

    func show(animated:Bool){
        self.backgroundView.alpha = 0
        self.dialogView.center = CGPoint(x: self.center.x, y: self.frame.height + self.dialogView.frame.height/2)
        UIApplication.shared.delegate?.window??.rootViewController?.view.addSubview(self)
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 0.66
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 10, options: UIViewAnimationOptions(rawValue: 0), animations: {
                self.dialogView.center  = self.center
            }, completion: { (completed) in
                
            })
        }else{
            self.backgroundView.alpha = 0.66
            self.dialogView.center  = self.center
        }
    }
    
    func dismiss(animated:Bool){
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                self.backgroundView.alpha = 0
            }, completion: { (completed) in
                
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIViewAnimationOptions(rawValue: 0), animations: {
                self.dialogView.center = CGPoint(x: self.center.x, y: self.frame.height + self.dialogView.frame.height/2)
            }, completion: { (completed) in
                self.removeFromSuperview()
            })
        }else{
            self.removeFromSuperview()
        }
        
    }
}
