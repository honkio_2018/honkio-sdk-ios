//
//  ServerFile.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "ServerFile.h"

@implementation ServerFile

-(NSString*)dataToString {
    return [[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding];
}

@end
