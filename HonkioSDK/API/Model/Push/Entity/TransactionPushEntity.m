//
//  TransactionPush.m
//  HonkioApi
//
//  Created by Shurygin Denis on 2/12/15.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import "TransactionPushEntity.h"
#import "Push.h"

@implementation TransactionPushEntity

-(id)initWithContent:(NSDictionary*)content {
    return [super initWithType:PUSH_TYPE_TRANSACTION andContent:content];
}

-(NSString*) value1 {
    return self.transactionId;
}

-(NSString*) value2 {
    return self.shopId;
}

@end
