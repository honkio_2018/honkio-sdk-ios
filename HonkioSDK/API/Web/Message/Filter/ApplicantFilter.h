//
//  ApplicantFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/9/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "AbsListFilter.h"

@interface ApplicantFilter : AbsListFilter

@property (nonatomic) NSString* orderId;
@property (nonatomic) NSString* queryRole;

@end
