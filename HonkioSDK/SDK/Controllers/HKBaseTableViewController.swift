//
//  HKBaseTableViewController.swift
//  WorkPilots
//
//  Created by developer on 1/18/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

// TODO doc write about itemsSource

/**
HKBaseTableViewController is the class for working with UITableView. Provides basic functionality for finding and manipulating table data.
+ SeeAlso: HKBaseViewController
*/

open class HKBaseTableViewController: HKBaseViewController, UITableViewDataSource, UITableViewDelegate {
    
    public typealias HKTableViewItemInitializer = ((_ viewController: UIViewController, _ cell: UITableViewCell, _ item: Any?) -> Void)
    public typealias HKTableViewItem = (identifierOrNibName: String, item: Any?, initializer: HKTableViewItemInitializer?)
    public typealias HKTableViewSection = HKCollectionSection<HKTableViewItem>

    /// Containes the sections the table is to be populated with.
    open var itemsSource: [HKTableViewSection] = []

    /**
    - Returns: Number of sections in itemsSource.
    - parameter tableView: The calling table.
    */
    open func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.itemsSource.count
    }

    /**
    - Returns: Number of items in section.
    - parameter tableView: The calling table.
    - parameter section: The index of section.
    */
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.itemsSource[section].items.count
    }

    /**
    Tries to obtaine the cell, if failed then creates new and populate.
    - Returns: Populated table cell.
    */
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = self.itemsSource[(indexPath as IndexPath).section]
        let item = section.items[(indexPath as IndexPath).row]
        var cell : UITableViewCell!
        
        if !item.identifierOrNibName.isEmpty {
            
            cell = tableView.dequeueReusableCell(withIdentifier: item.identifierOrNibName)
            if cell == nil {
                
                cell = Bundle.main.loadNibNamed(item.identifierOrNibName, owner: self, options: nil)!.last as? UITableViewCell
            }
        }
        
        if cell == nil {
            cell = UITableViewCell()
        }
        
        item.initializer?(self, cell, item.item)
        
        cell.needsUpdateConstraints()
        cell.updateConstraintsIfNeeded()
        
        return cell
    }
    
    open func sourceItem(_ indexPath: IndexPath) -> HKTableViewItem? {
        if itemsSource.count > indexPath.section {
            if itemsSource[indexPath.section].items.count > indexPath.row {
                return itemsSource[indexPath.section].items[indexPath.row]
            }
        }
        return nil
    }

    /**
    Tries to find the cell by identifier.
    - parameter identifier: Identifier of a cell the IndexPath is to be found for.
    - Returns: IndexPath.
    */
    open func findCellByIdentifer(_ identifer: String) -> IndexPath? {
        for section in 0..<self.itemsSource.count {
            for row in 0..<self.itemsSource[section].items.count {
                if self.itemsSource[section].items[row].identifierOrNibName == identifer {
                    return IndexPath(row: row, section: section)
                }
            }
        }
        return nil
    }
}
