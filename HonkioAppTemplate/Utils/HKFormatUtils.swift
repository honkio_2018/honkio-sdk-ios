//
//  HKFormatUtils.swift
//  HonkioSDK
//
//  Created by Dev on 30.08.16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation
import UIKit
/**
The class for formatting values to strings.
*/
open class HKFormatUtils: NSObject {
    /**
    Formats the provided Int value to date string.
    - parameter timeMillis: The value of time in milliseconds.
    - Returns: The String value formatted by localized days, hours, minutes values.
    */
    open class func formatTime(_ timeMillis : Int) -> String {
        
        let seconds = (timeMillis / 1000) % 60
        let minutes = (timeMillis / (1000 * 60)) % 60
        let hours = (timeMillis / 1000 - minutes * 60 - seconds) / 3600
        let days = (timeMillis / 1000 - hours * 3600 - minutes * 60 - seconds) / 86400
    
        var stringBuilder : String = ""
        
        if days > 0 {
            stringBuilder += "\(days) \("capt_days_abr".localized)"
        }
        
        if hours > 0 {
            if stringBuilder.count > 0 {
                stringBuilder += " "
            }
            
            stringBuilder += "\(hours) \("capt_hours_abr".localized)"
        }
        
        if minutes > 0 {
            if stringBuilder.count > 0 {
                stringBuilder += " "
            }
            
            stringBuilder += "\(minutes) \("capt_minuts_abr".localized)"
        }
        
        if stringBuilder.count == 0 {
            stringBuilder += "0"
        }
        
        return stringBuilder
    }
}
