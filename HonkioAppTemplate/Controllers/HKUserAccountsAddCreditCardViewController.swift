//
//  AddCreditCardViewController.swift
//  WorkPilots
//
//  Created by developer on 12/28/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit
/**
The view controller managing the adding of new credit card payment method.
*/
open class HKUserAccountsAddCreditCardViewController: HKBaseUserAccountsAddCreditCardViewController {

    /**
    On viewDidLoad sets the title to "screen_title_add_creditcard".localized value.
    */
    open override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "screen_title_add_creditcard".localized
    }

    /**
    Called if the veb view failed to load frame. If the error was not handled by application then the custom
    alert will be shown within the error description.
    - parameter webView: The web view failed to load the frame.
    - parameter error: The NSError object describing the problem.
    */
    open func webView(_ webView: UIWebView, didFailLoadWithError error: NSError?) {
        CustomAlertView(error: error, okAction: nil).show()
    }

    /**
    Tries handle the error. If error was not handle by the application the custom alert will be shown withing the
    action of popping navigation controller.
    - Returns: true.
    */
    open override func handleError(_ response: Response) -> Bool {
        let action = {
            self.notifyCompletionDelegate()
            return
        }
        if !AppErrors.handleError(self, response: response, fallAction: action) {
            
            CustomAlertView(title: "pay_creditcard_add_dlg_failed_title".localized, message: "pay_creditcard_add_dlg_failed_message".localized, cancelButtonTitle: "dlg_common_button_ok".localized, cancelButtonAction: action).show()
        }
        
        return true;
    }

    /**
    Shows progress indicator.
    - parameter view: The view the progress indicator is attached to.
    */
    open override func showProgress(_ view: UIView) {
        BusyIndicator.shared.showProgressView(self.view)
    }

    /**
    Hides progress indicator
    */
    open override func hideProgress() {
        BusyIndicator.shared.hideProgressView()
    }
}
