//
//  Inventory.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InventoryProduct.h"

@class InventoryProduct;

@interface Inventory : NSObject

@property (nonatomic, strong) NSArray<InventoryProduct*>*  list;

-(InventoryProduct*)findProductById:(NSString*)productId;
-(InventoryProduct*)findProductById:(NSString*)productId onlyValid:(BOOL)onlyValid;

-(InventoryProduct*)findGroupedProductById:(NSString*)productId;

-(InventoryProduct*)findProductForTarget:(NSString*)targetId;

-(NSDate*)expireDate;

-(NSArray*)groupByProduct;

@end
