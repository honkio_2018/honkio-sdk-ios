//
//  HKModelEntity.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 5/17/18.
//  Copyright © 2018 developer. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface HKModelEntity : NSObject

-(instancetype)init;
-(instancetype)initFromEntity:(HKModelEntity*) entity;
-(instancetype)init:(NSDictionary*) propertiesDict;

-(id) getProperty:(NSString*) key;
-(void) setProperty:(NSString*) key value:(id) value;

@end
