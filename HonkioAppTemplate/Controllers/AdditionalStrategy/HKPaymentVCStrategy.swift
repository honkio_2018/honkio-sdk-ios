//
//  HKPaymentVCStrategy.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 4/6/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
* HKPaymentVCStrategy is a class should be used for payment perfornig.
* It hooks up PIN enter/cancel events, and starts payments.
* This class should not be instaciated due to prevent incorrect or unintended error processing.
*/

open class HKPaymentVCStrategy: HKBasePaymentVCStrategy, HKUserAccountSelectionVCStrategyDelegate, HKPinRequestVCStrategyDelegate {
    
    public enum State {
        case paymentCalled
        case accountAsked
        case accountSelected
        case paymentRequestWillStart
        case pinAsked
        case pinEntered
        
        case completed
        case aborted
        case error
    }
    
    open var callbacks : HKPaymentVCStrategyCallback? {
        set {
            _callbacks = newValue
        }
        
        get {
            if _callbacks == nil {
                return self.parent as? HKPaymentVCStrategyCallback
            }
            return _callbacks
        }
    }
    fileprivate var _callbacks : HKPaymentVCStrategyCallback?
    
    fileprivate var pinRequestStrategy : HKPinRequestVCStrategy?
    
    open override func paymentRequest(_ shop: Shop?, orderId: String?, products: [BookedProduct], flags: Int) {
        self.notifyStateChanged(.paymentCalled)
        super.paymentRequest(shop, orderId: orderId, products: products, flags: flags)
    }
    
    open override func paymentRequest(_ shop: Shop?, orderId: String?, amount: Double, currency: String?, flags: Int) {
        self.notifyStateChanged(.paymentCalled)
        super.paymentRequest(shop, orderId: orderId, amount: amount, currency: currency, flags: flags)
    }
    
    override func paymentWillStart(_ payParams: PayParams) {
        self.notifyStateChanged(.paymentRequestWillStart)
        super.paymentWillStart(payParams)
    }

    override func paymentDidComplete(_ response: Response, payParams: UserPayment) {
        self.notifyStateChanged(.completed)
        super.paymentDidComplete(response, payParams: payParams)
    }
    
    open override func onError(_ model: BaseProcessModel!, response: Response!) {
        self.notifyStateChanged(.error)
        super.onError(model, response: response)
    }
    
    open override func abort() {
        self.notifyStateChanged(.aborted)
        super.abort()
    }
    
    /**
    * It to be called when the PIN code has to be requested.
    - parameter reason: Defines the reason of entering pin code.
    */
    override open func requestPin(_ reason: Int) {
        self.notifyStateChanged(.pinAsked)
        if self.pinRequestStrategy == nil {
            self.pinRequestStrategy = HKPinRequestVCStrategy(parent: self.parent)
            self.pinRequestStrategy?.delegate = self
        }
        
        self.pinRequestStrategy?.requestPin(reason, message: "pin_reason_purchare".localized)
    }

    /**
    * Is to be used as notifier that the payment process can not be completed because user doesn't have payment account.
    */
    override open func noPaymentAccount(_ payParams: PayParams) {
        self.notifyStateChanged(.accountAsked)
        
        let accountSelectStrategy = HKUserAccountSelectionVCStrategy(parent: self.parent)
        accountSelectStrategy.delegate = self
        accountSelectStrategy.removeOnCompletion = true
        accountSelectStrategy.disallowedAccounts = payParams.disallowedAccounts as? Set<String>
        
        accountSelectStrategy.selectAccount(payParams.shop, payParams.amount, payParams.currency)
    }
    
    public func pinEntered(_ strategy: HKPinRequestVCStrategy, _ pinCode: String!, _ reason: Int!) {
        self.notifyStateChanged(.pinEntered)
        self.pinEntered(pinCode, reason: reason)
    }
    
    public func pinCanceled(_ strategy: HKPinRequestVCStrategy, _ reason: Int!) {
        self.pinCanceled(reason)
    }
    
    open func acountDidSelect(_ controller: HKUserAccountSelectionVCStrategy, account: UserAccount?) {
        if account != nil {
            self.notifyStateChanged(.accountSelected)
            self.runPaymentIfOk(account)
        }
        else {
            self.onError(nil, response: Response.withError(ErrApi.canceledByUser.rawValue, desc: nil))
        }
    }
    
    private func notifyStateChanged(_ state: State) {
        self.callbacks?.paymentStateDidChange(self, state: state)
    }
}
