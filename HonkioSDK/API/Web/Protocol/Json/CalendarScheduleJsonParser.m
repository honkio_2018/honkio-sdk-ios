//
//  CalendarScheduleJsonParser.m
//  HonkioSDK
//
//  Created by Mikhail Li on 27/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "CalendarScheduleJsonParser.h"
#import "CalendarEventJsonParser.h"
#import "CalendarSchedule.h"
#import "CalendarEvent.h"
#import "ScheduleFilter.h"

@implementation CalendarScheduleJsonParser
 
-(CalendarSchedule *)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    NSMutableArray<CalendarEvent *> *list = [NSMutableArray new];
    
    NSArray *eventsJsonArray = dict[@"schedule"];
    if (eventsJsonArray) {
        for (NSDictionary *eventJson in eventsJsonArray) {
            [list addObject:[CalendarEventJsonParser parseCalendarEvent:eventJson]];
        }
    }
    
    CalendarSchedule *schedule = [CalendarSchedule new];
    schedule.list = list;
    
    id nullParameter = [message parameter:(NSString *)[NSNull null]];
    if ([nullParameter isKindOfClass:[ScheduleFilter class]])
        schedule.filter = (ScheduleFilter *)nullParameter;
    return schedule;;
}

@end
