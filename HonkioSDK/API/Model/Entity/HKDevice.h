//
//  HKDevice.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/12/19.
//  Copyright © 2019 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DEVICE_ID                   @"id"
#define DEVICE_APP_KEY              @"app"
#define DEVICE_TOKEN_KEY            @"token"
#define DEVICE_TYPE_KEY             @"type"
#define DEVICE_NAME_KEY             @"name"
#define DEVICE_NOTIFY_KEY           @"notify"
#define DEVICE_MANUFACTURER_KEY     @"manufacturer"
#define DEVICE_MODEL_KEY            @"model"
#define DEVICE_OS_VERSION_KEY       @"os_version"

#define DEVICE_PLATFORM_IOS         @"ios"
#define DEVICE_PLATFORM_ANDROID     @"android"
#define DEVICE_PLATFORM_WP          @"wp"

// Types of notifications that the Devise can receive.
/** Device will receive all types of notifications. */
#define DEVICE_NOTIFY_NONE          @"none"
/** Device will not receive a notifications. */
#define DEVICE_NOTIFY_ALL           @"all"

/**
 * Data object that hold information about device. Also this class has several methods which allow to used it like a Filter.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface HKDevice: NSObject
    
/** Unique Device ID. */
@property (nonatomic, strong) NSString* device_id;

/** Application ID for which is Device registered. */
@property (nonatomic, strong) NSString* app;

/** Token which used by server to send push notifications. */
@property (nonatomic, strong) NSString* token;

/** Platform of the Device. Default value @c DEVICE_PLATFORM_IOS. */
@property (nonatomic, strong) NSString* platform;

/** Name of the Device. */
@property (nonatomic, strong) NSString* name;

/** Notify type which used for Device.
 * @see DEVICE_NOTIFY_NONE, DEVICE_NOTIFY_ALL
 */
@property (nonatomic, strong) NSString* notifyType;

/** Name of device manufacturer. */
@property (nonatomic, strong) NSString* manufacturer;

/** Name of device model. */
@property (nonatomic, strong) NSString* model;

/** Operation System version. */
@property (nonatomic, strong) NSString* osVersion;

-(id)init;
-(id)initFrom:(HKDevice*)device;

/**
 * Check if specified notify type used for Device.
 * @param type Notify type to check.
 * @return YES if Device notify type equal specified type, NO otherwise.
 */
-(BOOL)isNotifyType:(NSString*)type;

/**
 * Convert Device to Object which can be used in the web requests.
 * @return Object which can be used in the web requests.
 */
-(NSDictionary*)toParams;
    

@end
