//
//  TagsListJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 5/15/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "TagsListJsonParser.h"
#import "TagsList.h"

@implementation TagsListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    NSMutableDictionary<NSString*, Tag*>* resultDict = [NSMutableDictionary new];
    
    NSArray* tagsArray = dict[@"tags"];
    if (tagsArray) {
        for (NSDictionary* tagDict in tagsArray) {
            Tag* tag = [Tag new];
            
            tag.name = tagDict[@"name"];
            tag.tagId = tagDict[@"tag_id"];
            tag.desc = tagDict[@"description"];
            tag.currency = tagDict[@"currency"];
            tag.price = [tagDict[@"amount"] floatValue];
            tag.productId = tagDict[@"product_id"];
            
            resultDict[tag.tagId] = tag;
        }
    }
    
    TagsList* tagsDict = [TagsList new];
    
    tagsDict.dict = resultDict;
    
    return tagsDict;
}

@end
