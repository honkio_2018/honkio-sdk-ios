//
//  RoleChangedPushEntity.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 11/7/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "RoleChangedPushEntity.h"
#import "Push.h"

@implementation RoleChangedPushEntity

-(id)initWithContent:(NSDictionary*)content {
    return [super initWithType:PUSH_TYPE_ROLE_CHANGED andContent:content];
}

- (NSString*)roleDisplayName {
    return [self getContent:@"display_name"];
}

- (void)setRoleDisplayName:(NSString*)value {
    [self setContent:@"display_name" value:value];
}

- (NSString*)roleId {
    return [self getContent:@"role_id"];
}

- (void)setRoleId:(NSString*)value {
    [self setContent:@"role_id" value:value];
}

- (NSString*)merchantId {
    return [self getContent:@"merchant"];
}

- (void)setMerchantId:(NSString*)value {
    [self setContent:@"merchant" value:value];
}

- (NSString*)action {
    return [self getContent:@"action"];
}

- (void)setAction:(NSString*)value {
    [self setContent:@"action" value:value];
}

@end
