//
//  HKUserRoleStructure.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKStructure.h"

//TODO write docs
@interface HKUserRoleStructure : NSObject

@property (nonatomic) NSString* objectId;
@property (nonatomic) NSString* merchant;
@property (nonatomic) NSString* name;
@property (nonatomic) NSString* title;
@property (nonatomic) NSString* desc;
@property (nonatomic) NSString* roleId;
@property (nonatomic) NSDictionary<NSString*, HKStructureProperty*>* publicProperties;
@property (nonatomic) NSDictionary<NSString*, HKStructureProperty*>* privateProperties;


-(HKStructureProperty*)getProperty:(NSString*)name;

-(nullable HKStructureEnumPropertyValue *)findEnumPropertyValue:(NSString *)propertyName value:(NSString *)value;

@end
