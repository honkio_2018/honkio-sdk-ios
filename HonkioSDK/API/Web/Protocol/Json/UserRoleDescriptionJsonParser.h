//
//  UserRoleDescriptionJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/5/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "UserRoleDescription.h"

// TODO write docs
@interface UserRoleDescriptionJsonParser : BaseJsonResponseParser

+(UserRoleDescription*) parseRoleDescription:(NSDictionary*) dict;

@end
