//
//  UserRoleDescriptionsFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/24/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "UserRoleDescriptionsFilter.h"

@implementation UserRoleDescriptionsFilter

-(void)fillParams:(NSMutableDictionary*)params {
    [AbsFilter applyIfNotNil:@"query_type" value:self.type forParams:params];
    
    [super fillParams:params];
}

@end
