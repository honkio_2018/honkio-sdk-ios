//
//  WebServiceExecutorInstance.h
//  HonkioSDK
//
//  Created by Dash on 30.04.2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "WebServiceExecutorInstanceProtocol.h"

@interface WebServiceExecutorInstance : NSObject<WebServiceExecutorInstanceProtocol>

@end
