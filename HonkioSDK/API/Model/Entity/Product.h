//
//  Product.h
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/3/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKModelEntity.h"

#define HK_PRODUCT_TYPE_FEE @"435100000000000000000001"

@class UserAccount;
@class PicInfo;
@class Target;

/**
 * Data object that hold information about a product.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface Product : HKModelEntity

// TODO write docs
-(id)initFrom:(Product*) product;
-(instancetype)init:(NSDictionary *) propertiesDict;

@property (nonatomic) NSString* productId;
@property (nonatomic, nullable) NSString* name;
@property (nonatomic, nullable) NSString* desc;
@property (nonatomic, nullable) NSString* currency;
@property (nonatomic) double amount;
@property (nonatomic) double amount_vat;
@property (nonatomic) double amount_no_vat;
@property (nonatomic) double amount_vat_percent;
@property (nonatomic, assign) BOOL isActive;
@property (nonatomic, assign, getter=isVisible) BOOL visible;
@property (nonatomic) long duration;
@property (nonatomic, nullable) NSString *typeId;

@property (nonatomic) long validityTime;
@property (nonatomic, strong) NSString* validityTimeType;

/** Supplus key/value map.<br>Key is the Payment Account Type, the value is amount of money which will added to original price when specified account will used for a payment. */
@property (nonatomic, strong) NSDictionary* surplus;

/** Pictures list of the Product. */
@property (nonatomic, strong) NSArray<PicInfo*>* pics;

/** Tags list of the Product. */
@property (nonatomic, strong) NSArray<NSString*>* tags;

// TODO write docs
@property (nonatomic, strong) NSArray<Target*>* targetProducts;

// TODO write docs
@property (nonatomic, strong) NSSet* disallowedAccounts;

/**
 * Gets Supplus for specified Payment Account Type. Returned value is amount of money which will added to original price when specified account will used for a payment.
 * @param accountType Payment Account Type
 * @return Supplus for specified Payment Account Type. Returned value is amount of money which will added to original price when specified account will used for a payment.
 */
-(double)supplusFor:(NSString*)accountType;

-(BOOL)isHasTag:(NSString*)tagId;

-(BOOL)isHasTimeValidity;
-(BOOL)isCanBeExchanged;

-(Target*)findTarget:(NSString*)productId;

-(NSDictionary *)toParams;

@end

/**
 * Data object that hold information about a picture of a product.
 */
@interface PicInfo : NSObject

/** URL of the picture. */
@property (nonatomic, strong) NSString* url;

/** Width of the picture. */
@property (nonatomic, strong) NSNumber* width;

/** Height of the picture. */
@property (nonatomic, strong) NSNumber* heigth;

@end

// TODO write docs
@interface Target : NSObject

@property (nonatomic, strong) NSString* productId;
@property (nonatomic) int count;

@end



