//
//  HKUserChangePasswordViewController.swift
//  WorkPilots
//
//  Created by developer on 2/29/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

/**
The view controller for change password view.
*/

open class HKUserChangePasswordViewController: HKBaseViewController {
    /// The outlet for old password text field.
    @IBOutlet open weak var oldPasswordField: UITextField!
    /// The outlet for new password text field.
    @IBOutlet open weak var newPasswordField: UITextField!
    /// The outlet for switcher describing whether the entered passwords are displaying as texts.
    @IBOutlet open weak var showPasswordSwitch: UISwitch!
    
    /**
    The action to be run on showPasswordSwitch. Sets the passwords readable in text fields based on the switch state.
    */
    @IBAction open func showPasswordAction(_ sender: UISwitch) {
        
        self.oldPasswordField.isSecureTextEntry = !sender.isOn
        self.newPasswordField.isSecureTextEntry = !sender.isOn
    }

    /**
    The action to be performed on Next button clicked. In this action checks the passwords length, if the length is
    not appropreate, the error messages will be shown for the corresponding text field. If the length is ok, the busy
    indicator will be shown and request to the server for updating the password will be sent. When the response from
    the server is received the user will be notified whether the operation was completed successfully by CustomAlertView.
    - parameter sender: Not used.
    */
    @IBAction open func nextAction(_ sender: AnyObject) {
        
        if !self.oldPasswordField.checkPassword {
            
            self.oldPasswordField.setErrorMessage("text_validator_passwor_short".localized)
        }
        
        if !self.newPasswordField.checkPassword {
            
            self.newPasswordField.setErrorMessage("text_validator_passwor_short".localized)
        }
        
        if self.oldPasswordField.hasError || self.newPasswordField.hasError {
            
            return
        }
        
        BusyIndicator.shared.showProgressView(self.view)
        
        HonkioApi.userUpdatePassword(self.oldPasswordField.text!, newPassword: self.newPasswordField.text!, flags: 0) { (response) in
            
            BusyIndicator.shared.hideProgressView()
            
            if response.isStatusAccept() {
                
                CustomAlertView(okTitle: "change_password_dlg_accept_title".localized, okMessage: "change_password_dlg_accept_message".localized, okAction: {
                    
                    self.notifyCompletionDelegate()
                    
                }).show()
            }
            else {
                
                if response.anyError().code == ErrCommon.invalidUserLogin.rawValue {
                    
                    CustomAlertView(okTitle: "change_password_dlg_invalid_title".localized, okMessage: "change_password_dlg_invalid_message".localized, okAction: nil).show()
                }
                else {
                    
                    CustomAlertView(okTitle: "change_password_dlg_failed_title".localized, okMessage: "change_password_dlg_failed_message".localized, okAction: nil).show()
                }
            }
        }
    }

    /**
    On viewDidLoad attaches to old and new password text fields.
    */
    override open func viewDidLoad() {
        super.viewDidLoad()

        self.oldPasswordField.delegate = self
        self.newPasswordField.delegate = self
    }
}
