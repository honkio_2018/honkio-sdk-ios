//
//  HKBaseMapViewController.swift
//  BookAHorse
//
//  Created by Shurygin Denis on 6/29/18.
//  Copyright © 2018 Honkio. All rights reserved.
//

import UIKit

public class HKMapViewAnnotation: NSObject {
    
    public var coordinate: CLLocationCoordinate2D!
    public var title: String?
    public var subtitle: String?
    public var icon: UIImage?
    public var item: Any?
    
}

public protocol HKMapViewProtocol {
    
    var viewController: HKBaseMapViewController? { get set }
    var showsUserLocation: Bool { get set }
    
    func addAnnotation(_ annotation: HKMapViewAnnotation)
    func removeAllAnnotations()
    
    func moveToLocation(_ coordinate: CLLocationCoordinate2D, animated: Bool)
    var center: CLLocationCoordinate2D { get }
    var radius: CLLocationDistance { get }
    
}

open class HKBaseMapViewController: HKBaseViewController {
    
    public private(set) var mapViewProtocol: HKMapViewProtocol!
    public var showsUserLocation: Bool = false {
        didSet {
            self.mapViewProtocol?.showsUserLocation = self.showsUserLocation
        }
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.mapViewProtocol = self.buildMapViewProtocol()
        self.mapViewProtocol.showsUserLocation = self.showsUserLocation
        self.mapViewProtocol.viewController = self
    }
    
    open func cameraDidMove(latitude: CLLocationDegrees, longitude: CLLocationDegrees, radius: CLLocationDistance) {
        
    }
    
    open func mapDidTap(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
    }
    
    open func mapDidDoubleTap(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
    }
    
    open func annotationDidSelect(_ annotation: HKMapViewAnnotation?) {
        
    }
    
    open func addAnnotation(_ annotation: HKMapViewAnnotation) {
        self.mapViewProtocol.addAnnotation(annotation)
    }
    
    open func removeAllAnnotations() {
        self.mapViewProtocol.removeAllAnnotations()
    }
    
    open func moveToLocation(_ coordinate: CLLocationCoordinate2D, animated: Bool) {
        self.mapViewProtocol.moveToLocation(coordinate, animated: animated)
    }
    
    open func moveToUserLocation(animated: Bool) {
        if let location = HonkioApi.deviceLocation() {
            self.mapViewProtocol.moveToLocation(location.coordinate, animated: animated)
        }
    }
    
    open func buildMapViewProtocol() -> HKMapViewProtocol {
        preconditionFailure("Method buildMapView must be overriden!")
    }
}
