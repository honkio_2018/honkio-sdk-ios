//
//  RoleChangedPushEntity.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 11/7/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasePushEntity.h"

#define PUSH_ENTITY_ROLE_CAHNGED_ACTION_CREATED @"created"
#define PUSH_ENTITY_ROLE_CAHNGED_ACTION_UPDATED @"updated"
#define PUSH_ENTITY_ROLE_CAHNGED_ACTION_REMOVED @"removed"

@interface RoleChangedPushEntity : BasePushEntity

@property (nonatomic, nullable) NSString *roleDisplayName;
@property (nonatomic, nullable) NSString *roleId;
@property (nonatomic, nullable) NSString *merchantId;
@property (nonatomic, nullable) NSString *action;

-(id)initWithContent:(NSDictionary*)content;

@end
