//
//  HKStructureList.h
//  HonkioSDK
//
//  Created by Mikhail Li on 21/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKStructure.h"

@interface HKStructureList : NSObject
/** Array of HkStructure items */
@property (nonatomic, copy) NSArray<HKStructure*> *list;

@end
