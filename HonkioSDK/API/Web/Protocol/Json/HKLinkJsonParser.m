//
//  HKLinkJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKLinkJsonParser.h"
#import "HKLink.h"

@implementation HKLinkJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    HKLink* link;
    
    NSDictionary<NSString*, NSObject*>* metaDict = dict[@"metadata"];
    if (metaDict)
        link = [[HKLink alloc] initWithMetaData: [NSMutableDictionary dictionaryWithDictionary: metaDict]];
    else
        link = [HKLink new];

    link.objectId = dict[@"id"];
    link.type = dict[@"type_id"];
    
    [link setFrom: dict[@"from_object"] type: dict[@"from_object_type"]];
    [link setTo: dict[@"to_object"] type: dict[@"to_object_type"]];
    
    return link;
}

@end
