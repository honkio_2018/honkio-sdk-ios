//
//  HKLinkFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKLinkFilter.h"

@implementation HKLinkFilter

-(void)fillParams:(NSMutableDictionary*)params {
    [AbsFilter applyIfNotNil:@"query_link_type" value:self.type forParams:params];
    [AbsFilter applyIfNotNil:@"query_from" value:self.fromObject forParams:params];
    [AbsFilter applyIfNotNil:@"query_from_type" value:self.fromType forParams:params];
    [AbsFilter applyIfNotNil:@"query_to" value:self.toObject forParams:params];
    [AbsFilter applyIfNotNil:@"query_to_type" value:self.toType forParams:params];
    [AbsFilter applyIfNotNil:@"query_sort" value:[self.sortFilter toParams] forParams:params];
    
    [super fillParams:params];
}

@end
