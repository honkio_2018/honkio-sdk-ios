//
//  HKAssetMediaListViewStrategy.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 2/28/19.
//  Copyright © 2019 developer. All rights reserved.
//

import UIKit

open class HKAssetMediaListViewStrategy: HKBaseMediaListViewStrategy {
    
    open var asset : HKAsset! {
        didSet {
            if asset != nil {
                setObject(asset.assetId, HK_OBJECT_TYPE_ASSET)
            }
        }
    }
    
    override open var isEditable: Bool {
        get {
            return super.isEditable && self.asset != nil
        }
    }
    
    override open func onListLoad(_ list: [HKMediaFile]) {
        super.onListLoad(list)
        
        // TODO
//        if list.count > 0 && self.asset?.logoUrl != nil && asset.logoUrl!.isEmpty {
//            setImageAsMain(list[0]);
//        }
    }

    
    
    private func setImageAsMain(_ mediaFile: HKMediaFile) {
        
    }
}
