//
//  ApiResponseHandler.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#ifndef ApiResponseHandler_h
#define ApiResponseHandler_h
#import "Response.h"
#import "ApiError.h"

/**
 * This method will called when the server return result. If response has error parameter err will contain this error, otherwise err will be nil.
 * @param response Object that represent the server response.
 */
typedef void (^ResponseHandler)(Response * _Nonnull res);

/**
 * This method will called when the server returned result. If response has error parameter err will contain this error, otherwise err will be nil.
 * @param error Error object with error description.
 */
typedef void (^SimpleResponseHandler)(ApiError * _Nullable err);

/**
 * This method will called when the server returned result with pending status.
 * @param response Response object with pending status.
 */
typedef void (^PendingHandler)(Response * _Nonnull res);

#endif
