//
//  HKTouInfo.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/31/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKTouInfo.h"

@implementation HKTouInfo

-(id)initWithKey:(NSString*) key url:(NSString*) url version:(int)version {
    self = [super init];
    if (self) {
        _key = key;
        _url = url;
        _version = version;
    }
    return self;
}

-(id)initWithUrl:(nonnull NSString*) url version:(int)version {
    return [self initWithKey:TOU_KEY_DEFAULT url:url version:version];
}

@end
