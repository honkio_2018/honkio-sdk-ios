//
//  HKTouVersion.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/31/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface HKTouVersion : NSObject

@property (nonatomic, readonly) int version;
@property (nonatomic, readonly, nonnull) NSDate* timestamp;

-(id)initWithVersion:(int) version timestamp:(nonnull NSDate*) timestamp;

@end
