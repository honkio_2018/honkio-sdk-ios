//
//  HKPinViewController.swift
//  WorkPilots
//
//  Created by developer on 12/17/15.
//  Copyright © 2015 developer. All rights reserved.
//

import UIKit

/**
Manages the PIN view behavior.
*/
open class HKPinViewController: HKBasePinViewController {
    
    @IBOutlet open weak var numPad: UIView!
    @IBOutlet open weak var dotView: UIView!

    /**
    Describes the reaction on PIN control button click.
    - parameter sender: The button object the click has been performed on.
    */
    @IBAction open func pinTapped(_ sender: UIButton) {
        self.addPinSign(String(sender.tag))
    }

    /**
    Proecsses the entered PIN code. The view will be notified for dismissing.
    Notification to listener (delegate) will be sent by calling of super class.
    - parameter pin: PIN code value having been entered.
    */
    open override func pinEntered(_ pin: String) {
        _ = Timer.schedule(delay: 0.1, handler: { timer in
            
            self.dismiss(animated: true, completion: nil)
            super.pinEntered(pin)
        })
    }

    /**
    Describes the behavior on delete button click. Originally performs delete recently entered character.
    - parameter sender: The object the click action was performed on.
    */
    @IBAction open func deleteAction(_ sender: AnyObject) {
        self.deleteLastPinSign()
    }
    
    //TODO write docs
    @IBAction open func clearAction(_ sender: AnyObject) {
        self.clearPin()
    }

    /**
    Sets the pin char symbol active/inactive
    - parameter dotNumber: The number of PIN symbol to be activated/deactivated.
    - parameter isActive: The value describes whether the symbol is to be activated or deactivated.
    */
    open override func setDotActive(_ dotNumber: Int, isActive:Bool) {
        if let view = self.dotView.viewWithTag(dotNumber) {
            if isActive {
                view.backgroundColor = UIColor(cgColor: view.layer.borderColor!)
            }
            else {
                view.backgroundColor = UIColor.clear
            }
        }
    }
}
