//
//  Response.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "Response.h"

@implementation Response

-(id)initWithResult:(NSObject*)result {
    self = [super init];
    if (self) {
        _result = result;
    }
    return self;
}

-(id)initFromTemplate:(Response*)templateResponse withResult:(NSObject*)result {
    self = [self initWithResult:result];
    if (self) {
        _status = templateResponse.status;
        _transactoinId = templateResponse.transactoinId;
        _respDescription = templateResponse.respDescription;
        _otp = templateResponse.otp;
        _shopId = templateResponse.shopId;
        _url = templateResponse.url;
        _pendingsList = templateResponse.pendingsList;
        _isOffline = templateResponse.isOffline;
    }
    return self;
}

-(BOOL)isStatusAccept {
    return self.status && [self.status isEqualToString:RESPONSE_STATUS_ACCEPT];
}

-(BOOL)isStatusError {
    return self.status && [self.status isEqualToString:RESPONSE_STATUS_ERROR];
}

-(BOOL)isStatusPending {
    return self.status && [self.status isEqualToString:RESPONSE_STATUS_PENDING];
}

-(BOOL)isStatusReject {
    return self.status && [self.status isEqualToString:RESPONSE_STATUS_REJECT];
}

-(BOOL)hasPending:(NSString*)pending {
    return self.pendingsList && [self.pendingsList indexOfObject:pending] != NSNotFound;
}

-(ApiError*)anyError {
    if (self.error) {
        return self.error;
    }
    else {
        return [[ApiError alloc] initWithCode:ErrApiUnknown desc:self.respDescription];
    }
}

+(Response*) withError:(int)code desc:(NSString*)desc {
    return [Response withApiError:[[ApiError alloc] initWithCode:code desc:desc]];
}

+(Response*) withApiError:(ApiError*)error {
    Response* response = [[Response alloc] initWithResult:nil];
    response.error = error;
    response.respDescription = error.desc;
    response.status = RESPONSE_STATUS_ERROR;
    return response;
}

@end
