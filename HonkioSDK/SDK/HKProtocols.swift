//
//  HKProtocols.swift
//
//  Created by developer on 12/12/15.
//  Copyright © 2015 developer. All rights reserved.
//

import Foundation
import UIKit

/// Containes list of globally available protocols.

/// HKPinProtocol dictates the functions to be implemented in PIN routine related classes.
public protocol HKPinProtocol : NSObjectProtocol {

    /**
    * Should be called when PIN entered.
    - parameter pinCode: Entered PIN code.
    - parameter reason: Defines the reason of entering pin code.
    */
    func pinEntered(_ pinCode: String!, reason: Int!)

    /**
    * Should be called when PIN pad canceled without entering a PIN.
    - parameter reason: Defines the reason of entering pin code.
    */
    func pinCanceled(_ reason: Int!)
}

/// Describes the functionality to be implemented in wizard based classes.
public protocol HKWizardProtocol : NSObjectProtocol {
    
    var wizardObject: AnyObject? { get set }
    
    func wizardComplete()
    
    func nextViewController()
}

/// Describes the functionality to be implemented in HKTableViewCellBinder based classes.
public protocol HKTableViewCellBinder : NSObjectProtocol {
    
    func bind(_ cell: UITableViewCell!, item: Any?)
}

/// Describes the functionality to be implemented in HKViewControllerCompletionDelegate based classes.
public protocol HKViewControllerCompletionDelegate : NSObjectProtocol {
    
    func onComplete(_ viewController: UIViewController, tag: String?)
}

/// Describes the functionality to be implemented in classes are expected to be notified wizard that it is being completed.
public protocol HKWizardViewControllerCompletionDelegate : NSObjectProtocol {
    
    func onWizardComplete(_ viewController: UIViewController, tag: String?, wizardObject: AnyObject?)
}
