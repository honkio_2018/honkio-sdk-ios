//
//  SimpleListItem.h
//  HonkioSDK
//
//  Created by Mikhail Li on 03/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * Data object that holds id, name and string that represent this object.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface SimpleListItem : NSObject

/** ID of the Item. */
@property (nonatomic, copy) NSString *itemId;
/** String value of the Item. */
@property (nonatomic, copy) NSString *string;
/** Name of the Item. */
@property (nonatomic, copy) NSString *name;

-(instancetype)initWithId:(NSString *)itemId string:(NSString *)string name:(NSString *)name;

@end
