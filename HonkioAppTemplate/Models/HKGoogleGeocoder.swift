//
//  GoogleGeocoder.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 8/25/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

open class HKGoogleGeocoder: HKGeocoderProtocol {
    
    private static let ApiUrl = "https://maps.googleapis.com/maps/api/geocode/json"
    private static let ApiKey = "AIzaSyAqNq30DT13uPY50dS77tq7oLAMrPNK9I4"
    
    private var currentTask: URLSessionDataTask?
    private var language: String
    
    public convenience init() {
        self.init(NSLocale.preferredLanguages[0])
    }
    
    public init(_ language: String) {
        self.language = language
    }

    public func fromLocationName(_ locationName: String, callback: @escaping ((_ place: HKAddress?, _ location: CLLocation?) -> Void)) {
        var components = URLComponents(string: HKGoogleGeocoder.ApiUrl)!
        
        components.queryItems = [
            URLQueryItem(name: "key", value: HKGoogleGeocoder.ApiKey),
            URLQueryItem(name: "language", value: self.language),
            URLQueryItem(name: "address", value: locationName)
        ]
        
        self.executeUrl(components.url!, callback: callback)
    }
    
    public func fromLocation(_ location: CLLocation, callback: @escaping ((_ place: HKAddress?, _ location: CLLocation?) -> Void)) {
        var components = URLComponents(string: HKGoogleGeocoder.ApiUrl)!
        
        components.queryItems = [
            URLQueryItem(name: "key", value: HKGoogleGeocoder.ApiKey),
            URLQueryItem(name: "language", value: self.language),
            URLQueryItem(name: "latlng", value: String(format: "%f, %f", location.coordinate.latitude, location.coordinate.longitude))
        ]
        
        self.executeUrl(components.url!, callback: callback)
    }
    
    public func cancel() {
        if let task = self.currentTask {
            task.cancel()
        }
    }
    
    private func executeUrl(_ url: URL, callback: @escaping ((_ place: HKAddress?, _ location: CLLocation?) -> Void)) {
        print("Google geocode url: \(url.absoluteString)")
        
        self.currentTask = URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data, let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, error == nil else {
                print(String(describing: response))
                print(String(describing: error))
                self.callInMainQueue(callback, result: nil, nil)
                return
            }
            
            guard let json = try! JSONSerialization.jsonObject(with: data) as? [String: Any] else {
                print("not JSON format expected")
                print(String(data: data, encoding: .utf8) ?? "Not string?!?")
                self.callInMainQueue(callback, result: nil, nil)
                return
            }
            
            print(String(describing: json))
            
            guard let results = json["results"] as? [[String: Any]],
                let status = json["status"] as? String,
                status == "OK" && results.count > 0 else {
                    print("no results")
                    self.callInMainQueue(callback, result: nil, nil)
                    return
            }
            
            self.callInMainQueue(callback, result: self.toAddress(results[0]), self.toLocation(results[0]))
            self.currentTask = nil
        }
        
        currentTask!.resume()
    }
    
    private func callInMainQueue(_ callback: @escaping ((_ place: HKAddress?, _ location: CLLocation?) -> Void), result: HKAddress?, _ location: CLLocation?) {
        DispatchQueue.main.async {
            callback(result, location)
        }
    }
    
    private func toLocation(_ dict: [String: Any]) -> CLLocation? {
        if let location = (dict["geometry"] as? [String: Any])?["location"] as? [String: Any] {
            return CLLocation(latitude: (location["lat"] as? Double) ?? 0, longitude: (location["lng"] as? Double) ?? 0)
        }
        return nil
    }
    
    private func toAddress(_ dict: [String: Any]) -> HKAddress? {
        let result = HKAddress()!
        
        var street_number: String? = nil
        var street_name: String? = nil
        
        var admin_area_1: String? = nil
        var admin_area_2: String? = nil
        var admin_area_3: String? = nil
        
        var postal_town: String? = nil
        
        if let addresComponents = dict["address_components"] as? [[String: Any]] {
            for component in addresComponents {
                if let types = component["types"] as? [String] {
                    
                    if self.array(types, contains: "street_number") {
                        street_number = component["long_name"] as? String
                    }
                    
                    if self.array(types, contains: "route") {
                        street_name = component["long_name"] as? String
                    }
                    
                    if self.array(types, contains: "locality") {
                        result.city = component["long_name"] as? String
                    }
                    
                    if self.array(types, contains: "postal_town") {
                        postal_town = component["long_name"] as? String
                    }
                    
                    if self.array(types, contains: "sublocality") {
                        result.address2 = component["long_name"] as? String
                    }
                    
                    if self.array(types, contains: "administrative_area_level_1") {
                        admin_area_1 = component["long_name"] as? String
                    }
                    
                    if self.array(types, contains: "administrative_area_level_2") {
                        admin_area_2 = component["long_name"] as? String
                    }
                    
                    if self.array(types, contains: "administrative_area_level_3") {
                        admin_area_3 = component["long_name"] as? String
                    }
                    
                    if self.array(types, contains: "postal_code") {
                        result.postalCode = component["long_name"] as? String
                    }
                    
                    if self.array(types, contains: "country") {
                        result.setCountry(component["short_name"] as? String, name: component["long_name"] as? String)
                    }
                }
            }
        }
        
        result.admin = admin_area_1 ?? admin_area_2 ?? ""
        result.address1 = "\(street_name ?? "") \(street_number ?? "")"
        
        if result.city == nil {
            result.city = admin_area_3 ?? postal_town
        }
        
        return result;
    }
    
    private func array(_ array: [String], contains value:String) -> Bool {
        for item in array {
            if item == value {
                return true
            }
        }
        return false
    }
}
