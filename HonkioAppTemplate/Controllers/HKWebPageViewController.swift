//
//  HKWebPageViewController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 7/11/18.
//  Copyright © 2018 developer. All rights reserved.
//

import UIKit

open class HKWebPageViewController: HKBaseViewController, HKWebPageViewDelegate {

    @IBOutlet open var webView: UIWebView!
    
    public var webViewDelegate: UIWebViewDelegate? {
        set {
            self.webView.delegate = newValue
        }
        get {
            return self.webView.delegate
        }
    }
    
    override open func viewDidLoad() {
        self.buildViewProgrammaticaly()
        super.viewDidLoad()
    }
    
    open func showUrl(_ url: URL) {
        self.webView.loadRequest(URLRequest(url: url))
    }
    
    open func showUrl(string: String) {
        self.showUrl(URL(string: string)!)
    }
    
    open func buildViewProgrammaticaly() {
        
        self.webView  = self.makeWebView()
        
        view.backgroundColor = UIColor(netHex: 0xffffff)
        
        view.addSubview(self.webView)
        
        let views : [String : AnyObject] = [
            "webView" : self.webView
        ]
        
        let formatArray : [String] = [
            "V:|-0-[webView]-0-|",
            "H:|-0-[webView]-0-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }
    
    open func makeWebView() -> UIWebView {
        let webView = UIWebView()
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.backgroundColor = UIColor.clear
        webView.layoutMargins = UIEdgeInsets.zero

        return webView
    }
}
