//
//  SimpleResponseJsonParser.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "BaseJsonResponseParser.h"

/**
 * Parse Response without result.
 */
@interface SimpleResponseJsonParser : BaseJsonResponseParser

@end
