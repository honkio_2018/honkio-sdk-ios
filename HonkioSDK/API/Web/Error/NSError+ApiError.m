//
//  NSError+ApiError.m
//  Honkio API
//
//  Created by Alexandr Kolganov on 4/15/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import "NSError+ApiError.h"

#define HONKIO_ERROR @"honkioApiError"
#define GROUP_MULT 1000

@implementation NSError (ApiError)

NSString* const ApiErrorDomain = @"com.riskpointer";
NSString* const ApiErrorFields = @"api_error_fields";

static NSDictionary* apiErrors = nil;

-(BOOL)isHonkioApiError {
    return [ApiErrorDomain isEqualToString:self.domain];
}

-(BOOL)isHonkioApiError:(int)errorCode {
    ApiError* error = [self honkioApiError];
    return error && error.code == errorCode;
}

-(ApiError*)honkioApiError {
    
    if ([self isHonkioApiError]) {
        
        if (self.userInfo[HONKIO_ERROR])
            return self.userInfo[HONKIO_ERROR];
    }
    return [ApiError unknown];
}

+(NSError*)errorWithGroup:(int)group subItem:(int)subItem string:(NSString*)string
{
    return [NSError errorWithError:[[ApiError alloc] initWithGroup:group subItem:subItem] string:(NSString*)string];
}

+(NSError*)errorWithCode:(int)code string:(NSString*)string {
    return [NSError errorWithError:[[ApiError alloc] initWithCode:code] string:(NSString*)string];
}

+(NSError*)errorWithError:(ApiError*)apiError string:(NSString*)description
{
    NSMutableDictionary* userInfo = nil;
    if (description || apiError) {
        userInfo = [NSMutableDictionary new];
        [userInfo setValue:(description ? description : @"") forKey:NSLocalizedDescriptionKey];
        
        NSArray* invalidParameters = nil;
        if(description && description.length > 5) {
            NSRange pos = [description rangeOfString:@" "];
            if(pos.location != NSNotFound)
                description =[description substringFromIndex:pos.location+1];
            
            pos = [description rangeOfString:@":"];
            if (pos.location != NSNotFound) {
                invalidParameters = [[description substringToIndex:pos.location] componentsSeparatedByString:@" "];
            }
        }
        
        if (invalidParameters) {
            [userInfo setValue:invalidParameters forKey:ApiErrorFields];        }
        
        if (apiError) {
            [userInfo setValue:apiError forKey:HONKIO_ERROR];
        }
    }
    return [NSError errorWithDomain:ApiErrorDomain code:apiError.code userInfo:userInfo];
}

+(NSError*)errorWithString:(NSString*)string
{
    return [NSError errorWithError:[ApiError unknown] string:string];
}

+(NSError*)errorFromResponse:(Response*)response error:(NSError*)error {
    if (error) {
        return error;
    }
    else if (!response) {
        return [NSError errorWithError:[ApiError unknown] string:@""];
    }
    else if([response isStatusError] || [response isStatusReject]) {
        return [NSError errorWithError:(response.error ? response.error : [ApiError unknown]) string:response.respDescription];
    }
    return nil;
}


@end
