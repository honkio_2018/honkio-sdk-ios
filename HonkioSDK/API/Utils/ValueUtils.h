//
//  ValueUtils.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/17/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValueUtils : NSObject
/**
 * Check if Finland SSN is valid.
 * @param ssn Social Security number
 * @return True if SSN is valid, false otherwise.
 */
+ (BOOL)checkSsn:(NSString*) ssnString;
+ (BOOL)checkSsn:(NSString *)ssnString withCountryCode:(NSString *)countryCode;

+ (NSDate*)getBirthdayFromSsn:(NSString *)ssnString withCountryCode:(NSString *)countryCode;

+ (BOOL)checkFinlandSsn:(NSString *)ssnString;
+ (NSDate*)getBirthdayFromFinlandSsn:(NSString *)ssnString;

+ (BOOL)checkItalianSsn:(NSString *)ssnString;
+ (BOOL)checkUKSsn:(NSString *)ssnString;
+ (BOOL)checkSwedenSsn:(NSString *)ssnString;

+ (BOOL)checkIban:(NSString*) ibanString;

@end
