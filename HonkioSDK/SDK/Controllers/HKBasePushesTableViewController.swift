//
//  HKBasePushesTableViewController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 4/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
Base ViewController for managing Push objects.
* Must be subclassed.
*/

open class HKBasePushesTableViewController: HKBaseLazyTableViewController {
    
    public fileprivate(set) var filter : AbsListFilter = AbsListFilter()
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(HKBasePushesTableViewController.refresh(_:)), name: NSNotification.Name(rawValue: NOTIFY_NAME_ON_PUSH_RECEIVED), object: nil)
    }
    
    /**
     Removes observers for MSG_COMMAND_USER_PAYMENT and MSG_COMMAND_USER_SET_ORDER messages on moved to parent controlled view.
     */
    open override func didMove(toParentViewController parent: UIViewController?) {
        if parent == nil {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NOTIFY_NAME_ON_PUSH_RECEIVED), object: nil)
        }
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isLasySection(itemsSource[indexPath.section]) {
            
            if let push = self.itemsSource[indexPath.section].items[indexPath.row].item as? Push {
                
                itemDidClicked(push, indexPath: indexPath)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

    open override func loadList(_ responseHandler: @escaping ResponseHandler, count: Int, skip: Int) {
        self.setupFilter(self.filter, count: count, skip: skip)
        
        HonkioApi.userGetPushes(self.filter, flags: 0, handler: responseHandler)
    }
    
    open override func toList(_ response : Response) -> [Any?] {
        return (response.result as! PushesList).list
    }
    
    open override func mapList(_ list : [Any?]) -> ([HKTableViewItem]) {
        var items : [HKTableViewItem] = []
        let itemBinder = buildCellBunder()
        
        for listItem in list {
            items.append((cellIdentifier(listItem as! Push), listItem, itemBinder))
        }
        return items
    }
    
    open func cellIdentifier(_ item: Push) -> String {
        preconditionFailure("Method cellIdentifier must be overridden")
    }

    open func buildCellBunder() -> HKTableViewItemInitializer? {
        preconditionFailure("Method buildCellBunder must be overridden")
    }
    
    open func itemDidClicked(_ item: Push, indexPath: IndexPath) {
        // Override to implement item click action
    }
    
    open func markAllAsRead() {
        for item in self.lazySection.items {
            if let push = item.item as? Push {
                push.isReaded = true
            }
        }
        
        self.tableView.reloadData()
        
        HonkioApi.userSetPush(true, filter: nil, flags: MSG_FLAG_NO_WARNING, handler: nil)
    }
    
    open func setupFilter(_ filter: AbsListFilter, count: Int, skip: Int) {
        filter.queryCount = Int32(count)
        filter.querySkip = Int32(skip)
    }
    
    @objc func refresh(_ notification : Notification) {
        refresh()
    }
}
