//
//  ServerFileJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "ServerFileJsonParser.h"
#import "ServerFile.h"

@implementation ServerFileJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    
    ServerFile* serverFile = [ServerFile new];
    serverFile.mime = dict[@"mime"];
    NSString* base64String = dict[@"data"];
    if (base64String)
        serverFile.data = [[NSData alloc] initWithBase64EncodedString:base64String options:0];

    return serverFile;
}

@end
