//
//  HKBundleUtils.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/12/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/// BundleUtils class should be used for access to application and framework resources which are contained
/// in the application and framework bundles.

open class HKBundleUtils {
    
    static let framewokBundle = Bundle(for: HKBundleUtils.self)
    
    // Main bundle must be first, this is provide opprtunity to override SDK resources.
    static var bundles: [Bundle] = [Bundle.main, framewokBundle]

    /**
    Adds bundle into bundle list in HKBundleUtils. If bundle list containes the provided bundle,
    this bundle will not be added.
     - parameter bundle: The NSBundle object to be added into bundle list.
    */
    public static func addBundle(_ bundle: Bundle) {
        if !HKBundleUtils.bundles.contains(bundle) {
            HKBundleUtils.bundles.append(bundle)
        }
    }

    /**
     Load image from bundle list.
     - parameter named: The String identifier of the image to be found and loaded from the bundle list.
     - Returns: Loaded image from bundle list if was found, otherwise nil.
    */
    public static func image(_ named: String) -> UIImage? {
        for bundle in HKBundleUtils.bundles {
            if let image = UIImage(named: named, in: bundle, compatibleWith: .none) {
                return image
            }
        }
        return nil
    }
}
