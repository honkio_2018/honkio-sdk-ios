//
//  Error.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HONKIO_ERR_GROUP_MULT 100

typedef NS_ENUM(int, ErrGroup) {
    
    
    /** Client Api errors. This errors invokes by client, not by server. */
    ErrGroupApi               = 100,
    
    ErrGroupSystem            = 00,
    ErrGroupCommon            = 01,
    
    ErrGroupSms               = 06, // TODO describe error codes
    ErrGroupVerify            = 07, // TODO describe error codes
    
    ErrGroupLoginStatus       = 11, // TODO describe error codes
    
    ErrGroupQuery             = 13,
    
    ErrGroupShopFind          = 14,
    
    ErrGroupServerTou         = 16,
    
    ErrGroupShopStamp         = 26, // TODO describe error codes
    ErrGroupStatusSet         = 28,
    
    ErrGroupUserDispute       = 50, // TODO describe error codes
    ErrGroupUser              = 51,
    ErrGroupUserFile          = 52,
    ErrGroupUserInvoice       = 53, // TODO describe error codes
    ErrGroupUserPayment       = 55,
    ErrGroupUserProductTags   = 60, // TODO describe error codes
    ErrGroupUserRole          = 61,
    ErrGroupUserOrder         = 62,
    ErrGroupUserPhoto         = 64,
    ErrGroupUserOrderComments = 67,
    ErrGroupUserOrderProposal = 68
    
};


// Group value 100
/** Client Api errors. This errors invokes by client, not by server. */
typedef NS_ENUM(int, ErrApi) {
    
    /** Unknown error */
    ErrApiNone                = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 0,
    
    /** Unknown error */
    ErrApiUnknown             = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 1,
    
    /** Bad connection. No Internet connection or request was closed by timeout. */
    ErrApiNoConnection        = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 2,
    
    /** Cache for re-enter not found or active connection was closed, need to login. */
    ErrApiNoLogin             = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 3,
    
    /** Server return code that no equal 200 or server response format is wrong.*/
    ErrApiBadResponse         = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 4,
    
    /** Server return result with wrong hash. */
    ErrApiWrongHash           = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 5,
    
    /** Operation was canceled by user. */
    ErrApiCanceledByUser      = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 6,
    
    /** User enter invalid PIN. */
    ErrApiInvalidPin          = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 7,
    
    /** Error for offline mode. Cache for request not found. */
    ErrApiCacheNotFound       = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 8,
    
    /** Error for offline mode. Cache for request not supported. */
    ErrApiCacheNotSupported   = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 9,
    
    /** API not initialised. */
    ErrApiNoInitialized       = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 10,
    
    /** Account not supported for selected shop. */
    ErrApiAccountNotSupported = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 11,
    
    /** Request not supported for selected shop. */
    ErrApiUnsupportedRequest = ErrGroupApi * HONKIO_ERR_GROUP_MULT + 12
    
};


// Group value 00
typedef NS_ENUM(int, ErrSystem) {
    
    /** Internal error */
    ErrSystemInternal                 = ErrGroupSystem * HONKIO_ERR_GROUP_MULT + 1,
    
    /** Invalid value for parameter */
    ErrSystemInvalidValueForParameter  = ErrGroupSystem * HONKIO_ERR_GROUP_MULT + 2,
    
    /** HTTP auth error */
    ErrSystemHttpAuth                 = ErrGroupSystem * HONKIO_ERR_GROUP_MULT + 4,
    
    /** Internal: malformed message */
    ErrSystemMailformedMessage        = ErrGroupSystem * HONKIO_ERR_GROUP_MULT + 10,
    
    /** Command not found */
    ErrSystemCommandNotFound          = ErrGroupSystem * HONKIO_ERR_GROUP_MULT + 11,
    
    /** Server on service work */
    ErrSystemServiceWorks             = ErrGroupSystem * HONKIO_ERR_GROUP_MULT + 12
    
};


// Group value 01
typedef NS_ENUM(int, ErrCommon) {
    
    /** Required parameter missing */
    ErrCommonParameterMissing         = ErrGroupCommon * HONKIO_ERR_GROUP_MULT + 1,
    
    /** Invalid value for parameter */
    ErrCommonInvalidValueForParameter  = ErrGroupCommon * HONKIO_ERR_GROUP_MULT + 2,
    
    /** Invalid message security */
    ErrCommonInvalidSecurity          = ErrGroupCommon * HONKIO_ERR_GROUP_MULT + 3,
    
    /** Invalid adminuserer login */
    ErrCommonInvalidAdminLogin        = ErrGroupCommon * HONKIO_ERR_GROUP_MULT + 5,
    
    /** Invalid user login */
    ErrCommonInvalidUserLogin         = ErrGroupCommon * HONKIO_ERR_GROUP_MULT + 6,
    
    /** Shop is not active yet */
    ErrCommonShopNotActive            = ErrGroupCommon * HONKIO_ERR_GROUP_MULT + 10,
    
    /** Merchant is not active yet */
    ErrCommonMerchantNotActive        = ErrGroupCommon * HONKIO_ERR_GROUP_MULT + 11,
    
    /** No such shop */
    ErrCommonShopNotExist             = ErrGroupCommon * HONKIO_ERR_GROUP_MULT + 13,
};


// Group value 13
typedef NS_ENUM(int, ErrQuery) {
    
    /** No such transaction in shop */
    ErrQueryNoTransaction = ErrGroupQuery * HONKIO_ERR_GROUP_MULT + 1
    
};


// Group value 14
typedef NS_ENUM(int, ErrShopFind) {
    
    /** No such shop */
    ErrShopFindNoShop                     = ErrGroupShopFind * HONKIO_ERR_GROUP_MULT + 1,
    
    /** Invalid value for parameter */
    ErrShopFindInvalidValueForParameter    = ErrGroupShopFind * HONKIO_ERR_GROUP_MULT + 2
    
};


// Group value 16
typedef NS_ENUM(int, ErrServerTou) {
    
    /** ToU not found */
    ErrServerTouNotFound = ErrGroupServerTou * HONKIO_ERR_GROUP_MULT + 1
    
};


// Group value 28
typedef NS_ENUM(int, ErrStatusSet) {
    
    /** Invalid transaction */
    ErrStatusSetInvalidTransaction    = ErrGroupStatusSet * HONKIO_ERR_GROUP_MULT + 1,
    
    /** Invalid status value */
    ErrStatusSetInvalidStatus         = ErrGroupStatusSet * HONKIO_ERR_GROUP_MULT + 2
    
};


// Group value 51
typedef NS_ENUM(int, ErrUser) {
    
    /** User already exists */
    ErrUserExists                     = ErrGroupUser * HONKIO_ERR_GROUP_MULT + 1,
    
    /** Invalid value for parameter */
    ErrUserInvalidValueForParameter    = ErrGroupUser * HONKIO_ERR_GROUP_MULT + 2,
    
    /** No registration transaction found */
    ErrUserNoRegistration             = ErrGroupUser * HONKIO_ERR_GROUP_MULT + 3,
    
    /** Registration transaction status is not reject */
    ErrUserRegistrationNotRejected    = ErrGroupUser * HONKIO_ERR_GROUP_MULT + 4,
    
    /** Manual review done */
    ErrUserManualReviewDone           = ErrGroupUser * HONKIO_ERR_GROUP_MULT + 5
    
};


// Group value 52
typedef NS_ENUM(int, ErrUserFile) {
    
    /** File not found */
    ErrUserFileNotFound           = ErrGroupUserFile * HONKIO_ERR_GROUP_MULT + 1
    
};


// Group value 55
typedef NS_ENUM(int, ErrUserPayment) {
    
    /** User is not active */
    ErrUserPaymentUserNotActive           = ErrGroupUserPayment * HONKIO_ERR_GROUP_MULT + 1,
    
    /** Shop type does not allow product list */
    ErrUserPaymentProductsNotSupported    = ErrGroupUserPayment * HONKIO_ERR_GROUP_MULT + 2,
    
    /** Wrong shop configuration, Mecsel mode required */
    ErrUserPaymentMecselModeRequired      = ErrGroupUserPayment * HONKIO_ERR_GROUP_MULT + 3,
    
    /** Required parameter missing: account_amount account_currency */
    ErrUserPaymentNoPrice                 = ErrGroupUserPayment * HONKIO_ERR_GROUP_MULT + 4,
    
    /** Required parameter missing: account_type account_number */
    ErrUserPaymentNoAccount               = ErrGroupUserPayment * HONKIO_ERR_GROUP_MULT + 5,
    
    /** Shop type does not allow amount */
    ErrUserPaymentAmountNotSupported      = ErrGroupUserPayment * HONKIO_ERR_GROUP_MULT + 10,
    
    /** Product parameter missing */
    ErrUserPaymentNoProduct               = ErrGroupUserPayment * HONKIO_ERR_GROUP_MULT + 11,
    
    /** Only dicts are allowed in product list */
    ErrUserPaymentInvalidProducts         = ErrGroupUserPayment * HONKIO_ERR_GROUP_MULT + 12,
    
    /** Invalid value product list count */
    ErrUserPaymentInvalidProductCount     = ErrGroupUserPayment * HONKIO_ERR_GROUP_MULT + 14
    
    // TODO describe error codes
    //        E5513 Product not in merchant list
};


// Group value 60
typedef NS_ENUM(int, ErrUserProductTags) {
    
    /** Tags not found */
    ErrUserProductTagsNotFound            = ErrGroupUserProductTags * HONKIO_ERR_GROUP_MULT + 1,
    
    /** Tag with specified name already exists */
    ErrUserProductTagsAlreadyExist        = ErrGroupUserProductTags * HONKIO_ERR_GROUP_MULT + 2
    
};


// Group value 61
typedef NS_ENUM(int, ErrUserRole) {
    
    /** User does not exists */
    ErrUserRoleUserNotExists              = ErrGroupUserRole * HONKIO_ERR_GROUP_MULT + 1,
    
    /** Role does not exists */
    ErrUserRoleRoleNotExists              = ErrGroupUserRole * HONKIO_ERR_GROUP_MULT + 2,
    
    /** User has not registered for the role */
    ErrUserRoleRoleNotRegistered          = ErrGroupUserRole * HONKIO_ERR_GROUP_MULT + 3,
    
    /** Role is not allowed for merchant */
    ErrUserRoleRoleNotAllowedForMerchant  = ErrGroupUserRole * HONKIO_ERR_GROUP_MULT + 4,
    
    /** User is not allowed to set the role */
    ErrUserRoleUserNotAllowedForEdit      = ErrGroupUserRole * HONKIO_ERR_GROUP_MULT + 5
    
};


// Group value 62
typedef NS_ENUM(int, ErrUserOrder) {
    
    /** User is not allowed to get the orders */
    ErrUserOrderUserNotAllowedToGet  = ErrGroupUserOrder * HONKIO_ERR_GROUP_MULT + 1,
    
    /** User is not allowed to set the order */
    ErrUserOrderUserNotAllowedToSet  = ErrGroupUserOrder * HONKIO_ERR_GROUP_MULT + 2,
    
    /** Order not found */
    ErrUserOrderNotFound             = ErrGroupUserOrder * HONKIO_ERR_GROUP_MULT + 3,
    
    /** Payment error while moving order on payment status */
    ErrUserOrderPaymentError         = ErrGroupUserOrder * HONKIO_ERR_GROUP_MULT + 4,
    
    /** Failed to reserve time for order in corespondet schedule */
    ErrUserOrderTimeReservationError = ErrGroupUserOrder * HONKIO_ERR_GROUP_MULT + 5
    
};


// Group value 64
typedef NS_ENUM(int, ErrUserPhoto) {
    
    /** Failed to create photo directory */
    ErrUserPhotoInnerError            = ErrGroupUserPhoto * HONKIO_ERR_GROUP_MULT + 1, // TODO merge with ErrUserPhotoFailedToSave
    
    /** Unsupported file type */
    ErrUserPhotoUnsupportedFileFormat = ErrGroupUserPhoto * HONKIO_ERR_GROUP_MULT + 2,
    
    /** Failed to save photo */
    ErrUserPhotoFailedToSave          = ErrGroupUserPhoto * HONKIO_ERR_GROUP_MULT + 3
    
};


// Group value 67
typedef NS_ENUM(int, ErrUserOrderComments) {
    
    /** Order not found */
    ErrUserOrderCommentsOrderNotFound     = ErrGroupUserOrderComments * HONKIO_ERR_GROUP_MULT + 1,
    
    /** The user is not allowed to rate order */
    ErrUserOrderCommentsUserNotAllowed    = ErrGroupUserOrderComments * HONKIO_ERR_GROUP_MULT + 2,
    
    /** User does not exist */
    ErrUserOrderCommentsUserNotExist      = ErrGroupUserOrderComments * HONKIO_ERR_GROUP_MULT + 3,
    
    /** Role does not exist */
    ErrUserOrderCommentsRoleNotExist      = ErrGroupUserOrderComments * HONKIO_ERR_GROUP_MULT + 4
    
};


// Group value 68
typedef NS_ENUM(int, ErrUserOrderProposal) {
    
    /** Order not found */
    ErrUserOrderProposalOrderNotFound     = ErrGroupUserOrderProposal * HONKIO_ERR_GROUP_MULT + 1,
    
    /** User is not allowed to propose for this order */
    ErrUserOrderProposalUserNotAllowed    = ErrGroupUserOrderProposal * HONKIO_ERR_GROUP_MULT + 2,
    
    /** Already proposed for this order */
    ErrUserOrderProposalAlreadyProposed   = ErrGroupUserOrderProposal * HONKIO_ERR_GROUP_MULT + 3
};

@interface ApiError : NSObject

@property(readonly, nonatomic) int group;
@property(readonly, nonatomic) int code;
@property(readonly, nonatomic) NSString* desc;
@property(readonly, nonatomic) NSArray* fields;


+(ApiError*)unknown;

-(id) initWithCode:(int)code;
-(id) initWithCode:(int)code desc:(NSString*)desc;
-(id) initWithGroup:(int)group subItem:(int)subItem;
-(id) initWithGroup:(int)group subItem:(int)subItem desc:(NSString*)desc;

@end
