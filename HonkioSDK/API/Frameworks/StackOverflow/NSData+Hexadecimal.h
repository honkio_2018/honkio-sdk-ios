//
//  NSData+Hexadecimal.h
//  Honkio API
//
//  Created by Alexandr Kolganov on 2/5/14.
//
// source: http://stackoverflow.com/questions/16521164/how-can-i-convert-nsdata-to-hex-string
//
//

#import <Foundation/Foundation.h>

@interface NSData (Hexadecimal)
- (NSString *)hexadecimalString;
@end
