//
//  OrderStatusChanged.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/1/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import "OrderStatusChangedPushEntity.h"
#import "Push.h"

@implementation OrderStatusChangedPushEntity

-(id)initWithContent:(NSDictionary*)content {
    return [super initWithType:PUSH_TYPE_ORDER_STATUS_CHANGED andContent:content];
}

-(NSString*) value1 {
    return self.orderId;
}

-(NSString*) value2 {
    return nil;
}

@end
