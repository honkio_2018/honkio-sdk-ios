//
//  TransactionPush.h
//  HonkioApi
//
//  Created by Shurygin Denis on 2/12/15.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasePushEntity.h"

@interface TransactionPushEntity : BasePushEntity

@property (nonatomic) NSString* type;
@property (nonatomic) NSString* shopId;
@property (nonatomic) NSString* transactionId;
@property (nonatomic) NSString* status;

-(id)initWithContent:(NSDictionary*)content;

@end
