FRAMEWORK=HonkioAppTemplate
PROJECT=HonkioSDK

BUILD=build
FRAMEWORK_FILE_NAME=$FRAMEWORK.framework

cd "`dirname \"$0\"`"

rm -Rf $BUILD
rm -f $FRAMEWORK.framework.tar.gz

xcodebuild archive -project $PROJECT.xcodeproj -scheme $FRAMEWORK -sdk iphoneos SYMROOT=$BUILD
xcodebuild build -project $PROJECT.xcodeproj -target $FRAMEWORK -sdk iphonesimulator SYMROOT=$BUILD

cp -RL $BUILD/Release-iphoneos $BUILD/Release-universal
cp -RL $BUILD/Release-iphonesimulator/$FRAMEWORK_FILE_NAME/Modules/$FRAMEWORK.swiftmodule/* $BUILD/Release-universal/$FRAMEWORK_FILE_NAME/Modules/$FRAMEWORK.swiftmodule

lipo -create $BUILD/Release-iphoneos/$FRAMEWORK_FILE_NAME/$FRAMEWORK $BUILD/Release-iphonesimulator/$FRAMEWORK_FILE_NAME/$FRAMEWORK -output $BUILD/Release-universal/$FRAMEWORK_FILE_NAME/$FRAMEWORK

tar -czv -C $BUILD/Release-universal -f $FRAMEWORK.tar.gz $FRAMEWORK_FILE_NAME $FRAMEWORK_FILE_NAME.dSYM
