//
//  HKMediaFileJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/13/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "HKMediaFile.h"

@interface HKMediaFileJsonParser : BaseJsonResponseParser

+(HKMediaFile*) parseMediaFile:(NSDictionary*) dict;

@end
