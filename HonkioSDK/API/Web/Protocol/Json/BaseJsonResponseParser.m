//
//  BaseJsonResponseParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "ApiError.h"
#import "ApiSecurity.h"
#import "ApiDateFormatter.h"
#import "Rate.h"
#import "Tag.h"
#import "HKLog.h"

@implementation BaseJsonResponseParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    return nil;
}

-(Response*)parse:(Message*)message response:(NSData*)responseData password:(NSString*)password {
    NSError* error = nil;
    NSMutableDictionary* responseDict = [NSJSONSerialization
                     JSONObjectWithData:responseData
                     options:NSJSONReadingAllowFragments + NSJSONReadingMutableContainers
                     error:&error];
    
    if(error) {
        [HKLog e: [NSString stringWithFormat: @"JSON deserialize error:%@", error.description]];
        return nil;
    }
    
    Response* response;
    
    if (![self checkHash:responseDict password:password]) {
        response = [Response withApiError:[[ApiError alloc] initWithCode:ErrApiWrongHash]];
    }
    else {
        [self removeNulls: responseDict];
        response = [self parseResponse:message dict:responseDict];
    }
    
    response.message = message;
    
    return response;
}

-(Response*)parseResponse:(Message*)message dict:(NSDictionary*)dict {
    Response* response = [[Response alloc] initWithResult:[self parseDictionary:message dict:dict]];
    
    response.status = dict[@"status"];
    response.transactoinId = dict[@"id"];
    response.otp = dict[@"otp"];
    response.respDescription = dict[@"description"];
    response.url = dict[@"url"];
    response.pendingsList = dict[@"pending"];
    response.shopReference = dict[@"shop_reference"];
    
    NSArray* error = dict[@"error"];
    if (error) {
        if (error.count >= 2) {
            response.error = [[ApiError alloc] initWithGroup:[((NSNumber*) error[0]) intValue]
                                                     subItem:[((NSNumber*) error[1]) intValue]
                                                        desc:dict[@"description"]];
        }
        else {
            response.error = [ApiError unknown];
        }
    }
    
    return response;
}

-(id)parseDictionary:(NSDictionary *)dict {
    return nil;
}

-(BOOL)checkHash:(NSDictionary*)dict password:(NSString*)password {
    if (!password || [password length] == 0)
        return YES;
    
    NSString* expectedHash = dict[@"hash"];
    if (expectedHash && [expectedHash length] > 0) {
        NSString* calculatedHash = [ApiSecurity calculateHashForKey:password andDictionary:dict];
        return [expectedHash isEqualToString:calculatedHash];
    }
    return YES;
}

+(void)parseShop:(Shop*)shop dict:(NSDictionary*)shopDict {
    if (shop && shopDict) {
        shop.name = shopDict[@"name"];
        shop.identityId = shopDict[@"id"];
        shop.latitude = [shopDict[@"latitude"] doubleValue];
        shop.longitude = [shopDict[@"longitude"] doubleValue];
        shop.maxDistance = [shopDict[@"int_max_distance"] doubleValue];
        shop.description = shopDict[@"str_description"];
        
        shop.address = shopDict[@"address"];// TODO use single variant for all requests
        if (!shop.address) {
            shop.address = shopDict[@"str_address"];
        }
        
        NSArray* ratingsArray = shopDict[@"ratings"];
        if (ratingsArray) {
            NSMutableArray<Rate*>* ratings = [NSMutableArray new];
            for (NSDictionary* rateDict in ratingsArray) {
                if (rateDict) {
                    Rate* rate = [[Rate alloc] initWithValue:((NSNumber*)rateDict[@"value"]).intValue
                                                    andCount:((NSNumber*)rateDict[@"count"]).intValue];
                    [ratings addObject:rate];
                }
            }
            shop.ratings = [ratings copy];
        }
        
        shop.publicKey = shopDict[@"key"];
        shop.type = shopDict[@"type"];
        shop.serviceType = shopDict[@"service_type"];
        shop.logoSmall = shopDict[@"logo_small"];
        shop.logoLarge = shopDict[@"logo_large"];
        
        NSMutableArray<Tag*>* tagsArray = [NSMutableArray new];
        NSArray* tagsJsonArray = shopDict[@"list_tags"];
        if (tagsJsonArray) {
            for (NSDictionary* tagDict in tagsJsonArray) {
                if (tagDict) {
                    Tag* tag = [Tag new];
                    tag.tagId = tagDict[@"id"];
                    tag.name = tagDict[@"name"];
                    if (tag.tagId && tag.name) {
                        [tagsArray addObject:tag];
                    }
                }
            }
        }
        shop.tags = [NSArray arrayWithArray:tagsArray];
        
        if (shopDict[@"opened_until"]) {
            NSString* timeString = shopDict[@"opened_until"];
            if (timeString && ![timeString isEqual:[NSNull null]] && ![timeString isEqualToString:@"null"]) {
                shop.closingTime = [ApiDateFormatter dateFromServerString:timeString];
            }
        }
        
        // Server send offset in minutes. Convert it to milliseconds
        shop.closingOffset = [shopDict[@"int_closing_offset"] longValue] * 60000;
        
        shop.preAuthorisationAmount = [shopDict[@"float_preauthorization_amount"] doubleValue];
    }
}

+(void)parseMerchantSimple:(MerchantSimple*)merchant dict:(NSDictionary *)merchantDict {
    if (merchant && merchantDict) {
        merchant.merchantId = merchantDict[@"id"];
        merchant.name = merchantDict[@"name"];
        merchant.businessId = merchantDict[@"businessid"];
        merchant.currency = merchantDict[@"currency"];
        merchant.logoUrl = merchantDict[@"logo"];
        merchant.supportEmail = merchantDict[@"support_email"];
        merchant.supportPhone = merchantDict[@"support_telephone"];
        if (merchantDict[@"loginshop_id"]) {
            merchant.loginShopIdentity = [[Identity alloc] initWithId:merchantDict[@"loginshop_id"] andPassword:merchantDict[@"loginshop_password"]];
        }
        if(merchantDict[@"registrationshop_id"]) {
            merchant.registrationShopIdentity = [[Identity alloc] initWithId:merchantDict[@"registrationshop_id"] andPassword:merchantDict[@"registrationshop_password"]];
        }
        if(merchantDict[@"lostpasswordshop_id"]) {
            merchant.lostPasswordShopIdentity = [[Identity alloc] initWithId:merchantDict[@"lostpasswordshop_id"] andPassword:merchantDict[@"lostpasswordshop_password"]];
        }
        
        NSString* accounts = merchantDict[@"account_type"];
        if(accounts)
            merchant.supportedAccounts = [accounts componentsSeparatedByString:@" "];
    }
}

+(void)parseProduct:(Product*)product dict:(NSDictionary *)prodData {
    if (product && prodData) {
        product.productId = prodData[@"id"];
        product.name = prodData[@"name"];
        product.desc = prodData[@"description"];
        product.amount = [prodData[@"amount"] doubleValue];
        product.amount_vat  = [prodData[@"amount_vat"] doubleValue];
        product.amount_no_vat = [prodData[@"amount_no_vat"] doubleValue];
        product.amount_vat_percent = [prodData[@"amount_vat_percent"] doubleValue];
        product.currency = prodData[@"currency"];
        product.typeId = prodData[@"type_id"];
        product.duration = [prodData[@"int_duration"] longValue];
        
        product.validityTime = [prodData[@"int_validity_time"] longValue];
        product.validityTimeType = prodData[@"str_validity_time_type"];
        
//        product.surplus = prodData[@"amount_surplus"];
        
        NSMutableArray<NSString*>* tags = [NSMutableArray array];
        NSArray* tagsArray = prodData[@"list_tags"];
        if (tagsArray) {
            for(int i = 0; i < [tagsArray count]; i++) {
                NSString* tagId = tagsArray[i];
                if (tagId) {
                    [tags addObject:tagId];
                }
            }
        }
        product.tags = tags;
        
        NSMutableArray<PicInfo*>* pics = [NSMutableArray array];
        NSArray* picsArray = prodData[@"list_pics"];
        if (picsArray) {
            for(int i = 0; i < [picsArray count]; i++) {
                NSDictionary* pic = picsArray[i];
                PicInfo* picInfo = [[PicInfo alloc] init];
                picInfo.url = pic[@"url"];
                picInfo.width = pic[@"x"];
                picInfo.heigth = pic[@"y"];
                [pics addObject:picInfo];
            }
        }
        product.pics = pics;
        
        
        NSMutableArray<Target*>* targetProducts = [NSMutableArray array];
        NSArray* targets = prodData[@"target_products"];
        if (targets) {
            for (int i = 0; i < [targets count]; i++) {
                NSDictionary* targetItem = targets[i];
                Target* target = [Target new];
                target.productId = targetItem[@"id"];
                target.count = [targetItem[@"required_count"] intValue];
                [targetProducts addObject:target];
            }
        }
        product.targetProducts = targetProducts;
        
        id isActiveValue = prodData[@"active"];
        product.isActive = (isActiveValue == nil || [isActiveValue boolValue]);
    }
}


+(void)parseBoughtProduct:(BoughtProduct*)product dict:(NSDictionary *)prodData {
    [BaseJsonResponseParser parseProduct:product dict:prodData];
    
    product.count = [prodData[@"count"] intValue];
    product.validFrom = [ApiDateFormatter dateFromServerString:prodData[@"validfrom"]];
    product.validTo = [ApiDateFormatter dateFromServerString:prodData[@"validto"]];
    product.signature = prodData[@"signature"];
    product.isValid = [prodData[@"valid"] boolValue];
}

+(void)parseInventoryProduct:(InventoryProduct*)product dict:(NSDictionary *)prodData {
    [BaseJsonResponseParser parseBoughtProduct:product dict:prodData];
    
    product.stamp = [ApiDateFormatter dateFromServerString:prodData[@"stamp"]];
    product.inventoryId = prodData[@"item_id"];
    
}

-(void)removeNulls:(id) object {
    if ([object isKindOfClass:[NSMutableDictionary class]]) {
        NSMutableDictionary* dict = object;
        for (id key in dict.allKeys) {
            if ([[NSNull null] isEqual:dict[key]])
                [dict removeObjectForKey: key];
            else
                [self removeNulls: dict[key]];
        }
    }
    else if ([object isKindOfClass:[NSMutableArray class]]) {
        NSMutableArray* array = object;
        for (id item in array) {
            [self removeNulls: item];
        }
    }
}

@end
