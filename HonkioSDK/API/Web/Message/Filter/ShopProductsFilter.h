//
//  ShopProductsFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 11/2/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsListFilter.h"

@interface ShopProductsFilter : AbsListFilter

@property (nonatomic) NSArray* tags;

@end
