//
//  HKGdprViewController.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 2/6/19.
//  Copyright © 2019 developer. All rights reserved.
//

import UIKit

class HKGdprViewController: HKBaseViewController {
    
    @IBAction func actionForgetMe(_ sender: Any?) {
        HKForgetConfirmAlertView(okAction: {
            BusyIndicator.shared.showProgressView(self.view)
            // TODO do request
            BusyIndicator.shared.hideProgressView()
        }).show(animated: true)
    }
    
    @IBAction func actionReport(_ sender: Any?) {
        CustomInputAlertView(title: "hk_screen_settings_gdpr_msg_report_title".localized, message: "hk_screen_settings_gdpr_msg_report_message".localized) { (email) in
            BusyIndicator.shared.showProgressView(self.view)
            // TODO do request
            BusyIndicator.shared.hideProgressView()
        }.input(HonkioApi.activeUser()?.login).show()
    }

}
