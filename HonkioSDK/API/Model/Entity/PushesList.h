//
//  PushesList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/18/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Push.h"

@interface PushesList : NSObject

@property (nonatomic) NSArray<Push*>* list;
@property (nonatomic) long totalCount;
@property (nonatomic) long unreadCount;

@end
