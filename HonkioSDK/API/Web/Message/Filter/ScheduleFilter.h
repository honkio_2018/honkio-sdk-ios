//
//  ScheduleFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsFilter.h"

// TODO write docs
@interface ScheduleFilter : AbsFilter

@property (nonatomic) NSDate *startDate;
@property (nonatomic) NSDate *endDate;

@property (nonatomic, copy) NSString *objectId;
@property (nonatomic, copy) NSString *objectType;
@property (nonatomic, copy) NSString *queryType;

@end
