//
//  BaseNavigationController.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/22/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

open class HKBaseNavigationController: UINavigationController, HKLifeCycleObserverParentViewController {
    
    fileprivate var lifeCycleObservers: [HKViewControllerLifeCycleObserver] = []
    
    /**
     Connects to the provided life cycle observer.
     - parameter observer: The life cycle observer we're going to connect to.
     */
    open func addLifeCycleObserver(_ observer: HKViewControllerLifeCycleObserver) {
        self.lifeCycleObservers.append(observer)
        observer.didAddToViewController(self)
    }
    
    open func getLifeCycleObserver(_ tag: String) -> HKViewControllerLifeCycleObserver? {
        for i in 0..<self.lifeCycleObservers.count {
            if self.lifeCycleObservers[i].tag == tag {
                return self.lifeCycleObservers[i]
            }
        }
        return nil
    }
    
    // TODO write docs
    open func removeLifeCycleObserver(_ observer: HKViewControllerLifeCycleObserver) {
        for i in 0..<self.lifeCycleObservers.count {
            if self.lifeCycleObservers[i] === observer {
                self.lifeCycleObservers.remove(at: i)
                observer.didAddToViewController(nil)
                return
            }
        }
    }
    
    /// Life cycle observer should be notified the ViewController state has changed.
    override open func viewDidLoad() {
        super.viewDidLoad()
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewDidLoad()
        }
    }
    
    /**
     Life cycle observer should be notified the ViewController state has changed.
     - parameter animated: Whether the state has changed with animation.
     */
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewWillAppear(animated)
        }
    }
    
    /**
     Life cycle observer should be notified the ViewController state has changed.
     - parameter animated: Whether the state has changed with animation.
     */
    override open func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewDidAppear(animated)
        }
    }
    
    /**
     Life cycle observer should be notified the ViewController state has changed.
     - parameter animated: Whether the state has changed with animation.
     */
    override open func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewWillDisappear(animated)
        }
    }
    
    /**
     Life cycle observer should be notified the ViewController state has changed.
     - parameter animated: Whether the state has changed with animation.
     */
    override open func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
        for observer : HKViewControllerLifeCycleObserver in self.lifeCycleObservers {
            observer.viewDidDisappear(animated)
        }
    }
}
