//
//  TaskWrapper.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "TaskWrapper.h"

@implementation TaskWrapper {
    id<Task> wrappedTask;
    BOOL isAborted;
}

-(id)init {
    self = [super init];
    if (self) {
        isAborted = NO;
    }
    return self;
}

-(id)initWithTask:(id<Task>)task {
    self = [self init];
    if (self) {
        wrappedTask = task;
    }
    return self;
}

-(void)set:(id<Task>)task {
    if (isAborted) {
        [task abort];
    }
    wrappedTask = task;
}

-(id<Task>)get {
    return wrappedTask;
}

-(void)abort {
    isAborted = YES;
    if (wrappedTask) {
        [wrappedTask abort];
    }
}

-(BOOL)isAborted {
    return isAborted;
}

-(BOOL)isCompleted {
    if (wrappedTask) {
        return [wrappedTask isCompleted];
    }
    return true;
}

@end
