//
//  HKUserAccountsAddSectionTableViewCell.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/29/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

class HKUserAccountsAddButtonTableViewCell: HKBaseTableViewCell {
    
    var isSetuped = false
    
    fileprivate(set) open lazy var button : UIButton = self.makeButtonView()
    
    override open func setupStyle() {
        super.setupStyle()
        
        selectionStyle = .none
        
        layoutMargins = UIEdgeInsets.zero
        separatorInset = UIEdgeInsets.zero
        
        selectedBackgroundView = UIView()
        backgroundColor = UIColor.clear
    }
    /**
     Add card image and card title into cell content.
     */
    override open func setupSubviews() {
        super.setupSubviews()
        
        contentView.addSubview(self.button)
    }
    /**
     Sets the card image and card title constraints.
     * "H:|-15-[cardImage(30)]-5-[cardTitle]-15-|", "V:|-15-[cardImage(30)]-15-|", "V:|-15-[cardTitle]-15-|".
     */
    override open func setupConstraints() {
        super.setupConstraints()
        
        let views : [String : AnyObject] = [
            "button" : self.button
        ]
        
        let formatArray : [String] = [
            "V:|-0-[button(50)]-35-|",
            "H:|-0-[button]-0-|"
        ]
        
        let constraints :[NSLayoutConstraint] =  NSLayoutConstraint.constraintsWithFormatArray(
            formatArray, options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: views)
        
        NSLayoutConstraint.activate(constraints)
    }

    
    fileprivate func makeButtonView() -> UIButton {
        let view : UIButton = UIButton()
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        view.setTitle("pay_accounts_list_btn_card_add".localized, for: UIControlState())
        view.tintColor = UIColor.white
        view.backgroundColor = UIColor(netHex: 0x094A84)
        
        return view;
    }
}
