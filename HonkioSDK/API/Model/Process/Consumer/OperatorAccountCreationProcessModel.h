//
//  OperatorAccountCreationProcessModel.h
//  HonkioApi
//
//  Created by Shurygin Denis on 8/4/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "AccountCreationProcessModel.h"

/**
 * Process model for User payment account creation with type OPERATOR.
 */
@interface OperatorAccountCreationProcessModel : AccountCreationProcessModel

/**
 * Init Model with specified handler.
 * @param handler ResponseHandler implementation.
 */
-(id)initWithHandler:(ResponseHandler)handler;

@end
