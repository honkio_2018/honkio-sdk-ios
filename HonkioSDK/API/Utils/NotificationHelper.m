//
//  NotificationHelper.m
//  HonkioApi
//
//  Created by Shurygin Denis on 5/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "NotificationHelper.h"

@implementation NotificationHelper

+(void)postOnPushReceived:(Push*)push {
    if (push) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_NAME_ON_PUSH_RECEIVED
                                                            object:nil
                                                          userInfo:@{NOTIFY_EXTRA_PUSH : push}];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_NAME_ON_PUSH_RECEIVED
                                                            object:nil
                                                          userInfo:nil];
    }
}

+(void)postOnLocationChanged:(CLLocation*)location {
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_NAME_ON_PUSH_RECEIVED
                                                        object:nil
                                                      userInfo:@{NOTIFY_EXTRA_LOCATION : location}];
}

+(NSString*)notifyNameOnComplete:(NSString*)command {
    return [NSString stringWithFormat:@"%@.%@", NOTIFY_NAME_ON_COMPLETE, command];
}

+(void)postOnComplete:(NSString*)command response:(Response*)response {
    [[NSNotificationCenter defaultCenter] postNotificationName:[NotificationHelper notifyNameOnComplete:command]
                                                        object:nil
                                                      userInfo:@{NOTIFY_EXTRA_COMMAND : command,
                                                                               NOTIFY_EXTRA_RESULT : response}];
}

+(void)postOnLogin {
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_NAME_ON_LOGIN
                                                        object:nil
                                                      userInfo:nil];
}

+(void)postOnInitialized {
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_NAME_ON_INITIALIZED
                                                        object:nil
                                                      userInfo:nil];

}

+(void)postUnhandledError:(NSString*)command error:(NSError*)error {
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_NAME_UNHANDLED_ERROR
                                                        object:nil
                                                      userInfo:@{NOTIFY_EXTRA_COMMAND : command,
                                                                   NOTIFY_EXTRA_ERROR : error}];
}
+(void)postUnhandledResponse:(NSString*)command response:(Response*)response {
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_NAME_UNHANDLED_RESPONSE
                                                        object:nil
                                                      userInfo:@{NOTIFY_EXTRA_COMMAND : command,
                                                                  NOTIFY_EXTRA_RESULT : response}];
}

//[[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateHistoryNotification" object:nil];
//[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refrash:) name:@"UpdateHistoryNotification" object:nil];
//[[NSNotificationCenter defaultCenter] removeObserver:self name:@"UpdateHistoryNotification" object:nil];

@end
