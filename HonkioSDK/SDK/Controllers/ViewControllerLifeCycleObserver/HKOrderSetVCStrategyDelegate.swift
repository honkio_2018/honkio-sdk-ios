//
//  OrderSetControllerDelegate.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/28/16.
//  Copyright © 2016 developer. All rights reserved.
//

import Foundation

/**
* Describes the set of functions to be implemented for an instance to be used in HKSimpleOrderSetVCStrategy.
*/

@objc public protocol HKOrderSetVCStrategyDelegate {
    /// Should be called before the setting of the Order.
    func orderWillSet(_ vcStrategy: HKSimpleOrderSetVCStrategy)
    /// Should be called after Order is set.
    func orderDidSet(_ vcStrategy: HKSimpleOrderSetVCStrategy, order : Order)
    /// Should be called if error occured during setting of the Order.
    func onOrderSetError(_ vcStrategy: HKSimpleOrderSetVCStrategy, error: ApiError)
}
