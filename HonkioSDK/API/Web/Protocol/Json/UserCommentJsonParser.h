//
//  UserCommentJsonParser.h
//  HonkioSDK
//
//  Created by Dash on 28.09.2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "UserComment.h"

@interface UserCommentJsonParser : BaseJsonResponseParser

+(UserComment*) parseComment:(NSDictionary*) dict;

@end
