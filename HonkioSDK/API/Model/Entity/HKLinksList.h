//
//  HKLinksList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKAsset.h"
#import "HKLink.h"

@interface HKLinksList : NSObject

@property (nonatomic) NSArray<HKLink*> *list;
@property (nonatomic) NSDictionary<NSString*, HKAsset*> *assets;

@end
