//
//  UserRoleDescription.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 9/5/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define USER_ROLE_DESC_TYPE_PUBLIC @"public"
#define USER_ROLE_DESC_TYPE_PRIVATE @"private"

// TODO write docs
@interface UserRoleDescription : NSObject

@property (nonatomic) NSString* roleId;
@property (nonatomic) NSString* name;
@property (nonatomic) NSString* displayName;
@property (nonatomic) NSString* desc;
@property (nonatomic) int totalUsers;

@end
