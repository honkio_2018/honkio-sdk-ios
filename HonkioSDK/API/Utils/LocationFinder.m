//
//  LocationFinder.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "LocationFinder.h"

@implementation LocationFinder {
    NSMutableArray* findLiteners;
}

- (id)init
{
    self = [super init];
    
    if(self) {
        
        //init and update location
        _locationManager = [[CLLocationManager alloc] init];
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.delegate = self;
        [self updateLocation];
    }
    return self;
}

-(void) registerLocationFindListener:(LocationFindListener)listeer {
    if (self.locations && [self.locations count] > 0) {
        listeer();
    }
    else {
        if (!findLiteners) {
            findLiteners = [NSMutableArray arrayWithObject:listeer];
        }
        else {
            [findLiteners addObject:listeer];
        }
    }
}

-(void) updateLocation {
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    self.lastLocationUpdate = [NSDate date];
}

-(void) updateLocationIfNeeded {
    NSTimeInterval inter = [self.lastLocationUpdate timeIntervalSinceNow];
    if(inter < -10.0) {
        [self updateLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.locations = locations;
    if (findLiteners && [findLiteners count] > 0) {
        for (int i = 0; i < [findLiteners count]; i++) {
            LocationFindListener listener = findLiteners[i];
            listener();
        }
        [findLiteners removeAllObjects];
    }
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    self.lastLocationUpdate = [NSDate date];
//    //    self.locations = locations;
//}

//- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
//    if (status == kCLAuthorizationStatusDenied) {
//        // The user denied authorization
//    }
//    else if (status == kCLAuthorizationStatusAuthorized) {
//        // The user accepted authorization
//    }
//    CLAuthorizationStatus newStatus = status;
//}

//- (void)locationManager:(CLLocationManager *)manager
//       didFailWithError:(NSError *)error {
//}

-(BOOL)isHasLocation {
    return [self.locations count] > 0;
}

@end
