//
//  MerchantsListJsonParser.m
//  HonkioSDK
//
//  Created by Mikhail Li on 03/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "MerchantsListJsonParser.h"
#import "HKMerchantsList.h"

@implementation MerchantsListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    HKMerchantsList* list = [HKMerchantsList new];
    
    list.list = [SimpleListJsonParser parseList:message list:dict[@"merchants"]];
    
    return list;
}

@end
