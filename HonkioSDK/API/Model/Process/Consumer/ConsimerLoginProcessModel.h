//
//  ConsimerLoginProcessModel.h
//  HonkioApi
//
//  Created by Shurygin Denis on 11/27/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "BaseProcessModel.h"
#import "ApiResponseHandler.h"

@interface ConsimerLoginProcessModel : BaseProcessModel

-(id)initWithResponseHandler:(ResponseHandler)responseHandler;

/**
 * Do login using login and password.
 * @param login User login.
 * @param password User password.
 * @param flags Flags for requests.
 */
-(void)loginWithLogin:(NSString*)login password:(NSString*)password flags:(int)flags;

/**
 * Do login using PIN code.
 * @param pin PIN code.
 * @param flags Flags for requests.
 */
-(void)loginWithPin:(NSString*)pin flags:(int)flags;

/**
 * Do login using saved PIN code.
 * @param flags Flags for requests.
 */
-(void)loginWithSavedPin:(int)flags;

/**
 * Called when PIN code required. In this case PIN pad should be shown.
 * @param reason Reason for the PIN request.
 * @see PinReason
 */
-(void)requestPin:(int)reason;

/**
 * Should be called when PIN entered.
 * @param pin Entered PIN code.
 * @param reason Reason for the PIN request.
 */
-(void)pinEntered:(NSString*)pin reason:(int)reason;

/**
 * Should be called when PIN pad canceled without entering a PIN.
 * @param reason Reason for the PIN request.
 */
-(void)pinCanceled:(int)reason;

/**
 * Sets callback for the process.
 * @param callback Request callback.
 */
-(void)setResponseHandler:(ResponseHandler)responseHandler;

@end
