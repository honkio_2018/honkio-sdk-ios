//
//  LoadProperties.h
//  Honkio API
//
//  Created by Alexandr Kolganov on 3/3/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (PropertyParser)
-(void)saveStringProperties:(NSString*)key value:(NSString*)value onlyNotEmpty:(BOOL)notEmpty;
-(void)saveDictionaryProperties:(NSString*)key value:(NSDictionary*)value onlyNotEmpty:(BOOL)notEmpty;
-(void)saveBoolProperties:(NSString*)key value:(NSNumber*)value onlyNotEmpty:(BOOL)notEmpty;

@end