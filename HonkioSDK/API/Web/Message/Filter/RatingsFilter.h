//
//  RatingsFilter.h
//  HonkioSDK
//
//  Created by Mikhail Li on 07/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsListFilter.h"

@interface RatingsFilter : AbsListFilter

@property (nonatomic, copy) NSString *object;
@property (nonatomic, copy) NSString *objectType;

@end
