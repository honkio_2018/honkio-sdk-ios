//
//  WebServiceAsync.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "WebServiceAsync.h"
#import "PriorityQueue.h"
#import "UIApplication+NetworkActivity.h"
#import "NSError+ApiError.h"
#import "NotificationHelper.h"
#import "WebServiceCache.h"
#import "HKLog.h"

@implementation WebServiceAsync {
    ResponseHandler respHandler;
    Message* msg;
    
    int errorCode;
    NSString* errorDesc;
    
    NSBlockOperation* operation;
    BOOL isCompleted;
}

-(instancetype)initWithMessage:(Message*)message responseHandler:(ResponseHandler)responseHandler {
    
    self = [super init];
    
    if (self) {
        msg = message;
        respHandler = responseHandler;
        
        errorCode = ErrApiNone;
        errorDesc = nil;
    }
    
    return self;
}

-(WebServiceAsync*)execute {
    if ([msg isOffline])
        [self processOffline];
    else
        [self processOnline];
    return self;
}

-(void)processOffline {
    operation = [NSBlockOperation blockOperationWithBlock:^{
        NSString* cacheFileName = [self->msg cacheFileName];
        if (!cacheFileName || ![self->msg isSupportOfline]) {
            [self performError:ErrApiCacheNotSupported desc:nil];
            return;
        }
        
        NSString* userName = [WebServiceCache getUserNameFromMessage:self->msg];
        
        if ([WebServiceCache isHasCache:cacheFileName user:userName]) {
            NSData* responseData = [WebServiceCache get:cacheFileName user:userName];
            
            if (responseData) {
                Response* response = [self->msg parseResponse:responseData];
                if (response) {
                    [response setIsOffline:YES];
                    [self performOnComplete:response];
                    return;
                }
            }
        }
        
        [self performError:ErrApiCacheNotFound desc:nil];
    }];
    [PriorityQueue addOperation:operation];
}

-(void)processOnline {
    
    operation = [NSBlockOperation blockOperationWithBlock:^{
        
        if ([self->msg connection] && !self->msg.connection.isValid) {
            
            [self performError:ErrApiNoLogin desc:nil];
            return;
        }
        
        NSData* responseData = [self sendSyncMessage];
        
        if (self->errorCode != ErrApiNone) {
            
            [self performError:self->errorCode desc:self->errorDesc];
        }
        else if (!responseData) {
            
            [self performError:ErrApiUnknown desc:self->errorDesc];
        } else {
            
            Response* response = [self->msg parseResponse:responseData];
            
            if (response) {
                
                if (response.otp) {
                    
                    [self->msg.connection setOtp:response.otp];
                }

                if ([response isStatusAccept] && ![self->msg isHasFlag:MSG_FLAG_NO_CACHE]) {
                    
                    NSString* cacheFileName = [self->msg cacheFileName];
                    
                    if (cacheFileName) {
                        [WebServiceCache put:responseData name:cacheFileName user:[WebServiceCache getUserNameFromMessage:self->msg]];
                    }
                }
                    
                [self performOnComplete:response];
            
            } else {
                
                [self performError:ErrApiUnknown desc:nil];
            }
        }
    }];
    
    [PriorityQueue addOperation:operation];
}

-(void)performError:(int)code desc:(NSString*)desc {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (self->respHandler) {
            self->respHandler([Response withError:code desc:desc]);
        }
    }];
}

-(void)performOnComplete:(Response*)response {
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        if (self->respHandler)
            self->respHandler(response);
        if ([response isStatusAccept]) {
            [NotificationHelper postOnComplete:[self->msg notificationCommand] response:response];
        }
    }];
}

-(NSData*) sendSyncMessage {

    [HKLog d: [NSString stringWithFormat:@"POST to URL = %@", [msg url]]];
    
    NSData* body = [msg buildRequest];
    if (!body) {
        
        [HKLog d: [NSString stringWithFormat:@"Request %@. Body is Empty!", [msg command]]];
        return nil;
    }
    
    
    [HKLog d: [NSString stringWithFormat:@"Request %1$@ =>>\n%2$@", [msg command], [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding]]];
    
    //create a mutable HTTP request
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:msg.url];
    
    //set headers
    [urlRequest setTimeoutInterval:20.0f];
    [urlRequest setHTTPMethod:@"POST"];
    
    //set body
    if (![msg binary]) {
        [urlRequest setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [urlRequest setHTTPBody:body];
    }
    else {
        MessageBinary* binary = [msg binary];
        NSString* boundary = [NSString stringWithFormat:@"Boundary-%@", [[NSUUID UUID] UUIDString]];
        
        [urlRequest setValue:[NSString stringWithFormat: @"multipart/form-data; boundary=%@", boundary] forHTTPHeaderField:@"Content-Type"];
        
        NSMutableData* httpBody = [NSMutableData data];
        
        // JSON message
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[@"Content-Disposition: form-data; name=\"message\"; filename=\"message.json\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[@"Content-Type: application/json; charset=utf-8\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:body];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        // File
        [httpBody appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", @"file", [binary fileName]] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", [binary mime]] dataUsingEncoding:NSUTF8StringEncoding]];
        [httpBody appendData:[binary binData]];
        [httpBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [httpBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [urlRequest setHTTPBody:httpBody];
    }

    //Loads the data for a URL request and executes a handler block on an
    //operation queue when the request completes or fails.
    
    NSURLResponse *response;
    NSError* responseError;
    NSData* data = [NSURLConnection sendSynchronousRequest:urlRequest  returningResponse:&response error:&responseError];
    
    [HKLog d: [NSString stringWithFormat:@"Response %1$@ <<=\n%2$@\n--------------------------------------------------------------------------\n", [msg command], [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding]]];
    
    if(responseError) {
        [HKLog d: [NSString stringWithFormat:@"HTTP request error:%@", responseError.description]];
        if([@"NSURLErrorDomain" isEqualToString: responseError.domain]) {
            errorCode = ErrApiNoConnection;
            errorDesc = responseError.userInfo[NSLocalizedDescriptionKey];
        }
        else {
            errorCode = ErrApiUnknown;
            errorDesc = responseError.userInfo[NSLocalizedDescriptionKey];
        }
        return nil;
    }
    else if(response && [response isKindOfClass:[NSHTTPURLResponse class]] && ((NSHTTPURLResponse*) response).statusCode != 200) {
        errorCode = ErrApiBadResponse;
        errorDesc = [NSString stringWithFormat: @"Bad response status code. Status code = %ld", (long)((NSHTTPURLResponse*) response).statusCode];
        return nil;
    }
    return data;
}

-(void)abort {
    if (!isCompleted && operation) {
        [operation cancel];
    }
}

-(BOOL)isAborted {
    return !isCompleted && [operation isCancelled];
}

-(BOOL)isCompleted {
    return isCompleted;
}

@end
