//
//  HonkioOAuth.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/21/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKDevice.h"

@interface HonkioOAuth : NSObject

+(NSString*)authorisationUrl:(HKDevice*) device forClient:(NSString*)clientId redirect:(NSString*)redirectUrl;

+(NSString*)accessTokenUrl:(HKDevice*) device forClient:(NSString*)clientId clientSecret:(NSString*)clientSecret redirect:(NSString*)redirectUrl token:(NSString*)authorizationToken;

@end
