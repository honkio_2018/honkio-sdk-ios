//
//  ApiShopInfo.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shop.h"
#import "MerchantSimple.h"

/**
 * Data object that holds information about shop and his merchant.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface ShopInfo : NSObject

/** Server time when ShopInfo was sent. */
@property (nonatomic) NSDate*  timeStamp;
@property (nonatomic) Shop* shop;
@property (nonatomic) MerchantSimple* merchant;

@end
