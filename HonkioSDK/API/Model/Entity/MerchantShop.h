//
//  MerchantShop.h
//  HonkioSDK
//
//  Created by Mikhail Li on 21/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Identity.h"

#define MERCHANT_SHOP_FIELD_NAME @"str_name"
#define MERCHANT_SHOP_FIELD_DESCRIPTION @"str_description"
#define MERCHANT_SHOP_FIELD_ADDRESS @"str_address"
#define MERCHANT_SHOP_FIELD_LOGO_SMALL @"str_logo_small"
#define MERCHANT_SHOP_FIELD_LOGO_LARGE @"str_logo_large"
#define MERCHANT_SHOP_FIELD_MAX_DISTANCE @"int_max_distance"
#define MERCHANT_SHOP_FIELD_PARENT @"parent"
#define MERCHANT_SHOP_FIELD_SHOP_TYPE @"shop_type"
#define MERCHANT_SHOP_FIELD_SERVICE_TYPE @"service_type"
#define MERCHANT_SHOP_FIELD_SHOP_MODE @"str_shop_mode"
#define MERCHANT_SHOP_FIELD_QR_CODE @"str_qrid"
#define MERCHANT_SHOP_FIELD_ACTIVE @"active"
#define MERCHANT_SHOP_FIELD_DAY_BREAK @"int_day_break"
#define MERCHANT_SHOP_FIELD_EMAIL @"str_email"
#define MERCHANT_SHOP_FIELD_LATITUDE @"latitude"
#define MERCHANT_SHOP_FIELD_LONGITUDE @"longitude"
#define MERCHANT_SHOP_FIELD_TAG_LIST @"list_tags"
#define MERCHANT_SHOP_FIELD_COORDINATES @"coordinates"
#define MERCHANT_SHOP_FIELD_CREATE @"create"

#define MERCHANT_SHOP_TYPE_SHOP @"shop"
#define MERCHANT_SHOP_TYPE_PAYMENT @"payment"
#define MERCHANT_SHOP_TYPE_LOGIN @"login"
#define MERCHANT_SHOP_TYPE_REGISTRATION @"registration"
#define MERCHANT_SHOP_TYPE_LOST_PASSWORD @"lostpassword"

#define MERCHANT_SHOP_SERVICE_TYPE_ECOMMERCE @"ecommerce"
#define MERCHANT_SHOP_SERVICE_TYPE_SYSTEM @"system"
#define MERCHANT_SHOP_SERVICE_TYPE_GAS_STATION @"gas_station"
#define MERCHANT_SHOP_SERVICE_TYPE_CAR_WASH @"car_wash"
#define MERCHANT_SHOP_SERVICE_TYPE_VENDING @"vending"
#define MERCHANT_SHOP_SERVICE_TYPE_GATE_PARKING @"gate_parking"
#define MERCHANT_SHOP_SERVICE_TYPE_OPEN_PARKING @"open_parking"
#define MERCHANT_SHOP_SERVICE_TYPE_TIME_RESERVATION @"time_reservation"

#define MERCHANT_SHOP_MODE_NORMAL @"Normal"
#define MERCHANT_SHOP_MODE_HONKIO @"Honkio"
#define MERCHANT_SHOP_MODE_MECSEL @"Mecsel"

@interface MerchantShop : Identity

@property (nonatomic) NSMutableDictionary *settings;

-(void)setGeneralFields:(NSArray<NSString*>*)fields;
-(void)resetGeneralFields;

-(NSDictionary<NSString*, id>*)toParams;

@end
