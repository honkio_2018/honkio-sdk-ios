//
//  ApiSecurity.m
//  Honkio API
//
//  Created by Alexandr Kolganov on 2/5/14.
//  Copyright (C) 2014 RiskPointer.  All rights reserved.
//

#import "ApiSecurity.h"
#import <CommonCrypto/CommonHMAC.h>
#import <Security/Security.h>

#import "NSData+Hexadecimal.h"
#import "NSString+AESCrypt.h"

@interface ApiSecurity()

//+(NSString*)valueToHashString: (id)value;
//+(NSString*)arrayToHashString: (NSArray*)arr;

@end

@implementation ApiSecurity

+(NSString*) getRandomString
{
    
    NSMutableData* data = [NSMutableData dataWithLength:32];
    SecRandomCopyBytes(kSecRandomDefault, 32, [data mutableBytes]);
    return [data hexadecimalString];
}


+(NSString*) dictionaryToHashString: (NSDictionary*)dict
{
    
    NSArray *keys = [dict allKeys];
    NSArray *sortedKeys = [keys sortedArrayUsingSelector:@selector(compare:)];
    
    NSMutableArray* joinedArray = [NSMutableArray new];

    for(NSString* key in sortedKeys) {
        if([key isEqualToString:@"hash"] || [key hasPrefix:@"rp-api-"])
            continue;
        
        id value = dict[key];
        
        if(value == nil || [value isKindOfClass:[NSNull class]]) {
            [joinedArray addObject:[NSString stringWithFormat:@"%@=null", key]];
        }
        else if([value isKindOfClass:[NSDictionary class]]) {
            [joinedArray addObject:[self dictionaryToHashString:value]];
        }
        else if([value isKindOfClass:[NSArray class]]) {
            NSArray* arr = value;
            if([arr count] == 0) {
                [joinedArray addObject:[NSString stringWithFormat:@"%@=", key]];
            }
            else if([arr[0] isKindOfClass:[NSDictionary class]]) {
                for(NSDictionary* dict2 in arr) {
                    [joinedArray addObject:[self dictionaryToHashString:dict2]];
                }
            }
            else {
                NSString* string = [arr componentsJoinedByString:@","];
                [joinedArray addObject:[NSString stringWithFormat:@"%@=%@", key, string]];
            }
        }
        else if([value isKindOfClass:[NSNumber class]]) {
            
            if(strcmp([value objCType], @encode(char)) == 0) { //true or false
                [joinedArray addObject:[NSString stringWithFormat:@"%@=%@",key, ([value boolValue] ? @"true": @"false")]];
            }
            else if (strcmp([value objCType], @encode(int)) == 0) {
                [joinedArray addObject:[NSString stringWithFormat:@"%@=%d",key, [(NSNumber *)value intValue]]];
            }
            else if (strcmp([value objCType], @encode(double)) == 0) {
                    [joinedArray addObject:[NSString stringWithFormat:@"%@=%.1f", key, round([(NSNumber *)value doubleValue] * 10.0) / 10.0]];
            }
            else if (strcmp([value objCType], @encode(float)) == 0) {
                    [joinedArray addObject:[NSString stringWithFormat:@"%@=%.1f", key, roundf([(NSNumber *)value floatValue] * 10.0) / 10.0]];
            }
            else if (strcmp([value objCType], @encode(bool)) == 0) {
                
                [joinedArray addObject:[NSString stringWithFormat:@"%@=%@", key, ([(NSNumber *)value boolValue] ? @"true" : @"false")]];
            }
            else {
                [joinedArray addObject:[NSString stringWithFormat:@"%@=%@",key, value]];
            }
        }
        else {
           [joinedArray addObject:[NSString stringWithFormat:@"%@=%@",key, value]];
        }
    }
    return [joinedArray componentsJoinedByString:@"*"];
}

+(NSString*) calculateHashForKey:(NSString*)key andString:(NSString*)str
{
    NSData *keyData = [key dataUsingEncoding:NSUTF8StringEncoding];
    NSData *strData = [str dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableData* hash = [NSMutableData dataWithLength:CC_SHA512_DIGEST_LENGTH ];
    CCHmac(kCCHmacAlgSHA512, keyData.bytes, keyData.length, strData.bytes, strData.length, hash.mutableBytes);
    //NSString *base64Hash = [hash base64EncodedStringWithOptions:0];
    NSString *hexString = [hash hexadecimalString];
    return hexString;
}

+(NSString*) calculateHashForKey:(NSString*)key andDictionary:(NSDictionary*)dict
{
    return [ApiSecurity calculateHashForKey:key andString:[ApiSecurity dictionaryToHashString:dict]];
}

+(NSString*) encryptString:(NSString*)string withKey:(NSString*)key {
    NSString* str;
    @try {
        str = [string AES256EncryptWithKey:key];
    }
    @catch(NSException *exception) {
    }
    @finally {
        if(!str)
            str = @"";
        return str;
    }
    //return [NSString stringWithFormat: @"%@%@", key, string];
}

+(NSString*) decryptString:(NSString*)string withKey:(NSString*)key {
    
    NSString* str;
    @try {
        str = [string AES256DecryptWithKey:key];
    }
    @catch(NSException *exception) {
    }
    @finally {
        if(!str)
            str = @"";
        return str;
    }
    
}

@end
