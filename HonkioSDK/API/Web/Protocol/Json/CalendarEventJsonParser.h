//
//  CalendarEventJsonParser.h
//  HonkioSDK
//
//  Created by Mikhail Li on 27/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "CalendarEvent.h"

@interface CalendarEventJsonParser : BaseJsonResponseParser

+(CalendarEvent *)parseCalendarEvent:(NSDictionary *)dict;

@end
