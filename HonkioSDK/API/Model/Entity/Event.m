//
//  Event.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "Event.h"

@implementation Event

-(BOOL)isBusy {
    return [EVENT_TYPE_BUSY isEqualToString:self.type];
}

@end
