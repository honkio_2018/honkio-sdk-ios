//
//  HonkioApiNew.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sys/utsname.h>
#import "HonkioApi.h"
#import "NSError+ApiError.h"
#import "Message.h"
#import "ProtocolFactory.h"
#import "WebServiceExecutor.h"
#import "RequestBuilder.h"
#import "JsonProtocolFactory.h"
#import "TaskWrapper.h"
#import "LocationFinder.h"
#import "ShopFilter.h"
#import "BookedProduct.h"
#import "SimpleAsyncPost.h"
#import "UserAccessList.h"
#import "HKApiState.h"


@interface HonkioApi()
    
    @property HKApiState* state;

    @property Connection* connection;
    @property id<ProtocolFactory> protocolFactory;

    @property (nonatomic) LocationFinder *locationFinder;

@end

@implementation HonkioApi

CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(HonkioApi);

-(id)init {
    self = [super init];
    if (self) {
        _protocolFactory = [[JsonProtocolFactory alloc] init];
        _locationFinder = [[LocationFinder alloc] init];
    }
    return self;
}

+ (void)initInstanceWithUrl:(nonnull NSString*)apiUrl app:(nonnull Identity*)appIdentity {
    [HonkioApi shared].state = [[HKApiState alloc] initWithUrl:[NSURL URLWithString:apiUrl] appIdentity:appIdentity];
}

+ (void)initInstanceWithApp:(nonnull Identity*)appIdentity {
    [HonkioApi initInstanceWithUrl:@"https://api.honkio.com/" app:appIdentity ];
}

+ (nonnull id<Task>)loadServerData:(int)flags handler:(nonnull SimpleResponseHandler)responseHandler {
    
    TaskWrapper* taskWrapper = [TaskWrapper new];
    
    [HonkioApi loadServerData_StepDevice:flags task:taskWrapper handler:responseHandler];
    
    return taskWrapper;
}

+ (void)loadServerData_StepDevice:(int)flags task:(nonnull TaskWrapper*)taskWrapper handler:(nonnull SimpleResponseHandler)responseHandler {
    if (![taskWrapper isAborted]) {
        if ([[[HonkioApi shared] state] device]) {
            [HonkioApi loadServerData_StepServerInfo:flags task:taskWrapper handler:responseHandler];
        }
        else {
            HKDevice* device = [HKDevice new];
            
            device.app = [[HonkioApi appIdentity] identityId];
            device.name = [[UIDevice currentDevice] name];
            device.platform = DEVICE_PLATFORM_IOS;
            device.manufacturer = @"Apple";
            device.notifyType = DEVICE_NOTIFY_ALL;
            device.osVersion = [[UIDevice currentDevice] systemVersion];
            
            struct utsname systemInfo;
            uname(&systemInfo);
            device.model = [NSString stringWithCString:systemInfo.machine
                                      encoding:NSUTF8StringEncoding];
            
            Message* message = [HonkioApi buildMessage:MSG_COMMAND_SET_DEVICE connection:[HonkioApi activeConnection] shopIdentity:nil responseParser:[[HonkioApi protocolFactory] newParser: HKDevice.class] flags:flags];
            
            [message addParameters: [device toParams]];
            
            [taskWrapper set:[WebServiceExecutor build:message handler:^(Response * _Nullable response) {
                if ([response isStatusAccept]) {
                    [[[HonkioApi shared] state] deviceInfoDidGet:(HKDevice*) response.result];
                    [HonkioApi loadServerData_StepServerInfo:flags task:taskWrapper handler:responseHandler];
                }
                else {
                    responseHandler([response anyError]);
                }
            }]];
        }
    }
}

+ (void)loadServerData_StepServerInfo:(int)flags task:(nonnull TaskWrapper*)taskWrapper handler:(nonnull SimpleResponseHandler)responseHandler {
    if (![taskWrapper isAborted]) {
        [taskWrapper set:[HonkioApi serverGetInfo:flags handler:^(Response * _Nullable response) {
            if ([response isStatusAccept]) {
                [[[HonkioApi shared] state] serverInfoDidGet:(ServerInfo*) response.result];
                [HonkioApi loadServerData_StepAppInfo:flags task:taskWrapper handler:responseHandler];
            }
            else {
                responseHandler([response anyError]);
            }
        }]];
    }
}

+ (void)loadServerData_StepAppInfo:(int)flags task:(nonnull TaskWrapper*)taskWrapper handler:(nonnull SimpleResponseHandler)responseHandler {
    if (![taskWrapper isAborted]) {
        [taskWrapper set:[HonkioApi appGetInfo:flags handler:^(Response * _Nullable response) {
            if ([response isStatusAccept]) {
                [[[HonkioApi shared] state] appInfoDidGet:(AppInfo*) response.result];
                [HonkioApi loadServerData_StepShopInfo:flags task:taskWrapper handler:responseHandler];
            }
            else {
                responseHandler([response anyError]);
            }
        }]];
    }
}

+ (void)loadServerData_StepShopInfo:(int)flags task:(nonnull TaskWrapper*)taskWrapper handler:(nonnull SimpleResponseHandler)responseHandler {
    if (![taskWrapper isAborted]) {
        [taskWrapper set:[HonkioApi shopGetInfo:[HonkioApi mainShopIdentity] flags:flags handler:^(Response *response) {
            if ([response isStatusAccept]) {
                [[[HonkioApi shared] state] shopInfoDidGet:(ShopInfo*) response.result];
                [HonkioApi loadServerData_Complete:responseHandler];
            }
            else {
                responseHandler([response anyError]);
            }
        }]];
    }
}

+ (void)loadServerData_Complete:(nonnull SimpleResponseHandler)responseHandler {
    [HonkioApi shared].connection = nil;
    responseHandler(nil);
    
    [NotificationHelper postOnInitialized];
}


+ (nonnull id<Task>)serverGetInfo:(int)flags handler:(nonnull ResponseHandler)responseHandler {
    Identity* dummyShopIdentity = [[Identity alloc] initWithId:@"none" andPassword:@""];
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_SERVER_INFO connection:nil shopIdentity:dummyShopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[ServerInfo class]]  flags:flags];
    message.isIgnoreHash = YES;
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+ (nonnull id<Task>)serverGetTou:(int)flags handler:(nonnull ResponseHandler)responseHandler {
    
    Connection* connection = [HonkioApi activeConnection];
    if (![connection isValid])
        connection = nil;
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_SERVER_GET_TOU connection:connection shopIdentity:nil responseParser:[[HonkioApi protocolFactory] newParser:[ServerFile class]] flags:flags];
    [message addParameter:MSG_PARAM_QUERY_LANGUAGE value:[[HonkioApi serverInfo] suitableLanguage]];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(nonnull id<Task>)appGetInfo:(int)flags handler:(nonnull ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_APP_INFO connection:nil shopIdentity:[[Identity alloc] initWithId:@"none" andPassword:@""] responseParser:[[HonkioApi protocolFactory] newParser:[AppInfo class]]  flags:flags];
    [message setCacheFileName:MSG_COMMAND_APP_INFO dependsOnShop:NO];
    [message setIsIgnoreHash:YES];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)shopGetInfo:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_SHOP_INFO connection:nil shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[ShopInfo class]] flags:flags];
    
    ResponseHandler responseWrapper = ^(Response *response) {
        if (response && shopIdentity) {
            ShopInfo* shopInfo = (ShopInfo*) response.result;
            if (shopInfo && shopInfo.shop) {
                shopInfo.shop.password = shopIdentity.password;
            }
        }
        if (responseHandler)
            responseHandler(response);
    };
    
    return [WebServiceExecutor build:message handler:responseWrapper];
}

+(id<Task>)shopFind:(ShopFilter*)filter flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi shopFind:filter params:nil flags:flags handler:responseHandler];
}

+(id<Task>)shopFind:(ShopFilter*)filter params:(NSDictionary*) params flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi shopFind:responseHandler filter:filter params:params flags:flags ignoreEmptyLocation:NO];
}

// Private ===============
+(id<Task>)shopFind:(ResponseHandler)responseHandler filter:(ShopFilter*)filter params:(NSDictionary*) params flags:(int)flags ignoreEmptyLocation:(BOOL)ignoreEmptyLocation {
    if (ignoreEmptyLocation || [[HonkioApi shared].locationFinder isHasLocation] || [AbsFilter isHasLocation:params] || (filter && [filter isHasLocation])) {
        Message* message = [HonkioApi buildMessage:MSG_COMMAND_SHOP_FIND connection:nil shopIdentity:nil responseParser:[[HonkioApi protocolFactory] newParser:[ShopFind class]] flags:flags];
        
        if (filter)
            [filter apply:message];
        
        if (params)
            [message addParameters:params];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    else {
        [[HonkioApi shared].locationFinder registerLocationFindListener:^{
            [HonkioApi shopFind:filter params:params flags:flags handler:responseHandler];
        }];
        // TODO return Task object
        return nil;
    }
}

+(id<Task>)shopFindById:(NSString *)shopId flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi shopFind:responseHandler filter:nil params:@{@"shopid" : shopId} flags:flags ignoreEmptyLocation:YES];
}

+(id<Task>)shopFindBuyQrId:(NSString *)qrId flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi shopFind:responseHandler filter:nil params:@{@"qrid" : qrId} flags:flags ignoreEmptyLocation:YES];
}

+(id<Task>)shopGetProducts:(ShopProductsFilter*)filter flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi shopGetProducts:filter shop:[HonkioApi mainShopIdentity] flags:flags handler:responseHandler];
}

+(id<Task>)shopGetProducts:(ShopProductsFilter*)filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_SHOP_PRODUCTS connection:nil shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[ShopProducts class]] flags:flags];
    
    [message addParameter:MSG_PARAM_QUERY_LANGUAGE value:[[HonkioApi serverInfo] suitableLanguage]];
    
    if (filter)
        [filter apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(nonnull id<Task>)shopGetProductTags:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_GET_PRODUCT_TAGS shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[TagsList class]] flags:flags];
    
    [message addParameter:MSG_PARAM_QUERY_LANGUAGE value:[[HonkioApi serverInfo] suitableLanguage]];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(Message *)getSchedule:(ScheduleFilter *)filter shopIdentity:(nullable Identity *)shopIdentity flags:(int)flags {
    Message *message = [HonkioApi buildMessage:MSG_COMMAND_GET_SCHEDULE connection:[HonkioApi activeConnection] shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[CalendarSchedule class]] flags:flags];
    
    if (filter)
        [filter apply:message];
    
    return message;
}

+(id<Task>)getSchedule:(nullable ScheduleFilter *)filter shop:(nullable Identity *)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        return [WebServiceExecutor build:[HonkioApi getSchedule:filter shopIdentity:shopIdentity flags:flags] handler:responseHandler];
    }
    return nil;
}

+(id<Task>)getScheduleProducts:(ScheduleFilter*)filter shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_GET_SCHEDULE_PRODUCTS shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[ShopProducts class]] flags:flags];
    
    if (filter) {
        [filter apply:message];
    }
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)getSplitSchedule:(SplitScheduleFilter*)filter shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_GET_SPLIT_SCHEDULE shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[Schedule class]] flags:flags];
    
    if (filter) {
        [filter apply:message];
    }
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+ (id<Task>)shopGetComments:(nullable FeedbackFilter*)filter
                       shop:(nullable Identity*)shopIdentity
                      flags:(int)flags
                    handler:(nullable ResponseHandler)responseHandler {
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_SHOP_GET_RATE
                                  shopIdentity:shopIdentity
                                responseParser:[[HonkioApi protocolFactory] newParser:[UserCommentsList class]]
                                         flags:flags];
    
    if (filter) {
        [filter apply:message];
    }
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+ (id<Task>)shopGetComments:(nullable NSDictionary*)params
                              flags:(int)flags
                            handler:(nullable ResponseHandler)responseHandler {
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_SHOP_GET_RATE
                                  shopIdentity:nil
                                responseParser:[[HonkioApi protocolFactory] newParser:[UserCommentsList class]]
                                         flags:flags];
    
    for (NSString *key in params.allKeys) {
        [message addParameter:key value:params[key]];
    }
    
    
    
    return [WebServiceExecutor build:message handler:responseHandler];
}



+(id<Task>)transactionSetStatus:(NSString*)status transactionId:(NSString*)transactionId shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_TRANSACTION_STATUS_SET shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:nil] flags:flags];
    
    [message addParameter:MSG_PARAM_STATUS value:status];
    [message addParameter:MSG_PARAM_ID value:transactionId];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(BOOL)checkInitialisation:(ResponseHandler)handler {
    if(![HonkioApi isInitialised]) {
        if(handler)
            handler([Response withError:ErrApiNoInitialized desc:nil]);
        return false;
    }
    return true;
}

+(BOOL)checkConnection:(ResponseHandler)handler {
    if(![HonkioApi isConnected]) {
        if(handler)
            handler([Response withError:ErrApiNoLogin desc:nil]);
        return false;
    }
    return true;
}

+(BOOL)checkApi:(ResponseHandler)handler {
    return [self checkInitialisation:handler] && [self checkConnection:handler];
}

+(BOOL)isInitialised {
    return [HonkioApi device] && [HonkioApi appInfo] && [HonkioApi mainShopInfo] && [HonkioApi serverInfo];
}

+(BOOL)isConnected {
    return [HonkioApi activeConnection] && [[HonkioApi activeConnection] isValid] && [self isInitialised];
}

//============================================================
// Chat     ==================================================
//============================================================
+(nonnull id<Task>)userGetChatMessages:(nullable ChatMessageFilter*)filter
                                 flags:(int)flags
                               handler:(nullable ResponseHandler)responseHandler {
    
    
    Message* message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_GET_CHAT
                                    responseParser:[[HonkioApi protocolFactory] newParser:[ChatMessages class]]
                                             flags:flags];
    if (filter)
        [filter apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(nonnull id<Task>)userSendChatMessage:(nonnull ChatMessage*)chatMessage
                                  flags:(int)flags
                                handler:(nonnull ResponseHandler)responseHandler {
    
    Message *message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_SEND_CHAT
                                    responseParser:[[HonkioApi protocolFactory] newParser:[ChatMessages class]]
                                             flags:flags];
    
    [chatMessage apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

//============================================================
// User ==================================================
//============================================================

+(id<Task>)userGetProducts:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userGetProducts:[HonkioApi mainShopIdentity] flags:flags handler:responseHandler];
}

+(id<Task>)userGetProducts:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_SHOP_PRODUCTS shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[ShopProducts class]] flags:flags];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(BOOL)checkReLogin:(ResponseHandler)handler {
    if([HonkioApi checkInitialisation:handler]) {
        if([Connection isHasSoredOtp]) {
            return true;
        }
        else handler([Response withError:ErrApiNoLogin desc:nil]);
    }
    return false;
}

+(id<Task>)logout:(int)flags handler:(SimpleResponseHandler)responseHandler {
    if (![HonkioApi isConnected] || (flags & MSG_FLAG_OFFLINE_MODE) == MSG_FLAG_OFFLINE_MODE) {
        [HonkioApi logout];
        
        if (responseHandler)
            responseHandler(nil);
            
        [NotificationHelper postOnComplete:MSG_COMMAND_LOGOUT response:[[Response alloc] initWithResult:nil]];
        
        return nil;
    }
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_LOGOUT shopIdentity:nil responseParser:[[HonkioApi protocolFactory] newParser: nil] flags:flags];
    
    return [WebServiceExecutor build:message handler:^(Response * _Nonnull res) {
        if ([res isStatusAccept]) {
            [HonkioApi logout];
            
            if (responseHandler)
                responseHandler(nil);
        }
        else if (responseHandler) {
            responseHandler([res anyError]);
        }
    }];
}

+(void)logout {
    [[HonkioApi shared].connection exit];
    [Connection removeSoredOtp];
    [HonkioApi shared].connection = nil;
}

+(id<Task>)loginWithPin:(NSString*)pin flags:(int)flags handler:(ResponseHandler)responseHandler {
    Connection* connection = [[Connection alloc] initWithPin:pin];
    
    ResponseHandler wrapper =  ^(Response *response) {
        if ([[response error] code] == ErrCommonInvalidUserLogin) {
            [[HonkioApi activeConnection] increasePinFails];
        }
        if (response && [response otp]) {
            [HonkioApi shared].connection = [[response message] connection];
        }
        responseHandler(response);
    };
    
    Identity* shopIdentity = [[[HonkioApi mainShopInfo] merchant] loginShopIdentity];
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET connection:connection shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[User class]] flags:flags];
    [message addParameter:MSG_PARAM_USER_LOGIN value:[NSNumber numberWithBool:YES]];
    
    return [WebServiceExecutor build:message handler:[HonkioApi wrapUserResponseHandler:wrapper]];
}

+(id<Task>)loginWithToken:(NSString*)token flags:(int)flags handler:(ResponseHandler)responseHandler {
    [HonkioApi logout: MSG_FLAG_OFFLINE_MODE handler: nil];
    
    Identity* shopIdentity = [[[HonkioApi mainShopInfo] merchant] loginShopIdentity];
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET connection:nil shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[User class]] flags:flags];
    [message addParameter:MSG_PARAM_AUTH_TOKEN value:token];
    [message addParameter:MSG_PARAM_USER_LOGIN value:[NSNumber numberWithBool:YES]];
    
    return [WebServiceExecutor build:message handler:[HonkioApi wrapUserResponseHandler:responseHandler]];
}

+(id<Task>)userReRegister:(User*) user flags:(int)flags handler:(ResponseHandler)responseHandler {
    Identity* shopIdentity = [[[HonkioApi mainShopInfo] merchant] registrationShopIdentity];
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_SET connection:nil shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[User class]] flags:flags];
    
    if (user)
        [user apply:message];
    [message addParameter:MSG_PARAM_USER_RE_REGISTRATION value:[NSNumber numberWithBool:YES]];
    
    return [WebServiceExecutor build:message handler:[HonkioApi wrapUserRegistrationResponseHandler:responseHandler]];
}

+(Message *)userAccessList:(NSString *)parentMerchant flags:(int)flags {
    Message *message = [HonkioApi buildMessage:MSG_COMMAND_USER_ACCESS_LIST
                                    connection:[HonkioApi activeConnection]
                                  shopIdentity:[HonkioApi mainShopIdentity]
                                responseParser:[[HonkioApi protocolFactory] newParser:[UserAccessList class]]
                                         flags:flags
                        ];
    [message addParameters: @{@"query_parent": parentMerchant} ];
    return message;
}

+(nullable id<Task>)userAccessList:(NSString *)parentMerchant flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioApi userAccessList:parentMerchant flags:(int)flags];
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(Message *)userMerchantAccessList:(NSString *)merchant flags:(int)flags {
    Message *message = [HonkioApi buildMessage:MSG_COMMAND_USER_ACCESS_LIST
                                    connection:[HonkioApi activeConnection]
                                  shopIdentity:[HonkioApi mainShopIdentity]
                                responseParser:[[HonkioApi protocolFactory] newParser:[UserAccessList class]]
                                         flags:flags
                        ];
    [message addParameters: @{@"query_merchant": merchant} ];
    return message;
}

+(nullable id<Task>)userMerchantAccessList:(NSString *)merchant flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message *message = [HonkioApi userMerchantAccessList:merchant flags:(int)flags];
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(id<Task>)userGet:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userGet:responseHandler connection:nil flags:flags];
}

/**
 * PRIVATE
 */
+(id<Task>)userGet:(ResponseHandler)responseHandler connection:(Connection*)connection flags:(int)flags {
    Identity* shopIdentity = [[[HonkioApi mainShopInfo] merchant] loginShopIdentity];
    
    if (!connection) {
        connection = [HonkioApi activeConnection];
    }
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET
                                    connection:connection
                                  shopIdentity:shopIdentity
                                responseParser:[[HonkioApi protocolFactory] newParser:[User class]]
                                         flags:flags];
    
    return [WebServiceExecutor build:message
                             handler:[HonkioApi wrapUserResponseHandler:responseHandler]];
}

/**
 * PRIVATE
 */
+(id<Task>)userUpdate:(ResponseHandler)responseHandler params:(NSDictionary*) params flags:(int)flags {
    Message* message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_SET responseParser:[[HonkioApi protocolFactory] newParser:[User class]] flags:flags];
    
    [message addParameters:params];
    
    return [WebServiceExecutor build:message handler:[HonkioApi wrapUserResponseHandler:responseHandler]];
}

+(id<Task>)userUpdate:(User*) user flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userUpdate:responseHandler params:[user toParams] flags:flags];
}


// TODO write docs
+(id<Task>)userClearMediaUrl:(NSString*) urlId flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_SET_MEDIA
                                        responseParser:[[HonkioApi protocolFactory] newParser:[HKMediaUrl class]]
                                                 flags:flags];
        
        [message addParameter:@"id" value:urlId];
        [message addParameter:@"access" value:@"public"];
        [message addParameter:@"object" value:@""];
        [message addParameter:@"object_type" value:@""];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

// TODO write docs
+(id<Task>)userGetMediaUrl:(NSString*) objectId type:(NSString*)objectType flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_SET_MEDIA
                                        responseParser:[[HonkioApi protocolFactory] newParser:[HKMediaUrl class]]
                                                 flags:flags];
        
        [message addParameter:@"access" value:@"public"];
        [message addParameter:@"object" value:objectId];
        [message addParameter:@"object_type" value:objectType];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

// TODO write docs
+(nullable id<Task>)userUpdateMedia:(nullable HKMediaUrl*)url data:(nonnull MessageBinary*)data flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        
        Message* message =  [[Message alloc] initWithCommand:MSG_COMMAND_USER_SET_MEDIA url:[url uploadUrl] connection:[HonkioApi activeConnection] device:[HonkioApi device] shopIdentity:[HonkioApi mainShopIdentity] requestBuilder:[HonkioApi newRequestBuilder] responseParser:[[HonkioApi protocolFactory] newParser:nil] flags:flags];
        
        message.isSupportOfline = false;
        message.binary = data;
        
        [message addParameter:@"upload_token" value:url.uploadToken];
        [message addParameter:@"id" value:url.objectId];
        [message addParameter:@"access" value:@"public"];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(id<Task>)userMediaFilesList:(MediaFileFilter*) filter flags:(int)flags handler:(ResponseHandler)responseHandler {
    if ([HonkioApi checkInitialisation:responseHandler]) {
        Message* message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_GET_MEDIA_FILES_LIST
                                        responseParser:[[HonkioApi protocolFactory]
                                                        newParser:[HKMediaFileList class]]
                                                 flags:flags];
        
        [filter apply:message];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(id<Task>)userAddAccount:(UserAccount*) account flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userUpdateAccount:responseHandler account:@{@"index": @"+",
                                                             @"type" : account.type,
                                                             @"number": [account number]
                                                             } flags:flags];
}

+(id<Task>)userEditAccount:(UserAccount*) account flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userUpdateAccount:responseHandler account:@{@"index": [NSNumber numberWithInteger:account.index],
                                                                    @"type" : account.type,
                                                                    @"number": [account number]
                                                                    } flags:flags];
}

+(id<Task>)userMoveAccount:(UserAccount*) account position:(NSInteger)position flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userUpdateAccount:responseHandler account:@{@"index": [NSNumber numberWithInteger:account.index],
                                                             @"move" : [NSNumber numberWithInteger:position]
                                                             } flags:flags];
}

+(id<Task>)userDeleteAccount:(UserAccount*) account flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userUpdateAccount:responseHandler account:@{@"index": [NSNumber numberWithInteger:account.index],
                                                             @"move" : @"-"
                                                             } flags:flags];
}

/**
 * PRIVATE
 */
+(id<Task>)userUpdateAccount:(ResponseHandler)responseHandler account:(NSDictionary*)account flags:(int)flags {
    return [HonkioApi userUpdate:responseHandler params:@{@"consumer_account" : account} flags:flags];
}

+(id<Task>)userGetRole:(NSString*)roleId flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userGetRole:roleId userId:nil flags:flags handler:responseHandler];
}

+(id<Task>)userGetRole:(NSString *)roleId userId:(NSString*)userId flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userGetRole:roleId userId:userId shop:nil flags:flags handler:responseHandler];
}

+(nonnull id<Task>)userGetRole:(nonnull NSString*)roleId userId:(nullable NSString*)userId shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET_ROLE connection:[HonkioApi activeConnection] shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[UserRole class]] flags:flags];
    
    [message addParameter:MSG_PARAM_ROLE value:roleId];
    if (userId)
    [message addParameter:MSG_PARAM_USERID value:userId];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)userGetRoles:(NSArray *)roleIds userId:(NSString*)userId shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET_ROLES shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[UserRolesList class]] flags:flags];
    
    if (roleIds)
        [message addParameter:@"roles" value:roleIds];

    if (userId)
        [message addParameter:MSG_PARAM_USERID value:userId];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)userGetRoles:(NSArray *)roleIds userId:(NSString*)userId flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userGetRoles:roleIds userId:userId shop:nil flags:flags handler:responseHandler];
}

+(nonnull id<Task>)userGetRoles:(nullable NSArray *)roleIds flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    return [HonkioApi userGetRoles:roleIds userId:nil flags:flags handler:responseHandler];
}

+(id<Task>)userSetRole:(nonnull UserRole*) role flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    return [HonkioApi userSetRole:role shop:nil flags:flags handler:responseHandler];
}

+(nonnull id<Task>)userSetRole:(nonnull UserRole*) role shop:(nullable Identity*)shopIdentity  flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_SET_ROLE connection:[HonkioApi activeConnection] shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[UserRole class]] flags:flags];
    
    [role apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(nonnull id<Task>)userDeleteRole:(nonnull NSString*) roleId flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    return [HonkioApi userDeleteRole:roleId shop:nil flags:flags handler:responseHandler];
}

+(nonnull id<Task>)userDeleteRole:(nonnull NSString*) roleId shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_SET_ROLE connection:[HonkioApi activeConnection] shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[UserRole class]] flags:flags];
    
    [message addParameter:@"role_id" value:roleId];
    [message addParameter:@"delete" value:@"Yes"];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(nonnull id<Task>)userUpdatePhoto:(nonnull MessageBinary*)data flags:(int)flags responseHandler:(nullable ResponseHandler)responseHandler {
    
    Message* message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_SET_PHOTO responseParser:[[HonkioApi protocolFactory] newParser:nil] flags:flags];
    message.isSupportOfline = NO;
    message.binary = data;
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+ (id<Task>)userCheckPassword:(nonnull NSString *)password flags:(int)flags andRespone:(nonnull ResponseHandler)responseHandler {
    
    Message* message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_GET responseParser:[[HonkioApi protocolFactory] newParser:[User class]] flags:flags];
    
    [message addParameter:MSG_PARAM_LOGIN_PASSWORD value:password];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)userUpdatePassword:(NSString*)oldPassword newPassword:(NSString*)newPassword flags:(int)flags handler:(ResponseHandler)responseHandler {
    
    Message* message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_GET responseParser:[[HonkioApi protocolFactory] newParser:[User class]] flags:flags];
    
    if (oldPassword)
        [message addParameter:MSG_PARAM_LOGIN_PASSWORD value:oldPassword];
    
    ResponseHandler wrapper = ^(Response *response) {
        if ([response isStatusAccept] || [response isStatusPending]) {
            User* user = [User new];
            user.password = newPassword;
            
            [HonkioApi userUpdate:user flags:flags handler:responseHandler];
        }
        else
            responseHandler(response);
    };
    
    return [WebServiceExecutor build:message handler:wrapper];
}

+(id<Task>)userUpdateDictValues:(NSDictionary*)dict flags:(int)flags handler:(ResponseHandler)responseHandler {
    User* user = [User new];
    user.userKeystore = dict;
    
    return [HonkioApi userUpdate:user flags:flags handler:responseHandler];
}

+(id<Task>)userSetDevice:(HKDevice*)device flags:(int)flags handler:(ResponseHandler)responseHandler {
    if ([HonkioApi checkInitialisation:responseHandler]) {
        Message* message = [HonkioApi buildMessage:MSG_COMMAND_SET_DEVICE
                                        connection:device.device_id != nil ? [HonkioApi activeConnection] : nil
                                      shopIdentity:nil
                                    responseParser:[[HonkioApi protocolFactory] newParser: HKDevice.class]
                                             flags:flags];
        
        [message addParameters: [device toParams]];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+(id<Task>)userTouAgree:(NSString*)touUid flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_ACCEPT_TOU connection:[HonkioApi activeConnection] shopIdentity:nil responseParser:[[HonkioApi protocolFactory] newParser: nil] flags:flags];
    
    message.isSupportOfline = false;
    
    if (touUid != nil && ![TOU_KEY_DEFAULT isEqualToString:touUid]) {
        [message addParameter:MSG_PARAM_USER_TOU_KEY value:touUid];
    }
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)userGetFile:(NSString*)fileName flags:(int)flags handler:(ResponseHandler)responseHandler {
    Connection* connection = [HonkioApi activeConnection];
    
    if (![connection isValid])
        connection = nil;
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET_FILE connection:connection shopIdentity:nil responseParser:[[HonkioApi protocolFactory] newParser:[ServerFile class]] flags:flags];
    
    [message addParameter:MSG_PARAM_ID value:fileName];
    if (!connection) {
        [message addParameter:MSG_PARAM_LOGIN_IDENTITY value: @"-"];
        [message addParameter:MSG_PARAM_LOGIN_PASSWORD value: @"-"];
        [message addParameter:MSG_PARAM_QUERY_LANGUAGE value: [[HonkioApi serverInfo] suitableLanguage]];
    }
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)userCreateTempPassword:(NSString*)login flags:(int)flags handler:(ResponseHandler)responseHandler {
    Identity* shopIdentity = [[[HonkioApi mainShopInfo] merchant] lostPasswordShopIdentity];
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_LOST_PASSWORD shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:nil] flags:flags];
    
    [message addParameter:MSG_PARAM_LOGIN_IDENTITY value:login];
    [message addParameter:MSG_PARAM_LOGIN_PASSWORD value:@"-"];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)queryUserRegister:(NSString*)queryId flags:(int)flags handler:(ResponseHandler)responseHandler {
    Identity* shopIdentity = [[[HonkioApi mainShopInfo] merchant] registrationShopIdentity];
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_QUERY shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[User class]] flags:flags];
    
    [message addParameter:MSG_PARAM_ID value:queryId];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)userGetTransactions:(TransactionFilter*)filter flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userGetTransactions:filter shop:[HonkioApi mainShopIdentity] flags:flags handler:responseHandler];
}

+(id<Task>)userGetTransactions:(TransactionFilter*)filter shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_TRANSACTION shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[Transactions class]] flags:flags];
    
    if (filter)
        [filter apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(id<Task>)userGetOrders:(OrderFilter*)filter flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userGetOrders:filter shop:[HonkioApi mainShopIdentity] flags:flags handler:responseHandler];
}

+ (id<Task>)userGetOrders:(OrderFilter*)filter shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET_ORDERS shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[OrdersList class]] flags:flags];
    
    if (filter)
        [filter apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+ (id<Task>)userSetOrder:(Order*)order flags:(int)flags handler:(ResponseHandler)responseHandler {
    
    Identity* identity = [HonkioApi mainShopIdentity];
    if (order.shopId == nil || [identity.identityId isEqualToString:order.shopId]) {
        
        return [HonkioApi userSetOrder:order shop:[HonkioApi mainShopIdentity] flags:flags handler:responseHandler];
    }
    
    __block TaskWrapper* wrapper = [[TaskWrapper alloc] initWithTask: [HonkioApi shopFindById:order.shopId flags:0 handler:^(Response * _Nullable res) {
        if (res && [res isStatusAccept]) {
            ShopFind* shopsList = (ShopFind*) res.result;
            if (shopsList.list.count > 0) {
                Identity* shop = (Identity*) shopsList.list[0];
                [wrapper set:[HonkioApi userSetOrder:order shop:shop flags:flags handler:responseHandler]];
            }
            else {
                responseHandler([Response withError:ErrApiUnknown desc:nil]);
            }
        }
        else {
            responseHandler([Response withError:ErrApiUnknown desc:nil]);
        }
        
    }]];
    
    return wrapper;
}

+ (id<Task>)userSetOrder:(Order*)order shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_SET_ORDER shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[Order class]] flags:flags];
    
    [order apply:message];
    
    if (order.account && [[[HonkioApi activeUser] userId] isEqualToString:order.userOwner]) {
        [order.account apply:message];
    }
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+ (id<Task>)userSetOrderApplication:(NSString*)applicationId status:(NSString*)status buildNewOrder:(BOOL)buildNewOrder shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    if ([HonkioApi checkApi:responseHandler]) {
        Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_SET_ORDER_APPLICATION shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[Order class]] flags:flags];
        
        [message addParameter: @"application_id" value: applicationId];
        [message addParameter: @"status" value: status];
        [message addParameter: @"new_order" value: [NSNumber numberWithBool:buildNewOrder]];
        
        return [WebServiceExecutor build:message handler:responseHandler];
    }
    return nil;
}

+ (id<Task>)userSetOrderStatus:(NSString*)orderStatus orderId:(NSString*)orderId shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_SET_ORDER shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[Order class]] flags:flags];
    
    [message addParameter:MSG_PARAM_ORDER_ID value:orderId];
    [message addParameter:MSG_PARAM_ORDER_STATUS value:orderStatus];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+ (id<Task>)userGetComments:(FeedbackFilter*)filter flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi userGetComments:filter shop:nil flags:flags handler:responseHandler];
}

+ (id<Task>)userGetComments:(FeedbackFilter*)filter shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET_COMMENTS shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[UserCommentsList class]] flags:flags];
    
    if (filter)
        [filter apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+ (id<Task>)userAddComment:(UserComment*)comment flags:(int)flags handler:(ResponseHandler)handler {
    return [HonkioApi userAddComment:comment shop: nil flags:flags handler:handler];
}

+ (id<Task>)userAddComment:(UserComment*)comment shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)handler {
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_VOTE shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:nil] flags:flags];
    
    [comment apply:message];
    
    return [WebServiceExecutor build:message handler:handler];
}

+ (nullable id<Task>)userDonate:(UserAccount*)account amount:(double)amount email:(NSString*)userEmail message:(NSString*)desc shop:(Identity*)shopIdentity flags:(int)flags response:(ResponseHandler)handler {
    if ([HonkioApi checkApi:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_DONATE shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[Invitation class]] flags:flags];
        
        [account apply:message];
        
        [message addParameter:@"account_amount" value: [NSString stringWithFormat:@"%.3f", amount]];
        [message addParameter:@"email" value:userEmail];
        
        if (desc)
            [message addParameter:@"message" value:desc];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+ (nullable id<Task>)userAcceptInvitation:(nonnull NSString*)invitationid flags:(int)flags response:(nullable ResponseHandler)handler {
    if ([HonkioApi checkApi:handler]) {
        Message * message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_ACCEPT_INVITATION responseParser:[[HonkioApi protocolFactory] newParser:[Invitation class]] flags:flags];
        
        [message addParameter:@"id" value:invitationid];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+ (nullable id<Task>)getRate:(RatingsFilter *)filter flags:(int)flags response:(nullable ResponseHandler)handler {
    if ([HonkioApi checkInitialisation:handler]) {
        return [WebServiceExecutor build:[HonkioApi getRate:(RatingsFilter *)filter flags:(int)flags] handler:handler];
    }
    return nil;
}

+ (id<Task>)userApplyForOrder:(NSString *)orderId proposedTime:(NSString *)proposedTime flags:(int)flags response:(ResponseHandler)handler {
    
    Message * message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_APPLY_FOR_ORDER responseParser:[[HonkioApi protocolFactory] newParser:nil] flags:flags];
    [message addParameter:@"order_id" value:orderId];
    
    if (proposedTime != nil || ![proposedTime  isEqual: @""]) {
        
        [message addParameter:@"time_proposal" value:proposedTime];
    }
    
    return [WebServiceExecutor build:message handler:handler];
}

+ (id<Task>)userCancelForOrder:(NSString *)orderId flags:(int)flags response:(ResponseHandler)handler {
    
    Message * message = [HonkioApi buildUserMessage:MSG_COMMAND_USER_APPLY_FOR_ORDER responseParser:[[HonkioApi protocolFactory] newParser:nil] flags:flags];
    [message addParameter:@"order_id" value:orderId];
    [message addParameter:@"delete" value:@"true"];
    
    return [WebServiceExecutor build:message handler:handler];
}

+(nonnull id<Task>)userGetInventory:(int)flags handler:(nullable ResponseHandler)handler {
    return [HonkioApi userGetInventory:nil flags:flags handler:handler];
}

+(nonnull id<Task>)userGetInventory:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler {
    Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_INVENTORY_LIST shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[Inventory class]] flags:flags];
    return [WebServiceExecutor build:message handler:handler];
}

+(nonnull id<Task>)userInventoryConvert:(nonnull NSString*)productId target:(nonnull NSString*)targetId flags:(int)flags handler:(nullable ResponseHandler)handler {
    return [HonkioApi userInventoryConvert:productId target:targetId shop:nil flags:flags handler:handler];
}

//TODO write docs
+(id<Task>)userInventoryConvert:(NSString*)productId target:(NSString*)targetId shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)handler {
    Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_INVENTORY_CONVERT shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[Inventory class]] flags:flags];
    [message addParameter:@"product_id" value:productId];
    [message addParameter:@"target_product_id" value:targetId];
    return [WebServiceExecutor build:message handler:handler];
}

+(id<Task>)smsVerify:(NSString*)smsCode handler:(SimpleResponseHandler)responseHandler {
    NSMutableString* urlStr = [NSMutableString stringWithString: [[HonkioApi serverInfo] verifyURL]];
    
    [urlStr appendString:smsCode];
    
    NSMutableString* bodyStr = [NSMutableString new];
    [bodyStr appendString:[NSString stringWithFormat:@"identity_plugindata=%@",@"dWJdqkf6iJpVYhjk6weV4vtioeJY8=" ]];
    
    //TODO: add latitude ...
    
    NSData* body = [bodyStr dataUsingEncoding:NSUTF8StringEncoding];
    
    UrlResponseHandler wrapper = ^(NSURLResponse* response, NSData* data, NSError* connectionError) {
        // TODO: Convert NSError to ApiError
        responseHandler(connectionError);
    };
    
    SimpleAsyncPost* post = [[SimpleAsyncPost alloc] initWithUrl:[NSURL URLWithString:urlStr] heders:nil body:body responseHandler:wrapper];
    [post execute];
    
    return post;
}


+(id<Task>)orderGetApplicants:(nonnull ApplicantFilter*)filter flags:(int)flags handler:(nullable ResponseHandler)handler{
    return [HonkioApi orderGetApplicants:filter shop:nil flags:flags handler:handler];
}

+(id<Task>)orderGetApplicants:(nonnull ApplicantFilter*)filter shop:(nullable Identity*)shopIdentity flags:(int)flags handler:(nullable ResponseHandler)handler {
    if ([HonkioApi checkApi:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET_APPLICANTS shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[ApplicantsList class]] flags:flags];
        
        [filter apply:message];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+(id<Task>)userRoleDescriptionsList:(UserRoleDescriptionsFilter*) filter shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)handler {
    if ([HonkioApi checkInitialisation:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_MERCHANT_ROLE shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[UserRoleDescriptionsList class]] flags:flags];
        
        [filter apply:message];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+(id<Task>)userRoleDescription:(NSString*) code shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)handler {
    if ([HonkioApi checkInitialisation:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_MERCHANT_ROLE shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[UserRoleDescriptionsList class]] flags:flags];
        
        [message addParameter:@"query_code" value:code];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+(id<Task>)assetStructureList:(NSArray<NSString*>*) structureIds shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)handler {
    if ([HonkioApi checkInitialisation:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_ASSET_STRUCTURE connection:nil shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[HKStructureList class]] flags:flags];
        
        [message addParameter:@"query_id" value:structureIds];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+(id<Task>)userStructureDescriptionList:(NSArray<NSString*>*) roleIds shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)handler {
    if ([HonkioApi checkInitialisation:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_ROLES_DESCRIPTIONS connection:nil shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[HKUserRoleStructureList class]] flags:flags];
        
        [message addParameter:@"roles" value:roleIds];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+(id<Task>)userGetPushes:(AbsListFilter*)filter flags:(int)flags handler:(ResponseHandler)handler {
    if ([HonkioApi checkApi:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET_NOTIFICATIONS shopIdentity:nil responseParser:[[HonkioApi protocolFactory] newParser:[PushesList class]] flags:flags];
        
        if (filter) {
            [filter apply:message];
        }
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+(id<Task>)userSetPush:(BOOL)isReaded filter:(PushFilter*)filter flags:(int)flags handler:(ResponseHandler)handler {
    if ([HonkioApi checkApi:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_SET_NOTIFICATION
                                       shopIdentity:nil
                                     responseParser:[[HonkioApi protocolFactory] newParser:nil]
                                              flags:flags];
        
        if (filter) {
            [filter apply:message];
        }
        
        [message addParameter:@"readed" value:[NSNumber numberWithBool:isReaded]];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+(id<Task>)qrInfo:(NSString*)qrId flags:(int)flags handler:(ResponseHandler)handler {
    if ([HonkioApi checkInitialisation:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_QR_INFO connection:nil shopIdentity:nil responseParser:[[HonkioApi protocolFactory] newParser:[HKQrCode class]] flags:flags];
        
        [message addParameter:@"id" value:qrId];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

+(id<Task>)userSendAbuse:(NSString *)merchant linkId:(NSString *)linkId linkType:(NSString *)linkType comment:(NSString *)comment flags:(int)flags handler:(ResponseHandler)handler {
    Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_SET_ABUSE
//                               shopIdentity:[HonkioApi mainShopIdentity]
                                     connection:[HonkioApi activeConnection]
                                   shopIdentity:nil
                             responseParser:[[HonkioApi protocolFactory] newParser:nil] flags:flags];
    [message setIsSupportOfline:NO];
    [message addParameter:@"merchant" value:merchant];
    [message addParameter:@"object" value:linkId];
    [message addParameter:@"object_type" value:linkType];
    [message addParameter:@"comment" value:comment];
        
    return [WebServiceExecutor build:message handler:handler];
}


+(id<Task>)userGetAsset:(NSString*)assetId flags:(int)flags handler:(ResponseHandler)handler {
    if ([HonkioApi checkInitialisation:handler]) {
        Message * message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET_ASSET
                                       shopIdentity:nil
                                     responseParser:[[HonkioApi protocolFactory] newParser:[HKAsset class]]
                                              flags:flags];
        
        [message addParameter:@"id" value:assetId];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}


+(id<Task>)userAssetList:(AssetFilter*)filter shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)handler {
    if ([HonkioApi checkInitialisation:handler]) {
        Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_ASSET_LIST
                                      shopIdentity:shopIdentity
                                    responseParser:[[HonkioApi protocolFactory] newParser:[HKAssetList class]]
                                             flags:flags];
        
        if (filter)
            [filter apply:message];
        
        return [WebServiceExecutor build:message handler:handler];
    }
    return nil;
}

// Payments =============\/=============

+(id<Task>)paymentRequest:(NSArray*)bookProducts account:(UserAccount*)account flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi paymentRequest:bookProducts account:account shop:[HonkioApi mainShopInfo].shop flags:flags handler:responseHandler];
}

+(id<Task>)paymentRequest:(NSArray*)bookProducts account:(UserAccount*)account shop:(Shop*)shop flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi paymentRequest:bookProducts account:account shop:shop params:nil flags:flags handler:responseHandler];
}

+(id<Task>)paymentRequest:(NSArray*)bookProducts account:(UserAccount*)account shop:(Shop*)shop params:(NSDictionary*)params flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi paymentRequest:bookProducts account:account shop:shop order:nil params:params flags:flags handler:responseHandler];
}

+(id<Task>)paymentRequest:(NSArray*)bookProducts account:(UserAccount*)account shop:(Shop*)shop order:(NSString*)orderId params:(NSDictionary*)params flags:(int)flags handler:(ResponseHandler)responseHandler; {
    
    NSMutableArray* productsArray = [NSMutableArray new];
    if(bookProducts) {
        for(BookedProduct* bookProduct in bookProducts) {
            NSDictionary* productDict = @{MSG_PARAM_ID : bookProduct.product.productId,
                                          @"count" : [NSNumber numberWithInt:bookProduct.count]};
            
            [productsArray addObject:productDict];
        }
    }
    
    NSMutableDictionary* paymentsParams;
    if (params)
        paymentsParams = [NSMutableDictionary dictionaryWithDictionary:params];
    else
        paymentsParams = [NSMutableDictionary new];
    
    [paymentsParams setValue:productsArray forKey:MSG_PARAM_PRODUCT];
    
    return [HonkioApi doPaymentRequest:responseHandler shop:shop order:orderId account:account params:paymentsParams flags:flags];
}

+(id<Task>)paymentRequest:(double)amount currency:(NSString*)currency account:(UserAccount*)account flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi paymentRequest:amount currency:currency account:account shop:[HonkioApi mainShopInfo].shop flags:flags handler:responseHandler];
}

+(id<Task>)paymentRequest:(double)amount currency:(NSString*)currency account:(UserAccount*)account shop:(Shop*)shop flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi paymentRequest:amount currency:currency account:account shop:shop params:nil flags:flags handler:responseHandler];
}

+(id<Task>)paymentRequest:(double)amount currency:(NSString*)currency account:(UserAccount*)account shop:(Shop*)shop params:(NSDictionary*)params flags:(int)flags handler:(nullable ResponseHandler)responseHandler {
    return [HonkioApi paymentRequest:amount currency:currency account:account shop:shop order:nil params:params flags:flags handler:responseHandler];
}

+(id<Task>)paymentRequest:(double)amount currency:(NSString*)currency account:(UserAccount*)account shop:(Shop*)shop order:(NSString*)orderId params:(NSDictionary*)params flags:(int)flags handler:(ResponseHandler)responseHandler {
    NSMutableDictionary* paymentsParams;
    if (params)
        paymentsParams = [NSMutableDictionary dictionaryWithDictionary:params];
    else
        paymentsParams = [NSMutableDictionary new];
    
    [paymentsParams setValue:[NSString stringWithFormat:@"%f", amount] forKey:MSG_PARAM_ACCOUNT_AMOUNTH];
    if (currency) {
        [paymentsParams setValue:currency forKey:MSG_PARAM_ACCOUNT_CURRENCY];
    }
    
    return [HonkioApi doPaymentRequest:responseHandler shop:shop order:orderId account:account params:paymentsParams flags:flags];
}

/**
 * PRIVATE
 */
+(id<Task>)doPaymentRequest:(ResponseHandler)responseHandler shop:(Shop*)shop order:(NSString*)orderId account:(UserAccount*)account params:(NSDictionary*)params flags:(int)flags {
    
    responseHandler = [HonkioApi wrapPaymentResponseHandler:responseHandler];
    
    if ([account typeIs:ACCOUNT_TYPE_OPERATOR]) {
        responseHandler = [HonkioApi wrapOperatorPaymentResponseHandler:responseHandler shopIdentity:shop];
    }
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_PAYMENT shopIdentity:shop responseParser:[[HonkioApi protocolFactory] newParser:[UserPayment class]] flags:flags];
    
    if ([message isHasFlag:MSG_FLAG_SIGN]) {
        [message addParameter:MSG_PARAM_ACCOUNT_SIGN value:[NSNumber numberWithBool: true]];
    }
    if (orderId) {
        [message addParameter:MSG_PARAM_ORDER value:orderId];
    }
    [message addParameters:params];
    
    [account apply:message];
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

//===================== Current ===========>

+(id<Task>)queryPaymentRequest:(NSString*)queryId flags:(int)flags handler:(ResponseHandler)responseHandler {
    return [HonkioApi queryPaymentRequest:queryId  shop:[HonkioApi mainShopIdentity] flags:flags handler:responseHandler];
}

+(id<Task>)queryPaymentRequest:(NSString*)queryId shop:(Identity*)shopIdentity flags:(int)flags handler:(ResponseHandler)responseHandler {
    
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_QUERY shopIdentity:shopIdentity responseParser:[[HonkioApi protocolFactory] newParser:[UserPayment class]] flags:flags];
    
    [message addParameter:MSG_PARAM_ID value:queryId];
    message.notificationCommand = MSG_COMMAND_USER_PAYMENT;
    
    return [WebServiceExecutor build:message handler:responseHandler];
}

+(BOOL)checkPaymentAvailable:(nonnull UserAccount*)account shop:(nonnull Shop*)shop handler:(ResponseHandler)responseHandler {
    if (!account) {
        @throw [NSException exceptionWithName:@"InvalidParameterException" reason:@"UserAccount must be not null." userInfo:nil];
    }
    if (!shop) {
        shop = [[HonkioApi mainShopInfo] shop];
    }
    if ([HonkioApi checkApi:responseHandler]) {
        if ([[shop merchant] isAccountSupported:account.type]) {
            return YES;
        }
        else
            responseHandler([Response withError:ErrApiAccountNotSupported desc:nil]);
    }
    return NO;
}

//============================================================
// Other    ==================================================
//============================================================

+(nullable id<ProtocolFactory>)protocolFactory {
    return [HonkioApi shared].protocolFactory;
}

+(id<RequestBuilder>)newRequestBuilder {
    id<ProtocolFactory> buildersFactory = [HonkioApi protocolFactory];
    id<RequestBuilder> result = [buildersFactory newRequestBuilder];
    return result;
}

+(NSURL*)apiUrl {
    return [[[HonkioApi shared] state] apiUrl];
}

+(Identity*)appIdentity {
    return [[[HonkioApi shared] state] appIdentity];
}
    
+(nullable HKDevice*)device {
    return [[[HonkioApi shared] state] device];
}

+(Identity*)mainShopIdentity {
    return [[[[HonkioApi shared] state] appInfo] shopIdentity];
}

+(Connection*)activeConnection {
    return [HonkioApi shared].connection;
}

+(ServerInfo*)serverInfo {
    return [[[HonkioApi shared] state] serverInfo];
}

+(NSString *)getSuitableLanguage {
    ServerInfo *serverInfo = [HonkioApi serverInfo];
    if (serverInfo)
        return [serverInfo suitableLanguage];
    return [NSLocale canonicalLanguageIdentifierFromString:[[NSLocale preferredLanguages] firstObject]];
}

+(nullable AppInfo*)appInfo {
    return [[[HonkioApi shared] state] appInfo];
}

+(ShopInfo*)mainShopInfo {
    return [[[HonkioApi shared] state] shopInfo];
}

+(User*)activeUser {
    return [[[HonkioApi shared] state] user];
}

+(void)changePin:(NSString*)newPin {
    [[HonkioApi activeConnection] changePin:newPin];
}

+(BOOL)changePin:(NSString*)currentPin newPin:(NSString*)newPin {
    return [[HonkioApi activeConnection] changePin:currentPin newPin:newPin];
}

+(BOOL)checkPin:(NSString*)pin {
    return [[HonkioApi activeConnection] checkPin:pin];
}

+(CLLocation*)deviceLocation {
    CLLocation* location = [[HonkioApi shared].locationFinder.locations lastObject];
    [[HonkioApi shared].locationFinder updateLocationIfNeeded];
    return location;
}

+(ResponseHandler)wrapUserResponseHandler:(ResponseHandler)responseHandler {
    ResponseHandler wrapper = ^(Response *response) {
        if ([response isStatusError] || [response isStatusReject]) {
            if (responseHandler)
                responseHandler(response);
        }
        else {
            User* user = (User*) response.result;
            
            if ([response isStatusAccept]) {
                [[[HonkioApi shared] state] userDidGet: user];
            }
            
            if (!response.isOffline && !user.isActive) {
                [HonkioApi handleRegistration:responseHandler response:response];
            }
            else {
                if (![HonkioApi activeConnection]) {
                    Connection* connection = [[Connection alloc] initWithLogin:user.login password:response.otp];
                    connection.otp = response.otp;
                    [HonkioApi shared].connection = connection;
                    [NotificationHelper postOnLogin];
                }
                
                NSString* suitableLanguage = [[HonkioApi serverInfo] suitableLanguage];
                if (suitableLanguage && ![suitableLanguage isEqualToString:[user language]]) {
                    [user setLanguage:suitableLanguage];
                    [HonkioApi userUpdate:user flags:0 handler:nil];
                }
                
                if (responseHandler)
                    responseHandler(response);
            }
        }
    };
    return wrapper;
}

+(ResponseHandler)wrapUserRegistrationResponseHandler:(ResponseHandler)responseHandler {
    ResponseHandler wrapper = ^(Response *response) {
        if (response) {
            [HonkioApi handleRegistration:responseHandler response:response];
        }
        else if (responseHandler)
            responseHandler(response);
    };
    return wrapper;
}

+(void)handleRegistration:(ResponseHandler)responseHandler response:(Response*)response{
    User* user = (User*) response.result;
    if (user) {
        [HonkioApi shared].connection = [[Connection alloc] initWithLogin:user.login password:response.otp];
        [NotificationHelper postOnLogin];
        
        if ([response isStatusPending]) {
            responseHandler(response);
            return;
        }
        if ([response isStatusAccept] && !user.isActive) {
//            ResponseHandler queryCallback = ^(Response *response) {
                // TODO
//            };
            // TODO
            responseHandler(response);
            return;
        }
    }
    responseHandler(response);
}

+(ResponseHandler)wrapPaymentResponseHandler:(ResponseHandler)responseHandler {
    ResponseHandler wrapper = ^(Response *response) {
        if (response && [response isStatusAccept]) {
            [HonkioApi userGet:MSG_FLAG_ONLINE_MODE handler:nil];
        }
        if (responseHandler)
            responseHandler(response);
    };
    return wrapper;
}

+(ResponseHandler)wrapOperatorPaymentResponseHandler:(ResponseHandler)responseHandler shopIdentity:(Identity*)shopIdentity {
    ResponseHandler wrapper = ^(Response *response) {
        if (response && [response isStatusPending] && response.url) {
            SimpleAsyncGet* asyncGet = [[SimpleAsyncGet alloc] initWithUrl:[NSURL URLWithString:response.url] heders:nil responseHandler:^(NSURLResponse* __nullable webResponse, NSData* __nullable data, NSError* __nullable connectionError) {
                if (connectionError) {
                    if (responseHandler)
                        responseHandler([Response withError:ErrApiUnknown desc:[connectionError localizedDescription]]);
                }
                else {
                    [HonkioApi queryPaymentRequest:response.transactoinId shop:shopIdentity flags:0 handler:^(Response* response) {
                        if (responseHandler)
                            responseHandler(response);
                    }];
                }
            }];

            [asyncGet execute];
        }
        else if (responseHandler)
            responseHandler(response);
    };
    return wrapper;
}

+(Message*)buildMessage:(NSString*)command shopIdentity:(Identity*)shopIdentity responseParser:(id<ResponseParser>)responseParser flags:(int)flags {
    return [HonkioApi buildMessage:command connection:[HonkioApi activeConnection] shopIdentity:shopIdentity responseParser:responseParser flags:flags];
}

+(Message*)buildUserMessage:(NSString*)command responseParser:(id<ResponseParser>)responseParser flags:(int)flags {
    
    Identity* shopIdentity = [[[HonkioApi mainShopInfo] merchant] loginShopIdentity];
    
    return [HonkioApi buildMessage:command connection:[HonkioApi activeConnection] shopIdentity:shopIdentity responseParser:responseParser flags:flags];
}

// TODO write docs
+(Message *) userVote:(UserComment *)userComment flags:(int)flags {
    Identity* shopIdentity = [[[HonkioApi mainShopInfo] merchant] loginShopIdentity];
    Message* message = [HonkioApi buildMessage:MSG_COMMAND_USER_VOTE
                                  shopIdentity:shopIdentity
                                responseParser:[[HonkioApi protocolFactory]
                                                newParser:[UserComment class]]
                                         flags:flags];
    [userComment apply:message];
    
    return message;
}

+(Message *)getRate:(RatingsFilter *)filter flags:(int)flags {
    Message *message = [HonkioApi buildMessage:MSG_COMMAND_USER_GET_RATE
                                  shopIdentity:[HonkioApi mainShopIdentity]
                                responseParser:[[HonkioApi protocolFactory] newParser:[RateList class]]
                                         flags:flags];
    if (filter) {
        [filter apply:message];
    }
    return message;
}

// TODO write docs
+(nullable id<Task>)userVote:(UserComment *)userComment flags:(int)flags handler:(nullable ResponseHandler)handler {
    
    Message *message = [HonkioApi userVote:userComment flags:flags];
    return [WebServiceExecutor build:message handler:handler];
}

+(Message*)buildMessage:(NSString*)command connection:(Connection*)connection shopIdentity:(Identity*)shopIdentity responseParser:(id<ResponseParser>)responseParser flags:(int)flags {
    if (!shopIdentity) {
        shopIdentity = [HonkioApi mainShopIdentity];
    }
    return [[Message alloc] initWithCommand:command url:[HonkioApi apiUrl] connection:connection device:[HonkioApi device] shopIdentity:shopIdentity requestBuilder:[HonkioApi newRequestBuilder] responseParser:responseParser flags:flags];
}

+(NetworkStatus) networkStatus
{
    RPReachability* reachability = [RPReachability reachabilityWithHostName:@"google.com"];
    return[reachability currentReachabilityStatus];
}

+ (NSString *)getUserPhotoURL:(NSString *)userId {
    
    return [NSString stringWithFormat:@"%@/user_photo/%@.jpg", [HonkioApi.serverInfo.userURL rightStrip:@"/"], userId];
}

@end
