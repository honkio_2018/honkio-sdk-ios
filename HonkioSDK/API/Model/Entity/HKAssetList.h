//
//  HKAssetList.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 3/1/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HKAsset.h"

@interface HKAssetList : NSObject

@property (nonatomic) NSArray<HKAsset*>* list;

@end
