//
//  UserRole.h
//  HonkioApi
//
//  Created by Shurygin Denis on 12/8/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsFilter.h"

/**
 *  Describes a type of user role.
 */
@interface UserRole: AbsFilter

-(instancetype)initFrom:(UserRole *)role;

/**
 *  User's role identifier.
 */
@property (nonatomic) NSString* roleId;

/**
 *  Object identifier.
 */
@property (nonatomic) NSString* objectId;

/**
 *  User's indentifier.
 */
@property (nonatomic) NSString* userId;

/**
 *  Merchant ot the role
 */
@property (nonatomic) NSString* merchantId;

/**
 *  Name of role.
 */
@property (nonatomic) NSString* roleName;

/**
 *  Display name of role
 */
@property (nonatomic) NSString* roleDisplayName;

/**
 *  Description of role.
 */
@property (nonatomic) NSString* roleDesc;

/**
 *  Firts name of user.
 */
@property (nonatomic) NSString* userFirstName;

/**
 *  Last name of user.
 */
@property (nonatomic) NSString* userLastName;

/**
 *  Url of user photo.
 */
@property (nonatomic) NSString* userPhotoURL;

@property (nonatomic) NSNumber * userJobsTotal;
@property (nonatomic) NSNumber * userRating;

/**
 *  Additional optional private properties.
 */
@property (nonatomic) NSMutableDictionary *privateProperties;

/**
 *  Additional optional public properties.
 */
@property (nonatomic) NSMutableDictionary *publicProperties;

/**
 *  Information about user.
 */
@property (nonatomic) NSMutableDictionary *userContactData;

/**
 *  Name of user (First name + Last name).
 *
 *  @return Name of user (First name + Last name)
 */
-(NSString*) userName;

/**
 *  Define that role is active.
 */
@property (nonatomic) BOOL isActive;

-(nullable id)getPrivateProperty:(nonnull NSString *)key;
    
-(nullable id)getPublicProperty:(nonnull NSString *)key;

@end
