//
//  HKPropertiesFilter
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/26/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "AbsFilter.h"

@protocol HKPropertiesFilterItem

-(NSDictionary<NSString*, NSObject*>*)toParams;

@end

@interface HKPropertiesFilter : AbsFilter

-(void)addItem:(id<HKPropertiesFilterItem>)item forKey:(NSString*)key;
-(void)remove:(NSString*)key;
-(void)removeAll;

@end

@interface HKPropertiesFilterEqual<__covariant ObjectType> : NSObject<HKPropertiesFilterItem>

@property (nonatomic, nullable) ObjectType value;

-(id)initWithValue:(nullable ObjectType)value;

@end

@interface HKPropertiesFilterNotEqual<__covariant ObjectType> : HKPropertiesFilterEqual<ObjectType>

@end


@interface HKPropertiesFilterRange<__covariant ObjectType> : NSObject<HKPropertiesFilterItem>

@property (nonatomic, nullable) ObjectType start;
@property (nonatomic, nullable) ObjectType end;

@property (nonatomic) BOOL includeStart;
@property (nonatomic) BOOL includeEnd;

-(id)initWithStart:(nullable ObjectType)start end:(nullable ObjectType)end;
-(id)initWithStart:(nullable ObjectType)start includeStart:(BOOL)includeStart end:(nullable ObjectType)end includeEnd:(BOOL)includeEnd;

@end

@interface HKPropertiesFilterInList<__covariant ObjectType> : NSObject<HKPropertiesFilterItem>

@property (nonatomic, nullable) NSArray<ObjectType>* list;

-(id)initWithList:(nonnull NSArray<ObjectType>*)list;
-(id)initWithItems:(nonnull ObjectType)items, ...;

@end

@interface HKPropertiesFilterAllInList<__covariant ObjectType> : HKPropertiesFilterInList<ObjectType>

@end

@interface HKPropertiesFilterNotInList<__covariant ObjectType> : HKPropertiesFilterInList<ObjectType>

@end
