//
//  UserRoleDescriptionsFilter.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/24/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsListFilter.h"

@interface UserRoleDescriptionsFilter : AbsListFilter

@property (nonatomic) NSString* type;

@end
