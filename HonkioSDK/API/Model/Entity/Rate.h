//
//  Rate.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 8/3/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rate : NSObject

@property (nonatomic) int value;
@property (nonatomic) int count;

-(id)initWithValue:(int)value andCount:(int)count;

+(int)positiveRatesCount:(NSArray*) ratings;
+(int)negativeRatesCount:(NSArray*) ratings;

@end
