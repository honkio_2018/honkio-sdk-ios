//
//  ShopProducts.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"

/**
 * ArrayList that holds list of the products for concrete shop.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface ShopProducts : NSObject

@property (nonatomic) NSArray<Product*>* list;

/** ID of the Shop that contains this Products. */
@property (nonatomic) NSString* shopId;

//TODO write docs
-(Product*)findProductById:(NSString*)productId;

@end
