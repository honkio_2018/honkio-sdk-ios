//
//  HKTouVersion.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/31/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKTouVersion.h"

@implementation HKTouVersion

-(id)initWithVersion:(int) version timestamp:(nonnull NSDate*) timestamp {
    self = [super init];
    if (self) {
        _version = version;
        _timestamp = timestamp;
    }
    return self;
}

@end
