//
//  HKBaseAppDelegate.swift
//  HonkioTemplate
//
//  Created by Shurygin Denis on 4/11/16.
//  Copyright © 2016 RiskPointer. All rights reserved.
//

import Foundation
import AVFoundation
/**
The base class for moonitoring and manage the applocation life cycle.
Should not be instantiate.
*/
open class HKBaseAppDelegate: UIResponder, UIApplicationDelegate, HKBaseForegroundNotificationDelegate {
    /// The accessor to the application UIWindow
    open var window: UIWindow?
    
    var _currentDeviceIdString : String?
    public var deviceId : String {
        get {
            if self._currentDeviceIdString == nil {
                self._currentDeviceIdString = UIDevice.current.identifierForVendor!.uuidString + HonkioApi.appIdentity()!.identityId
            }
            return self._currentDeviceIdString!
        }
    }
    
    private var urlHandler : HKUrlHandler?
    private var ignoredPushes : [(type: String, value1: String?, value2: String?)] = []
    private var tempToken: String?
    
    // MARK: - Lifecycle
    public override init() {
        super.init()
        self.applicationShouldInit()
        self.urlHandler = self.buildUrlHandler()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HKBaseAppDelegate.userDidLogin(_:)), name: NSNotification.Name(rawValue: NOTIFY_NAME_ON_LOGIN), object: nil)
    }
    
    open func buildUrlHandler() -> HKUrlHandler? {
        return nil
    }
    
    public static func addPushToIgnore(_ type: String, value1: String?, value2: String?) {
        if let baseAppDelegate = UIApplication.shared.delegate as? HKBaseAppDelegate {
            baseAppDelegate.ignoredPushes.append((type, value1, value2))
        }
    }
    
    public static func removePushFromIgnore(_ type: String, value1: String?, value2: String?) {
        if let baseAppDelegate = UIApplication.shared.delegate as? HKBaseAppDelegate {
            for (position, ignored) in baseAppDelegate.ignoredPushes.enumerated() {
                if ignored.type == type && ignored.value1 == value1 && ignored.value2 == value2 {
                    baseAppDelegate.ignoredPushes.remove(at: position)
                    return
                }
            }
        }
    }
    
    open func isPushIgnored(_ push: Push) -> Bool {
        
        if let baseAppDelegate = UIApplication.shared.delegate as? HKBaseAppDelegate {
            for ignored in baseAppDelegate.ignoredPushes {
                if ignored.type == push.type && ignored.value1 == push.entity.value1() && ignored.value2 == push.entity.value2() {
                    return true
                }
            }
        }
        return false
    }
    
    /**
    On applicationdidFinishLaunchingWithOptions...
    - Retunes: true.
    */
    open func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        
        if launchOptions != nil {

            if let userInfo = launchOptions![UIApplicationLaunchOptionsKey.remoteNotification] as? NSDictionary {

                if let apsInfo = userInfo["aps"] as? [AnyHashable : Any]{

                    if let push = PushParser.parse(apsInfo) {

                        NotificationHelper.post(onPushReceived: push)
                    }
                }
            }
        }
        
        return true
    }
    
    open func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        if let url = userActivity.webpageURL {
            if userActivity.activityType == NSUserActivityTypeBrowsingWeb && self.urlHandler != nil && self.urlHandler!.handle(url) {
                return true
            }
            
            application.openURL(url)
        }
        return false
    }
    
    /**
    Sent when the application is about to move from active to inactive state. This can occur
    for certain types of temporary interruptions (such as an incoming phone call or
    SMS message) or when the user quits the application and it begins the transition to
    the background state.
    Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES
    frame rates. Games should use this method to pause the game.
    */
    open func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur
        // for certain types of temporary interruptions (such as an incoming phone call or
        // SMS message) or when the user quits the application and it begins the transition to
        // the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES
        // frame rates. Games should use this method to pause the game.
    }
    /**
    Use this method to release shared resources, save user data, invalidate timers, and
    store enough application state information to restore your application to its current
    state in case it is terminated later.
    If your application supports background execution, this method is called instead of
    applicationWillTerminate: when the user quits.
    */
    open func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and
        // store enough application state information to restore your application to its current
        // state in case it is terminated later.
        // If your application supports background execution, this method is called instead of
        // applicationWillTerminate: when the user quits.
    }
    
    open func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you
        // can undo many of the changes made on entering the background.
    }
    /**
    Restart any tasks that were paused (or not yet started) while the application was
    inactive. If the application was previously in the background, optionally refresh
    the user interface.
    */
    open func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was
        // inactive. If the application was previously in the background, optionally refresh
        // the user interface.
    }
    /**
    Called when the application is about to terminate. Save data if appropriate.
    - SeeAlso: applicationDidEnterBackground:.
    */
    open func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate.
        // See also applicationDidEnterBackground:.
    }
    /**
    Called on applicationShouldInit.
    * Must be overriden.
    */
    open func applicationShouldInit() {
        preconditionFailure("Method applicationShouldInit must be overrided.")
    }
    /**
    Called on applicationdidRegisterUserNotificationSettings. Registers application for remote notifications.
    - parameter application: The rference to UIApplication.
    - parameter notificationSettings: The UIUserNotificationSettings to be checked.
    */
    // MARK: - Notifications
    open func application(_ application: UIApplication, didRegister
        notificationSettings: UIUserNotificationSettings) {
        
        if notificationSettings.types != UIUserNotificationType() {
            application.registerForRemoteNotifications()
        }
    }
    /**
    On applicationdidRegisterForRemoteNotificationsWithDeviceToken saves the notofication token to Server.
    - parameter application: The reference to UIApplication. Not used.
    - parameter deviceToken: The NSData object containing the token to be saved.
    */
    open func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken
        deviceToken: Data) {
        
        HKLog.d("My token is: \(String(describing: deviceToken as NSData))")
        HKLog.d("Received device token from APN")
        
        self.tempToken = deviceToken.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        self.updateDeviceToken(HonkioApi.device(), self.tempToken)
    }
    
    /**
    Called if the registration for remote notifications got failed. Saves the error to log.
    - parameter application: The reference to UIApplication.
    - parameter error: The NSError object containing the description of the problem.
    */
    open func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        HKLog.d("Failed to get token, error: \(String(describing: error as NSError))")
    }
    /**
    Called on the application receives local notification. Saves the event to log.
    - parameter application: The reference to UIApplication.
    - parameter notification: The UILocalNotification received.
    */
    open func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        HKLog.d("didReceiveLocalNotification")
    }
    /**
    Called when remote notification has been received. Notifies active user the push notoficaton is receifed.
    - parameter application: The reference to UIApplication.
    - parameter userInfo: The information stored in push.
    - parameter completionHendler: The action to be called on processing the push.
    */
    open func application(_ application: UIApplication, didReceiveRemoteNotification
        userInfo: [AnyHashable: Any], fetchCompletionHandler
        completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        HKLog.d("Received remote notification: \(String(describing: userInfo))")
        
        if HonkioApi.activeUser() == nil {
            HKLog.d("User not logged in; not responding to notification. (Shouldn't have received " +
                "notification in the first place, if uesr not logged in.")
            completionHandler(UIBackgroundFetchResult.noData)
            return
        }
        
        let push = PushParser.parse(userInfo)
        if (push != nil) {
            if !isPushIgnored(push!) {
                
                if application.applicationState == UIApplicationState.active {
                    let notification = HKBaseForegroundNotification(userInfo: userInfo as [NSObject : AnyObject])
                    
                    notification.presentNotification()
                    notification.delegate = self
                }
            }
            
            NotificationHelper.post(onPushReceived: push)
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    // MARK: - BSForegroundNotificationDelegate
    /**
    Should be called when user interact the received notofication.
    - parameter userInfo: The information stored in push.
    */
    open func foregroundRemoteNotificationWasTouched(_ userInfo: [AnyHashable: Any]) {
        HKLog.d("foregroundRemoteNotificationWasTouched")
        
        let push = PushParser.parse(userInfo)
        if (push != nil) {
            NotificationHelper.post(onPushReceived: push)
        }
    }
    
    // MARK: - Public methods
    /**
    Re-registrate application for push notifications.
    */
    open func reRegisterForRemoteNotifications() {
        if HonkioApi.activeUser() != nil {
            HKLog.d("Notifications enabled by user for this device; updating registration with APN")
            
            let app = UIApplication.shared
            if app.responds(to: #selector(UIApplication.registerUserNotificationSettings(_:))) {
                let settings = UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
                app.registerUserNotificationSettings(settings)
            }
        }
    }
    /**
    Creates and adds the snapshot to the ViewController.
    - parameter viewController: The ViewController the snapshot will be added.
    */
    open func changeRootViewController(_ viewController: UIViewController) {
        
        if let snapshot = self.window?.snapshotView(afterScreenUpdates: true) {
            
            viewController.view.addSubview(snapshot)
            
            self.window?.rootViewController = viewController
            
            UIView.animate(withDuration: 0.2, animations: {
                
                snapshot.layer.opacity = 0
                snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5)
                
                }, completion: {finished in
                    
                    snapshot.removeFromSuperview()
            })
        }
    }
    
    @objc func userDidLogin(_ any: Any?) {
        let newDevice = HKDevice(from: HonkioApi.device())
        
        if self.tempToken != nil {
            newDevice?.token = self.tempToken
        }
        self.updateDevice(newDevice)
    }
    
    private func updateDeviceToken(_ device: HKDevice?, _ token: String?) {
        if token != nil && device != nil && token != device?.token {
            let newDevice = HKDevice(from: device)
            newDevice?.token = token
            self.updateDevice(newDevice)
        }
    }
    
    private func updateDevice(_ device: HKDevice?) {
        self.updateDevice(device, retry: 3)
    }
    
    private func updateDevice(_ device: HKDevice?, retry: Int) {
        if !HonkioApi.isConnected() || retry <= 0 {
            return
        }
        
        if device != nil {
            HonkioApi.userSetDevice(device!, flags: MSG_FLAG_LOW_PRIORITY) { (response) in
                if !response.isStatusAccept() {
                    let error = response.anyError()
                    switch error?.code {
                    case ErrApi.noConnection.rawValue:
                        self.updateDevice(device, retry: retry - 1)
                        break
                    case ErrCommon.invalidUserLogin.rawValue:
                        self.reRegisterDevice(device)
                        break
                    default:
                        break
                    }
                }
            }
        }
    }
    
    private func reRegisterDevice(_ device: HKDevice?) {
        let newDevice = HKDevice(from: device)
        newDevice?.device_id = nil
        
        HonkioApi.userSetDevice(device!, flags: MSG_FLAG_HIGH_PRIORITY) { (response) in
            // Do nothing
        }
    }
}
