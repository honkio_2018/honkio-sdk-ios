//
//  Transaction.m
//  HonkioApi
//
//  Created by Shurygin Denis on 5/12/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "Transaction.h"
#import "ApiDateFormatter.h"

@implementation Transaction

-(NSString*) toString {
    if(self.products && [self.products count] > 0) {
        BoughtProduct* product = self.products[0];
        NSMutableString* name = [NSMutableString stringWithString:product.name];
        for (int i = 1; i < [self.products count]; i++) {
            product = self.products[i];
            [name appendString:@", "];
            [name appendString:product.name];
        }
        return name;
    }
    else
        return @"-";
}

@end

@implementation MessageTransaction

@end
