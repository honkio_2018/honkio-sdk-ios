//
//  HKUserChangePinViewController.swift
//  WorkPilots
//
//  Created by developer on 1/18/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

/**
The view controller for containing the view for changing PIN code.
*/

open class HKUserChangePinViewController: HKBaseViewController, HKPinRequestVCStrategyDelegate {
    /// The outlet for text field containing user password.
    @IBOutlet open weak var passwordTextField: UITextField!

    /**
    The action to be performed on Next button. In this action checks the user's password length, if the length is
    not appropreate, the error messages will be shown for the corresponding text field. If the length is ok, the busy
    indicator will be shown and request to the server for checking apsswrod will be sent. When the response from
    the server is received the user will be notified if the password was incorrect by CustomAlertView. If the password
    is correct the busy indicator will be hidden and the  PIN code view will be shown.
    - parameter sender: Not used.
    */
    @IBAction open func nextAction(_ sender: AnyObject) {
    
        if !self.passwordTextField.checkPassword {
            
            self.passwordTextField.setErrorMessage("text_validator_passwor_short".localized)
            return
        }
        
        BusyIndicator.shared.showProgressView(self.view)
        
        HonkioApi.userCheckPassword(self.passwordTextField.text!, flags: 0) {[unowned self] (resp) in
            
            BusyIndicator.shared.hideProgressView()
            
            if resp.isStatusAccept() {
                let pinRequestStrategy = HKPinRequestVCStrategy(parent: self)
                pinRequestStrategy.delegate = self
                pinRequestStrategy.requestPin(Int(PIN_REASON_NEW), message: "pin_reason_new".localized)
            }
            else if resp.anyError().code == ErrCommon.invalidUserLogin.rawValue {
                
                CustomAlertView(okTitle: "change_pin_title".localized, okMessage: "change_pin_wrong_password".localized, okAction: nil).show()
            }
            else if !AppErrors.handleError(self, response: resp) {
                
                CustomAlertView(apiError: resp.anyError(), okAction: nil).show()
            }
        }
    }
    
    /**
    Notifies the PIN code has been entered. Performs API call for changing user PIN code,
    dismisses the current view controller.
    - parameter pinCode: The PIN code was entered.
    - parameter reason: The reason of entering the PIN code. Not used.
    */
    open func pinEntered(_ strategy: HKPinRequestVCStrategy, _ pinCode: String!, _ reason: Int!) {
        HonkioApi.changePin(pinCode)
        self.dismiss(animated: true, completion: nil)
    }

    /**
    Notifies the PIN code entering process has been cancelled.
    - parameter reason: Defines the reason of entering pin code. Not used.
    - parameter view: Parent view. Not used
    */
    open func pinCanceled(_ strategy: HKPinRequestVCStrategy, _ reason: Int!) {
        // Do nothing
    }
}
