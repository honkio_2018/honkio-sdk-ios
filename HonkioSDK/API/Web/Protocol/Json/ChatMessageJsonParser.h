//
//  ChatMessageJsonParser.h
//  HonkioSDK
//
//  Created by Dev on 01.09.16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "BaseJsonResponseParser.h"
#import "ChatMessage.h"

@interface ChatMessageJsonParser : BaseJsonResponseParser

+(ChatMessage*)parseChatMessage:(NSDictionary*)dict;

@end
