//
//  Inventory.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/5/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "Inventory.h"
#import "InventoryProduct.h"

@implementation Inventory


-(InventoryProduct*)findProductById:(NSString*)productId {
    return [self findProductById:productId onlyValid:true];
}

-(InventoryProduct*)findProductById:(NSString*)productId onlyValid:(BOOL)onlyValid {
    if (!productId)
        return nil;
    
    InventoryProduct* invalidProduct = nil;
    
    for (InventoryProduct* product in self.list) {
        if ([productId isEqualToString:[product productId]]) {
            if ([product isValid])
                return product;
            else if(!onlyValid && !invalidProduct)
                invalidProduct = product;
        }
    }
    
    return invalidProduct;
}


-(InventoryProduct*)findGroupedProductById:(NSString*)productId {
    if (!productId)
        return nil;
    
    InventoryProduct* groupedProduct = nil;
    
    for (InventoryProduct* product in self.list) {
        if ([productId isEqualToString:[product productId]] && [product isValid]) {
            if (!groupedProduct)
                groupedProduct = product;
            else
                groupedProduct.count += product.count;
        }
    }
    
    return groupedProduct;
}

-(InventoryProduct*)findProductForTarget:(NSString*)targetId {
    NSMutableDictionary* map = [NSMutableDictionary new];
    for (InventoryProduct* product in self.list) {
        Target* target = [product findTarget:targetId];
        if (target) {
            InventoryProduct* group = map[product.productId];
            if (!group) {
                group = [[InventoryProduct alloc] initFrom:product];
                map[product.productId] = group;
            }
            else {
                group.count += product.count;
            }
            
            if (target.count <= group.count) {
                return group;
            }
        }
    }
    return nil;
}

-(NSDate*)expireDate {
    NSDate* date = nil;
    
    for (InventoryProduct* product in self.list) {
        if (product.validTo) {
            if (date) {
                if (date > product.validTo) {
                    date = product.validTo;
                }
            }
            else {
                date = product.validTo;
            }
        }
    }
    return date;
}


-(NSArray*)groupByProduct {
    NSMutableDictionary* map = [NSMutableDictionary new];
    NSMutableArray* list = [NSMutableArray new];
    for (InventoryProduct* product in self.list) {
        InventoryProduct* group = map[product.productId];
        if (!group) {
            group = [[InventoryProduct alloc] initFrom:product];
            map[product.productId] = group;
            [list addObject:group];
        }
        else {
            group.count += product.count;
        }
    }
    return list;
}

@end
