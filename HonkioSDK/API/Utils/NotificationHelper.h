//
//  NotificationHelper.h
//  HonkioApi
//
//  Created by Shurygin Denis on 5/19/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "NSError+ApiError.h"
#import "Push.h"
#import "Response.h"

#define NOTIFY_NAME_ON_INITIALIZED      @"com.riskpointer.api.ON_INITIALIZED"
#define NOTIFY_NAME_ON_PUSH_RECEIVED    @"com.riskpointer.api.ON_PUSH_RECEIVED"
#define NOTIFY_NAME_ON_LOCATION_CHANGED @"com.riskpointer.api.ON_LOCATION_CHANGED"
#define NOTIFY_NAME_ON_COMPLETE         @"com.riskpointer.api.ON_COMPLETE"
#define NOTIFY_NAME_ON_LOGIN            @"com.riskpointer.api.ON_LOGIN"
#define NOTIFY_NAME_UNHANDLED_ERROR     @"com.riskpointer.api.UNHANDLED_ERROR"
#define NOTIFY_NAME_UNHANDLED_RESPONSE  @"com.riskpointer.api.UNHANDLED_RESPONSE"

#define NOTIFY_EXTRA_PUSH     @"push"
#define NOTIFY_EXTRA_LOCATION @"location"
#define NOTIFY_EXTRA_RESULT   @"response"
#define NOTIFY_EXTRA_COMMAND  @"command"
#define NOTIFY_EXTRA_ERROR    @"error"

@interface NotificationHelper : NSObject

+(void)postOnPushReceived:(Push*)push;
+(void)postOnLocationChanged:(CLLocation*)location;

+(NSString*)notifyNameOnComplete:(NSString*)command;
+(void)postOnComplete:(NSString*)command response:(Response*)response;

+(void)postOnLogin;
+(void)postOnInitialized;

+(void)postUnhandledError:(NSString*)command error:(NSError*)error;
+(void)postUnhandledResponse:(NSString*)command response:(Response*)response;

@end
