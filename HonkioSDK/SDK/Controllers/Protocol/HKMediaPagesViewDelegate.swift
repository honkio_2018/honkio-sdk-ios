//
//  HKMediaPagesViewDelegate.swift
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/18/18.
//  Copyright © 2018 developer. All rights reserved.
//

public protocol HKMediaPagesViewDelegate: class {
    
    var mediaList : [HKMediaFile]? { get set }
    var initialPage: Int { get set }
    
}
