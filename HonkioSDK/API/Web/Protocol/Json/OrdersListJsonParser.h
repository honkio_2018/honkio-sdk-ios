//
//  OrdersListJsonParser.h
//  HonkioApi
//
//  Created by Shurygin Denis on 12/2/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "BaseJsonResponseParser.h"

/**
 *  The parser of order list from a JSON response.
 */
@interface OrdersListJsonParser : BaseJsonResponseParser

@end
