//
//  OrdersListJsonParser.m
//  HonkioApi
//
//  Created by Shurygin Denis on 12/2/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "OrdersListJsonParser.h"
#import "OrderJsonParser.h"
#import "OrdersList.h"

@implementation OrdersListJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    OrdersList* ordersList = [OrdersList new];
    
    ordersList.totalCount = [dict[@"orders_count"] integerValue];
    
    NSMutableArray<Order*>* list = [NSMutableArray new];

    NSArray* orders = dict[@"orders"];
    if (orders) {
        for (NSDictionary* order in orders) {
            if([order isKindOfClass:[NSDictionary class]]) {
                [list addObject:[OrderJsonParser parseOrder:order]];
            }
        }
    }
    ordersList.list = list;
    
    return ordersList;
}

@end
