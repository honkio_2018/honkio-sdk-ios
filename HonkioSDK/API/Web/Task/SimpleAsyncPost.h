//
//  SimpleAsyncPost.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/25/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "SimpleAsyncGet.h"

/**
 * Task that sends POST requests on the given URL.<br>
 * Returns nil if request successful, error otherwise.
 *  <br><br>
 *  SimpleAsyncPost has Task protocol, so you have opportunity to abort task.
 */
@interface SimpleAsyncPost : SimpleAsyncGet

/**
 * Init new SimpleAsyncPost.
 * @param requestUrl URL to send GET request.
 * @param headers Headers for the request.
 * @param bodyData Body of the request.
 * @param responseHandler Handler that will receive response.
 */
-(id)initWithUrl:(NSURL*)requestUrl heders:(NSDictionary*)headers body:(NSData*)bodyData responseHandler:(UrlResponseHandler)responseHandler;

@end
