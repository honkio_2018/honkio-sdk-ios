//
//  AssetChangedPushEntity.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/18/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "AssetChangedPushEntity.h"
#import "Push.h"

@implementation AssetChangedPushEntity

-(id)initWithContent:(NSDictionary*)content {
    return [super initWithType:PUSH_TYPE_ASSET_CHANGED andContent:content];
}

- (NSString*)assetId {
    return [self getContent:@"asset_id"];
}

- (void)setAssetId:(NSString*)value {
    [self setContent:@"asset_id" value:value];
}

- (NSString*)assetName {
    return [self getContent:@"asset_name"];
}

- (void)setAssetName:(NSString*)value {
    [self setContent:@"asset_name" value:value];
}

- (NSString*)structureId {
    return [self getContent:@"structure_id"];
}

- (void)setStructureId:(NSString*)value {
    [self setContent:@"structure_id" value:value];
}

- (NSString*)merchantId {
    return [self getContent:@"merchant"];
}

- (void)setMerchantId:(NSString*)value {
    [self setContent:@"merchant" value:value];
}

- (NSString*)action {
    return [self getContent:@"action"];
}

- (void)setAction:(NSString*)value {
    [self setContent:@"action" value:value];
}
@end
