//
//  WebServiceExecutor.h
//  HonkioSDK
//
//  Created by Dash on 30.04.2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CWLSynthesizeSingleton.h"
#import "WebServiceExecutorInstanceProtocol.h"

@interface WebServiceExecutor : NSObject

CWL_DECLARE_SINGLETON_FOR_CLASS(nonnull WebServiceExecutor);

+(void)setInstance:(id<WebServiceExecutorInstanceProtocol>)instance;

/**
 *  Method to build Web request.
 *
 *  @param message         Instance of Message you want to execute.
 *  @param responseHandler Instance of json server response parser.
 *  @param execute         Set YES if you want to execute the request otherwise NO.
 *
 *  @return Instance of id<Task>.
 */
+(id<Task>)build:(Message*)message handler:(ResponseHandler)responseHandler;

@end
