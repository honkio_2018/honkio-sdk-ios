//
//  LocationFinder.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void (^LocationFindListener) (void);

/**
 * Helper class which provide easy way to get device location.
 */
@interface LocationFinder : NSObject<CLLocationManagerDelegate>
@property (nonatomic, strong) CLLocationManager* locationManager;
@property (nonatomic) NSDate* lastLocationUpdate;
@property (nonatomic) NSArray* locations;

-(void)registerLocationFindListener: (LocationFindListener)listeer;
-(void)updateLocation;
-(void)updateLocationIfNeeded;

-(BOOL)isHasLocation;

@end
