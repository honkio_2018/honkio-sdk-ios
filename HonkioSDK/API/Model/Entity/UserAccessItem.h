//
//  UserAccessItem.h
//  HonkioSDK
//
//  Created by Mikhail Li on 15/05/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserAccessItem : NSObject

@property (nonatomic) NSString *merchant;
@property (nonatomic) NSArray<NSString*> *access;

-(BOOL)isHasAccess:(NSArray *)accessList;

@end
