//
//  HKUserEditProfileViewController.swift
//  WorkPilots
//
//  Created by developer on 2/26/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit
/**
The view controller manages the view for editing user's profile.
*/
open class HKUserEditProfileViewController: HKBaseViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    /// The outlet for login.
    @IBOutlet open weak var loginField: UITextField!
    /// The outlet for First Name
    @IBOutlet open weak var firstNameField: UITextField!
    /// The outlet for the Last Name
    @IBOutlet open weak var lastNameField: UITextField!
    /// The outlet for address.
    @IBOutlet open weak var addressField: UITextField!
    /// The outlet for Post Code.
    @IBOutlet open weak var postCodeField: UITextField!
    /// The outlet for Phone Number.
    @IBOutlet open weak var phoneNumberField: UITextField!
    /// The outlet for user photo image
    @IBOutlet open weak var photoView: UIImageView!
    
    fileprivate let nameTextValidator = HKTextValidator(errorMessage: "text_validator_name_length".localized, rool: HKTextValidator.ruleName())
    fileprivate let phoneTextValidator = HKTextValidator(errorMessage: "text_validator_invalid_phone".localized, rool: HKTextValidator.rulePhoneNumber())
    fileprivate let emptyTextValidator = HKTextValidator(errorMessage: "text_validator_field_empty".localized, rool: HKTextValidator.ruleNotEmpty())
    
    /**
    The action to be performed on save button.
    Validates all input values by the corresponding validation rules. If any value is not valid does nothing.
    If all values are valid shows busy indicator, creates a new User instance and runs the update user request
    to the server. If server request failed the notification as CustomAlertView will be shown to a user.
    - parameter sender: Not used.
    */
    @IBAction open func saveAction(_ sender: AnyObject) {
        
        var isValid = self.nameTextValidator.validate(self.firstNameField)
        isValid = self.nameTextValidator.validate(self.lastNameField) && isValid
        isValid = self.phoneTextValidator.validate(self.phoneNumberField) && isValid
        
        if isValid {
        
            let user = User()
            user.address1 = self.addressField.text
            user.zip = self.postCodeField.text
            user.phone = self.phoneNumberField.text
            user.firstname = self.firstNameField.text
            user.lastname = self.lastNameField.text
            
            BusyIndicator.shared.showProgressView(self.view)
            
            HonkioApi.userUpdate(user, flags: 0) { (response) in
                
                BusyIndicator.shared.hideProgressView()
                
                if !AppErrors.isError(response) {
                    if response.isStatusAccept() {
                        _ = self.navigationController?.popViewController(animated: true)
                        return
                    }
                }
                
                if !AppErrors.handleError(self, response: response) {
                    CustomAlertView(okTitle: nil, okMessage: "settings_user_fail_message".localized, okAction: nil).show()
                }
                
            }
        }
    }
    
    @IBAction open func deleteProfileAction(_ sender: AnyObject) {
        UIApplication.shared.openURL(URL(string: "https://consumer.honkio.com/mydetails/erase")!);
    }
    
    /**
    On viewDidLoad attaches to all text fields delegates.
    */
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        let user : User! = HonkioApi.activeUser()
        
        self.loginField.text = user.login
        self.loginField.delegate = self
        
        self.firstNameField.text = user.firstname
        self.firstNameField.delegate = self
        
        self.lastNameField.text = user.lastname
        self.lastNameField.delegate = self
        
        self.addressField.text = user.address1
        self.addressField.delegate = self
        
        self.postCodeField.text = user.zip
        self.postCodeField.delegate = self
        
        self.phoneNumberField.text = user.phone
        self.phoneNumberField.delegate = self
        
        
        self.photoView.clipsToBounds = true
//        let photoBounds = self.photoView.bounds
        //        let radius = min(photoBounds.width, photoBounds.height) / 2
//        self.photoView.layer.cornerRadius = radius
        self.photoView.layer.cornerRadius = 20
        self.photoView.imageFromURL(link: HonkioApi.getUserPhotoURL(user.userId), errorImage: nil, contentMode: nil, isCache: false)
    }
    
    @IBAction open func changePhotoAction(_ sender: AnyObject) {
        let ac = UIAlertController(title: "dlg_pick_image_title".localized, message: nil, preferredStyle: .actionSheet)
        
        // From album
        ac.addAction(UIAlertAction(title: "dlg_pick_image_option_folder".localized, style: .default, handler: {[unowned self] (UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                let library = UIImagePickerController()
                library.delegate = self
                library.sourceType = .photoLibrary
                library.allowsEditing = true
                self.present(library, animated: true, completion: nil)
                
            } else {
                // TODO translate strings
                CustomAlertView(okTitle: "Error!".localized, okMessage: "Unable to find a photo album on your device".localized, okAction: nil).show()
            }
            }))
        
        ac.addAction(UIAlertAction(title: "dlg_pick_image_option_camera".localized, style: .default, handler: {[unowned self] (UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                
                let camera = UIImagePickerController()
                camera.delegate = self
                camera.sourceType = .camera
                camera.allowsEditing = true
                
                self.present(camera, animated: true, completion: nil)
                
            } else {
                // TODO translate strings
                CustomAlertView(okTitle: "Error!".localized, okMessage: "Camera Unavailable".localized, okAction: nil).show()
            }
            }))
        
        ac.addAction(UIAlertAction(title: "dlg_common_button_cancel".localized,
                                   style: .destructive,
                                   handler: nil))
        
        self.present(ac, animated: true, completion: nil)
    }
    
    @objc open func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.photoView.image = image
            self.uploadUserPhoto(image)
        }
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func uploadUserPhoto(_ image: UIImage!) {
        
        if image != nil {
            
            let data = UIImageJPEGRepresentation(image.fixRotation(), 0.85)
            
            URLCache.shared.removeAllCachedResponses()
            HonkioApi.userUpdatePhoto(MessageBinary(data: data, andMimeType: "image/jpeg"), flags: 0, responseHandler: { (resp) in
                // Do nothing
            })
        }
    }
}
