//
//  HKBaseUrlHandler.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 12/5/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

open class HKBaseUrlHandler: HKUrlHandler {
    
    public static let PathLogin = "login"
    
    public static let PathQr = "qr"
    public static let PathOrder = "order"
    public static let PathInvite = "invite"
    public static let PathAsset = "asset"
    
    public init() { }
    
    open func handle(_ url: URL) -> Bool {
        if let components = URLComponents(url: url, resolvingAgainstBaseURL: true) {
            return self.handle(athority: components.scheme!,
                         pathComponents: url.pathComponents,
                                  query: components.queryItems)
        }
        return false
    }
    
    open func handle(athority: String, pathComponents: [String], query: [URLQueryItem]?) -> Bool {
        if !self.checkAuthority(athority) {
            return false
        }
        
        // Screen
        if pathComponents.count >= 2 {
            switch pathComponents[pathComponents.count - 1] {
            case HKBaseUrlHandler.PathLogin:
                return self.handleLogin(query: query)
            default:
                break
            }
        }
        
        // Item
        if pathComponents.count >= 3 {
            let id = pathComponents[pathComponents.count - 1]
            switch pathComponents[pathComponents.count - 2] {
            case HKBaseUrlHandler.PathQr:
                return self.handleQrId(id, query: query)
            case HKBaseUrlHandler.PathOrder:
                return self.handleOrder(id, query: query)
            case HKBaseUrlHandler.PathInvite:
                return self.handleInvite(id, query: query)
            case HKBaseUrlHandler.PathAsset:
                return self.handleAsset(id, query: query)
            default:
                return false
            }
        }
        
        // Deprecated share url
        if pathComponents.count == 4 && pathComponents[1] == "share" && pathComponents[2] == HKBaseUrlHandler.PathOrder {
            return self.handleOrder(pathComponents[3], query: query)
        }
        
        return false
    }
    
    open func checkAuthority(_ authority: String) -> Bool {
        return true
    }
    
    open func handleLogin(query: [URLQueryItem]?) -> Bool {
        // Do nothing
        return true
    }
    
    open func handleQrId(_ id: String, query: [URLQueryItem]?) -> Bool {
        return false
    }
    
    open func handleOrder(_ id: String, query: [URLQueryItem]?) -> Bool {
        return false
    }
    
    open func handleInvite(_ id: String, query: [URLQueryItem]?) -> Bool {
        return false
    }
    
    open func handleAsset(_ id: String, query: [URLQueryItem]?) -> Bool {
        return false
    }

}
