//
//  WebServiceExecutor.m
//  HonkioSDK
//
//  Created by Dash on 30.04.2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "WebServiceExecutor.h"
#import "WebServiceExecutorInstance.h"

@interface WebServiceExecutor()

@property id<WebServiceExecutorInstanceProtocol> instance;

@end

@implementation WebServiceExecutor

CWL_SYNTHESIZE_SINGLETON_FOR_CLASS(WebServiceExecutor);

+(void)setInstance:(id<WebServiceExecutorInstanceProtocol>)instance {
    if (WebServiceExecutor.shared.instance != nil) {
        //TODO throw exception
    }
    WebServiceExecutor.shared.instance = instance;
}

+(id<Task>)build:(Message*)message handler:(ResponseHandler)responseHandler {
    return [[WebServiceExecutor getInstance] build:message handler:responseHandler];
}

+(id<WebServiceExecutorInstanceProtocol>)getInstance {
    if (WebServiceExecutor.shared.instance == nil) {
        [WebServiceExecutor setInstance:[WebServiceExecutorInstance new]];
    }
    return WebServiceExecutor.shared.instance;
}

@end
