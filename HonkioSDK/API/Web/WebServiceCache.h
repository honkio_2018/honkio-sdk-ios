//
//  WebServiceCache.h
//  HonkioApi
//
//  Created by Shurygin Denis on 7/30/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
#import "Connection.h"
#import "Message.h"

/**
 * Helper class that allow to store server responses into files in the internal storage.
 */
@interface WebServiceCache : NSObject

/**
 * Puts NSData object into cache file.
 * @param data An Object which will be cached.
 * @param name Name of the cache file exclude extension.
 * @param connection User Connection. For different users used different cache storage. Use null for common cache storage.
 */
+(void) put:(NSData*)data name:(NSString*)name connection:(Connection*)connection;

/**
 * Puts NSData object into cache file.
 * @param data An Object which will be cached.
 * @param name Name of the cache file exclude extension.
 * @param userName User name. For different users used different cache storage. Use null for common cache storage.
 */
+(void) put:(NSData*)data name:(NSString*)name user:(NSString*)userName;

/**
 * Gets cached object from the cache file.
 * @param name Name of the cache file exclude extension.
 * @param connection User Connection. For different users used different cache storage. Use null for common cache storage.
 * @return Cached object or null if file doesn't exists.
 */
+(NSData*) get:(NSString*)name connection:(Connection*)connection;

/**
 * Gets cached object from the cache file.
 * @param name Name of the cache file exclude extension.
 * @param userName User name. For different users used different cache storage. Use null for common cache storage.
 * @return Cached object or null if file doesn't exists.
 */
+(NSData*) get:(NSString*)name user:(NSString*)userName;

/**
 * Check if cache storage has specified cache file.
 * @param name Name of the cache file exclude extension.
 * @param connection User Connection. For different users used different cache storage. Use null for common cache storage.
 * @return YES if file exists, NO otherwise.
 */
+(BOOL) isHasCache:(NSString*)name connection:(Connection*)connection;

/**
 * Check if cache storage has specified cache file.
 * @param name Name of the cache file exclude extension.
 * @param userName User name. For different users used different cache storage. Use null for common cache storage.
 * @return YES if file exists, NO otherwise.
 */
+(BOOL) isHasCache:(NSString*)name user:(NSString*)userName;

/**
 * Removes all cache files for specified Connection. If connection is nil all cache files for all users will be removed.
 * @param connection User Connection. For different users used different cache storage. Use nil to remove all cache files for all users.
 */
+(void) clearForConnection:(Connection*)connection;

/**
 * Removes all cache files for specified User name. If User name is nil all cache files for all users will be removed.
 * @param userName User name. For different users used different cache storage. Use nil to remove all cache files for all users.
 */
+(void) clearForUser:(NSString*)userName;

/**
 * Removes expired cache files.
 */
+(void) trim;

+(NSString*)getUserNameFromMessage:(Message*)msg;

@end
