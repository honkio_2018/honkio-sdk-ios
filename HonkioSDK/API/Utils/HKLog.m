//
//  HKLog.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 10/26/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "HKLog.h"
#import <os/log.h>

@implementation HKLog

+(void)d:(NSString*)string {
    
    #if __IPHONE_10_0
        os_log_debug(OS_LOG_DEFAULT, "%@", string);
    #else
        NSLog([[NSString stringWithFormat:@"HKLog %@ : %@\n", [ApiDateFormatter serverStringFromDate:[NSDate new]], string] UTF8String]);
    #endif
}


+(void)e:(NSString*)string {
    
    #if __IPHONE_10_0
        os_log_error(OS_LOG_DEFAULT, "%@", string);
    #else
        NSLog([[NSString stringWithFormat:@"HKLog %@ : %@\n", [ApiDateFormatter serverStringFromDate:[NSDate new]], string] UTF8String]);
    #endif
}

@end
