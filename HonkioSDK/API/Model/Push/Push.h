//
//  BasePush.h
//  HonkioApi
//
//  Created by Shurygin Denis on 5/15/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TransactionPushEntity.h"
#import "OrderStatusChangedPushEntity.h"
#import "OrderChatMessagePushEntity.h"
#import "RoleChangedPushEntity.h"
#import "ProposalPushEntity.h"
#import "AssetChangedPushEntity.h"
#import "HKSystemMessagePushEntity.h"

#define PUSH_TYPE_NONE @""
#define PUSH_TYPE_SYSYEM @"system"
#define PUSH_TYPE_TRANSACTION @"transactionupdate"
#define PUSH_TYPE_ORDER_STATUS_CHANGED @"orderstatuschanged"
#define PUSH_TYPE_ORDER_CHAT_MESSAGE @"newchatmessage"
#define PUSH_TYPE_ORDER_PROPOSAL @"workerapply"
#define PUSH_TYPE_ROLE_CHANGED @"userrolechanged"
#define PUSH_TYPE_ASSET_CHANGED @"assetchanged"
#define PUSH_TYPE_INVITATION @"newinvitation"

// DEPRECATED
#define PUSH_TYPE_ROLE_ASSIGNED @"userroleassigned"

@interface Push : NSObject

@property (nonatomic) NSString* pushId;
@property (nonatomic) BasePushEntity* entity;
@property (nonatomic, readonly) NSString* type;
@property (nonatomic) NSString* message;
@property (nonatomic) NSDictionary* body;
@property (nonatomic) NSDate* date;
@property (nonatomic) BOOL isReaded;

@end
