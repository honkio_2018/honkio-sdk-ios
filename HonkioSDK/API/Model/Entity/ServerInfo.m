//
//  ServerInfo.m
//  HonkioApi
//
//  Created by Alexandr Kolganov on 4/21/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import "ServerInfo.h"
#import "ApiDateFormatter.h"

@implementation CountryInfo

@end

@implementation ServerInfo

-(int)findCountryByISO:(NSString*)iso {
    for(int i=0; i<self.countries.count; i++) {
        CountryInfo* country = [self.countries objectAtIndex:i];
        if([country.iso isEqualToString:iso])
            return i;
    }
    return -1;
}

-(int)findCountryByLocale:(NSString*)locale {
    for(int i=0; i<self.countries.count; i++) {
        CountryInfo* country = [self.countries objectAtIndex:i];
        if([[country.locale substringToIndex:2] isEqualToString:locale])
            return i;
    }
    return -1;
}

-(NSString*)suitableLanguage
{
    for(NSString* language in [NSLocale preferredLanguages]) {
        if (language) {
            for (NSString* serverLang in self.languages) {
                if (serverLang && [language hasPrefix:serverLang]) {
                    return serverLang;
                }
            }
        }
    }
    return @"en";
}

@end
