//
//  AppInfoJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/28/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import "AppInfoJsonParser.h"
#import "AppInfo.h"
#import "HKTouInfo.h"
#import "Message.h"

@implementation AppInfoJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    AppInfo* appInfo = [[AppInfo alloc] initFrom: [message appIdentity]];
    
    NSDictionary* shopDict = dict[@"shop"];
    if (shopDict) {
        appInfo.shopIdentity = [[Identity alloc] initWithId:shopDict[@"id"] andPassword:shopDict[@"password"]];
    }
    
    appInfo.oauthIdentity = [[Identity alloc] initWithId:dict[@"oauth_id"] andPassword:dict[@"oauth_secret"]];
    
    [appInfo setTouInfo: [[HKTouInfo alloc] initWithUrl:dict[@"tou_url"] version:[dict[@"tou_version"] intValue]]];
     
     NSDictionary* touCustom = dict[@"dict_tou_custom"];
     if (touCustom) {
         for (NSString* key in [touCustom allKeys]) {
             NSDictionary* touInfoDict = touCustom[key];
             if (touInfoDict) {
                 [appInfo setTouInfo: [[HKTouInfo alloc] initWithKey:key url:touInfoDict[@"str_tou_url"] version:[touInfoDict[@"version"] intValue]]];
             }
         }
     }
    
    return appInfo;
}

@end
