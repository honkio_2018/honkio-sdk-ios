//
//  Error.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import "ApiError.h"

@implementation ApiError

static ApiError* unknownError;

+(ApiError*)unknown {
    if (!unknownError) {
        unknownError = [[ApiError alloc] initWithCode:ErrApiUnknown];
    }
    return unknownError;
}

-(id) initWithCode:(int)code {
    return [self initWithCode:code desc:nil];
}

-(id) initWithCode:(int)code desc:(NSString*)desc {
    self = [self init];
    if (self) {
        _group = code < 0 ? ErrGroupApi : code / HONKIO_ERR_GROUP_MULT;
        _code = code;
        _desc = desc;
        _fields = [ApiError readFields:desc];
    }
    return self;
}

-(id) initWithGroup:(int)group subItem:(int)subItem; {
    return [self initWithGroup:group subItem:subItem desc:nil];
}

-(id) initWithGroup:(int)group subItem:(int)subItem desc:(NSString*)desc {
    self = [self init];
    if (self) {
        _group = group;
        _code = HONKIO_ERR_GROUP_MULT * group + subItem;
        _desc = desc;
        _fields = [ApiError readFields:desc];
    }
    return self;
}

+(NSArray*)readFields:(NSString*)description {
    if(description && description.length > 5) {
        NSRange pos = [description rangeOfString:@" "];
        if(pos.location != NSNotFound)
            description =[description substringFromIndex:pos.location+1];
        
        pos = [description rangeOfString:@":"];
        if (pos.location != NSNotFound) {
            return [[description substringToIndex:pos.location] componentsSeparatedByString:@" "];
        }
    }
    return nil;
}

@end
