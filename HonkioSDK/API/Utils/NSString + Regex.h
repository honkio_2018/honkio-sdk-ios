//
//  NSString + Regex.h
//  HonkioApi
//
//  Created by developer on 1/22/16.
//  Copyright © 2016 iQ Payment Oy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Regex)

- (NSString *)rightStrip:(NSString*)character;

@end
