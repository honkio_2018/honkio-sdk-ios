//
//  HKMediaUrlJsonParser.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 6/19/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import "HKMediaUrlJsonParser.h"
#import "HKMediaUrl.h"

@implementation HKMediaUrlJsonParser

-(id)parseDictionary:(Message*)message dict:(NSDictionary*)dict {
    HKMediaUrl *result = [HKMediaUrl new];
    
    result.objectId = dict[@"id"];
    result.uploadToken = dict[@"upload_token"];
    if (dict[@"upload_url"])
        result.uploadUrl = [[NSURL alloc] initWithString:dict[@"upload_url"]];
    
    return result;
}

@end
