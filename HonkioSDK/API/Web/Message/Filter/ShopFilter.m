//
//  ShopFilter.m
//  HonkioApi
//
//  Created by Shurygin Denis on 8/4/14.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import "ShopFilter.h"

//#define SHOP_SERVICE_TYPE @"service_type";
//#define SHOP_MERCHANT_ID @"merchantid";
//#define SHOP_NETWORK_ID @"network";

@implementation ShopFilter

-(BOOL)isHasLocation {
    return self.latitude && self.longitude;
}

- (void)fillParams:(NSMutableDictionary *)params
{
    [AbsFilter applyIfNotNil:MSG_PARAM_SHOP_SERVICE_TYPE value:self.serviceType forParams:params];
    [AbsFilter applyIfNotNil:MSG_PARAM_SHOP_MERCHANT_ID value:self.merchantId forParams:params];
    [AbsFilter applyIfNotNil:MSG_PARAM_SHOP_NETWORK_ID value:self.networkId forParams:params];
    [AbsFilter applyIfNotNil:MSG_PARAM_LOC_LATITUDE value:self.latitude forParams:params];
    [AbsFilter applyIfNotNil:MSG_PARAM_LOC_LONGITUDE value:self.longitude forParams:params];
    [AbsFilter applyOrRemove:MSG_PARAM_LOC_ACCURACY value:self.radius forParams:params];
    
    if (self.isQuerySubMerchants)
        [AbsFilter applyOrRemove:@"query_submerchants" value:[NSNumber numberWithBool: self.isQuerySubMerchants] forParams:params];
    
    if (self.isQueryAllMerchants)
        [AbsFilter applyOrRemove:@"query_all_merchants" value:[NSNumber numberWithBool: self.isQueryAllMerchants] forParams:params];
    
    if (self.isQueryRatings)
        [AbsFilter applyOrRemove:@"query_show_ratings" value:[NSNumber numberWithBool: self.isQueryRatings] forParams:params];
    
    [super fillParams:params];
}

@end
