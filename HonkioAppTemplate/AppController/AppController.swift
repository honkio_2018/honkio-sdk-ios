//
//  BaseAppController.swift
//  WorkPilots
//
//  Created by Shurygin Denis on 4/1/16.
//  Copyright © 2016 developer. All rights reserved.
//

import UIKit

/**
The class contains the functionality for finding and creating the ViewControllers and StoryBoards
by the set of predefined identifiers. Class should not be instantiated.
- TODO: Rename this class.
*/
open class AppController: NSObject {
    
    public static let Custom =            0x40000000 // 1000000 0000 0000 0000000000000000
    public static let Vc =                0x20000000 // 0100000 0000 0000 0000000000000000
    public static let View =              0x10000000 // 0010000 0000 0000 0000000000000000
    public static let Shop =              0x00800000 // 0000000 1000 0000 0000000000000000
    public static let Settings =          0x00400000 // 0000000 0100 0000 0000000000000000
    public static let Registration =      0x00200000 // 0000000 0010 0000 0000000000000000
    
    public static let Launch =                  Vc + 0
    
    /** Returned ViewController should respond to protocol HKLoginViewProtocol. */
    public static let Login =                   Vc + 1
    public static let Pin =                     Vc + 2
    public static let Main =                    Vc + 3
    public static let Tou =                     Vc + 4
    public static let ForgotPassword =          Vc + 6
    public static let Gdpr =                    Vc + 7
    public static let ChangePin =               Vc + 8
    
    /** Returned ViewController should respond to protocol HKOrderViewProtocol. */
    public static let OrderDetails =            Vc + 9
    public static let UserDetails =             Vc + 10
    public static let PostRating =              Vc + 11
    public static let ApplicantsPager =         Vc + 13
    public static let Applicants =              Vc + 14
    public static let ManageCards =             Vc + 15
    
    /** ViewController must respond to protocol HKUserAccountSelectionProtocol */
    public static let UserAccountSelection =    Vc + 16
    public static let ChangePassword =          Vc + 17
    public static let PushTable =               Vc + 18
    public static let OrderChat =               Vc + 19
    
    /** ViewController must respond to protocol HKQrCodeViewProtocol */
    public static let QrCode =                  Vc + 20
    /** ViewController must respond to protocol HKQrCodeViewProtocol */
    public static let QrCodeNoType =            Vc + 21
    
    /** ViewController must respond to protocol HKAssetViewProtocol */
    public static let Asset =                   Vc + 22
    
    /** ViewController must respond to protocol HKMediaPagesViewDelegate */
    public static let MediaFiles =              Vc + 23
    
    /** ViewController must respond to protocol HKWebPageViewDelegate */
    public static let WebView =                 Vc + 24
    
    public static let RegistrationPin =               Vc + Registration + 4
    public static let RegistrationCreditCard =        Vc + Registration + 12
    
    public static let SettingsMain =                  Vc + Settings + 1
    public static let SettingsUserAccount =           Vc + Settings + 2
    public static let SettingsAbout =                 Vc + Settings + 3
    /** ViewController must respond to protocol HKUserAccountAddViewProtocol */
    public static let SettingsUserAddCreditCard =     Vc + Settings + 4
    public static let SettingsUserAccountLoan =       Vc + Settings + 5
    public static let SettingsUserProfile =           Vc + Settings + 6
    public static let SettingsUserInvoicers =         Vc + Settings + 7
    
    public static let SettingsWorkerProfile =         Custom + Vc + Settings + 1
    public static let SettingsEmployerProfile =       Custom + Vc + Settings + 2
    
    public static let Shopslist =      Vc + Shop + 1
    public static let ShopProduct =    Vc + Shop + 3
    public static let PaymentQuery =   Vc + Shop + 4
    public static let Push =           Vc + Shop + 5
    public static let Transaction =    Vc + Shop + 6
    public static let Transactions =   Vc + Shop + 7

    /**
    Contains the instance of the AppController.
    */
    fileprivate(set) public static var instance: HKAppViewControllerFactory!

    /**
    Initialize the instance of AppController.
    - parameter instance: The instance to be set.
    */
    public static func initInstance(_ instance: HKAppViewControllerFactory) {
        if AppController.instance != nil {
            preconditionFailure("Instance should be initialized only once.")
        }
        AppController.instance = instance
    }

    /**
    - Returns: The ViewController for the storuboard found in the provided ViewController.
    - parameter controllerId: The identificator the story board identifier will be searched for.
    - parameter viewController: The view controller the story board identifier will be searched in.
    */
    public static func getViewControllerById(_ controllerId: Int, viewController: UIViewController) -> UIViewController? {
        let instance = AppController.instance
        let controller = instance?.getViewControllerById(controllerId, viewController: viewController)
        if controller == nil {
            preconditionFailure("ViewController not found!")
        }
        return controller
    }
}
