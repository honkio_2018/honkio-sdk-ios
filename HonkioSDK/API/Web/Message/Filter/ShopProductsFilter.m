//
//  ShopProductsFilter.m
//  HonkioSDK
//
//  Created by Shurygin Denis on 11/2/17.
//  Copyright © 2017 developer. All rights reserved.
//

#import "ShopProductsFilter.h"

@implementation ShopProductsFilter

- (void)fillParams:(NSMutableDictionary *)params
{
    [AbsFilter applyIfNotNil:@"query_tags" value:self.tags forParams:params];

    [super fillParams:params];
}

@end
