//
//  HKBaseMediaListViewStrategy.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 2/28/19.
//  Copyright © 2019 developer. All rights reserved.
//

import UIKit

open class HKBaseMediaListViewStrategy: HKViewControllerLifeCycleObserver {
    
    public static var TAG = "HKBaseMediaListViewStrategy.TAG"
    
    public var isReadOnly = false
    
    public var selectionDelegate: ((_ mediaFile: HKMediaFile)->Void)?
    public var addActionDelegate: (()->Void)?
    
    open var isEditable: Bool {
        get {
            return !isReadOnly
        }
    }
    
    // TODO make private(set)
    public var mediaList : [HKMediaFile]!
    public private(set) var isDeferredList = false
    
    private var deffedImages: [Any] = []

    private var isContentShown = false
    private var collectionView: UICollectionView?
    private var progressView: UIView?
    
    private var objectId: String?
    private var objectType: String?
    
    open func showProgress(animated: Bool) {
        self.progressView?.isHidden = false
        
        if !animated || !isContentShown {
            self.collectionView?.alpha = 0.0
            self.progressView?.alpha = 1.0
        }
        else {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {[weak self] in
                self?.collectionView?.alpha = 0.0
                self?.progressView?.alpha = 1.0
            }) { (isCompleted) in
                // DO nothing
            }
        }
        isContentShown = false
    }
    
    open func showContent(animated: Bool) {
        if !animated || isContentShown {
            self.collectionView?.alpha = 1.0
            self.progressView?.alpha = 0.0
            self.progressView?.isHidden = true
        }
        else {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {[weak self] in
                self?.collectionView?.alpha = 1.0
                self?.progressView?.alpha = 0.0
            }) {[weak self] (isCompleted) in
                self?.progressView?.isHidden = true
            }
        }
        isContentShown = true
    }
    
    open func setObject(_ objectId: String?, _ objectType: String?) {
        self.isDeferredList = objectId == nil || objectId!.isEmpty
        
        if objectType != nil {
            self.objectId = objectId
            self.objectType = objectType
            
            if self.isDeferredList {
                // TODO
                onListLoad([])
                showContent(animated: false)
            }
            else {
                loadMediaList(objectId ?? "")
                showProgress(animated: false)
            }
        }
    }
    
    open func listItemIdentifier(_ mediaFile: HKMediaFile?) -> String {
        return "HKMediaFileTableViewCell"
    }
    
    open func listItemAddIdentifier() -> String {
        return "HKMediaFileAddTableViewCell"
    }
    
    open func setViews(_ collection: UICollectionView, _ progress: UIView?) {
        self.collectionView = collection
        self.progressView = progress
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        if isContentShown {
            showContent(animated: false)
        }
        else {
            showProgress(animated: false)
        }
    }
    
    open func onListLoad(_ list: [HKMediaFile]) {
        mediaList = list
        
        if mediaList == nil {
            mediaList = [HKMediaFile]()
        }
        
        if isReadOnly && mediaList.count == 0 {
            mediaList.append(HKMediaFile(id: "", url: ""))
        }
        
        self.collectionView?.reloadData()
    }
    
    public func removeMediaFile(_ mediaFile: HKMediaFile) {
        BusyIndicator.shared.showProgressView(self.viewController.view)
        HonkioApi.userClearMediaUrl(mediaFile.objectId, flags: 0) {[weak self] (response) in
            BusyIndicator.shared.hideProgressView()
            if response.isStatusAccept() {
                if let index = self?.mediaList?.index(of: mediaFile) {
                    self?.mediaList?.remove(at: index)
                    self?.collectionView?.reloadData()
                }
            }
            else {
                self?.onError(response.anyError())
            }
        }
    }
    
    public func addMediaFile() {
        self.addActionDelegate?()
    }
    
    public func saveDeferred(_ objectId: String, _ callback: @escaping (_ error: ApiError?)->Void) {
        self.saveDeferredRecursive(objectId, self.objectType ?? "", callback)
    }
    
    private func saveDeferredRecursive(_ objectId: String, _ objectType: String, _ callback: @escaping (_ error: ApiError?)->Void) {
        if self.deffedImages.count == 0 {
            BusyIndicator.shared.hideProgressView()
            callback(nil)
        }
        else {
            BusyIndicator.shared.showProgressView(self.viewController.view)
            let item = self.deffedImages.remove(at: 0)
            var uploadMediaModel: DeferredMediaUploadProcessModel? = nil
            if let image = item as? UIImage {
                uploadMediaModel = DeferredMediaUploadProcessModel(object: objectId, type: objectType, data: UIImageJPEGRepresentation(image.fixRotation(), 0.85), mimeType: "image/jpeg")
            }
            else if let url = item as? URL {
                uploadMediaModel = DeferredMediaUploadProcessModel(object: objectId, type: objectType, filePath: url.path)
            }
            else {
                saveDeferredRecursive(objectId, objectType, callback)
                return
            }
            
            uploadMediaModel?.deferredCallback = {[weak self] (error) in
                if error != nil {
                    callback(error)
                }
                else {
                    self?.saveDeferredRecursive(objectId, objectType, callback)
                }
            }
            uploadMediaModel?.start()
        }
    }
    
    private func loadMediaList(_ assetId: String) {
        let fileFilter = MediaFileFilter()
        fileFilter.objectId = assetId
        fileFilter.mediaType = HK_MEDIA_FILE_TYPE_IMAGE
        
        HonkioApi.userMediaFilesList(fileFilter, flags: 0) { (response) in
            if response.isStatusAccept() {
                self.showContent(animated: true)
                self.onListLoad((response.result as! HKMediaFileList).list)
            }
            else {
                let error = response.anyError()
                if !AppErrors.handleError(self.viewController, error: error) {
                    CustomAlertView(apiError: error, okAction: nil).show()
                }
            }
        }
    }
    
    private func mediaFileDidSelect(_ mediaFile: HKMediaFile) {
        self.selectionDelegate?(mediaFile)
    }
}

// MARK: - HKMediaPickViewControllerDelegate
extension HKBaseMediaListViewStrategy: HKMediaPickViewControllerDelegate {

    public func imageDidPicked(_ viewController: HKBaseMediaPickViewController, _ image: UIImage) {
        self.viewController?.navigationController?.popViewController(animated: true)
        
        if self.isDeferredList {
            self.deffedImages.append(image)
            self.collectionView?.reloadData()
        }
        else {
            BusyIndicator.shared.showProgressView(self.viewController.view)
            let uploadMediaModel = MediaUploadProcessModel(object: self.objectId, type: self.objectType, data: UIImageJPEGRepresentation(image.fixRotation(), 0.85), mimeType: "image/jpeg")
            uploadMediaModel?.processDelegate = self
            uploadMediaModel?.start()
        }
    }
    
    public func mediaDidPicked(_ viewController: HKBaseMediaPickViewController, _ type: String, _ url: NSURL) {
        self.viewController?.navigationController?.popViewController(animated: true)
        
        if self.isDeferredList {
            self.deffedImages.append(url)
            self.collectionView?.reloadData()
        }
        else {
            BusyIndicator.shared.showProgressView(self.viewController.view)
            let uploadMediaModel = MediaUploadProcessModel(object: self.objectId, type: self.objectType, filePath: url.path)
            uploadMediaModel?.processDelegate = self
            uploadMediaModel?.start()
        }
    }
}

fileprivate class DeferredMediaUploadProcessModel: MediaUploadProcessModel {
    
    override init!(object: String!, type: String!, data: Data!, mimeType: String!) {
        super.init(object: object, type: type, data: data, mimeType: mimeType)
    }
    
    override init!(object: String!, type: String!, filePath path: String!) {
        super.init(object: object, type: type, filePath: path)
    }
    
    fileprivate var deferredCallback: ((_ error: ApiError?)->Void)? = nil
    
    override func onComplete(_ response: Response!) {
        super.onComplete(response)
        self.deferredCallback?(nil)
    }
    
    override func onError(_ response: Response!) {
        super.onError(response)
        self.deferredCallback?(response.anyError())
    }
}

// MARK: - ProcessProtocol
extension HKBaseMediaListViewStrategy: ProcessProtocol {
    
    public func onComplete(_ model: BaseProcessModel!, response: Response!) {
        BusyIndicator.shared.hideProgressView()
        self.loadMediaList(self.objectId ?? "")
    }
    
    public func onError(_ model: BaseProcessModel!, response: Response!) {
        BusyIndicator.shared.hideProgressView()
        onError(response.anyError())
    }
    
    public func onError(_ error: ApiError) {
        if !AppErrors.handleError(self.viewController, error: error) {
            CustomAlertView(apiError: error, okAction: nil).show()
        }
    }
}

// MARK: - UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
extension HKBaseMediaListViewStrategy: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    open func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if mediaList != nil {
            if self.isEditable {
                return mediaList.count + 1 + self.deffedImages.count
            }
            return mediaList.count + self.deffedImages.count
        }
        return 0
    }
    
    open func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if mediaList != nil {
            if indexPath.item >= mediaList.count + deffedImages.count {
                return collectionView.dequeueReusableCell(withReuseIdentifier: self.listItemAddIdentifier(), for: indexPath)
            } else if indexPath.item >= mediaList.count {
                let identifier = self.listItemIdentifier(nil)
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as!  HKMediaFileTableViewCell
                cell.fillUi(deffedImages[indexPath.item - mediaList.count])
                return cell
            } else {
                let mediaFile = mediaList[indexPath.item]
                let identifier = self.listItemIdentifier(mediaFile)
                let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as!  HKMediaFileTableViewCell
                cell.fillUi(mediaFile: mediaFile)
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    open func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.item >= mediaList.count + deffedImages.count {
            self.addMediaFile()
        }
        else if indexPath.item >= mediaList.count {
            // TODO deferred image click
        }
        else {
            self.mediaFileDidSelect(self.mediaList[indexPath.item])
        }
    }
}
