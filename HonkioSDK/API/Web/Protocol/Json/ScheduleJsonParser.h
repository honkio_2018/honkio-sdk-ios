//
//  ScheduleJsonParser.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 7/8/16.
//  Copyright © 2016 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseJsonResponseParser.h"

@interface ScheduleJsonParser : BaseJsonResponseParser

@end
