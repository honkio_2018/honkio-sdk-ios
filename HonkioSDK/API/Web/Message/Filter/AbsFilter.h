//
//  AbsFilter.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2014 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Message.h"

/**
 * Abstract filter that can be used for setting a filter parameters for the message.
 */
@interface AbsFilter : NSObject

/**
 * Apply filter for specified message.
 * @param message Message for apply filter.
 */
-(void) apply:(Message*) message;

/**
 * Convert filter to NSDictionary that hold set of params.
 * @return NSDictionary that represent this filter.
 */
-(NSDictionary*) toParams;

/**
 * Sets parameter for message if value is not nil.
 * @param key Key of the parameter.
 * @param value Value of the parameter.
 * @param message Message for which the parameter is set.
 */
+(void) applyIfNotNil:(NSString*) key value:(NSObject*) value forMessage:(Message*) message;

/**
 * Sets parameter for NSMutableDictionary if value is not nil.
 * @param key Key of the parameter.
 * @param value Value of the parameter.
 * @param params NSMutableDictionary for which the parameter is set.
 */
+(void) applyIfNotNil:(NSString*) key value:(NSObject*) value forParams:(NSMutableDictionary*)params;

/**
 * Sets parameter for NSMutableDictionary if value is not nill, otherwise remove previous value.
 * @param key Key of the parameter.
 * @param value Value of the parameter.
 * @param params NSMutableDictionary for which the parameter is set.
 */
+(void) applyOrRemove:(NSString*) key value:(NSObject*) value forParams:(NSMutableDictionary*)params;

/**
 * Check if specified NSDictionary has set location parameters.
 * @param params NSDictionary to check.
 * @return YES if specified NSDictionary has set location parameters, false otherwise.
 */
+(BOOL) isHasLocation:(NSDictionary *)params;

@end
