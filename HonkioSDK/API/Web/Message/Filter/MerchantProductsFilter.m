//
//  MerchantProductsFilter.m
//  HonkioSDK
//
//  Created by Mikhail Li on 20/06/2018.
//  Copyright © 2018 developer. All rights reserved.
//

#import "MerchantProductsFilter.h"


@implementation MerchantProductsFilter

-(NSDictionary *)toParams {
    NSMutableDictionary* result = [[NSMutableDictionary alloc] initWithCapacity:4];
    
    [self fillParams:result];
    
    return result;
}

-(void)fillParams:(NSMutableDictionary *)params {
    [AbsFilter applyIfNotNil:@"query_merchant" value:self.merchantId forParams:params];
    [super fillParams:params];
}

@end
