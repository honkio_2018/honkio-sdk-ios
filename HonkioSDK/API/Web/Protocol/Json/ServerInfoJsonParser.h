//
//  ServerInfoJsonParser.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/20/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "BaseJsonResponseParser.h"

/**
 * Parse ServerInfo.
 */
@interface ServerInfoJsonParser : BaseJsonResponseParser

@end
