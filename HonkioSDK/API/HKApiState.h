//
//  HKApiState.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 2/12/19.
//  Copyright © 2019 developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Identity.h"
#import "HKDevice.h"
#import "ServerInfo.h"
#import "AppInfo.h"
#import "ShopInfo.h"
#import "User.h"

@interface HKApiState : NSObject
    
    @property(readonly) NSURL* apiUrl;
    @property(readonly) Identity* appIdentity;
    @property(readonly) HKDevice* device;

    @property(readonly) ServerInfo* serverInfo;
    @property(readonly) AppInfo* appInfo;
    @property(readonly) ShopInfo* shopInfo;
    
    @property(readonly) User* user;
    
    -(id)initWithUrl:(nonnull NSURL*)apiUrl appIdentity:(nonnull Identity*)appIdentity;
    
    -(void)deviceInfoDidGet:(HKDevice*)device;
    -(void)serverInfoDidGet:(ServerInfo*)serverInfo;
    -(void)appInfoDidGet:(AppInfo*)appInfo;
    -(void)shopInfoDidGet:(ShopInfo*)shopInfo;
    
    -(void)userDidGet:(User*)user;
    
@end
