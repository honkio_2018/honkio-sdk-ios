//
//  BasePaymentProcessModel.m
//  HonkioApi
//
//  Created by Shurygin Denis on 11/27/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "BasePaymentProcessModel.h"
#import "HonkioApi.h"
#import "PinReason.h"

@implementation PayParams

-(id) initWithShop:(Shop*)shop oirderId:(NSString*)orderId account:(UserAccount*)account flags:(int)flags {
    self = [super init];
    if (self) {
        _shop = shop;
        _orderId = orderId;
        _account = account;
        _flags = flags;
    }
    return self;
}

-(BOOL) isAccountValid {
    return self.account != nil && (self.disallowedAccounts == nil || ![self.disallowedAccounts containsObject:self.account.type]);
}

@end

@implementation BasePaymentProcessModel {
    PayParams* modelPayParams;
}

-(void)reset {
    [self abort];
    [super reset];
    [self setQueryModel:nil];
}

-(void)abort {
    [super abort];
    if (self.queryProcessModel) {
        [self.queryProcessModel abort];
    }
}

-(void)query {
    if (self.queryProcessModel) {
        [self.queryProcessModel query];
    }
}

-(void)queryLoop:(long)loopDelay {
    if (self.queryProcessModel) {
        [self.queryProcessModel query:loopDelay];
    }
}

-(void)stopQuery {
    if (self.queryProcessModel) {
        [self.queryProcessModel stopQuery];
    }
}

-(void)pay:(PayParams*)payParams {
    [self start];
    
    if ([HonkioApi checkPaymentAvailable:payParams.account shop:payParams.shop handler:^(Response *response) {
        [self.processDelegate onError:self response:response];
    }]) {
        modelPayParams = payParams;
        if ([self pinDelegate]) {
            [self requestPin:PIN_REASON_PURCHASE];
        }
        else {
            [self doPayment:payParams handler:[self buildHandler]];
        }
    }
}

-(void)pinEntered:(NSString*)pin reason:(int)reason {
    if (reason == PIN_REASON_PURCHASE) {
        if ([HonkioApi checkPin:pin]) {
            [self doPayment:modelPayParams handler:[self buildHandler]];
        }
        else {
            [self onError:[Response withError:ErrApiInvalidPin desc:nil]];
            modelPayParams = nil;
        }
    }
}

-(void)pinCanceled:(int)reason {
    [self onError:[Response withError:ErrApiCanceledByUser desc:nil]];
    modelPayParams = nil;
}

-(void)requestPin:(int)reason {
    [[self pinDelegate] requestPin:self reason:reason];
}

-(PayParams*) payParams {
    return modelPayParams;
}

-(void) setQueryModel:(QueryPaymentProcessModel*) model {
    _queryProcessModel = model;
}

-(ResponseHandler) buildHandler {
    __weak BasePaymentProcessModel* weakModel = self;
    return ^(Response* response) {
        if (weakModel != nil) {
            if (response && [response isStatusPending]) {
                [weakModel setQueryModel:[[QueryPaymentProcessModel alloc] initWithProcessModel:weakModel shopIdentity:[weakModel payParams].shop transactionId:response.transactoinId]];
                [weakModel onPending:response];
            }
            else if (response && [response isStatusAccept]) {
                [weakModel setQueryModel:nil];
                [weakModel onComplete:response];
            }
            else {
                [weakModel onError:response];
            }
        }
    };
}

-(ResponseHandler) buildQueryHandler {
    __weak BasePaymentProcessModel* weakModel = self;
    return ^(Response* response) {
        if (weakModel != nil) {
            if (response && [response isStatusPending]) {
                // Do nothing
            }
            else if (response && [response isStatusAccept]) {
                [weakModel setQueryModel:nil];
                [weakModel onComplete:response];
            }
            else {
                [weakModel onError:response];
            }
        }
    };
}

@end
