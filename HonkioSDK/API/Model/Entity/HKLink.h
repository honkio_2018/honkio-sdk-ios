//
//  HKLink.h
//  HonkioSDK
//
//  Created by Shurygin Denis on 12/21/18.
//  Copyright © 2018 developer. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HK_LINK_FIELD_META_DATA @"metadata"

// TODO docs
@interface HKLink : NSObject

@property (nonatomic, strong) NSString* objectId;
@property (nonatomic, strong) NSString* type;

@property (nonatomic, strong, readonly) NSString* fromObject;
@property (nonatomic, strong, readonly) NSString* fromType;

@property (nonatomic, strong, readonly) NSString* toObject;
@property (nonatomic, strong, readonly) NSString* toType;

@property (nonatomic) NSMutableDictionary<NSString*, NSObject*>* metaData;

-(id) init;
-(id) initWithMetaData:(NSMutableDictionary<NSString*, NSObject*>*) metaData;
-(id) initFrom:(HKLink*)link;

-(void) setFrom:(NSString*)object type:(NSString*)type;
-(void) setTo:(NSString*)object type:(NSString*)type;

-(NSDictionary*) toParams;

@end
