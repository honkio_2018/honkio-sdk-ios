//
//  HKBaseShopViewController.swift
//  HonkioAppTemplate
//
//  Created by Shurygin Denis on 11/20/17.
//  Copyright © 2017 developer. All rights reserved.
//

import UIKit

open class HKBaseShopViewController: HKBaseLazyContentViewController<ShopInfo> {
    
    open var shopInfo: ShopInfo? {
        get { return self.contentItem }
        set { self.contentItem = newValue }
    }
    
    open func load(byQrId qrId: String) {
        HonkioApi.shopFindBuyQrId(qrId, flags: 0) { [unowned self] (response) in
            if response.isStatusAccept() {
                if let result = response.result as? ShopFind {
                    if result.list.count > 0 {
                        self.load(byIdentyty: result.list[0])
                        return
                    }
                }
            }
            self.shopLoadingError(response.anyError())
        }
    }
    
    open func load(byId shopId: String) {
        HonkioApi.shopFind(byId: shopId, flags: 0) { [unowned self] (response) in
            if response.isStatusAccept() {
                if let result = response.result as? ShopFind {
                    if result.list.count > 0 {
                        self.load(byIdentyty: result.list[0])
                        return
                    }
                }
            }
            self.shopLoadingError(response.anyError())
        }
    }
    
    open func load(byIdentyty shopIdentity: Identity) {
        HonkioApi.shopGetInfo(shopIdentity, flags: 0) { [weak self] (response) in
            if response.isStatusAccept() {
                self?.contentItem = response.result as? ShopInfo
            }
            else {
                self?.shopLoadingError(response.anyError())
            }
        }
    }
    
    open func shopLoadingError(_ error: ApiError) {
        preconditionFailure("Method buildViewController must be overridden")
    }
    
}
