//
//  ConsimerLoginProcessModel.m
//  HonkioApi
//
//  Created by Shurygin Denis on 11/27/15.
//  Copyright © 2015 iQ Payment Oy. All rights reserved.
//

#import "ConsimerLoginProcessModel.h"
#import "HonkioApi.h"
#import "PinReason.h"

@implementation ConsimerLoginProcessModel {
    ResponseHandler handler;
    int modelFlags;
    Response* modelResponse;
}

-(id)initWithResponseHandler:(ResponseHandler)responseHandler {
    self = [super init];
    if (self) {
        handler = responseHandler;
    }
    return self;
}
-(void)loginWithPin:(NSString*)pin flags:(int)flags {
    [self start];
    if ([HonkioApi checkReLogin:handler]) {
        [HonkioApi loginWithPin:pin flags:flags handler:[self buildHandler]];
    }
}

-(void)loginWithSavedPin:(int)flags {
    modelFlags = flags;
    [self start];
    if ([HonkioApi checkReLogin:handler]) {
        [self requestPin:PIN_REASON_REENTER];
    }
}

-(void)pinEntered:(NSString*)pin reason:(int)reason {
    switch (reason) {
        case PIN_REASON_REENTER: {
            [HonkioApi loginWithPin:pin flags:modelFlags handler:[self buildHandler]];
            break;
        }
        case PIN_REASON_NEW: {
            [HonkioApi changePin:nil newPin:pin];
            handler(modelResponse);
            modelResponse = nil;
            break;
        }
    }
}

-(void)pinCanceled:(int)reason {
    switch (reason) {
        case PIN_REASON_REENTER: {
            [self abort];
            handler([Response withError:ErrApiCanceledByUser desc:nil]);
            break;
        }
        case PIN_REASON_NEW: {
            [self abort];
            [[HonkioApi activeConnection] exit];
            handler([Response withError:ErrApiCanceledByUser desc:nil]);
            modelResponse = nil;
            break;
        }
    }
}

-(void)setResponseHandler:(ResponseHandler)responseHandler {
    handler = responseHandler;
}

-(void)reset {
    [super reset];
    modelResponse = nil;
}

-(ResponseHandler)buildHandler {
    return ^(Response* response) {
        if (response && ([response isStatusAccept] || [response isStatusPending])) {
            if(![response hasPending:RESPONSE_PENDING_SMS_VERIFY]) {
                modelResponse = response;
                [self requestPin:PIN_REASON_NEW];
                return;
            }
        }
        handler(response);
    };
}

@end
