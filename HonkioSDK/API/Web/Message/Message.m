//
//  Message.m
//  HonkioApi
//
//  Created by Shurygin Denis on 3/18/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import "Message.h"
#import <UIKit/UIDevice.h>
#import "ApiSecurity.h"
#import "User.h"
#import "HonkioApi.h"
#import "HKLog.h"

@implementation Message {
    id<RequestBuilder> reqsBuilder;
    id<ResponseParser> respParser;
    NSMutableDictionary* parameters;
    
    NSString* cacheFileName;
    BOOL cacheDependsOnShop;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.isIgnoreHash = NO;
        parameters = [[NSMutableDictionary alloc] init];
    }
    return self;
}

-(id)initWithCommand:(NSString*)command url:(NSURL*)url connection:(Connection*)connection device:(HKDevice*)device shopIdentity:(Identity*)shopIdentity requestBuilder:(id<RequestBuilder>)requestBuilder responseParser:(id<ResponseParser>)responseParser flags:(int)flags {
    self = [self init];
    if (self) {
        _command = command;
        _url = url;
        _device =device;
        _connection = connection;
        _appIdentity = [HonkioApi appIdentity];
        if (shopIdentity)
            _shopIdentity = shopIdentity;
        else
            _shopIdentity = [HonkioApi mainShopIdentity];
        reqsBuilder = requestBuilder;
        respParser = responseParser;
        _flags = flags;
        
        if (![MSG_COMMAND_SET_DEVICE isEqualToString:[self command]] && [[[self device] device_id] length] > 0) {
            [self addParameter:MSG_PARAM_IDENTITY_PLUGIN value:[[self device] device_id]];
        }
        
        [self addParameter:MSG_PARAM_VERSION value:API_VERSION];
        [self addParameter:MSG_PARAM_RANDOM value:[ApiSecurity getRandomString]];
        NSString* identityClient = [self identityClient];
        if (identityClient)
            [self addParameter:MSG_PARAM_IDENTITY_CLIENT value:identityClient];
        
        CLLocation* location = [HonkioApi deviceLocation];
        //    CLLocation* location = [[CLLocation alloc] initWithLatitude:60.4578 longitude:22.27156];
        if(location) {
            [self addParameter:MSG_PARAM_LOC_LONGITUDE value:[NSString stringWithFormat:@"%f", location.coordinate.longitude]];
            [self addParameter:MSG_PARAM_LOC_LATITUDE value:[NSString stringWithFormat:@"%f", location.coordinate.latitude]];

            int accuraty = location.verticalAccuracy;
            if(accuraty > 0)
                [self addParameter:MSG_PARAM_LOC_ACCURACY value:[NSString stringWithFormat:@"%d", accuraty]];
        }
    }
    return self;
}

-(void)addParameter:(NSString*)key value:(NSObject*)value {
    if (!value) {
        value = @"";
    }
    [parameters setValue:value forKey:key];
}

-(void)addParameters:(NSDictionary*)params {
    if (params) {
        [parameters setValuesForKeysWithDictionary:params];
    }
}

-(NSObject*)parameter:(NSString*)key {
    return [parameters objectForKey:key];
}

-(void)removeParameter:(NSString*)key {
    [parameters removeObjectForKey:key];
}

-(BOOL)isHasFlag:(int)flag {
    return (self.flags & flag) == flag;
}

-(BOOL)isOffline {
    return ((self.flags & MSG_FLAG_ONLINE_MODE) != MSG_FLAG_ONLINE_MODE) &&
				((self.flags & MSG_FLAG_OFFLINE_MODE) == MSG_FLAG_OFFLINE_MODE);
}

-(NSData*)buildRequest {
    [reqsBuilder reset];
    
    if (self.shopIdentity) {
        [reqsBuilder putKey:MSG_PARAM_SHOP_ID value:self.shopIdentity.identityId];
    }
    
    if(self.connection && [self.connection isValid]) {
        [reqsBuilder putKey:MSG_PARAM_LOGIN_IDENTITY value:[self.connection login]];
        [reqsBuilder putKey:MSG_PARAM_LOGIN_PASSWORD value:[self.connection otp]];
    }
    
    NSString* language = [[HonkioApi serverInfo] suitableLanguage];
    if (language) {
        [reqsBuilder putKey:MSG_PARAM_LANGUAGE value:language];
    }
    
    NSEnumerator *enumerator = [parameters keyEnumerator];
    id key;
    while((key = [enumerator nextObject])) {
        if (key) {
            [reqsBuilder putKey:key value:[parameters objectForKey:key]];
        }
    }
    
    [reqsBuilder putKey:MSG_PARAM_COMMAND value:self.command];
    
    NSString* hashPassword = [self hashPassword];
    if (hashPassword && hashPassword.length > 0) {
        [reqsBuilder calculateHash:hashPassword];
    }
    
    return [reqsBuilder entity];
}

-(Response*)parseResponse:(NSData*)responseData {
    Response* response = [respParser parse:self response:responseData password:self.isIgnoreHash ? nil : [self hashPassword]];
    response.shopId = self.shopIdentity.identityId;
    response.message = self;
    return response;
}

-(void)setCacheFileName:(NSString*)name dependsOnShop:(BOOL)dependsOnShop {
    cacheFileName = name;
    cacheDependsOnShop = dependsOnShop;
}

-(NSString*)cacheFileName {
    if (cacheFileName && cacheDependsOnShop && [self.shopIdentity identityId])
        return [NSString stringWithFormat:@"%@_%@", cacheFileName, [self.shopIdentity identityId]];
    return cacheFileName;
}

-(NSString*) notificationCommand {
    if (_notificationCommand) {
        return _notificationCommand;
    }
    return self.command;
}

-(NSString*)identityClient {
    NSString* appId = [self.appIdentity identityId];
    if (appId) {
        NSString* appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        if (!appVersion) {
            appVersion = @"0.00.0000";
        }
        return [NSString stringWithFormat:@"%@;%@;%@", appId, DEVICE_PLATFORM_IOS, appVersion];
    }
    return nil;
}

-(NSString*)hashPassword {
    NSString* password = nil;
    
    if (self.shopIdentity && self.shopIdentity.password)
        password = self.shopIdentity.password;
    
    if ([self.appIdentity password]) {
        if (!password)
            password = [self.appIdentity password];
        else
            password = [password stringByAppendingString:[self.appIdentity password]];
    }
    return password;
}

@end


@import MobileCoreServices;
@implementation MessageBinary {
    NSString* file;
    NSData* bData;
}

-(id) initWithFile:(NSString*)filePath {
    self = [super init];
    if (self) {
        file = filePath;
        _mime = [self mimeFromPath:file];
    }
    return self;
}

-(id) initWithData:(NSData*)data andMimeType:(NSString*)mimeType {
    
    self = [super init];
    if (self) {
        bData = data;
        _mime = mimeType;
    }
    return self;
}

-(NSData*)binData {
    if (file) {
        NSError* error = nil;
        NSData* data = [NSData dataWithContentsOfFile:file options:0 error: &error];
        if (data == nil) {
            [HKLog e: [NSString stringWithFormat: @"Failed to read file, error %@", error]];
        }
        return data;
    }
    return bData;
}

-(NSString*)fileName {
    if (file)
        return [file lastPathComponent];
    
    NSArray<NSString*>* components = [self->_mime componentsSeparatedByString:@"/"];
    if ([components count] == 2)
        return [NSString stringWithFormat:@"%@.%@", components[0], components[1]];
    
    return @"file";
}

-(NSString*) mimeFromPath:(NSString*)path {
    // get a mime type for an extension using MobileCoreServices.framework
    
    CFStringRef extension = (__bridge CFStringRef)[path pathExtension];
    CFStringRef UTI = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, extension, NULL);
    assert(UTI != NULL);
    
    NSString *mimetype = CFBridgingRelease(UTTypeCopyPreferredTagWithClass(UTI, kUTTagClassMIMEType));
    assert(mimetype != NULL);
    
    CFRelease(UTI);
    
    return mimetype;
}

@end
