//
//  ServerFile.h
//  HonkioApi
//
//  Created by Shurygin Denis on 3/24/15.
//  Copyright (c) 2015 RiskPointer. All rights reserved.
//

#import <Foundation/Foundation.h>

/** License file name pattern. */
#define LICENSE_PATTERN @"%@.license.html"

/**
 * Data object that hold loaded file.
 * <br><br>Note: Changes in properties of this object does not affect the server data.
 */
@interface ServerFile : NSObject

@property (nonatomic) NSString* mime;
@property (nonatomic) NSData* data;

-(NSString*)dataToString;

@end
